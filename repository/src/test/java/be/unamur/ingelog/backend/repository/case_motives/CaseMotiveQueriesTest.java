package be.unamur.ingelog.backend.repository.case_motives;

import be.unamur.ingelog.backend.dto.cases.CaseMotiveDTO;
import be.unamur.ingelog.backend.repository.queries.cases.CaseQueries;
import be.unamur.ingelog.backend.repository.util.RepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CaseMotiveQueriesTest extends RepositoryTest {
    @Autowired
    private CaseQueries caseQueries;

    @Test
    @Sql(scripts = "classpath:scripts/schema.sql")
    void findAll_hasRightSize() {
        List<CaseMotiveDTO> motives = caseQueries.findAllMotives();

        assertThat(motives).hasSize(4);
    }

    @Test
    @Sql(scripts = "classpath:scripts/schema.sql")
    void findAll_hasRightContent() {
        List<CaseMotiveDTO> motives = caseQueries.findAllMotives();

        assertThat(motives)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(
                        new CaseMotiveDTO("complaint"),
                        new CaseMotiveDTO("denunciation"),
                        new CaseMotiveDTO("investigation_request"),
                        new CaseMotiveDTO("regularization_control")
                );
    }
}
