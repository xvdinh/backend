package be.unamur.ingelog.backend.repository.util;

import org.junit.ClassRule;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class RepositoryTest {
    @ClassRule
    public static final PostgreSQLContainer CONTAINER = CustomPostgresqlContainer.getInstance();

    static {
        CONTAINER.start();
    }
}
