insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-L33TZ', 'JMS construct SPRL', '5 rue de Bruxelles', '5000', 'Namur', 'Belgique',
        'BE');

insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-T0T0L', 'Constructions Jamart SA', '13 rue de l''Ange', '5000', 'Namur',
        'Belgique', 'BE');

insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-7RZE6', 'All Clean SPRL', '7 rue de la Loi', '1000', 'Bruxelles', 'Belgique',
        'BE');
