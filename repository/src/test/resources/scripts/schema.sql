-- Drop all sequences
drop sequence if exists agents_seq;
drop sequence if exists cases_seq;
drop sequence if exists employers_seq;
drop sequence if exists inquiries_seq;
drop sequence if exists reports_seq;
drop sequence if exists offences_seq;
drop sequence if exists law_articles_seq;
drop sequence if exists attachments_seq;
drop sequence if exists case_comments_seq;
drop sequence if exists report_observations_seq;

-- Drop all tables
drop table if exists report_observations;
drop table if exists reports;
drop table if exists report_status;
drop table if exists report_states;
drop table if exists offences;
drop table if exists offence_priorities;
drop table if exists law_articles;
drop table if exists attachments;
drop table if exists inquiries;
drop table if exists inquiry_states;
drop table if exists inquiry_results;
drop table if exists case_comments;
drop table if exists cases;
drop table if exists case_origins;
drop table if exists case_motives;
drop table if exists case_states;
drop table if exists employers;
drop table if exists agents;
drop table if exists agent_roles;



create table agent_roles
(
    role_id character varying(255) not null,
    constraint roles_pkey primary key (role_id)
);



create table agents
(
    agent_id   integer                not null,
    username   character varying(255) not null,
    password   character varying(255) not null,
    first_name character varying(255) not null,
    last_name  character varying(255) not null,
    role_id    character varying(255) not null,
    constraint agent_pkey primary key (agent_id),
    constraint fk_role_id foreign key (role_id)
        references agent_roles (role_id) match simple
        on update no action
        on delete no action
);

create sequence agents_seq start with 1 increment by 1;



create table employers
(
    employer_id        integer                not null,
    employer_reference character varying(255) not null,
    name               character varying(255) not null,
    street             character varying(255) not null,
    postalCode         character varying(10)  not null,
    locality           character varying(255) not null,
    country            character varying(255) not null,
    country_code       character varying(2)   not null,
    constraint employer_pkey primary key (employer_id)
);

create sequence employers_seq start with 1 increment by 1;



create table case_motives
(
    motive_id character varying(255) not null,
    constraint case_motive_pkey primary key (motive_id)
);



create table case_origins
(
    origin_id character varying(255) not null,
    motive_id character varying(255) not null,
    constraint case_origin_pkey primary key (origin_id),
    constraint fk_motive foreign key (motive_id)
        references case_motives (motive_id) match simple
        on update no action
        on delete no action
);



create table case_states
(
    state_id character varying(255) not null,
    constraint case_states_pkey primary key (state_id)
);



create table cases
(
    case_id        integer                not null,
    case_reference character varying(255) not null,
    creator        integer                not null,
    trigger_date   date                   not null,
    open_date      timestamp              not null,
    close_date     timestamp,
    motive         character varying(255) not null,
    origin         character varying(255) not null,
    state          character varying(255) not null,
    description    character varying(255) not null,
    constraint cases_pkey primary key (case_id),
    constraint fk_creator foreign key (creator)
        references agents (agent_id) match simple
        on update no action
        on delete no action,
    constraint fk_motive foreign key (motive)
        references case_motives (motive_id) match simple
        on update no action
        on delete no action,
    constraint fk_state foreign key (state)
        references case_states (state_id) match simple
        on update no action
        on delete no action
);

create sequence cases_seq start with 1 increment by 1;



create table inquiry_states
(
    state_id character varying(255) not null,
    constraint inquiry_state_pkey primary key (state_id)
);



create table inquiry_results
(
    result_id character varying(255) not null,
    constraint inquiry_result_pkey primary key (result_id)
);



create table inquiries
(
    inquiry_id        integer                not null,
    inquiry_reference character varying(255) not null,
    case_id           integer                not null,
    employer_id       integer                not null,
    supervisor        integer,
    assignee          integer,
    state             character varying(255) not null,
    result            character varying(255),
    open_date         timestamp              not null,
    close_date        timestamp,
    report_date       date,
    street            character varying(255) not null,
    postalCode        character varying(10)  not null,
    locality          character varying(255) not null,
    priority          character varying(255),
    legal_delay       integer,
    deadline          date,
    constraint inquiry_pkey primary key (inquiry_id),
    constraint fk_case_id foreign key (case_id)
        references cases (case_id) match simple
        on update no action
        on delete no action,
    constraint fk_employer_id foreign key (employer_id)
        references employers (employer_id) match simple
        on update no action
        on delete no action,
    constraint fk_supervisor foreign key (supervisor)
        references agents (agent_id) match simple
        on update no action
        on delete no action,
    constraint fk_assignee foreign key (assignee)
        references agents (agent_id) match simple
        on update no action
        on delete no action,
    constraint fk_state foreign key (state)
        references inquiry_states (state_id) match simple
        on update no action
        on delete no action,
    constraint fk_result foreign key (result)
        references inquiry_results (result_id) match simple
        on update no action
        on delete no action
);

create sequence inquiries_seq start with 1 increment by 1;



create table attachments
(
    attachment_id  integer                not null,
    inquiry_id     integer                not null,
    document_type  character varying(255) not null,
    reception_date timestamp              not null,
    source         character varying(255) not null,
    path           character varying(255) not null,
    constraint attachment_pkey primary key (attachment_id),
    constraint fk_inquiry_id foreign key (inquiry_id)
        references inquiries (inquiry_id) match simple
        on update no action
        on delete no action
);

create sequence attachments_seq start with 1 increment by 1;



create table law_articles
(
    law_article_id        integer                not null,
    law_article_reference character varying(255) not null,
    label                 character varying(255) not null,
    starting_date         date                   not null,
    ending_date           date,
    constraint law_article_pkey primary key (law_article_id)
);

create sequence law_articles_seq start with 1 increment by 1;



create table offence_priorities
(
    offence_priority_id character varying(255) not null,
    constraint offence_priority_pkey primary key (offence_priority_id)
);



create table offences
(
    offence_id        integer                not null,
    offence_reference character varying(255) not null,
    law_article_id    integer                not null,
    label             character varying(255) not null,
    starting_date     date                   not null,
    ending_date       date,
    legal_delay       integer,
    priority          character varying(255) not null,
    constraint offence_pkey primary key (offence_id),
    constraint fk_law_article foreign key (law_article_id)
        references law_articles (law_article_id) match simple
        on update no action
        on delete no action,
    constraint fk_priority foreign key (priority)
        references offence_priorities (offence_priority_id) match simple
        on update no action
        on delete no action
);

create sequence offences_seq start with 1 increment by 1;



create table report_states
(
    state_id character varying(255) not null,
    constraint report_state_pkey primary key (state_id)
);



create table report_status
(
    status_id character varying(255) not null,
    constraint report_status_pkey primary key (status_id)
);



create table reports
(
    report_id        integer                not null,
    report_reference character varying(255) not null,
    inquiry_id       integer                not null,
    state            character varying(255) not null,
    status           character varying(255),
    offence_id       integer,
    deadline         date,
    controls_count   integer,
    constraint report_pkey primary key (report_id),
    constraint fk_inquiry_id foreign key (inquiry_id)
        references inquiries (inquiry_id) match simple
        on update no action
        on delete no action,
    constraint fk_state foreign key (state)
        references report_states (state_id) match simple
        on update no action
        on delete no action,
    constraint fk_status foreign key (status)
        references report_status (status_id) match simple
        on update no action
        on delete no action,
    constraint fk_offence_id foreign key (offence_id)
        references offences (offence_id) match simple
        on update no action
        on delete no action
);

create sequence reports_seq start with 1 increment by 1;



create table case_comments
(
    comment_id    integer                 not null,
    content       character varying(1000) not null,
    author        integer                 not null,
    case_id       integer                 not null,
    creation_date timestamp               not null,
    constraint case_comment_pkey primary key (comment_id),
    constraint fk_author foreign key (author)
        references agents (agent_id) match simple
        on update no action
        on delete no action,
    constraint fk_case_id foreign key (case_id)
        references cases (case_id) match simple
        on update no action
        on delete no action
);

create sequence case_comments_seq start with 1 increment by 1;



create table report_observations
(
    observation_id     integer                not null,
    report_id          integer                not null,
    observation_number integer                not null,
    title              character varying(255) not null,
    value              character varying(255),
    unit               character varying(255) not null,
    description        character varying(1000),
    constraint report_observation_pkey primary key (observation_id),
    constraint fk_report_id foreign key (report_id)
        references reports (report_id) match simple
        on update no action
        on delete no action
);

create sequence report_observations_seq start with 1 increment by 1;


-- remplissage des valeurs prédéfinies de la table 'roles'
insert into agent_roles(role_id)
values ('INSPECTOR');
insert into agent_roles(role_id)
values ('CHIEF_INSPECTOR');
insert into agent_roles(role_id)
values ('AGENT_ADM');


-- remplissage des valeurs prédéfinies de la table 'case_states'
insert into case_states(state_id)
values ('initiated');
insert into case_states(state_id)
values ('configured');
insert into case_states(state_id)
values ('approved');
insert into case_states(state_id)
values ('closed');
insert into case_states(state_id)
values ('archived');


-- remplissage des valeurs prédéfinies de la table 'case_motives'
insert into case_motives(motive_id)
values ('complaint');
insert into case_motives(motive_id)
values ('denunciation');
insert into case_motives(motive_id)
values ('investigation_request');
insert into case_motives(motive_id)
values ('regularization_control');


-- remplissage des valeurs prédéfinies de la table 'case_origins'
insert into case_origins(origin_id, motive_id)
values ('anonymous', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('individual', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('employee', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('employer', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('police', 'investigation_request');
insert into case_origins(origin_id, motive_id)
values ('spf', 'investigation_request');
insert into case_origins(origin_id, motive_id)
values ('inspection_services', 'investigation_request');
insert into case_origins(origin_id, motive_id)
values ('onss', 'investigation_request');


-- remplissage des valeurs prédéfinies de la table 'offence_priorities'
insert into offence_priorities(offence_priority_id)
values ('1');
insert into offence_priorities(offence_priority_id)
values ('2');
insert into offence_priorities(offence_priority_id)
values ('3');
insert into offence_priorities(offence_priority_id)
values ('4');
insert into offence_priorities(offence_priority_id)
values ('5');


-- remplissage des valeurs prédéfinies de la table 'report_states'
insert into report_states(state_id)
values ('created');
insert into report_states(state_id)
values ('encoded');
insert into report_states(state_id)
values ('evaluated');
insert into report_states(state_id)
values ('closed');


-- remplissage des valeurs prédéfinies de la table 'report_status'
insert into report_status(status_id)
values ('pending');
insert into report_status(status_id)
values ('not_applicable');
insert into report_status(status_id)
values ('not_controlled');
insert into report_status(status_id)
values ('positive');
insert into report_status(status_id)
values ('negative');


-- remplissage des valeurs prédéfinies de la table 'inquiry_states'
insert into inquiry_states(state_id)
values ('open');
insert into inquiry_states(state_id)
values ('pending_validation');
insert into inquiry_states(state_id)
values ('incomplete');
insert into inquiry_states(state_id)
values ('approved');
insert into inquiry_states(state_id)
values ('assigned');
insert into inquiry_states(state_id)
values ('completed');
insert into inquiry_states(state_id)
values ('pending_closure');
insert into inquiry_states(state_id)
values ('closed');


-- remplissage des valeurs prédéfinies de la table 'inquiry_results'
insert into inquiry_results(result_id)
values ('positive');
insert into inquiry_results(result_id)
values ('negative');

