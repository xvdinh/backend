insert into case_states(state_id)
values ('initiated');
insert into case_states(state_id)
values ('configured');
insert into case_states(state_id)
values ('approved');
insert into case_states(state_id)
values ('closed');
insert into case_states(state_id)
values ('archived');


insert into case_motives(motive_id)
values ('complaint');
insert into case_motives(motive_id)
values ('denunciation');
insert into case_motives(motive_id)
values ('investigation_request');
insert into case_motives(motive_id)
values ('regularization_control');


insert into case_origins(origin_id, motive_id)
values ('anonymous', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('individual', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('employee', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('employer', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('police', 'investigation_request');
insert into case_origins(origin_id, motive_id)
values ('spf', 'investigation_request');
insert into case_origins(origin_id, motive_id)
values ('inspection_services', 'investigation_request');
insert into case_origins(origin_id, motive_id)
values ('onss', 'investigation_request');


insert into inquiry_states(state_id)
values ('open');
insert into inquiry_states(state_id)
values ('pending_validation');
insert into inquiry_states(state_id)
values ('incomplete');
insert into inquiry_states(state_id)
values ('approved');
insert into inquiry_states(state_id)
values ('assigned');
insert into inquiry_states(state_id)
values ('completed');
insert into inquiry_states(state_id)
values ('pending_closure');
insert into inquiry_states(state_id)
values ('closed');


insert into inquiry_results(result_id)
values ('positive');
insert into inquiry_results(result_id)
values ('negative');


insert into agent_roles(role_id)
values ('INSPECTOR');
insert into agent_roles(role_id)
values ('CHIEF_INSPECTOR');
insert into agent_roles(role_id)
values ('AGENT_ADM');


insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSA', 'insa', 'Alexandre', 'Fortin', 'INSPECTOR');

insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CINA', 'cina', 'Xavier', 'Cloutier', 'CHIEF_INSPECTOR');


insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-Z0RR0', 'ZORRO SPRL', '3 Rue du Bois', '5000', 'Namur', 'Belgique', 'BE');


insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-P0P0L', 1, '2020-01-01', '2020-01-02T00:00:00', null, 'denunciation',
        'anonymous', 'initiated', 'lorem ipsum');


insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay, deadline)
values (nextval('inquiries_seq'), '2020-ENQ-D4ZZR', 1, 1, 2, 1, 'assigned', null, '2020-04-01T00:00:00', null, null,
        '3 rue de l''Impasse', '1000', 'Bruxelles', null, null, null);
