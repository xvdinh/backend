insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CINA', 'cina', 'Bob', 'Morane', 'CHIEF_INSPECTOR');

insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSA', 'insa', 'Louise', 'Attaque', 'INSPECTOR');



insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-L33TZ', 'JMS construct SPRL', '5 rue de Bruxelles', '5000', 'Namur', 'Belgique',
        'BE');

insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-T0T0L', 'Constructions Jamart SA', '13 rue de l''Ange', '5000', 'Namur',
        'Belgique', 'BE');

insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-7RZE6', 'All Clean SPRL', '7 rue de la Loi', '1000', 'Bruxelles', 'Belgique',
        'BE');



insert into cases (case_id, case_reference, creator, trigger_date, open_date, motive, origin, state, description)
values (nextval('cases_seq'), '2020-DOS-R3DL0', 1, '2020-03-13', '2020-03-13', 'denunciation', 'anonymous',
        'configured', 'premier dossier');

insert into cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
values (nextval('cases_seq'), '2020-DOS-09ZAK', 1, '2020-03-14', '2020-03-15', '2020-03-19', 'denunciation',
        'anonymous', 'closed', 'troisième dossier');

insert into cases (case_id, case_reference, creator, trigger_date, open_date, motive, origin, state, description)
values (nextval('cases_seq'), '2020-DOS-T78JI', 2, '2020-02-01', '2020-03-28', 'investigation_request', 'police',
        'configured', 'deuxième dossier');



insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date,
                      street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-D4ZZR', 1, 1, 1, 2, 'assigned', 'negative', '2020-04-01',
        '5 rue de Bruxelles', '5000', 'Namur');

insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date,
                      close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-HUHU4', 1, 2, 1, 2, 'open', 'negative', '2020-03-21', '2020-03-22',
        '2020-03-26', '13 rue de l''Ange', '5000', 'Namur');

insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date,
                      street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-AL74K', 3, 3, 1, 2, 'open', 'negative', '2020-04-02',
        '7 rue de la Loi', '1000', 'Bruxelles');
