insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CINA', 'cina', 'Bob', 'Morane', 'CHIEF_INSPECTOR');

insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSA', 'insa', 'Louise', 'Attaque', 'INSPECTOR');



insert into cases (case_id, case_reference, creator, trigger_date, open_date, motive, origin, state, description)
values (nextval('cases_seq'), '2020-DOS-R3DL0', 1, '2020-03-13', '2020-03-13', 'denunciation', 'anonymous',
        'configured', 'premier dossier');

insert into cases (case_id, case_reference, creator, trigger_date, open_date, motive, origin, state, description)
values (nextval('cases_seq'), '2020-DOS-T78JI', 2, '2020-02-01', '2020-03-28', 'investigation_request', 'police',
        'configured', 'deuxième dossier');

insert into cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
values (nextval('cases_seq'), '2020-DOS-09ZAK', 1, '2020-03-14', '2020-03-15', '2020-03-19', 'denunciation',
        'anonymous', 'closed', 'troisième dossier');
