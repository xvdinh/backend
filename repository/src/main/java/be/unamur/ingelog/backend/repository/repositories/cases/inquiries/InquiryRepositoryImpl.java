package be.unamur.ingelog.backend.repository.repositories.cases.inquiries;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.InquiryNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.*;

@Repository
public class InquiryRepositoryImpl extends BaseRepository implements InquiryRepository {
    @Override
    public void create(Inquiry inquiry) {
        template.update(
                "insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, " +
                        "state, open_date, street, postalcode, locality) " +
                        "values (nextval('inquiries_seq'), :inquiryReference, :caseId, :employerId, " +
                        ":state, :openDate, :street, :postalCode, :locality)",
                parameters()
                        .addValue("inquiryReference", inquiry.getInquiryReference())
                        .addValue("caseId", inquiry.getRelatedCase().getCaseId())
                        .addValue("employerId", inquiry.getTargetedEmployer().getEmployerId())
                        .addValue("state", inquiry.getInquiryState().toString())
                        .addValue("openDate", inquiry.getOpenDate())
                        .addValue("street", inquiry.getStreet())
                        .addValue("postalCode", inquiry.getPostalCode())
                        .addValue("locality", inquiry.getLocality())
        );
    }

    @Override
    public void update(Inquiry inquiry) {
        template.update(
                "update inquiries set " +
                        "employer_id = :employerId, " +
                        "supervisor = :supervisorId, " +
                        "assignee = :assigneeId, " +
                        "state = :state, " +
                        "result = :result, " +
                        "close_date = :closeDate, " +
                        "report_date = :reportDate, " +
                        "street = :street, " +
                        "postalcode = :postalCode, " +
                        "locality = :locality, " +
                        "priority = :priority, " +
                        "legal_delay = :legalDelay, " +
                        "deadline = :deadline " +
                        "where inquiry_id = :inquiryId",
                parameters()
                        .addValue("employerId", toEmployerId(inquiry.getTargetedEmployer()))
                        .addValue("supervisorId", toAgentId(inquiry.getSupervisor()))
                        .addValue("assigneeId", toAgentId(inquiry.getAssignee()))
                        .addValue("state", toStringFromEnum(inquiry.getInquiryState()))
                        .addValue("result", toStringFromEnum(inquiry.getInquiryResult()))
                        .addValue("closeDate", inquiry.getCloseDate())
                        .addValue("reportDate", inquiry.getReportDate())
                        .addValue("street", inquiry.getStreet())
                        .addValue("postalCode", inquiry.getPostalCode())
                        .addValue("locality", inquiry.getLocality())
                        .addValue("priority", inquiry.getPriority())
                        .addValue("legalDelay", inquiry.getLegalDelay())
                        .addValue("deadline", inquiry.getDeadline())
                        .addValue("inquiryId", inquiry.getInquiryId())
        );
    }

    @Override
    public Inquiry findByInquiryReference(String inquiryReference) {
        return template.query(
                "select * from inquiries " +
                        "where inquiry_reference = :inquiryReference",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new InquiryMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(InquiryNotFoundException::new);
    }

    @Override
    public Inquiry findByReportReference(String reportReference) {
        return template.query(
                "select * from inquiries " +
                        "where inquiry_id in " +
                        "(select inquiry_id from reports where report_reference = :reportReference)",
                parameters()
                        .addValue("reportReference", reportReference),
                new InquiryMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(InquiryNotFoundException::new);
    }

    @Override
    public List<Inquiry> findByCaseReference(String caseReference) {
        return template.query(
                "select * from inquiries " +
                        "where case_id in " +
                        "(select case_id from cases where case_reference = :caseReference)",
                parameters()
                        .addValue("caseReference", caseReference),
                new InquiryMapper()
        );
    }

    @Override
    public Boolean isLegalInquiryResult(String result) {
        return template.queryForObject(
                "select exists (select * from inquiry_results where result_id = :result)",
                parameters()
                        .addValue("result", result),
                Boolean.class);
    }
}
