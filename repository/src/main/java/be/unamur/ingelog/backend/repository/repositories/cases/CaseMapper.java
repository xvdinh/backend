package be.unamur.ingelog.backend.repository.repositories.cases;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.cases.Case;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.*;

public class CaseMapper implements RowMapper<Case> {
    @Override
    public Case mapRow(ResultSet result, int row) throws SQLException {
        return Case.builder()
                .caseId(toInteger(result.getInt("case_id")))
                .caseReference(result.getString("case_reference"))
                .creator(Agent.builder()
                        .agentId(toInteger(result.getInt("creator")))
                        .build())
                .triggerDate(toLocalDate(result.getDate("trigger_date")))
                .openDate(toLocalDateTime(result.getTimestamp("open_date")))
                .closeDate(toLocalDateTime(result.getTimestamp("close_date")))
                .caseMotive(toCaseMotive(result.getString("motive")))
                .caseOrigin(result.getString("origin"))
                .caseState(toCaseState(result.getString("state")))
                .description(result.getString("description"))
                .build();
    }
}
