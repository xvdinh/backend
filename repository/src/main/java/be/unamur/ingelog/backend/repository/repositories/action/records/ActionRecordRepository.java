package be.unamur.ingelog.backend.repository.repositories.action.records;

import be.unamur.ingelog.backend.domain.action.records.ActionRecord;

public interface ActionRecordRepository {
    /**
     * Enregistre une action réalisée par l'utilisateur dans la base de données.
     *
     * @param actionRecord l'action réalisée
     */
    void create(ActionRecord actionRecord);
}
