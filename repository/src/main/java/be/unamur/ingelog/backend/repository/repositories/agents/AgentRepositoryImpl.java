package be.unamur.ingelog.backend.repository.repositories.agents;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.AgentNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public class AgentRepositoryImpl extends BaseRepository implements AgentRepository {
    @Override
    public Agent findByUsername(String username) {
        return template.query(
                "select * from agents " +
                        "where username = :username",
                parameters()
                        .addValue("username", username),
                new AgentMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(AgentNotFoundException::new);
    }
}
