package be.unamur.ingelog.backend.repository.queries.notifications;

import be.unamur.ingelog.backend.dto.notifications.NotificationDetailDTO;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NotificationQueriesImpl extends BaseRepository implements NotificationQueries {
    @Override
    public List<NotificationDetailDTO> findByUsername(String username) {
        return template.query(
                "select notification_reference, is_read, username as author_username, type, resource_type, " +
                        "resource_reference, notification_timestamp " +
                        "from notifications n " +
                        "left join agents a on a.agent_id = n.author_id " +
                        "where recipient_id in " +
                        "(select agent_id from agents where username = :username)",
                parameters()
                        .addValue("username", username),
                new NotificationRowMappers.NotificationDetailMapper()
        );
    }

    @Override
    public Boolean existsByNotificationReference(String notificationReference) {
        return template.queryForObject(
                "select exists " +
                        "(select * from notifications where notification_reference = :notificationReference)",
                parameters()
                        .addValue("notificationReference", notificationReference),
                Boolean.class
        );
    }
}
