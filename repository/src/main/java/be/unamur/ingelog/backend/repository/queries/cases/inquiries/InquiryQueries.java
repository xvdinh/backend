package be.unamur.ingelog.backend.repository.queries.cases.inquiries;

import be.unamur.ingelog.backend.dto.cases.inquiries.*;

import java.util.List;

public interface InquiryQueries {
    /**
     * Retourne la liste de toutes les enquêtes.
     *
     * @return la liste de toutes les enquêtes
     */
    List<InquiryOverviewDTO> findAllInquiries();

    /**
     * Trouve une enquête sur base de sa référence.
     *
     * @param inquiryReference la référence de l'enquête à trouver
     * @return un Optional de l'enquête correspondant à la référence, si elle existe;
     * un Optional vide sinon
     */
    InquiryDetailDTO findByInquiryReference(String inquiryReference);

    /**
     * Trouve les enquêtes liées à la référence dossier fournie.
     *
     * @param caseReference la référence du dossier pour lequel trouver les enquêtes liées
     * @return la liste des enquêtes liées au dossier
     */
    List<InquiryDetailDTO> findByCaseReference(String caseReference);

    /**
     * Indique si un dossier existe déjà pour la référence fournie.
     *
     * @param inquiryReference la référence à vérifier
     * @return true si un dossier a été trouvé pour cette référence; false sinon
     */
    Boolean existsByInquiryReference(String inquiryReference);

    /**
     * Retourne la liste de tous les résultats d'enquête disponibles.
     *
     * @return la liste de toutes les résultats d'enquête
     */
    List<InquiryResultDTO> findAllResults();

    /**
     * Retourne la liste des enquêtes liées à un employeur spécifié.
     *
     * @param employerReference la référence de l'employeur
     * @return la liste des enquêtes liées
     */
    List<InquiryTargetDTO> findByTargetedEmployer(String employerReference);

    /**
     * Retourne le compte rendu de l'enquête spécifiée.
     *
     * @param inquiryReference la référence de l'enquête
     * @return le compte rendu de l'enquête
     */
    InquirySummaryDTO getSummary(String inquiryReference);
}
