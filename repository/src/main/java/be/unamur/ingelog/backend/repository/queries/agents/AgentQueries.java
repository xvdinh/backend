package be.unamur.ingelog.backend.repository.queries.agents;

import be.unamur.ingelog.backend.dto.agents.AgentAuthDTO;
import be.unamur.ingelog.backend.dto.agents.AgentDetailDTO;

import java.util.List;

public interface AgentQueries {
    /**
     * Retourne la liste de tous les agents.
     */
    List<AgentDetailDTO> findAll();

    /**
     * Trouve un agent dans la base de données sur base de son nom d'utilisateur et de son mot de passe.
     *
     * @param username le nom d'utilisateur
     * @return un Optional contenant l'agent si une correspondance est trouvée pour le nom d'utilisateur et
     * le mot de passe fournis; un Optional vide sinon
     */
    AgentAuthDTO authenticate(String username);

    /**
     * Trouve un agent dans la base de données sur base de son nom d'utilisateur.
     *
     * @param username le nom d'utilisateur
     * @return un Optional contenant l'agent si une correspondance est trouvée pour le nom d'utilisateur;
     * un Optional vide sinon
     */
    AgentDetailDTO findByUsername(String username);

    /**
     * Indique si un agent existe pour le nom d'utilisateur fourni.
     *
     * @param username le nom d'utilisateur à vérifier
     * @return true si un agent a été trouvé pour ce nom d'utilisateur; false sinon
     */
    Boolean existsByUsername(String username);
}
