package be.unamur.ingelog.backend.repository.queries.law.articles;

import be.unamur.ingelog.backend.dto.law.articles.OffenceOverviewDTO;

import java.util.List;
import java.util.Map;

public interface OffenceQueries {
    /**
     * Retourne la liste des infractions correspondant au filtre éventuel.
     *
     * @param filter le filtre éventuel
     * @return la liste des infractions
     */
    List<OffenceOverviewDTO> findAll(Map<String, String> filter);

    /**
     * Indique si une infraction existe déjà pour la référence fournie.
     *
     * @param reference la référence à vérifier
     * @return true si une infraction a été trouvée pour cette référence; false sinon
     */
    Boolean existsByReference(String reference);
}
