package be.unamur.ingelog.backend.repository.queries.dashboard;

import be.unamur.ingelog.backend.dto.dashboard.*;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DashboardQueriesImpl extends BaseRepository implements DashboardQueries {

    @Override
    public List<TotalInquiriesByStateDTO> getTotalInquiriesByState() {
        return template.query(
                "select state_id as inquiry_state, " +
                        "count(state) as total_inquiries " +
                        "from inquiry_states " +
                        "left join inquiries i on inquiry_states.state_id = i.state and " +
                        "(close_date is null or " +
                        "date_part('year', close_date) = date_part('year', current_date)) " +
                        "group by inquiry_state " +
                        "order by inquiry_state ",
                new DashboardRowMappers.TotalInquiriesByStateMapper()
        );
    }

    @Override
    public List<TotalInquiriesByPriorityDTO> getTotalInquiriesByPriority() {
        return template.query(
                "select offence_priority_id as inquiry_priority, count(priority) as total_inquiries " +
                        "from offence_priorities op " +
                        "left join inquiries i on i.priority = offence_priority_id and close_date is null " +
                        "group by offence_priority_id " +
                        "order by offence_priority_id",
                new DashboardRowMappers.TotalInquiriesByPriorityMapper()
        );
    }

    @Override
    public List<InquiriesDistributionByResultDTO> getInquiriesDistributionByResult() {
        return template.query(
                "select result_id as inquiry_result, " +
                        "count(result) * 1.0 / " +
                        "greatest(1, (select count(*) " +
                        "from inquiries " +
                        "where close_date is not null " +
                        "and result is not null " +
                        "and date_part('year', close_date) = date_part('year', current_date))) as inquiries_distribution " +
                        "from inquiry_results " +
                        "left join inquiries i on inquiry_results.result_id = i.result and close_date is not null " +
                        "and date_part('year', close_date) = date_part('year', current_date) " +
                        "group by result_id " +
                        "order by result_id",
                new DashboardRowMappers.InquiriesDistributionByResultMapper()
        );
    }

    @Override
    public List<TotalInquiriesByAgentDTO> getTotalInquiriesByAgent() {
        return template.query(
                "select username, " +
                        "count(assignee) as total_inquiries " +
                        "from agents " +
                        "left join inquiries i on agents.agent_id = i.assignee and close_date is null " +
                        "where role_id = 'INSPECTOR' " +
                        "or role_id = 'CHIEF_INSPECTOR' " +
                        "group by username " +
                        "order by username",
                new DashboardRowMappers.TotalInquiriesByAgentMapper()
        );
    }

    @Override
    public List<TotalClosedInquiriesByYearDTO> getTotalClosedInquiriesByYear() {
        var result = new ArrayList<TotalClosedInquiriesByYearDTO>();
        var currentYear = LocalDate.now().getYear();

        for (int i = 0; i < 5; i++) {
            var dto = template.query(
                    "select :targetYear as year, " +
                            "count(case state when 'dismissed' then 1 end) as total_dismissed_inquiries, " +
                            "count(case result when 'positive' then 1 end) as total_closed_positive_inquiries, " +
                            "count(case result when 'negative' then 1 end) as total_closed_negative_inquiries " +
                            "from inquiries " +
                            "where close_date is not null and date_part('year', close_date) = :targetYear " +
                            "group by year " +
                            "order by year",
                    parameters()
                            .addValue("targetYear", currentYear - i),
                    new DashboardRowMappers.TotalClosedInquiriesByYearMapper()
            )
                    .stream()
                    .findFirst()
                    .orElse(TotalClosedInquiriesByYearDTO.builder()
                            .year(String.valueOf(currentYear - i))
                            .totalDismissedInquiries(0)
                            .totalClosedPositiveInquiries(0)
                            .totalClosedNegativeInquiries(0)
                            .build());
            result.add(dto);
        }

        return result;
    }
}
