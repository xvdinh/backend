package be.unamur.ingelog.backend.repository.queries.notifications;

import be.unamur.ingelog.backend.dto.notifications.NotificationDetailDTO;

import java.util.List;

public interface NotificationQueries {
    /**
     * Retourne la liste des notifications de l'utilisateur spécifié.
     *
     * @param username le nom d'utilisateur
     * @return la liste des notifications
     */
    List<NotificationDetailDTO> findByUsername(String username);

    /**
     * Indique si une notification existe pour la référence fournie.
     *
     * @param notificationReference la référence de la notification
     * @return true si une notification existe; false sinon
     */
    Boolean existsByNotificationReference(String notificationReference);
}
