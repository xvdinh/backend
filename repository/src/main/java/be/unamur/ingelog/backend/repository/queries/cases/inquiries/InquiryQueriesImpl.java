package be.unamur.ingelog.backend.repository.queries.cases.inquiries;

import be.unamur.ingelog.backend.dto.cases.inquiries.*;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.InquiryNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class InquiryQueriesImpl extends BaseRepository implements InquiryQueries {
    private static final String OVERVIEW_QUERY =
            "select inquiry_reference, " +
                    "i.state as inquiry_state, " +
                    "report_date, " +
                    "i.close_date, " +
                    "deadline, " +
                    "priority, " +
                    "employer_reference, " +
                    "name as employer_name, " +
                    "trigger_date, " +
                    "a.username as assignee, " +
                    "s.username as supervisor, " +
                    "result " +
                    "from inquiries i " +
                    "left join employers e on i.employer_id = e.employer_id " +
                    "left join cases c on i.case_id = c.case_id " +
                    "left join agents a on i.assignee = a.agent_id " +
                    "left join agents s on i.supervisor = s.agent_id ";

    private static final String DETAIL_QUERY =
            "select inquiry_reference, i.state as inquiry_state, i.result as inquiry_result, deadline, priority, " +
                    "i.open_date, i.close_date, i.report_date, i.street, i.postalcode, i.locality, " +
                    "case_reference, employer_reference, " +
                    "a.username as assignee, s.username as supervisor from inquiries i " +
                    "left join cases c on i.case_id = c.case_id " +
                    "left join employers e on i.employer_id = e.employer_id " +
                    "left join agents a on i.assignee = a.agent_id " +
                    "left join agents s on i.supervisor = s.agent_id ";

    @Override
    public List<InquiryOverviewDTO> findAllInquiries() {
        return template.query(
                OVERVIEW_QUERY + "order by trigger_date desc",
                new InquiryRowMappers.InquiryOverviewMapper());
    }

    @Override
    public InquiryDetailDTO findByInquiryReference(String inquiryReference) {
        return template.query(
                DETAIL_QUERY + "where inquiry_reference = :inquiryReference",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new InquiryRowMappers.InquiryDetailMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(InquiryNotFoundException::new);
    }

    @Override
    public List<InquiryDetailDTO> findByCaseReference(String caseReference) {
        return template.query(
                DETAIL_QUERY + "where i.case_id in " +
                        "(select case_id from cases where case_reference = :caseReference)",
                parameters()
                        .addValue("caseReference", caseReference),
                new InquiryRowMappers.InquiryDetailMapper()
        );
    }

    @Override
    public Boolean existsByInquiryReference(String inquiryReference) {
        return template.queryForObject(
                "select exists (select * from inquiries where inquiry_reference = :inquiryReference)",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                Boolean.class
        );
    }

    @Override
    public List<InquiryResultDTO> findAllResults() {
        return template.query(
                "select result_id from inquiry_results",
                new InquiryRowMappers.InquiryResultMapper()
        );
    }

    @Override
    public List<InquiryTargetDTO> findByTargetedEmployer(String employerReference) {
        return template.query(
                "select inquiry_reference, " +
                        "report_date, " +
                        "i.state as inquiry_state, " +
                        "result as inquiry_result, " +
                        "o.label as offence_label, " +
                        "r.status as report_status, " +
                        "r.state as report_state " +
                        "from inquiries i " +
                        "left join reports r on i.inquiry_id = r.inquiry_id " +
                        "left join offences o on r.offence_id = o.offence_id " +
                        "where i.employer_id in " +
                        "(select employer_id from employers where employer_reference = :employerReference)",
                parameters()
                        .addValue("employerReference", employerReference),
                new InquiryRowMappers.InquiryTargetExtractor()
        );
    }

    @Override
    public InquirySummaryDTO getSummary(String inquiryReference) {
        var inquirySummary = fetchInquirySummary(inquiryReference);
        var lawArticles = fetchLawArticles(inquiryReference);
        var comments = fetchComments(inquiryReference);
        var attachments = fetchAttachments(inquiryReference);

        inquirySummary.setLawArticles(lawArticles);
        inquirySummary.setComments(comments);
        inquirySummary.setAttachments(attachments);

        return inquirySummary;
    }

    private List<InquirySummaryAttachmentDTO> fetchAttachments(String inquiryReference) {
        return template.query(
                "select document_type, reception_date, source from attachments " +
                        "where inquiry_id in " +
                        "(select inquiry_id from inquiries where inquiry_reference = :inquiryReference)",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new InquiryRowMappers.InquirySummaryAttachmentMapper()
        );
    }

    private List<InquirySummaryCommentDTO> fetchComments(String inquiryReference) {
        return template.query(
                "select username as author_username, content, creation_date from case_comments c " +
                        "left join agents a on c.author = a.agent_id " +
                        "where c.case_id in " +
                        "(select i.case_id from inquiries i where inquiry_reference = :inquiryReference)",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new InquiryRowMappers.InquirySummaryCommentMapper()
        );
    }

    private List<InquirySummaryLawArticleDTO> fetchLawArticles(String inquiryReference) {
        return template.query(
                "select status as report_status, " +
                        "la.label as law_article_label, " +
                        "controls_count, " +
                        "o.label as offence_label, " +
                        "ro.title as observation_title, " +
                        "ro.value as observation_value, " +
                        "ro.unit as observation_unit, " +
                        "ro.description as observation_description " +
                        "from reports r " +
                        "left join offences o on r.offence_id = o.offence_id " +
                        "left join law_articles la on o.law_article_id = la.law_article_id " +
                        "left join report_observations ro on ro.report_id = r.report_id " +
                        "where inquiry_id in " +
                        "(select inquiry_id from inquiries where inquiry_reference = :inquiryReference)",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new InquiryRowMappers.InquirySummaryLawArticleExtractor()
        );
    }

    private InquirySummaryDTO fetchInquirySummary(String inquiryReference) {
        return template.query(
                "select inquiry_reference, " +
                        "result as inquiry_result, " +
                        "priority as inquiry_priority, " +
                        "i.open_date as inquiry_open_date, " +
                        "i.close_date as inquiry_close_date, " +
                        "report_date, " +
                        "i.street as report_street, " +
                        "i.locality as report_locality, " +
                        "i.postalcode as report_postal_code, " +
                        "deadline as inquiry_deadline, " +
                        "trigger_date, " +
                        "motive as case_motive, " +
                        "origin as case_origin, " +
                        "first_name as supervisor_first_name, " +
                        "last_name as supervisor_last_name, " +
                        "employer_reference, " +
                        "name as employer_name, " +
                        "e.street as employer_street, " +
                        "e.locality as employer_locality, " +
                        "e.postalcode as employer_postal_code, " +
                        "e.country as employer_country " +
                        "from inquiries i " +
                        "left join cases c on i.case_id = c.case_id " +
                        "left join agents a on i.supervisor = a.agent_id " +
                        "left join employers e on i.employer_id = e.employer_id " +
                        "where inquiry_reference = :inquiryReference",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new InquiryRowMappers.InquirySummaryMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(InquiryNotFoundException::new);
    }
}
