package be.unamur.ingelog.backend.repository.queries.employers;

import be.unamur.ingelog.backend.dto.employers.EmployerDetailDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.EmployerNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployerQueriesImpl extends BaseRepository implements EmployerQueries {
    @Override
    public List<EmployerDetailDTO> findAllEmployers() {
        return template.query(
                "select employer_reference, name, street, postalcode, locality, country from employers",
                new EmployerRowMappers.EmmployerDetailMapper());
    }

    @Override
    public EmployerDetailDTO findByEmployerReference(String reference) {
        return template.query(
                "select employer_reference, name, street, postalcode, locality, country from employers " +
                        "where employer_reference = :reference",
                parameters()
                        .addValue("reference", reference),
                new EmployerRowMappers.EmmployerDetailMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(EmployerNotFoundException::new);
    }

    @Override
    public Boolean existsByEmployerReference(String reference) {
        return template.queryForObject(
                "select exists (select * from employers where employer_reference = :reference)",
                parameters()
                        .addValue("reference", reference),
                Boolean.class);
    }
}
