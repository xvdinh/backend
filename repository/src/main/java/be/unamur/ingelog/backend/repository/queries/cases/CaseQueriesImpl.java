package be.unamur.ingelog.backend.repository.queries.cases;

import be.unamur.ingelog.backend.dto.cases.CaseDetailDTO;
import be.unamur.ingelog.backend.dto.cases.CaseMotiveDTO;
import be.unamur.ingelog.backend.dto.cases.CaseOriginDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.CaseNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CaseQueriesImpl extends BaseRepository implements CaseQueries {
    @Override
    public List<CaseDetailDTO> findAllCases() {
        return template.query(
                "select * from cases " +
                        "order by trigger_date desc",
                new CaseRowMappers.CaseDetailMapper());
    }

    @Override
    public CaseDetailDTO findByCaseReference(String caseReference) {
        return template.query(
                "select * from cases where case_reference = :caseReference",
                parameters()
                        .addValue("caseReference", caseReference),
                new CaseRowMappers.CaseDetailMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(CaseNotFoundException::new);
    }

    @Override
    public Boolean existsByCaseReference(String caseReference) {
        return template.queryForObject(
                "select exists (select * from cases where case_reference = :caseReference)",
                parameters()
                        .addValue("caseReference", caseReference),
                Boolean.class);
    }

    @Override
    public List<CaseMotiveDTO> findAllMotives() {
        return template.query(
                "select motive_id from case_motives",
                new CaseRowMappers.CaseMotiveMapper());
    }

    @Override
    public Boolean existingMotive(String motive) {
        return template.queryForObject(
                "select exists (select * from case_motives where motive_id = :motive)",
                parameters()
                        .addValue("motive", motive.toLowerCase()),
                Boolean.class);
    }

    @Override
    public List<CaseOriginDTO> findAllOrigins() {
        return template.query(
                "select origin_id, motive_id from case_origins",
                new CaseRowMappers.CaseOriginMapper());
    }

    @Override
    public List<CaseOriginDTO> findOriginsForMotive(String motive) {
        return template.query(
                "select * from case_origins " +
                        "where motive_id = :motive",
                parameters()
                        .addValue("motive", motive),
                new CaseRowMappers.CaseOriginMapper()
        );
    }
}
