package be.unamur.ingelog.backend.repository.repositories.law.articles;

import be.unamur.ingelog.backend.domain.law.articles.LawArticle;
import be.unamur.ingelog.backend.domain.law.articles.Offence;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toInteger;
import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toLocalDate;

public class OffenceMapper implements RowMapper<Offence> {
    @Override
    public Offence mapRow(ResultSet result, int row) throws SQLException {
        return Offence.builder()
                .offenceId(toInteger(result.getInt("offence_id")))
                .offenceReference(result.getString("offence_reference"))
                .lawArticle(LawArticle.builder()
                        .lawArticleId(toInteger(result.getInt("law_article_id")))
                        .build())
                .label(result.getString("label"))
                .startingDate(toLocalDate(result.getDate("starting_date")))
                .endingDate(toLocalDate(result.getDate("ending_date")))
                .legalDelay(toInteger(result.getInt("legal_delay")))
                .priority(result.getString("priority"))
                .build();
    }
}
