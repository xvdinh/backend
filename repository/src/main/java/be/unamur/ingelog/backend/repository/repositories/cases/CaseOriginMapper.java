package be.unamur.ingelog.backend.repository.repositories.cases;

import be.unamur.ingelog.backend.domain.cases.CaseOrigin;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CaseOriginMapper implements RowMapper<CaseOrigin> {
    @Override
    public CaseOrigin mapRow(ResultSet result, int row) throws SQLException {
        return CaseOrigin.valueOf(result.getString("origin").toUpperCase());
    }
}
