package be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.Report;

import java.util.List;

public interface ReportRepository {
    /**
     * Crée une nouvelle constatation dans la base de données.
     *
     * @param report la constatation à créer
     */
    void create(Report report);

    /**
     * Met à jour une constatation existante dans la base de données.
     *
     * @param report la constatation à mettre à jour
     */
    void update(Report report);

    /**
     * Trouve une constatation par sa référence.
     *
     * @param reportReference la référence de la constatation
     * @return la constatation correspondante
     */
    Report findByReportReference(String reportReference);

    /**
     * Trouve les constatations liées à la référence d'enquête fournie.
     *
     * @param inquiryReference la référence d'enquête
     * @return les constatations liées
     */
    List<Report> findByInquiryReference(String inquiryReference);
}
