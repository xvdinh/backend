package be.unamur.ingelog.backend.repository.queries.cases.inquiries.reports;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportDetailDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportStatusUpdateDTO;

import java.util.List;

public interface ReportQueries {
    /**
     * Retourne la liste des constatations associées à l'enquête dont la référence est fournie.
     *
     * @param inquiryReference la référence de l'enquête dont on veut obtenir les constatations associées
     * @return la liste des constatations associées à l'enquête
     */
    List<ReportDetailDTO> findByInquiryReference(String inquiryReference);

    /**
     * Trouve une constatation sur base de sa référence.
     *
     * @param reportReference la référence de la constatation
     * @return la constatation correspondant à la référence
     */
    ReportDetailDTO findByReportReference(String reportReference);

    /**
     * Trouve l'ensemble des constatations.
     *
     * @return la liste complète des constatations
     */
    List<ReportDetailDTO> findAllReports();

    /**
     * Indique si une constatation existe déjà pour la référence fournie.
     *
     * @param reportReference la référence à vérifier
     * @return true si une constatation a été trouvée pour cette référence; false sinon
     */
    Boolean existsByReportReference(String reportReference);

    /**
     * Retourne la liste de tous les statuts de constatation disponibles.
     *
     * @return la liste de toutes les statuts de constatation
     */
    List<ReportStatusUpdateDTO> findAllStatus();
}
