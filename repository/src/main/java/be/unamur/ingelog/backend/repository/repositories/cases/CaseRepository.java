package be.unamur.ingelog.backend.repository.repositories.cases;

import be.unamur.ingelog.backend.domain.cases.Case;
import be.unamur.ingelog.backend.domain.cases.CaseMotive;
import be.unamur.ingelog.backend.domain.cases.CaseOrigin;

import java.util.List;

public interface CaseRepository {
    /**
     * Crée un nouveau dossier dans la base de données.
     *
     * @param caseToCreate le dossier à créer
     */
    void create(Case caseToCreate);

    /**
     * Met à jour un dossier existant dans la base de données.
     *
     * @param caseUpdate le dossier à mettre à jour
     */
    void update(Case caseUpdate);

    /**
     * Trouve un dossier par sa référence.
     *
     * @param caseReference la référence du dossier
     * @return le dossier correspondant
     */
    Case findByCaseReference(String caseReference);

    /**
     * Trouve le dossier lié à la référence d'enquête fournie.
     *
     * @param inquiryReference la référence d'enquête
     * @return le dossier lié
     */
    Case findByInquiryReference(String inquiryReference);

    /**
     * Trouve les origines de dossier liées au motif fourni.
     *
     * @param motive le motif de dossier
     * @return la liste des origines
     */
    List<CaseOrigin> findOriginsForMotive(CaseMotive motive);
}
