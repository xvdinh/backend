package be.unamur.ingelog.backend.repository.queries.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationDetailDTO;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ObservationQueriesImpl extends BaseRepository implements ObservationQueries {
    @Override
    public List<ObservationDetailDTO> findByReportReference(String reportReference) {
        return template.query(
                "select report_reference, observation_number, title, value, unit, description from report_observations ro " +
                        "left join reports r on r.report_id = ro.report_id " +
                        "where ro.report_id in " +
                        "(select report_id from reports where report_reference = :reportReference) " +
                        "order by observation_number",
                parameters()
                        .addValue("reportReference", reportReference),
                new ObservationRowMappers.ObservationDetailMapper());
    }
}
