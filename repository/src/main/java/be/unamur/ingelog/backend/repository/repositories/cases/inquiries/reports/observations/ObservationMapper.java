package be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.Report;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportObservation;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toInteger;

public class ObservationMapper implements RowMapper<ReportObservation> {
    @Override
    public ReportObservation mapRow(ResultSet result, int row) throws SQLException {
        return ReportObservation.builder()
                .observationId(toInteger(result.getInt("observation_id")))
                .relatedReport(Report.builder()
                        .reportId(toInteger(result.getInt("report_id")))
                        .build())
                .observationNumber(toInteger(result.getInt("observation_number")))
                .title(result.getString("title"))
                .value(result.getString("value"))
                .unit(result.getString("unit"))
                .description(result.getString("description"))
                .build();
    }
}
