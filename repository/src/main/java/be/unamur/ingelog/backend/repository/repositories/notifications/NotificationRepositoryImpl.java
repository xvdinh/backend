package be.unamur.ingelog.backend.repository.repositories.notifications;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.notifications.Notification;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.NotificationNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NotificationRepositoryImpl extends BaseRepository implements NotificationRepository {
    @Override
    public void create(Notification notification) {
        template.update(
                "insert into notifications (notification_id, notification_reference, author_id, recipient_id, type, " +
                        "resource_type, resource_reference, notification_timestamp) " +
                        "values (nextval('notifications_seq'), :notificationReference, :authorId, :recipientId, :type, " +
                        ":resourceType, :resourceReference, :notificationTimestamp)",
                parameters()
                        .addValue("notificationReference", notification.getNotificationReference())
                        .addValue("authorId", notification.getAuthor().getAgentId())
                        .addValue("recipientId", notification.getRecipient().getAgentId())
                        .addValue("type", notification.getType().toString())
                        .addValue("resourceType", notification.getResourceType().toString())
                        .addValue("resourceReference", notification.getResourceReference())
                        .addValue("notificationTimestamp", notification.getNotificationTime())
        );
    }

    @Override
    public List<Agent> findRecipientsByCaseReference(String caseReference) {
        return template.query(
                "select distinct * from agents a " +
                        "left join cases c on case_reference = :caseReference " +
                        "left join inquiries i on i.case_id = c.case_id " +
                        "where a.agent_id = i.assignee or a.agent_id = i.supervisor",
                parameters()
                        .addValue("caseReference", caseReference),
                new AgentMapper()
        );
    }

    @Override
    public Notification findByNotificationReference(String notificationReference) throws NotificationNotFoundException {
        return template.query(
                "select * from notifications " +
                        "where notification_reference = :notificationReference",
                parameters()
                        .addValue("notificationReference", notificationReference),
                new NotificationMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(NotificationNotFoundException::new);
    }

    @Override
    public void markAsConsulted(String notificationReference) {
        template.update(
                "update notifications set is_read = true " +
                        "where notification_reference = :notificationReference",
                parameters()
                        .addValue("notificationReference", notificationReference)
        );
    }
}
