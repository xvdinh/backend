package be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.Report;
import be.unamur.ingelog.backend.domain.law.articles.Offence;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.*;

public class ReportMapper implements RowMapper<Report> {
    @Override
    public Report mapRow(ResultSet result, int row) throws SQLException {
        return Report.builder()
                .reportId(toInteger(result.getInt("report_id")))
                .reportReference(result.getString("report_reference"))
                .relatedInquiry(Inquiry.builder()
                        .inquiryId(toInteger(result.getInt("inquiry_id")))
                        .build())
                .reportState(toReportState(result.getString("state")))
                .reportStatus(toReportStatus(result.getString("status")))
                .offence(Offence.builder()
                        .offenceId(toInteger(result.getInt("offence_id")))
                        .build())
                .deadline(toLocalDate(result.getDate("deadline")))
                .totalOfControlledPersons(toInteger(result.getInt("controls_count")))
                .build();
    }
}
