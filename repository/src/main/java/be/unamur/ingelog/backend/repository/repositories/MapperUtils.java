package be.unamur.ingelog.backend.repository.repositories;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.cases.CaseMotive;
import be.unamur.ingelog.backend.domain.cases.CaseState;
import be.unamur.ingelog.backend.domain.cases.inquiries.InquiryResult;
import be.unamur.ingelog.backend.domain.cases.inquiries.InquiryState;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportState;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportStatus;
import be.unamur.ingelog.backend.domain.employers.Employer;
import be.unamur.ingelog.backend.domain.notifications.NotificationType;
import be.unamur.ingelog.backend.domain.notifications.ResourceType;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class MapperUtils {
    private MapperUtils() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static Integer toInteger(int source) {
        return source == 0 ? null : source;
    }

    public static InquiryState toInquiryState(String source) {
        return source == null ? null : InquiryState.valueOf(source.toUpperCase());
    }

    public static InquiryResult toInquiryResult(String source) {
        return source == null ? null : InquiryResult.valueOf(source.toUpperCase());
    }

    public static CaseMotive toCaseMotive(String source) {
        return source == null ? null : CaseMotive.valueOf(source.toUpperCase());
    }

    public static CaseState toCaseState(String source) {
        return source == null ? null : CaseState.valueOf(source.toUpperCase());
    }

    public static ReportState toReportState(String source) {
        return source == null ? null : ReportState.valueOf(source.toUpperCase());
    }

    public static ReportStatus toReportStatus(String source) {
        return source == null ? null : ReportStatus.valueOf(source.toUpperCase());
    }

    public static LocalDate toLocalDate(Date source) {
        return source == null ? null : source.toLocalDate();
    }

    public static LocalDateTime toLocalDateTime(Timestamp source) {
        return source == null ? null : source.toLocalDateTime();
    }

    public static String toStringFromEnum(Enum source) {
        return source == null ? null : source.toString().toLowerCase();
    }

    public static Integer toAgentId(Agent agent) {
        return agent == null ? null : agent.getAgentId();
    }

    public static Integer toEmployerId(Employer employer) {
        return employer == null ? null : employer.getEmployerId();
    }

    public static NotificationType toNotificationType(String source) {
        return source == null ? null : NotificationType.valueOf(source.toUpperCase());
    }

    public static ResourceType toResourceType(String source) {
        return source == null ? null : ResourceType.valueOf(source.toUpperCase());
    }
}
