package be.unamur.ingelog.backend.repository.queries.statistics;

import be.unamur.ingelog.backend.dto.statistics.EmployerDTO;
import be.unamur.ingelog.backend.dto.statistics.InspectionInquiryDTO;
import be.unamur.ingelog.backend.dto.statistics.ResultDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.MapperUtils.toInteger;
import static be.unamur.ingelog.backend.repository.MapperUtils.toLocalDateTime;

public class StatisticRowMappers {
    private StatisticRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static class InspectionInquiryMapper implements RowMapper<InspectionInquiryDTO> {
        @Override
        public InspectionInquiryDTO mapRow(ResultSet result, int row) throws SQLException {
            return InspectionInquiryDTO.builder()
                    .reference(result.getString("inquiry_reference"))
                    .startingDate(toLocalDateTime(result.getTimestamp("open_date")))
                    .closingDate(toLocalDateTime(result.getTimestamp("close_date")))
                    .targetEmployer(EmployerDTO.builder()
                            .legalName(result.getString("legal_name"))
                            .country(result.getString("country_code"))
                            .bce(null)
                            .vat(null)
                            .build())
                    .inquiryResults(null)
                    .legalDelay(toInteger(result.getInt("legal_delay")))
                    .priority(toPriorityEnum(result.getString("priority")))
                    .globalResult(toResultEnum(result.getString("global_result")))
                    .caseReference(result.getString("case_reference"))
                    .joinedInquiry(false)
                    .postCode(result.getString("postalcode"))
                    .build();
        }

        private String toPriorityEnum(String source) {
            if (source == null) return null;

            switch (source) {
                case "1":
                    return "HIGHEST";
                case "2":
                    return "MEDIUM";
                case "3":
                    return "NORMAL";
                case "4":
                    return "LOW";
                case "5":
                    return "LOWEST";
                default:
                    throw new IllegalArgumentException("Unsupported case");
            }
        }

        private String toResultEnum(String source) {
            if (source == null) return null;

            return source.toUpperCase();
        }
    }

    public static class ResultMapper implements RowMapper<ResultDTO> {
        @Override
        public ResultDTO mapRow(ResultSet result, int row) throws SQLException {
            return ResultDTO.builder()
                    .lawTypeCode(result.getString("law_article_reference"))
                    .violationTypeCode(result.getString("offence_reference"))
                    .status(toStatusEnum(result.getString("report_status")))
                    .totalOfControlledPerson(result.getInt("controls_count"))
                    .build();
        }

        private String toStatusEnum(String source) {
            return source == null ? null : source.toUpperCase();
        }
    }
}
