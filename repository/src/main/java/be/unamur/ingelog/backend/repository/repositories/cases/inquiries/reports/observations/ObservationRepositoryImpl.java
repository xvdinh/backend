package be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportObservation;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.ObservationNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ObservationRepositoryImpl extends BaseRepository implements ObservationRepository {
    @Override
    public void create(ReportObservation observation) {
        template.update(
                "insert into report_observations(observation_id, report_id, observation_number, title, value, unit, " +
                        "description) " +
                        "values(nextval('report_observations_seq'), :reportId, :observationNumber, :title, :value, " +
                        ":unit, :description)",
                parameters()
                        .addValue("reportId", observation.getRelatedReport().getReportId())
                        .addValue("observationNumber", observation.getObservationNumber())
                        .addValue("title", observation.getTitle())
                        .addValue("value", observation.getValue())
                        .addValue("unit", observation.getUnit())
                        .addValue("description", observation.getDescription())
        );
    }

    @Override
    public void update(ReportObservation reportObservation) {
        template.update(
                "update report_observations set " +
                        "title = :title, " +
                        "value = :value, " +
                        "unit = :unit, " +
                        "description = :description " +
                        "where report_id = :reportId " +
                        "and observation_number = :observationNumber",
                parameters()
                        .addValue("title", reportObservation.getTitle())
                        .addValue("value", reportObservation.getValue())
                        .addValue("unit", reportObservation.getUnit())
                        .addValue("description", reportObservation.getDescription())
                        .addValue("reportId", reportObservation.getRelatedReport().getReportId())
                        .addValue("observationNumber", reportObservation.getObservationNumber())
        );
    }

    @Override
    public ReportObservation findByReportReferenceAndObservationNumber(String reportReference,
                                                                       Integer observationNumber) {
        return template.query(
                "select * from report_observations " +
                        "where observation_number = :observationNumber " +
                        "and report_id in " +
                        "(select report_id from reports where report_reference = :reportReference)",
                parameters()
                        .addValue("observationNumber", observationNumber)
                        .addValue("reportReference", reportReference),
                new ObservationMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(ObservationNotFoundException::new);
    }

    @Override
    public List<ReportObservation> findByReportReference(String reportReference) {
        return template.query(
                "select * from report_observations " +
                        "where report_id in " +
                        "(select report_id from reports where report_reference = :reportReference)",
                parameters()
                        .addValue("reportReference", reportReference),
                new ObservationMapper()
        );
    }

    @Override
    public Boolean existsByNumberAndReportReference(int observationNumber, String reportReference) {
        return template.queryForObject(
                "select exists (select * from report_observations ro " +
                        "where observation_number = :observationNumber " +
                        "and ro.report_id in " +
                        "(select r.report_id from reports r where report_reference = :reportReference))",
                parameters()
                        .addValue("observationNumber", observationNumber)
                        .addValue("reportReference", reportReference),
                Boolean.class);
    }
}
