package be.unamur.ingelog.backend.repository.queries.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationDetailDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.MapperUtils.toInteger;

public final class ObservationRowMappers {
    private ObservationRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static class ObservationDetailMapper implements RowMapper<ObservationDetailDTO> {
        @Override
        public ObservationDetailDTO mapRow(ResultSet result, int row) throws SQLException {
            return ObservationDetailDTO.builder()
                    .reportReference(result.getString("report_reference"))
                    .observationNumber(toInteger(result.getInt("observation_number")))
                    .title(result.getString("title"))
                    .value(result.getString("value"))
                    .unit(result.getString("unit"))
                    .description(result.getString("description"))
                    .build();
        }
    }
}
