package be.unamur.ingelog.backend.repository.repositories.cases.comments;

import be.unamur.ingelog.backend.domain.cases.CaseComment;

public interface CommentRepository {
    /**
     * Crée un nouveau commentaire dans la base de données.
     *
     * @param comment le commentaire à créer
     */
    void create(CaseComment comment);
}
