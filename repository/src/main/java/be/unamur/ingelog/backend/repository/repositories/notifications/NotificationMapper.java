package be.unamur.ingelog.backend.repository.repositories.notifications;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.notifications.Notification;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.*;

public class NotificationMapper implements RowMapper<Notification> {
    @Override
    public Notification mapRow(ResultSet result, int row) throws SQLException {
        return Notification.builder()
                .notificationId(toInteger(result.getInt("notification_id")))
                .notificationReference(result.getString("notification_reference"))
                .isRead(result.getBoolean("is_read"))
                .author(Agent.builder()
                        .agentId(toInteger(result.getInt("author_id")))
                        .build())
                .recipient(Agent.builder()
                        .agentId(toInteger(result.getInt("recipient_id")))
                        .build())
                .type(toNotificationType(result.getString("type")))
                .resourceType(toResourceType(result.getString("resource_type")))
                .resourceReference(result.getString("resource_reference"))
                .notificationTime(toLocalDateTime(result.getTimestamp("notification_timestamp")))
                .build();
    }
}
