package be.unamur.ingelog.backend.repository.repositories.notifications;

import be.unamur.ingelog.backend.domain.agents.Agent;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toInteger;

public class AgentMapper implements RowMapper<Agent> {
    @Override
    public Agent mapRow(ResultSet result, int row) throws SQLException {
        return Agent.builder()
                .agentId(toInteger(result.getInt("agent_id")))
                .username(result.getString("username"))
                .firstName(result.getString("first_name"))
                .lastName(result.getString("last_name"))
                .build();
    }
}
