package be.unamur.ingelog.backend.repository.queries.cases.comments;

import be.unamur.ingelog.backend.dto.cases.comments.CommentDetailDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.MapperUtils.toLocalDateTime;

public final class CommentRowMappers {
    private CommentRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static class CommentDetailMapper implements RowMapper<CommentDetailDTO> {
        @Override
        public CommentDetailDTO mapRow(ResultSet result, int row) throws SQLException {
            return CommentDetailDTO.builder()
                    .content(result.getString("content"))
                    .creationDate(toLocalDateTime(result.getTimestamp("creation_date")))
                    .authorFirstName(result.getString("first_name"))
                    .authorLastName(result.getString("last_name"))
                    .build();
        }
    }
}
