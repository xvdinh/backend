package be.unamur.ingelog.backend.repository.queries.cases.comments;

import be.unamur.ingelog.backend.dto.cases.comments.CommentDetailDTO;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentQueriesImpl extends BaseRepository implements CommentQueries {
    @Override
    public List<CommentDetailDTO> findByCaseReference(String caseReference) {
        return template.query(
                "select content, creation_date, first_name, last_name from case_comments " +
                        "left join agents on agent_id = author " +
                        "where case_id in " +
                        "(select case_id from cases where case_reference = :caseReference)",
                parameters()
                        .addValue("caseReference", caseReference),
                new CommentRowMappers.CommentDetailMapper());
    }

}
