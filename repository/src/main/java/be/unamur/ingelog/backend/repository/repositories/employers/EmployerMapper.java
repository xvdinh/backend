package be.unamur.ingelog.backend.repository.repositories.employers;

import be.unamur.ingelog.backend.domain.employers.Employer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toInteger;

public class EmployerMapper implements RowMapper<Employer> {
    @Override
    public Employer mapRow(ResultSet result, int row) throws SQLException {
        return Employer.builder()
                .employerId(toInteger(result.getInt("employer_id")))
                .employerReference(result.getString("employer_reference"))
                .name(result.getString("name"))
                .street(result.getString("street"))
                .postalCode(result.getString("postalcode"))
                .locality(result.getString("locality"))
                .country(result.getString("country"))
                .countryCode(result.getString("country_code"))
                .build();
    }
}
