package be.unamur.ingelog.backend.repository.queries.employers;

import be.unamur.ingelog.backend.dto.employers.EmployerDetailDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.EmployerNotFoundException;

import java.util.List;

public interface EmployerQueries {
    /**
     * Trouve l'ensemble des employeurs.
     *
     * @return l'ensemble des employeurs
     */
    List<EmployerDetailDTO> findAllEmployers();

    /**
     * Trouve un employeur sur base de la référence fournie.
     *
     * @param reference la référence de l'employeur à trouver
     * @return l'employeur correspondant à la référence, s'il existe
     */
    EmployerDetailDTO findByEmployerReference(String reference) throws EmployerNotFoundException;

    /**
     * Indique si un employeur existe pour la référence fournie.
     *
     * @param reference la référence à vérifier
     * @return true si un employeur a été trouvé pour cette référence; false sinon
     */
    Boolean existsByEmployerReference(String reference);
}
