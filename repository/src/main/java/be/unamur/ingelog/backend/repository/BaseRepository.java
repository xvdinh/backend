package be.unamur.ingelog.backend.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public abstract class BaseRepository {
    @Autowired
    protected NamedParameterJdbcTemplate template;

    protected KeyHolder keyHolder = new GeneratedKeyHolder();

    protected MapSqlParameterSource parameters() {
        return new MapSqlParameterSource();
    }
}
