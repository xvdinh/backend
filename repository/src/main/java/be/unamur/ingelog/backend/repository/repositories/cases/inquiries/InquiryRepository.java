package be.unamur.ingelog.backend.repository.repositories.cases.inquiries;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;

import java.util.List;

public interface InquiryRepository {
    /**
     * Crée une nouvelle enquête dans la base de données.
     *
     * @param inquiry l'enquête à créer
     */
    void create(Inquiry inquiry);

    /**
     * Met à jour une enquête existante dans la base de données.
     *
     * @param inquiry l'enquête à mettre à jour
     */
    void update(Inquiry inquiry);

    /**
     * Trouve une enquête par sa référence.
     *
     * @param inquiryReference la référence de l'enquête
     * @return l'enquête correspondante
     */
    Inquiry findByInquiryReference(String inquiryReference);

    /**
     * Trouve l'enquête liée à la référence de constatation fournie.
     *
     * @param reportReference la référence de la constatation
     * @return l'enquête liée
     */
    Inquiry findByReportReference(String reportReference);

    /**
     * Trouve les enquêtes liées à la référence de dossier fournie.
     *
     * @param caseReference la référence du dossier
     * @return les enquêtes liées
     */
    List<Inquiry> findByCaseReference(String caseReference);

    /**
     * Indique si un résultat est un résultat d'enquête existant.
     *
     * @param result le résultat à vérifier
     * @return true si le résultat existe; false sinon
     */
    Boolean isLegalInquiryResult(String result);
}
