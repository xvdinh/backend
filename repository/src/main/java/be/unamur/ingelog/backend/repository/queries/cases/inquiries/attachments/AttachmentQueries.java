package be.unamur.ingelog.backend.repository.queries.cases.inquiries.attachments;

import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentDetailDTO;

import java.util.List;

public interface AttachmentQueries {
    /**
     * Trouve les pièces jointes associées avec la référence d'enquête fournie.
     *
     * @param inquiryReference la référence d'enquête
     * @return les pièces jointes associées
     */
    List<AttachmentDetailDTO> findByInquiryReference(String inquiryReference);

    /**
     * Indique si une pièce jointe existe pour la référence fournie.
     *
     * @param attachmentReference la référence de la pièce jointe
     * @return true si la pièce jointe existe; false sinon
     */
    Boolean existsByAttachmentReference(String attachmentReference);
}
