package be.unamur.ingelog.backend.repository.queries.cases.inquiries;

import be.unamur.ingelog.backend.dto.cases.inquiries.*;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static be.unamur.ingelog.backend.repository.MapperUtils.toLocalDate;
import static be.unamur.ingelog.backend.repository.MapperUtils.toLocalDateTime;
import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toInteger;

public final class InquiryRowMappers {
    private InquiryRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static class InquiryDetailMapper implements RowMapper<InquiryDetailDTO> {
        @Override
        public InquiryDetailDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquiryDetailDTO.builder()
                    .reference(result.getString("inquiry_reference"))
                    .caseReference(result.getString("case_reference"))
                    .employerReference(result.getString("employer_reference"))
                    .supervisorUsername(result.getString("supervisor"))
                    .assigneeUsername(result.getString("assignee"))
                    .state(result.getString("inquiry_state"))
                    .result(result.getString("inquiry_result"))
                    .openDate(toLocalDateTime(result.getTimestamp("open_date")))
                    .closeDate(toLocalDateTime(result.getTimestamp("close_date")))
                    .reportDate(toLocalDate(result.getDate("report_date")))
                    .street(result.getString("street"))
                    .postalCode(result.getString("postalcode"))
                    .locality(result.getString("locality"))
                    .deadline(toLocalDate(result.getDate("deadline")))
                    .priority(result.getString("priority"))
                    .build();
        }
    }

    public static class InquiryOverviewMapper implements RowMapper<InquiryOverviewDTO> {
        @Override
        public InquiryOverviewDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquiryOverviewDTO.builder()
                    .reference(result.getString("inquiry_reference"))
                    .state(result.getString("inquiry_state"))
                    .priority(result.getString("priority"))
                    .deadline(toLocalDate(result.getDate("deadline")))
                    .employerReference(result.getString("employer_reference"))
                    .employerName(result.getString("employer_name"))
                    .triggerDate(toLocalDate(result.getDate("trigger_date")))
                    .reportDate(toLocalDate(result.getDate("report_date")))
                    .closeDate(toLocalDateTime(result.getTimestamp("close_date")))
                    .assignee(result.getString("assignee"))
                    .supervisor(result.getString("supervisor"))
                    .result(result.getString("result"))
                    .build();
        }
    }

    public static class InquiryResultMapper implements RowMapper<InquiryResultDTO> {
        @Override
        public InquiryResultDTO mapRow(ResultSet result, int row) throws SQLException {
            return new InquiryResultDTO(result.getString("result_id"));
        }
    }

    public static class InquiryTargetExtractor implements ResultSetExtractor<List<InquiryTargetDTO>> {
        private final InquiryTargetMapper inquiryMapper = new InquiryTargetMapper();
        private final InquiryTargetReportMapper reportMapper = new InquiryTargetReportMapper();

        @Override
        public List<InquiryTargetDTO> extractData(ResultSet result) throws SQLException, DataAccessException {
            var inquiriesByInquiryReference = new HashMap<String, InquiryTargetDTO>();

            while (result.next()) {
                var inquiryReference = result.getString("inquiry_reference");
                var inquiry = inquiriesByInquiryReference.get(inquiryReference);

                if (inquiry == null) {
                    inquiry = inquiryMapper.mapRow(result, result.getRow());
                    inquiriesByInquiryReference.put(inquiryReference, inquiry);
                }

                var report = reportMapper.mapRow(result, result.getRow());
                assert inquiry != null;
                inquiry.addReport(report);
            }

            return new ArrayList<>(inquiriesByInquiryReference.values());
        }
    }

    public static class InquiryTargetMapper implements RowMapper<InquiryTargetDTO> {
        @Override
        public InquiryTargetDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquiryTargetDTO.builder()
                    .inquiryReference(result.getString("inquiry_reference"))
                    .reportDate(toLocalDate(result.getDate("report_date")))
                    .inquiryState(result.getString("inquiry_state"))
                    .inquiryResult(result.getString("inquiry_result"))
                    .build();
        }
    }

    public static class InquiryTargetReportMapper implements RowMapper<InquiryTargetReportDTO> {
        @Override
        public InquiryTargetReportDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquiryTargetReportDTO.builder()
                    .offenceLabel(result.getString("offence_label"))
                    .reportStatus(result.getString("report_status"))
                    .reportState(result.getString("report_state"))
                    .build();
        }
    }

    public static class InquirySummaryMapper implements RowMapper<InquirySummaryDTO> {
        @Override
        public InquirySummaryDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquirySummaryDTO.builder()
                    .inquiryReference(result.getString("inquiry_reference"))
                    .inquiryResult(result.getString("inquiry_result"))
                    .inquiryPriority(result.getString("inquiry_priority"))
                    .inquiryOpenDate(toLocalDateTime(result.getTimestamp("inquiry_open_date")))
                    .inquiryCloseDate(toLocalDateTime(result.getTimestamp("inquiry_close_date")))
                    .inquiryDeadline(toLocalDate(result.getDate("inquiry_deadline")))
                    .triggerDate(toLocalDate(result.getDate("trigger_date")))
                    .caseMotive(result.getString("case_motive"))
                    .caseOrigin(result.getString("case_origin"))
                    .reportDate(toLocalDate(result.getDate("report_date")))
                    .reportAddress(InquirySummaryReportAddressDTO.builder()
                            .street(result.getString("report_street"))
                            .locality(result.getString("report_locality"))
                            .postalCode(result.getString("report_postal_code"))
                            .build())
                    .inquirySupervisor(InquirySummarySupervisorDTO.builder()
                            .firstName(result.getString("supervisor_first_name"))
                            .lastName(result.getString("supervisor_last_name"))
                            .build())
                    .targetedEmployer(InquirySummaryEmployerDTO.builder()
                            .employerReference(result.getString("employer_reference"))
                            .employerName(result.getString("employer_name"))
                            .street(result.getString("employer_street"))
                            .locality(result.getString("employer_locality"))
                            .postalCode(result.getString("employer_postal_code"))
                            .country(result.getString("employer_country"))
                            .build())
                    .build();
        }
    }

    public static class InquirySummaryAttachmentMapper implements RowMapper<InquirySummaryAttachmentDTO> {
        @Override
        public InquirySummaryAttachmentDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquirySummaryAttachmentDTO.builder()
                    .documentType(result.getString("document_type"))
                    .source(result.getString("source"))
                    .receptionDate(toLocalDateTime(result.getTimestamp("reception_date")))
                    .build();
        }
    }

    public static class InquirySummaryCommentMapper implements RowMapper<InquirySummaryCommentDTO> {
        @Override
        public InquirySummaryCommentDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquirySummaryCommentDTO.builder()
                    .authorUsername(result.getString("author_username"))
                    .content(result.getString("content"))
                    .creationDate(toLocalDateTime(result.getTimestamp("creation_date")))
                    .build();
        }
    }

    public static class InquirySummaryLawArticleExtractor
            implements ResultSetExtractor<List<InquirySummaryLawArticleDTO>> {
        private final InquirySummaryLawArticleMapper lawArticleMapper = new InquirySummaryLawArticleMapper();
        private final InquirySummaryReportMapper reportMapper = new InquirySummaryReportMapper();
        private final InquirySummaryObservationMapper observationMapper = new InquirySummaryObservationMapper();

        @Override
        public List<InquirySummaryLawArticleDTO> extractData(ResultSet result)
                throws SQLException, DataAccessException {
            var lawArticlesByLabel = new HashMap<String, InquirySummaryLawArticleDTO>();
            var reportsByOffenceLabel = new HashMap<String, InquirySummaryReportDTO>();

            while (result.next()) {
                var lawArticleLabel = result.getString("law_article_label");
                var lawArticle = lawArticlesByLabel.get(lawArticleLabel);

                if (lawArticle == null) {
                    lawArticle = lawArticleMapper.mapRow(result, result.getRow());
                    lawArticlesByLabel.put(lawArticleLabel, lawArticle);
                }

                var offenceLabel = result.getString("offence_label");
                var report = reportsByOffenceLabel.get(offenceLabel);

                if (report == null) {
                    report = reportMapper.mapRow(result, result.getRow());
                    assert lawArticle != null;
                    lawArticle.addReport(report);
                    reportsByOffenceLabel.put(offenceLabel, report);
                }

                var observation = observationMapper.mapRow(result, result.getRow());
                assert report != null;
                report.addObservation(observation);

            }
            return new ArrayList<>(lawArticlesByLabel.values());
        }
    }

    public static class InquirySummaryLawArticleMapper implements RowMapper<InquirySummaryLawArticleDTO> {
        @Override
        public InquirySummaryLawArticleDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquirySummaryLawArticleDTO.builder()
                    .lawArticleLabel(result.getString("law_article_label"))
                    .build();
        }
    }

    public static class InquirySummaryReportMapper implements RowMapper<InquirySummaryReportDTO> {
        @Override
        public InquirySummaryReportDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquirySummaryReportDTO.builder()
                    .totalNumberOfControlledPersons(toInteger(result.getInt("controls_count")))
                    .offenceLabel(result.getString("offence_label"))
                    .reportStatus(result.getString("report_status"))
                    .build();
        }
    }

    public static class InquirySummaryObservationMapper implements RowMapper<InquirySummaryObservationDTO> {
        @Override
        public InquirySummaryObservationDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquirySummaryObservationDTO.builder()
                    .title(result.getString("observation_title"))
                    .value(result.getString("observation_value"))
                    .unit(result.getString("observation_unit"))
                    .description(result.getString("observation_description"))
                    .build();
        }
    }
}
