package be.unamur.ingelog.backend.repository.queries.cases.inquiries.reports;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportDetailDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportStatusUpdateDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.MapperUtils.toLocalDate;

public class ReportRowMappers {
    private ReportRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static class ReportDetailMapper implements RowMapper<ReportDetailDTO> {
        @Override
        public ReportDetailDTO mapRow(ResultSet result, int row) throws SQLException {
            return ReportDetailDTO.builder()
                    .reference(result.getString("report_reference"))
                    .inquiryReference(result.getString("inquiry_reference"))
                    .caseReference(result.getString("case_reference"))
                    .reportState(result.getString("report_state"))
                    .inquiryState(result.getString("inquiry_state"))
                    .status(result.getString("report_status"))
                    .offenceReference(result.getString("offence_reference"))
                    .offenceLabel(result.getString("offence_label"))
                    .offencePriority(result.getString("offence_priority"))
                    .offenceStartingDate(toLocalDate(result.getDate("starting_date")))
                    .offenceEndingDate(toLocalDate(result.getDate("ending_date")))
                    .lawArticleReference(result.getString("law_article_reference"))
                    .lawArticleLabel(result.getString("law_article_label"))
                    .lawArticleStartingDate(toLocalDate(result.getDate("law_article_starting_date")))
                    .lawArticleEndingDate(toLocalDate(result.getDate("law_article_ending_date")))
                    .deadline(toLocalDate(result.getDate("deadline")))
                    .totalOfControlledPersons(result.getInt("controls_count"))
                    .assigneeUsername(result.getString("assignee_username"))
                    .build();
        }
    }

    public static class ReportStatusMapper implements RowMapper<ReportStatusUpdateDTO> {
        @Override
        public ReportStatusUpdateDTO mapRow(ResultSet result, int row) throws SQLException {
            return new ReportStatusUpdateDTO(result.getString("status_id"));
        }
    }
}
