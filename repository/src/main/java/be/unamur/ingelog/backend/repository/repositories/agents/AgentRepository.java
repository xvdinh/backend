package be.unamur.ingelog.backend.repository.repositories.agents;

import be.unamur.ingelog.backend.domain.agents.Agent;

public interface AgentRepository {
    /**
     * Trouve un agent par son nom d'utilisateur.
     *
     * @param username le nom d'utilisateur
     * @return l'agent correspondant
     */
    Agent findByUsername(String username);
}
