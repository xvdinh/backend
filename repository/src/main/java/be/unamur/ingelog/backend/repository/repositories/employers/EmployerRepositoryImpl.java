package be.unamur.ingelog.backend.repository.repositories.employers;

import be.unamur.ingelog.backend.domain.employers.Employer;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.EmployerNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public class EmployerRepositoryImpl extends BaseRepository implements EmployerRepository {
    @Override
    public Employer findByEmployerReference(String employerReference) {
        return template.query(
                "select * from employers " +
                        "where employer_reference = :employerReference",
                parameters()
                        .addValue("employerReference", employerReference),
                new EmployerMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(EmployerNotFoundException::new);
    }
}
