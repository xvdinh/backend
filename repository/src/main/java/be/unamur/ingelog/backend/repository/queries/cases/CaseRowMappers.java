package be.unamur.ingelog.backend.repository.queries.cases;

import be.unamur.ingelog.backend.dto.cases.CaseDetailDTO;
import be.unamur.ingelog.backend.dto.cases.CaseMotiveDTO;
import be.unamur.ingelog.backend.dto.cases.CaseOriginDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.MapperUtils.toLocalDate;
import static be.unamur.ingelog.backend.repository.MapperUtils.toLocalDateTime;

public final class CaseRowMappers {
    private CaseRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class");
    }

    public static class CaseDetailMapper implements RowMapper<CaseDetailDTO> {
        @Override
        public CaseDetailDTO mapRow(ResultSet result, int row) throws SQLException {
            return CaseDetailDTO.builder()
                    .reference(result.getString("case_reference"))
                    .triggerDate(toLocalDate(result.getDate("trigger_date")))
                    .openDate(toLocalDateTime(result.getTimestamp("open_date")))
                    .closeDate(toLocalDateTime(result.getTimestamp("close_date")))
                    .motive(result.getString("motive"))
                    .origin(result.getString("origin"))
                    .state(result.getString("state"))
                    .description(result.getString("description"))
                    .build();
        }
    }

    public static class CaseMotiveMapper implements RowMapper<CaseMotiveDTO> {
        @Override
        public CaseMotiveDTO mapRow(ResultSet result, int row) throws SQLException {
            return new CaseMotiveDTO(result.getString("motive_id"));
        }
    }

    public static class CaseOriginMapper implements RowMapper<CaseOriginDTO> {
        @Override
        public CaseOriginDTO mapRow(ResultSet result, int row) throws SQLException {
            return CaseOriginDTO.builder()
                    .reference(result.getString("origin_id"))
                    .motiveReference(result.getString("motive_id"))
                    .build();
        }
    }
}
