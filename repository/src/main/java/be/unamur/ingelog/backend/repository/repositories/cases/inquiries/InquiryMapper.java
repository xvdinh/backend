package be.unamur.ingelog.backend.repository.repositories.cases.inquiries;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.cases.Case;
import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.domain.employers.Employer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.*;

class InquiryMapper implements RowMapper<Inquiry> {
    @Override
    public Inquiry mapRow(ResultSet result, int row) throws SQLException {
        return Inquiry.builder()
                .inquiryId(toInteger(result.getInt("inquiry_id")))
                .inquiryReference(result.getString("inquiry_reference"))
                .relatedCase(Case.builder()
                        .caseId(toInteger(result.getInt("case_id")))
                        .build())
                .targetedEmployer(Employer.builder()
                        .employerId(toInteger(result.getInt("employer_id")))
                        .build())
                .supervisor(Agent.builder()
                        .agentId(toInteger(result.getInt("supervisor")))
                        .build())
                .assignee(Agent.builder()
                        .agentId(toInteger(result.getInt("assignee")))
                        .build())
                .inquiryState(toInquiryState(result.getString("state")))
                .inquiryResult(toInquiryResult(result.getString("result")))
                .openDate(toLocalDateTime(result.getTimestamp("open_date")))
                .closeDate(toLocalDateTime(result.getTimestamp("close_date")))
                .reportDate(toLocalDate(result.getDate("report_date")))
                .street(result.getString("street"))
                .postalCode(result.getString("postalcode"))
                .locality(result.getString("locality"))
                .priority(result.getString("priority"))
                .legalDelay(toInteger(result.getInt("legal_delay")))
                .deadline(toLocalDate(result.getDate("deadline")))
                .build();
    }
}
