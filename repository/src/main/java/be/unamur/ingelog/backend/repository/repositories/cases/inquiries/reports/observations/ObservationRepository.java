package be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportObservation;

import java.util.List;

public interface ObservationRepository {
    /**
     * Crée une nouvelle observation dans la base de données.
     *
     * @param observation l'observation à créer
     */
    void create(ReportObservation observation);

    /**
     * Met à jour une observation existante dans la base de données.
     *
     * @param reportObservation l'observation à mettre à jour
     */
    void update(ReportObservation reportObservation);

    /**
     * Trouve une observation par son numéro et la référence de la constatation liée.
     *
     * @param reportReference   la référence de la constatation
     * @param observationNumber le numéro d'observation
     * @return l'observation correspondante
     */
    ReportObservation findByReportReferenceAndObservationNumber(String reportReference,
                                                                Integer observationNumber);

    /**
     * Trouve les observations liées à la référence de constatation fournie.
     *
     * @param reportReference la référence de la constatation
     * @return les observations liées
     */
    List<ReportObservation> findByReportReference(String reportReference);

    /**
     * Indique si une observation existe pour le numéro d'observation
     * et la référence de constatation fournis.
     *
     * @param observationNumber le numéro d'observation
     * @param reportReference   la référence de constatation
     * @return true si une observation a été trouvée pour ce numéro et
     * cette référence de constatation; false sinon
     */
    Boolean existsByNumberAndReportReference(int observationNumber, String reportReference);
}
