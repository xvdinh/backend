package be.unamur.ingelog.backend.repository.repositories.employers;

import be.unamur.ingelog.backend.domain.employers.Employer;

public interface EmployerRepository {
    /**
     * Trouve un employeur sur base de la référence fournie.
     *
     * @param employerReference la référence de l'employeur à trouver
     * @return l'employeur correspondant à la référence, s'il existe
     */
    Employer findByEmployerReference(String employerReference);
}
