package be.unamur.ingelog.backend.repository;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class MapperUtils {
    private MapperUtils() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static LocalDateTime toLocalDateTime(Timestamp source) {
        return source == null ? null : source.toLocalDateTime();
    }

    public static LocalDate toLocalDate(Date source) {
        return source == null ? null : source.toLocalDate();
    }

    public static Integer toInteger(int source) {
        return source == 0 ? null : source;
    }
}
