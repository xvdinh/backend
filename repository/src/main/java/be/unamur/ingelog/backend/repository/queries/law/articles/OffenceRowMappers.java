package be.unamur.ingelog.backend.repository.queries.law.articles;

import be.unamur.ingelog.backend.dto.law.articles.OffenceOverviewDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class OffenceRowMappers {
    private OffenceRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static class OffenceOverviewMapper implements RowMapper<OffenceOverviewDTO> {
        @Override
        public OffenceOverviewDTO mapRow(ResultSet result, int row) throws SQLException {
            return OffenceOverviewDTO.builder()
                    .offenceReference(result.getString("offence_reference"))
                    .offenceLabel(result.getString("offence_label"))
                    .build();
        }
    }
}
