package be.unamur.ingelog.backend.repository.queries.agents;

import be.unamur.ingelog.backend.dto.agents.AgentAuthDTO;
import be.unamur.ingelog.backend.dto.agents.AgentDetailDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class AgentRowMappers {
    private AgentRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static class AgentAuthMapper implements RowMapper<AgentAuthDTO> {
        @Override
        public AgentAuthDTO mapRow(ResultSet result, int row) throws SQLException {
            return AgentAuthDTO.builder()
                    .username(result.getString("username"))
                    .password(result.getString("password"))
                    .firstName(result.getString("first_name"))
                    .lastName(result.getString("last_name"))
                    .role(result.getString("role_id"))
                    .build();
        }
    }

    public static class AgentDetailMapper implements RowMapper<AgentDetailDTO> {
        @Override
        public AgentDetailDTO mapRow(ResultSet result, int row) throws SQLException {
            return AgentDetailDTO.builder()
                    .username(result.getString("username"))
                    .firstName(result.getString("first_name"))
                    .lastName(result.getString("last_name"))
                    .role(result.getString("role_id"))
                    .build();
        }
    }
}
