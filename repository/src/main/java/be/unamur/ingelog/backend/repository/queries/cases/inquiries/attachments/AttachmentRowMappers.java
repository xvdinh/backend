package be.unamur.ingelog.backend.repository.queries.cases.inquiries.attachments;

import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentDetailDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toLocalDateTime;

public class AttachmentRowMappers {
    private AttachmentRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utilty class)");
    }

    public static class AttachmentDetailMapper implements RowMapper<AttachmentDetailDTO> {
        @Override
        public AttachmentDetailDTO mapRow(ResultSet result, int row) throws SQLException {
            return AttachmentDetailDTO.builder()
                    .inquiryReference(result.getString("inquiry_reference"))
                    .attachmentReference(result.getString("attachment_reference"))
                    .originalName(result.getString("original_name"))
                    .documentType(result.getString("document_type"))
                    .source(result.getString("source"))
                    .receptionDate(toLocalDateTime(result.getTimestamp("reception_date")))
                    .build();
        }
    }
}
