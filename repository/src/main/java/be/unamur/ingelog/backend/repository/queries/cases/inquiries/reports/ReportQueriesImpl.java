package be.unamur.ingelog.backend.repository.queries.cases.inquiries.reports;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportDetailDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportStatusUpdateDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.ReportNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReportQueriesImpl extends BaseRepository implements ReportQueries {
    private static final String BASE_QUERY =
            "select report_reference, " +
                    "r.state as report_state, " +
                    "status as report_status, " +
                    "r.deadline, " +
                    "controls_count, " +
                    "inquiry_reference, " +
                    "i.state as inquiry_state, " +
                    "offence_reference, " +
                    "o.label as offence_label, " +
                    "o.priority as offence_priority, " +
                    "o.starting_date, " +
                    "o.ending_date, " +
                    "law_article_reference, " +
                    "la.label as law_article_label, " +
                    "la.starting_date as law_article_starting_date, " +
                    "la.ending_date as law_article_ending_date, " +
                    "a.username as assignee_username, " +
                    "case_reference " +
                    "from reports r " +
                    "left join inquiries i on i.inquiry_id = r.inquiry_id " +
                    "left join agents a on a.agent_id = i.assignee " +
                    "left join offences o on o.offence_id = r.offence_id " +
                    "left join law_articles la on o.law_article_id = la.law_article_id " +
                    "left join cases c on i.case_id = c.case_id";

    @Override
    public List<ReportDetailDTO> findByInquiryReference(String inquiryReference) {
        return template.query(
                BASE_QUERY + " where r.inquiry_id in " +
                        "(select inquiry_id from inquiries where inquiry_reference = :inquiryReference)",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new ReportRowMappers.ReportDetailMapper());
    }

    @Override
    public ReportDetailDTO findByReportReference(String reportReference) {
        return template.query(
                BASE_QUERY + " where report_reference = :reference",
                parameters()
                        .addValue("reference", reportReference),
                new ReportRowMappers.ReportDetailMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(ReportNotFoundException::new);
    }

    @Override
    public List<ReportDetailDTO> findAllReports() {
        return template.query(
                BASE_QUERY,
                new ReportRowMappers.ReportDetailMapper());
    }

    @Override
    public Boolean existsByReportReference(String reportReference) {
        return template.queryForObject(
                "select exists (select * from reports where report_reference = :reference)",
                parameters()
                        .addValue("reference", reportReference),
                Boolean.class);
    }

    @Override
    public List<ReportStatusUpdateDTO> findAllStatus() {
        return template.query(
                "select status_id from report_status",
                new ReportRowMappers.ReportStatusMapper());
    }
}
