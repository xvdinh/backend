package be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.Report;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.ReportNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toStringFromEnum;

@Repository
public class ReportRepositoryImpl extends BaseRepository implements ReportRepository {
    @Override
    public void create(Report report) {
        template.update(
                "insert into reports(report_id, report_reference, inquiry_id, state, offence_id, deadline) " +
                        "values(nextval('reports_seq'), :reportReference, :inquiryId, :state, :offenceId, " +
                        ":deadline)",
                parameters()
                        .addValue("reportReference", report.getReportReference())
                        .addValue("inquiryId", report.getRelatedInquiry().getInquiryId())
                        .addValue("state", toStringFromEnum(report.getReportState()))
                        .addValue("offenceId", report.getOffence().getOffenceId())
                        .addValue("deadline", report.getDeadline())
        );
    }

    @Override
    public void update(Report report) {
        template.update(
                "update reports set " +
                        "state = :state, " +
                        "status = :status, " +
                        "deadline = :deadline, " +
                        "controls_count = :controlsCount " +
                        "where report_id = :reportId",
                parameters()
                        .addValue("state", toStringFromEnum(report.getReportState()))
                        .addValue("status", toStringFromEnum(report.getReportStatus()))
                        .addValue("deadline", report.getDeadline())
                        .addValue("controlsCount", report.getTotalOfControlledPersons())
                        .addValue("reportId", report.getReportId())
        );
    }

    @Override
    public Report findByReportReference(String reportReference) {
        return template.query(
                "select * from reports " +
                        "where report_reference = :reportReference",
                parameters()
                        .addValue("reportReference", reportReference),
                new ReportMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(ReportNotFoundException::new);
    }

    @Override
    public List<Report> findByInquiryReference(String inquiryReference) {
        return template.query(
                "select * from reports " +
                        "where inquiry_id in " +
                        "(select inquiry_id from inquiries where inquiry_reference = :inquiryReference)",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new ReportMapper()
        );
    }
}
