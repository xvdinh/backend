package be.unamur.ingelog.backend.repository.queries.statistics;

import be.unamur.ingelog.backend.dto.statistics.CollectionOfInquiriesDTO;
import be.unamur.ingelog.backend.dto.statistics.CollectionOfResultsDTO;
import be.unamur.ingelog.backend.dto.statistics.InspectionInquiryDTO;
import be.unamur.ingelog.backend.dto.statistics.ResultDTO;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

@Repository
public class StatisticQueriesImpl extends BaseRepository implements StatisticQueries {
    @Override
    public CollectionOfInquiriesDTO findInquiriesData(Map<String, String> filter) {
        var result = new CollectionOfInquiriesDTO()
                .items(findInspectionInquiries(filter));

        for (var inquiry : result.getItems()) {
            String inquiryReference = inquiry.getReference();

            inquiry.setInquiryResults(new CollectionOfResultsDTO()
                    .items(findInquiryResults(inquiryReference)));
        }

        return result;
    }

    private List<InspectionInquiryDTO> findInspectionInquiries(Map<String, String> filter) {
        return template.query(
                "select inquiry_reference, i.open_date, i.close_date, name as legal_name, country_code, priority, " +
                        "result as global_result, case_reference, i.postalcode, legal_delay " +
                        "from inquiries i " +
                        "left join employers e on e.employer_id = i.employer_id " +
                        "left join cases c on c.case_id = i.case_id " + getSQLFilters(filter),
                parameters()
                        .addValue("closedAfter", filter.get("closedAfter"))
                        .addValue("closedBefore", filter.get("closedBefore")),
                new StatisticRowMappers.InspectionInquiryMapper());
    }

    private List<ResultDTO> findInquiryResults(String inquiryReference) {
        return template.query(
                "select status as report_status, controls_count, law_article_reference, offence_reference from reports r " +
                        "left join offences o on o.offence_id = r.offence_id " +
                        "left join law_articles la on o.law_article_id = la.law_article_id " +
                        "where r.inquiry_id in " +
                        "(select inquiry_id from inquiries where inquiry_reference = :inquiryReference)",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new StatisticRowMappers.ResultMapper());
    }

    /**
     * Fourni les filtres éventuels de la requête.
     *
     * @param filter les filtres éventuels à appliquer
     * @return les filtres éventuels sous forme de requête sql
     */
    private String getSQLFilters(Map<String, String> filter) {
        var sqlFilters = new StringJoiner(" \nand ", " \nwhere ", "").setEmptyValue("");

        sqlFilters.add("i.state = 'closed'");

        if (filter.get("closedAfter") != null) sqlFilters.add("i.close_date > :closedAfter::date");

        if (filter.get("closedBefore") != null) sqlFilters.add("i.close_date < :closedBefore::date");

        return sqlFilters.toString();
    }
}
