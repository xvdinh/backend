package be.unamur.ingelog.backend.repository.queries.employers;

import be.unamur.ingelog.backend.dto.employers.EmployerDetailDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class EmployerRowMappers {
    private EmployerRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static class EmmployerDetailMapper implements RowMapper<EmployerDetailDTO> {
        @Override
        public EmployerDetailDTO mapRow(ResultSet result, int row) throws SQLException {
            return EmployerDetailDTO.builder()
                    .reference(result.getString("employer_reference"))
                    .name(result.getString("name"))
                    .street(result.getString("street"))
                    .postalCode(result.getString("postalcode"))
                    .locality(result.getString("locality"))
                    .country(result.getString("country"))
                    .build();
        }
    }
}
