package be.unamur.ingelog.backend.repository.repositories.law.articles;

import be.unamur.ingelog.backend.domain.law.articles.Offence;

public interface OffenceRepository {
    /**
     * Trouve une infraction par sa référence.
     *
     * @param offenceReference la référence de l'infraction
     * @return l'infraction correspondante
     */
    Offence findByOffenceReference(String offenceReference);
}
