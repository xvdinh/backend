package be.unamur.ingelog.backend.repository.repositories.law.articles;

import be.unamur.ingelog.backend.domain.law.articles.Offence;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.OffenceNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public class OffenceRepositoryImpl extends BaseRepository implements OffenceRepository {
    @Override
    public Offence findByOffenceReference(String offenceReference) {
        return template.query(
                "select * from offences " +
                        "where offence_reference = :offenceReference",
                parameters()
                        .addValue("offenceReference", offenceReference),
                new OffenceMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(OffenceNotFoundException::new);
    }
}
