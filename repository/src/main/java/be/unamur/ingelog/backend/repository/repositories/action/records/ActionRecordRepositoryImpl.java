package be.unamur.ingelog.backend.repository.repositories.action.records;

import be.unamur.ingelog.backend.domain.action.records.ActionRecord;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Arrays;

@Repository
public class ActionRecordRepositoryImpl extends BaseRepository implements ActionRecordRepository {
    @Override
    public void create(ActionRecord actionRecord) {
        template.update(
                "insert into action_records (action_record_id, method_arguments, method_name, username, " +
                        "action_timestamp, response) " +
                        "values (nextval('action_records_seq'), :methodArguments, :methodName, :username, " +
                        ":actionTimestamp, :response)",
                parameters()
                        .addValue("methodArguments", Arrays.toString(actionRecord.getMethodArguments()))
                        .addValue("methodName", actionRecord.getMethodName())
                        .addValue("username", actionRecord.getUsername())
                        .addValue("actionTimestamp", actionRecord.getActionTimestamp())
                        .addValue("response", actionRecord.getResponse())
        );
    }
}
