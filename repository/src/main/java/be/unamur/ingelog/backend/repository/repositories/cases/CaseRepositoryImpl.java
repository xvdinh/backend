package be.unamur.ingelog.backend.repository.repositories.cases;

import be.unamur.ingelog.backend.domain.cases.Case;
import be.unamur.ingelog.backend.domain.cases.CaseMotive;
import be.unamur.ingelog.backend.domain.cases.CaseOrigin;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.CaseNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toStringFromEnum;

@Repository
public class CaseRepositoryImpl extends BaseRepository implements CaseRepository {
    @Override
    public void create(Case caseToCreate) {
        template.update(
                "insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, " +
                        "origin, state, description) " +
                        "values(nextval('cases_seq'), :caseReference, :creatorId, :triggerDate, :openDate, :closeDate, " +
                        ":motive, :origin, :state, :description)",
                parameters()
                        .addValue("caseReference", caseToCreate.getCaseReference())
                        .addValue("creatorId", caseToCreate.getCreator().getAgentId())
                        .addValue("triggerDate", caseToCreate.getTriggerDate())
                        .addValue("openDate", caseToCreate.getOpenDate())
                        .addValue("closeDate", caseToCreate.getCloseDate())
                        .addValue("motive", caseToCreate.getCaseMotive().toString())
                        .addValue("origin", caseToCreate.getCaseOrigin())
                        .addValue("state", caseToCreate.getCaseState().toString())
                        .addValue("description", caseToCreate.getDescription())
        );
    }

    @Override
    public void update(Case caseUpdate) {
        template.update(
                "update cases set " +
                        "close_date = :closeDate, " +
                        "state = :state " +
                        "where case_id = :caseId",
                parameters()
                        .addValue("closeDate", caseUpdate.getCloseDate())
                        .addValue("state", toStringFromEnum(caseUpdate.getCaseState()))
                        .addValue("caseId", caseUpdate.getCaseId())
        );
    }

    @Override
    public Case findByCaseReference(String caseReference) {
        return template.query(
                "select * from cases " +
                        "where case_reference = :caseReference",
                parameters()
                        .addValue("caseReference", caseReference),
                new CaseMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(CaseNotFoundException::new);
    }

    @Override
    public Case findByInquiryReference(String inquiryReference) {
        return template.query(
                "select * from cases " +
                        "where case_id in " +
                        "(select case_id from inquiries where inquiry_reference = :inquiryReference)",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new CaseMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(CaseNotFoundException::new);
    }

    @Override
    public List<CaseOrigin> findOriginsForMotive(CaseMotive motive) {
        return template.query(
                "select origin_id as origin from case_origins " +
                        "where motive_id = :motive",
                parameters()
                        .addValue("motive", motive.toString()),
                new CaseOriginMapper());
    }
}
