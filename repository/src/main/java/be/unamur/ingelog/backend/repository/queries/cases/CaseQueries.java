package be.unamur.ingelog.backend.repository.queries.cases;

import be.unamur.ingelog.backend.dto.cases.CaseDetailDTO;
import be.unamur.ingelog.backend.dto.cases.CaseMotiveDTO;
import be.unamur.ingelog.backend.dto.cases.CaseOriginDTO;

import java.util.List;

public interface CaseQueries {
    /**
     * Trouve tous les dossiers.
     *
     * @return la liste de tous les dossiers
     */
    List<CaseDetailDTO> findAllCases();

    /**
     * Trouve un dossier sur base de sa référence.
     *
     * @param caseReference la référence du dossier à trouver
     * @return un Optional du dossier correspondant à la référence, s'il existe;
     * un Optional vide sinon
     */
    CaseDetailDTO findByCaseReference(String caseReference);

    /**
     * Indique si un dossier existe pour la référence fournie.
     *
     * @param caseReference la référence à vérifier
     * @return true si un dossier a été trouvé pour cette référence; false sinon
     */
    Boolean existsByCaseReference(String caseReference);

    /**
     * Retourne la liste des motifs possibles pour un dossier.
     *
     * @return la liste des motifs possibles
     */
    List<CaseMotiveDTO> findAllMotives();

    /**
     * Indique si le motif fourni existe.
     *
     * @param motive le motif à vérifier
     * @return true si le motif a été trouvé; false sinon
     */
    Boolean existingMotive(String motive);

    /**
     * Retourne la liste de toutes les origines disponibles.
     *
     * @return la liste de toutes les origines
     */
    List<CaseOriginDTO> findAllOrigins();

    /**
     * Retourne la liste des origines permises pour le motif fourni.
     *
     * @param motive le motif
     * @return la liste des origines
     */
    List<CaseOriginDTO> findOriginsForMotive(String motive);
}
