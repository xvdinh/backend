package be.unamur.ingelog.backend.repository.repositories.cases.inquiries.attachments;

import be.unamur.ingelog.backend.domain.cases.inquiries.attachments.Attachment;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.AttachmentNotFoundException;

import java.util.List;

public interface AttachmentRepository {
    /**
     * Crée une nouvelle pièce jointe dans la base de données.
     *
     * @param attachment la pièce jointe à créer
     */
    void create(Attachment attachment);

    /**
     * Trouve les pièces jointes liées à l'enquête spécifiée.
     *
     * @param inquiryReference la référence de l'enquête
     * @return la liste des pièces jointes liées
     */
    List<Attachment> findByInquiryReference(String inquiryReference);

    /**
     * Trouve une pièce jointe par sa référence.
     *
     * @param attachmentReference la référence de la pièce jointe
     * @return la pièce jointe
     * @throws AttachmentNotFoundException si aucune pièce n'a été trouvée
     */
    Attachment findByAttachmentReference(String attachmentReference) throws AttachmentNotFoundException;
}
