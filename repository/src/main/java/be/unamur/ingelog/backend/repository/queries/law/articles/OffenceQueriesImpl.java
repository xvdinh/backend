package be.unamur.ingelog.backend.repository.queries.law.articles;

import be.unamur.ingelog.backend.dto.law.articles.OffenceOverviewDTO;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

@Repository
public class OffenceQueriesImpl extends BaseRepository implements OffenceQueries {
    @Override
    public List<OffenceOverviewDTO> findAll(Map<String, String> filter) {
        return template.query(
                "select offence_reference, label as offence_label from offences " +
                        getSQLFilters(filter),
                parameters()
                        .addValue("targetDate", filter.get("activeAt")),
                new OffenceRowMappers.OffenceOverviewMapper());
    }

    /**
     * Fourni les filtres éventuels de la requête.
     *
     * @param filter les filtres éventuels à appliquer
     * @return les filtres éventuels sous forme de requête sql
     */
    private String getSQLFilters(Map<String, String> filter) {
        StringJoiner sqlFilters = new StringJoiner(" \nand ", " \nwhere ", "").setEmptyValue("");

        if (filter.get("activeAt") != null) {
            sqlFilters.add("starting_date <= :targetDate::date");
            sqlFilters.add("(ending_date is null or ending_date >= :targetDate::date)");
        }

        return sqlFilters.toString();
    }

    @Override
    public Boolean existsByReference(String reference) {
        return template.queryForObject(
                "select exists (select * from offences where offence_reference = :reference)",
                parameters()
                        .addValue("reference", reference),
                Boolean.class);
    }
}
