package be.unamur.ingelog.backend.repository.queries.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationDetailDTO;

import java.util.List;

public interface ObservationQueries {
    /**
     * Trouve les observations liées à la référence de constatation fournie.
     *
     * @param reportReference la référence de la constatation pour laquelle trouver les observations liées
     * @return la liste des observations liées à la constatation
     */
    List<ObservationDetailDTO> findByReportReference(String reportReference);
}
