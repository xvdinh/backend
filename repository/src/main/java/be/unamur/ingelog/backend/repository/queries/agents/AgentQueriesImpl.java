package be.unamur.ingelog.backend.repository.queries.agents;

import be.unamur.ingelog.backend.dto.agents.AgentAuthDTO;
import be.unamur.ingelog.backend.dto.agents.AgentDetailDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.AgentNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AgentQueriesImpl extends BaseRepository implements AgentQueries {
    @Override
    public List<AgentDetailDTO> findAll() {
        return template.query(
                "select * from agents",
                new AgentRowMappers.AgentDetailMapper()
        );
    }

    @Override
    public AgentAuthDTO authenticate(String username) {
        return template.query(
                "select * from agents " +
                        "where username = :username",
                parameters()
                        .addValue("username", username),
                new AgentRowMappers.AgentAuthMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(() -> new BadCredentialsException("Wrong username or password"));
    }

    @Override
    public AgentDetailDTO findByUsername(String username) {
        return template.query(
                "select * from agents " +
                        "where username = :username",
                parameters()
                        .addValue("username", username),
                new AgentRowMappers.AgentDetailMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(AgentNotFoundException::new);
    }

    @Override
    public Boolean existsByUsername(String username) {
        return template.queryForObject(
                "select exists (select * from agents where username = :username)",
                parameters()
                        .addValue("username", username),
                Boolean.class);
    }
}
