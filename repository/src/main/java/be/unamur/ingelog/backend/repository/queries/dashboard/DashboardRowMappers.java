package be.unamur.ingelog.backend.repository.queries.dashboard;

import be.unamur.ingelog.backend.dto.dashboard.*;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Classe utilitaire contenant les mappers servant à l'extraction des DTOs liés au dashboard.
 */
public class DashboardRowMappers {
    private DashboardRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    /**
     * Mapper pour l'extraction d'un TotalInquiriesByStateDTO.
     */
    public static class TotalInquiriesByStateMapper implements RowMapper<TotalInquiriesByStateDTO> {
        @Override
        public TotalInquiriesByStateDTO mapRow(ResultSet result, int row) throws SQLException {
            return TotalInquiriesByStateDTO.builder()
                    .inquiryState(result.getString("inquiry_state"))
                    .totalInquiries(result.getInt("total_inquiries"))
                    .build();
        }
    }

    /**
     * Mapper pour l'extraction d'un TotalInquiriesByPriorityDTO.
     */
    public static class TotalInquiriesByPriorityMapper implements RowMapper<TotalInquiriesByPriorityDTO> {
        @Override
        public TotalInquiriesByPriorityDTO mapRow(ResultSet result, int row) throws SQLException {
            return TotalInquiriesByPriorityDTO.builder()
                    .priority(result.getString("inquiry_priority"))
                    .totalInquiries(result.getInt("total_inquiries"))
                    .build();
        }
    }

    /**
     * Mapper pour l'extraction d'un InquiriesDistributionByResultDTO.
     */
    public static class InquiriesDistributionByResultMapper implements RowMapper<InquiriesDistributionByResultDTO> {
        @Override
        public InquiriesDistributionByResultDTO mapRow(ResultSet result, int row) throws SQLException {
            return InquiriesDistributionByResultDTO.builder()
                    .inquiryResult(result.getString("inquiry_result"))
                    .inquiriesDistribution(result.getFloat("inquiries_distribution"))
                    .build();
        }
    }

    /**
     * Mapper pour l'extraction d'un TotalDismissedInquiriesByYearDTO.
     */
    public static class TotalClosedInquiriesByYearMapper implements RowMapper<TotalClosedInquiriesByYearDTO> {
        @Override
        public TotalClosedInquiriesByYearDTO mapRow(ResultSet result, int row) throws SQLException {
            return TotalClosedInquiriesByYearDTO.builder()
                    .year(result.getString("year"))
                    .totalDismissedInquiries(result.getInt("total_dismissed_inquiries"))
                    .totalClosedPositiveInquiries(result.getInt("total_closed_positive_inquiries"))
                    .totalClosedNegativeInquiries(result.getInt("total_closed_negative_inquiries"))
                    .build();
        }
    }

    /**
     * Mapper pour l'extraction d'un TotalInquiriesByAgentDTO.
     */
    public static class TotalInquiriesByAgentMapper implements RowMapper<TotalInquiriesByAgentDTO> {
        @Override
        public TotalInquiriesByAgentDTO mapRow(ResultSet result, int row) throws SQLException {
            return TotalInquiriesByAgentDTO.builder()
                    .username(result.getString("username"))
                    .totalInquiries(result.getInt("total_inquiries"))
                    .build();
        }
    }
}
