package be.unamur.ingelog.backend.repository.repositories.notifications;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.notifications.Notification;

import java.util.List;

public interface NotificationRepository {
    /**
     * Crée une nouvelle notification dans la base de données.
     *
     * @param notification la notification à créer
     */
    void create(Notification notification);

    /**
     * Renvoie la liste des destinataires sur base d'une référence de dossier.
     *
     * @param caseReference la référence du dossier
     * @return la liste des destinataires
     */
    List<Agent> findRecipientsByCaseReference(String caseReference);

    /**
     * Trouve une notification sur base de la référence fournie.
     *
     * @param notificationReference la référence de la notification
     * @return la notification correspondante
     */
    Notification findByNotificationReference(String notificationReference);

    /**
     * Indique qu'une notification a été consultée.
     *
     * @param notificationReference la référence de la notification
     */
    void markAsConsulted(String notificationReference);
}
