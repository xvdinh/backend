package be.unamur.ingelog.backend.repository.queries.cases.comments;

import be.unamur.ingelog.backend.dto.cases.comments.CommentDetailDTO;

import java.util.List;

public interface CommentQueries {
    /**
     * Trouve les commentaires liés à la référence dossier fournie.
     *
     * @param caseReference la référence du dossier pour lequel trouver les commentaires liés
     * @return la liste des commentaires liés au dossier
     */
    List<CommentDetailDTO> findByCaseReference(String caseReference);
}
