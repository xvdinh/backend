package be.unamur.ingelog.backend.repository.repositories.cases.inquiries.attachments;

import be.unamur.ingelog.backend.domain.cases.inquiries.attachments.Attachment;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.AttachmentNotFoundException;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AttachmentRepositoryImpl extends BaseRepository implements AttachmentRepository {
    @Override
    public void create(Attachment attachment) {
        template.update(
                "insert into attachments (attachment_id, attachment_reference, original_name, inquiry_id, document_type, " +
                        "reception_date, source, path) " +
                        "values (nextval('attachments_seq'), :attachmentReference, :originalName, :inquiryId, :documentType, " +
                        ":receptionDate, :source, :path)",
                parameters()
                        .addValue("inquiryId", attachment.getRelatedInquiry().getInquiryId())
                        .addValue("attachmentReference", attachment.getAttachmentReference())
                        .addValue("originalName", attachment.getOriginalName())
                        .addValue("documentType", attachment.getDocumentType())
                        .addValue("receptionDate", attachment.getReceptionDate())
                        .addValue("source", attachment.getSource())
                        .addValue("path", attachment.getPath())
        );
    }

    @Override
    public List<Attachment> findByInquiryReference(String inquiryReference) {
        return template.query(
                "select * from attachments " +
                        "where inquiry_id in " +
                        "(select inquiry_id from inquiries where inquiry_reference = :inquiryReference)",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new AttachmentMapper()
        );
    }

    @Override
    public Attachment findByAttachmentReference(String attachmentReference)
            throws AttachmentNotFoundException {
        return template.query(
                "select * from attachments " +
                        "where attachment_reference = :attachmentReference",
                parameters()
                        .addValue("attachmentReference", attachmentReference),
                new AttachmentMapper()
        )
                .stream()
                .findFirst()
                .orElseThrow(AttachmentNotFoundException::new);
    }
}
