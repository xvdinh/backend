package be.unamur.ingelog.backend.repository.queries.notifications;

import be.unamur.ingelog.backend.dto.notifications.NotificationDetailDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toLocalDateTime;

public class NotificationRowMappers {
    private NotificationRowMappers() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static class NotificationDetailMapper implements RowMapper<NotificationDetailDTO> {
        @Override
        public NotificationDetailDTO mapRow(ResultSet result, int row) throws SQLException {
            return NotificationDetailDTO.builder()
                    .notificationReference(result.getString("notification_reference"))
                    .consulted(result.getBoolean("is_read"))
                    .authorUsername(result.getString("author_username"))
                    .type(result.getString("type"))
                    .resourceType(result.getString("resource_type"))
                    .resourceReference(result.getString("resource_reference"))
                    .notificationTimestamp(toLocalDateTime(result.getTimestamp("notification_timestamp")))
                    .build();
        }
    }
}
