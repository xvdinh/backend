package be.unamur.ingelog.backend.repository.repositories.agents;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.agents.AgentRole;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.MapperUtils.toInteger;

public class AgentMapper implements RowMapper<Agent> {
    @Override
    public Agent mapRow(ResultSet result, int row) throws SQLException {
        return Agent.builder()
                .agentId(toInteger(result.getInt("agent_id")))
                .username(result.getString("username"))
                .password(result.getString("password"))
                .firstName(result.getString("first_name"))
                .lastName(result.getString("last_name"))
                .agentRole(toAgentRole(result.getString("role_id")))
                .build();
    }

    private AgentRole toAgentRole(String source) {
        return source == null ? null : AgentRole.valueOf(source);
    }
}
