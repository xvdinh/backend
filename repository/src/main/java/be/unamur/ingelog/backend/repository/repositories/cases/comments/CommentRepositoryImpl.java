package be.unamur.ingelog.backend.repository.repositories.cases.comments;

import be.unamur.ingelog.backend.domain.cases.CaseComment;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CommentRepositoryImpl extends BaseRepository implements CommentRepository {
    @Override
    public void create(CaseComment comment) {
        template.update(
                "insert into case_comments(comment_id, content, author, case_id, creation_date) " +
                        "values (nextval('case_comments_seq'), :content, :authorId, :caseId, :creationDate)",
                parameters()
                        .addValue("content", comment.getContent())
                        .addValue("authorId", comment.getAuthor().getAgentId())
                        .addValue("caseId", comment.getRelatedCase().getCaseId())
                        .addValue("creationDate", comment.getCreationDate())
        );
    }
}
