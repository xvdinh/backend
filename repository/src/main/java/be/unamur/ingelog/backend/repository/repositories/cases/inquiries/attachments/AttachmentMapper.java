package be.unamur.ingelog.backend.repository.repositories.cases.inquiries.attachments;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.domain.cases.inquiries.attachments.Attachment;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toInteger;
import static be.unamur.ingelog.backend.repository.repositories.MapperUtils.toLocalDateTime;

public class AttachmentMapper implements RowMapper<Attachment> {
    @Override
    public Attachment mapRow(ResultSet result, int row) throws SQLException {
        return Attachment.builder()
                .attachmentId(toInteger(result.getInt("attachment_id")))
                .attachmentReference(result.getString("attachment_reference"))
                .originalName(result.getString("original_name"))
                .relatedInquiry(Inquiry.builder()
                        .inquiryId(toInteger(result.getInt("inquiry_id")))
                        .build())
                .receptionDate(toLocalDateTime(result.getTimestamp("reception_date")))
                .documentType(result.getString("document_type"))
                .path(result.getString("path"))
                .source(result.getString("source"))
                .build();
    }
}
