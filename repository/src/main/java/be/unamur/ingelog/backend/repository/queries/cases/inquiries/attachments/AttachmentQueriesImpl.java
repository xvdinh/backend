package be.unamur.ingelog.backend.repository.queries.cases.inquiries.attachments;

import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentDetailDTO;
import be.unamur.ingelog.backend.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AttachmentQueriesImpl extends BaseRepository implements AttachmentQueries {
    @Override
    public List<AttachmentDetailDTO> findByInquiryReference(String inquiryReference) {
        return template.query(
                "select inquiry_reference, " +
                        "attachment_reference, " +
                        "original_name, " +
                        "document_type, " +
                        "source, " +
                        "reception_date " +
                        "from attachments a " +
                        "left join inquiries i on i.inquiry_id = a.inquiry_id " +
                        "where a.inquiry_id in " +
                        "(select inquiry_id from inquiries where inquiry_reference = :inquiryReference)",
                parameters()
                        .addValue("inquiryReference", inquiryReference),
                new AttachmentRowMappers.AttachmentDetailMapper()
        );
    }

    @Override
    public Boolean existsByAttachmentReference(String attachmentReference) {
        return template.queryForObject(
                "select exists (select * from attachments where attachment_reference = :attachmentReference)",
                parameters()
                        .addValue("attachmentReference", attachmentReference),
                Boolean.class
        );
    }
}
