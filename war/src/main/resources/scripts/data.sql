-- remplissage des valeurs prédéfinies de la table 'roles'
insert into agent_roles(role_id)
values ('INSPECTOR');
insert into agent_roles(role_id)
values ('CHIEF_INSPECTOR');
insert into agent_roles(role_id)
values ('AGENT_ADM');
insert into agent_roles(role_id)
values ('DIRECTOR_GENERAL');


-- remplissage des valeurs prédéfinies de la table 'case_states'
insert into case_states(state_id)
values ('initiated');
insert into case_states(state_id)
values ('in_progress');
insert into case_states(state_id)
values ('closed');


-- remplissage des valeurs prédéfinies de la table 'case_motives'
insert into case_motives(motive_id)
values ('COMPLAINT');
insert into case_motives(motive_id)
values ('DENUNCIATION');
insert into case_motives(motive_id)
values ('INVESTIGATION_REQUEST');
insert into case_motives(motive_id)
values ('REGULARIZATION_CONTROL');


-- remplissage des valeurs prédéfinies de la table 'case_origins'
insert into case_origins(origin_id, motive_id)
values ('ANONYMOUS', 'DENUNCIATION');
insert into case_origins(origin_id, motive_id)
values ('INDIVIDUAL', 'DENUNCIATION');
insert into case_origins(origin_id, motive_id)
values ('EMPLOYEE', 'DENUNCIATION');
insert into case_origins(origin_id, motive_id)
values ('EMPLOYER', 'DENUNCIATION');
insert into case_origins(origin_id, motive_id)
values ('POLICE', 'INVESTIGATION_REQUEST');
insert into case_origins(origin_id, motive_id)
values ('SPF', 'INVESTIGATION_REQUEST');
insert into case_origins(origin_id, motive_id)
values ('INSPECTION_SERVICES', 'INVESTIGATION_REQUEST');
insert into case_origins(origin_id, motive_id)
values ('ONSS', 'INVESTIGATION_REQUEST');


-- remplissage des valeurs prédéfinies de la table 'offence_priorities'
insert into offence_priorities(offence_priority_id)
values ('1');
insert into offence_priorities(offence_priority_id)
values ('2');
insert into offence_priorities(offence_priority_id)
values ('3');
insert into offence_priorities(offence_priority_id)
values ('4');
insert into offence_priorities(offence_priority_id)
values ('5');


-- remplissage des valeurs prédéfinies de la table 'report_states'
insert into report_states(state_id)
values ('created');
insert into report_states(state_id)
values ('encoded');
insert into report_states(state_id)
values ('evaluated');
insert into report_states(state_id)
values ('closed');


-- remplissage des valeurs prédéfinies de la table 'report_status'
insert into report_status(status_id)
values ('pending');
insert into report_status(status_id)
values ('not_applicable');
insert into report_status(status_id)
values ('not_controlled');
insert into report_status(status_id)
values ('positive');
insert into report_status(status_id)
values ('negative');


-- remplissage des valeurs prédéfinies de la table 'inquiry_states'
insert into inquiry_states(state_id)
values ('open');
insert into inquiry_states(state_id)
values ('pending_validation');
insert into inquiry_states(state_id)
values ('incomplete');
insert into inquiry_states(state_id)
values ('approved');
insert into inquiry_states(state_id)
values ('assigned');
insert into inquiry_states(state_id)
values ('completed');
insert into inquiry_states(state_id)
values ('pending_closure');
insert into inquiry_states(state_id)
values ('closed');
insert into inquiry_states(state_id)
values ('dismissed');
insert into inquiry_states(state_id)
values ('archived');


-- remplissage des valeurs prédéfinies de la table 'inquiry_results'
insert into inquiry_results(result_id)
values ('positive');
insert into inquiry_results(result_id)
values ('negative');



insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSA', '$2a$10$prDlq85qrAv.SAzJqph9e.dXhkkd3K9Swr6KSCVuIxDlhz2pTFNh6', 'Alexandre',
        'Fortin', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSB', '$2a$10$jaHKYVpxZoR71GUCe/vEV.utMdwK62w86mtaZyzTSjm71DbcPpUoK', 'Alizé',
        'Paquette', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSC', '$2a$10$RwCN5s5NInMYdC.TRIPrVOJCszFhezg4Fp5EdOEbm4vTnPpXgQDvW', 'Jacques',
        'Tousignant', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSD', '$2a$10$b2aOvSYuXsQvgfYYxkBMF.QrDp59PDxfmy8PsXP3lnSu4/DGvTrk.', 'Océane',
        'Girouard', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSE', '$2a$10$ez9gHYwEe6BfLocQYeU94.EQMHw7IPAAJHfTMkivTlY70aFcXB3Ja', 'Yves',
        'Quesnel', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSF', '$2a$10$f3/OVzV6i8a76WqB7CYpu.EAQoM9mU7WG1ZiJAD174qzI19kyDIUa', 'Sylvie',
        'Taylor', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSG', '$2a$10$rBSTxOnhcqG9s3pF58hMYupRcWUADdNdeOph4moFiQuCoOkgyXV/K', 'Gaspard',
        'Diaz', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSH', '$2a$10$xKZVHl.WHk1TE1fWRHG29O67J0e8Z68tNNJ3DEjyKofNFpeBHLIV.', 'Constance',
        'Gilbert', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CINA', '$2a$10$UGzNLXYC4h/aKAREehgWF.KvvJGHOge7czMEX.Qt9z90l8mYFs8Mq', 'Xavier',
        'Cloutier', 'CHIEF_INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CINB', '$2a$10$PeXCV7yPAIZjUzDzZfAWF.CI3EQ/nCFx0iphNuTy5XnMZyySYUoF6', 'Laurence',
        'Langlois', 'CHIEF_INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CINC', '$2a$10$wj6OjBWF8aAlOLqahYtMyeDkgDGymhAfmqbQciZqIMV54qmTbwabO', 'Georges',
        'Larouche', 'CHIEF_INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CIND', '$2a$10$myDw5UJ0YIUB9pKKuUjaxe9zbVV.Mi0SbSZmi5h4u/uQCrfCmVA1q', 'Arlette',
        'Moreau', 'CHIEF_INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'ADMA', '$2a$10$0k4lo8nglG3ca49oLwQoFuNu3/UJqmZt93J6h4BDXc1gx9Zt2mv/O', 'Nathalie',
        'Desrochers', 'AGENT_ADM');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'ADMB', '$2a$10$Ek1ABHf9qxhE0ZA8nHMAW.vNKweh89WqoAtTIKlMGwkxozlTKUGcu', 'Victor',
        'Collin', 'AGENT_ADM');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'DIRA', '$2a$10$ZXqf0NSi4eqQ3yGVtXJQF.yE68.7vchJ7GxRwbKoXX6Is0OYC4tHm', 'Ariane',
        'Brown', 'DIRECTOR_GENERAL');



insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-R2D2L', 'JMS Construct SPRL', '55 rue de la Loi', '1000', 'Bruxelles',
        'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-ROB78', 'Shady Business SA', '37 boulevard de l''Arnaque', '5000', 'Namur',
        'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-A2D2L', 'Great Construct', '200 rue des Amandiers', '1030', 'Schaerbeek',
        'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-AOB78', 'Alliance et fils', '1 rue de l''Arbre-Tout-Seul', '1060',
        'Saint-Gilles', 'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-R9D2L', 'ETC Filsol', '89 rue Ernest Boucquéau', '1082', 'Berchem-Sainte-Agathe',
        'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-R6B78', 'BESIX', '37 boulevard de l''Arnaque', '1340',
        'Ottignies-Louvain-la-Neuve',
        'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-R2T2L', 'Vieille-Montagne', '46 rue du Chemin de fer', '1370', 'Dongelberg',
        'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-ROP78', 'Englebert', '190 rue du Chemin de fer', '5000', 'Namur',
        'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-R2DLL', 'VERHAEREN en CO', '829 boulevard des Droits de l''Homme', '3510',
        'Kermt',
        'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-ROB38', 'EULER HERMES', '35 rue de la Fontaine de Spa', '4000', 'Liège',
        'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-R2D2Q', 'Cockerill-Sambre', '6 rue des Hortensias', '4400', 'Flémalle',
        'Belgique', 'BE');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country, country_code)
values (nextval('employers_seq'), 'EMP-ROB71', 'I.NA.CO - N.IM.BO', '32 rue de la Liberté', '6042', 'Charleroi',
        'Belgique', 'BE');



insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG001', 'Code pénal social', '2005-03-24');
insert into law_articles(law_article_id, law_article_reference, label, starting_date, ending_date)
values (nextval('law_articles_seq'), 'RG002001', 'Marchand de sommeil (CP art. 433 decies)', '2005-03-24',
        '2014-12-31');
insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG002002', 'Avantage patrimonial (CP art. 43)', '2005-03-24');
insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG002003', 'TEH - Exploitation économique (CP art.433 quinquies §1er 3°)',
        '2015-01-01');
insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG002004', 'TEH - Exploitation sexuelle (CP art. 433 quinquies §1er 1°)',
        '2015-01-01');
insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG002005', 'TEH - Autres formes (CP art. 433 quinquies)', '2015-01-01');
insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG012', 'Autres (volontaire, ...)', '2015-01-01');



insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG001001', 1, 'Faux / Usage de faux / Escroquerie aux cotisations 232-235 CPS',
        '2005-03-24', 30, '1');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG001002', 1, 'Non paiement de cotisations sociales', '2005-03-24', 60, '5');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG001003', 1, 'Obstacle à la surveillance', '2005-03-24', '2014-12-31', 60, '4');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG001004', 1, 'Surveillance / Moyens de défense', '2005-03-24', '2014-12-31', 60,
        '4');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG002001001', 2, 'Marchand de sommeil (CP art. 433 decies)', '2005-03-24',
        '2014-12-31', 45, '2');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG002002001', 3, 'Avantage patrimonial salaires', '2005-03-24', '2010-06-30', null,
        '5');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG002002002', 3, 'Avantage patrimonial cotisations', '2015-01-01', null, '5');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG002003001', 4, 'TEH - Exploitation économique (CP art. 433 quinquies §1er 3°)',
        '2015-01-01', '2015-12-31', 45, '2');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG002004001', 5, 'TEH - Exploitation sexuelle (CP art. 433 quinquies §1er 1°)',
        '2015-01-01', 30, '1');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG002005001', 6, 'Mendicité (CP art. 433 quinquies §1er 2°)', '2015-01-01', 60, '5');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG002005002', 6, 'Trafic d''organes (CP art. 433 quinquies §1er 4°)', '2015-01-01',
        '2017-06-30', 30, '1');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG012001', 7, 'Constaté la présence d''un volontaire (ou autre)', '2015-01-01', null,
        '3');


------- Pour l'année courante
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2016-DOS-QLPKZ', 10, '2016-03-12', '2016-05-16 00:05:24', '2016-05-16 08:05:24',
        'COMPLAINT', '2016-PLA-XO29Q', 'closed', 'Engagement de sans-papiers');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2016-DOS-PGII2', 10, '2016-01-15', '2016-05-16 00:08:48', '2016-05-16 08:05:24',
        'INVESTIGATION_REQUEST', 'INSPECTION_SERVICES', 'closed', 'Exploitation de mineurs');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2016-DOS-XI28V', 10, '2016-04-28', '2016-05-16 00:12:27', '2016-05-16 08:05:24',
        'INVESTIGATION_REQUEST', 'ONSS', 'closed', 'Abus transfrontaliers');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2016-DOS-CLM56', 10, '2016-02-12', '2016-05-16 00:20:34', '2016-05-16 08:05:24',
        'INVESTIGATION_REQUEST', 'POLICE', 'closed', 'Allocations perçues illégalement');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2016-DOS-CNPFC', 10, '2016-03-11', '2016-05-16 00:24:06', '2016-05-16 08:05:24',
        'INVESTIGATION_REQUEST', 'SPF', 'closed', 'Non-paiement des cotisations sociales');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2016-DOS-V1CT8', 10, '2016-04-16', '2016-05-16 00:27:28', '2016-05-16 08:05:24',
        'DENUNCIATION', 'ANONYMOUS', 'closed', 'Délation d''une suspicion d''avantage patrimonial cotisations');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2016-DOS-1KK6Y', 10, '2016-03-05', '2016-05-16 00:31:53', '2016-05-16 08:05:24',
        'DENUNCIATION', 'INDIVIDUAL', 'closed', 'Exploitation economique');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2017-DOS-7UOSQ', 10, '2017-03-12', '2017-05-16 00:05:24', '2017-05-16 04:08:48',
        'COMPLAINT', '2017-PLA-XONDQ', 'closed', 'Engagement de sans-papiers');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2017-DOS-I3TM6', 10, '2017-01-15', '2017-05-16 00:08:48', '2017-05-16 04:08:48',
        'INVESTIGATION_REQUEST', 'INSPECTION_SERVICES', 'closed', 'Exploitation de mineurs');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2018-DOS-JYP7V', 10, '2018-03-12', '2018-05-16 00:05:24', '2018-05-16 03:31:53',
        'COMPLAINT', '2018-PLA-XSNDQ', 'closed', 'Engagement de sans-papiers');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2019-DOS-S9Q8O', 10, '2019-03-12', '2019-05-16 00:05:24', '2019-05-16 13:05:24',
        'COMPLAINT', '2019-PLA-AXNDQ', 'closed', 'Engagement de sans-papiers');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2019-DOS-LFAM0', 10, '2019-01-15', '2019-05-16 00:08:48', '2019-05-16 13:05:24',
        'INVESTIGATION_REQUEST', 'INSPECTION_SERVICES', 'closed', 'Exploitation de mineurs');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-G77A5', 10, '2020-02-27', '2020-05-17 08:54:09', null, 'DENUNCIATION',
        'ANONYMOUS', 'initiated', 'Délation concernant une maison close potentielle non déclarée ');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-VIKTY', 10, '2020-03-03', '2020-05-17 08:50:38', null, 'REGULARIZATION_CONTROL',
        '2019-ENQ-RQVGH', 'in_progress', 'Récidive de fraude sur un chantier de Charleroi');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-R9VTS', 9, '2020-04-28', '2020-05-18 10:08:44', '2020-05-18 10:12:03',
        'INVESTIGATION_REQUEST', 'SPF', 'closed',
        'Incohérences dans la déclaration fiscale d''un chantier pour la construction d''une tour de bureaux');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-J9DCD', 5, '2020-01-15', '2020-05-18 10:44:33', null, 'INVESTIGATION_REQUEST',
        'ONSS', 'in_progress', 'Incohérences au niveau des chiffres renvoyés');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-K5L26', 9, '2020-04-28', '2020-05-18 10:14:41', '2020-05-18 10:18:44',
        'DENUNCIATION', 'ANONYMOUS', 'closed',
        'Dénonciation concernant un restaurant chinois sur le boulevard d''Avroy à Liège');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-566XO', 13, '2020-03-07', '2020-04-09 00:00:00', null, 'DENUNCIATION',
        'INDIVIDUAL', 'in_progress',
        'Dénonciation déposée pour suspicion de mendicité organisée par un tier. 3 personnes concernée; une avenue de l''Astronomie; deux rue de la Bienfaisance à 7190 Écaussinnes');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-WDVQ7', 10, '2020-04-14', '2020-05-17 08:40:30', null, 'DENUNCIATION',
        'ANONYMOUS', 'in_progress', 'Traite d''êtres humains');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-LO9XD', 12, '2020-05-06', '2020-05-18 10:30:07', null, 'INVESTIGATION_REQUEST',
        'INSPECTION_SERVICES', 'in_progress',
        'Problèmes au niveau du centre de Mons, plus spécifiquement dans la Tour d''Ivoire');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-ZYGYC', 10, '2020-02-13', '2020-05-17 08:45:33', null, 'INVESTIGATION_REQUEST',
        'ONSS', 'in_progress', 'ONSS a remaqué des données douteuses concernant un chantier sur Bruxelles');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-6ZNJH', 10, '2020-04-29', '2020-05-18 10:52:54', null, 'INVESTIGATION_REQUEST',
        'SPF', 'in_progress', 'Suspicion de squat au batiment rue de la loi');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-3MEWZ', 9, '2020-04-29', '2020-05-18 10:35:39', null, 'INVESTIGATION_REQUEST',
        'POLICE', 'in_progress', 'Demande de vérification concernant la Tour des Finances');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-LDMU0', 9, '2020-05-13', '2020-05-18 11:05:00', '2020-05-18 11:07:07',
        'INVESTIGATION_REQUEST', 'SPF', 'closed', 'Incohérence des données recues et celles calculées');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2020-DOS-TJZ3W', 9, '2020-05-06', '2020-05-18 11:36:18', null, 'INVESTIGATION_REQUEST',
        'SPF', 'in_progress', 'Problèmes à la Tour Paradis');
INSERT INTO cases (case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                   description)
VALUES (nextval('cases_seq'), '2014-DOS-BIUBK', 9, '2014-05-06', '2014-05-18 11:36:18', null, 'INVESTIGATION_REQUEST',
        'SPF', 'in_progress', 'Problèmes à la Tour Montparnasse');


--2014
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2014-ENQ-BKBB3', 26, 3, null, null, 'pending_validation', null,
        '2014-05-18 11:37:13', null, null, 'Rue du paradis 89', '4000', 'LIege', '4', null, '2014-07-29');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2014-ENQ-A0H9H', 26, 4, null, null, 'pending_validation', null,
        '2014-05-18 11:37:58', null, null, 'Rue du paradis 89', '4000', 'Liege', '4', null, '2014-07-29');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2014-ENQ-G01GV', 26, 12, null, null, 'pending_validation', null,
        '2014-05-18 11:38:28', null, null, 'Rue du paradis 89', '4000', 'Liege', '2', null, '2014-07-29');


--2016
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-I15GC', 1, 8, 10, 1, 'archived', 'negative', '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2017-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-511MC', 1, 10, 10, 4, 'archived', 'positive', '2016-05-16 00:41:21',
        '2016-05-16 00:41:21', '2016-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '3', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-202O9', 3, 5, 10, 1, 'archived', 'negative', '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '3', null, '2017-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-QN0GE', 2, 8, 10, 1, 'archived', 'negative', '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2017-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-2GJJE', 4, 9, 10, 4, 'archived', 'positive', '2016-05-16 00:41:21',
        '2016-05-16 00:41:21', '2016-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '3', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-EIKMU', 3, 8, 10, 1, 'archived', 'negative', '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '3', null, '2017-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-K0RQS', 6, 8, 10, 1, 'archived', 'negative', '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2017-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-NIGPB', 7, 10, 10, 4, 'dismissed', null, '2016-05-16 00:41:21',
        '2016-05-16 00:41:21', '2016-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '3', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-XHG5K', 5, 8, 10, 1, 'archived', 'negative', '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '3', null, '2017-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-14A8T', 3, 2, 10, 1, 'dismissed', null, '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2017-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-RQVGH', 2, 4, 10, 4, 'archived', 'positive', '2016-05-16 00:41:21',
        '2016-05-16 00:41:21', '2016-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '3', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-9X39B', 1, 8, 10, 1, 'dismissed', null, '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '3', null, '2017-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-H9QE3', 4, 8, 10, 1, 'archived', 'negative', '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2008-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-H5JIX', 3, 6, 10, 4, 'archived', 'positive', '2016-05-16 00:41:21',
        '2016-05-16 00:41:21', '2016-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '3', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-QASSC', 2, 8, 10, 1, 'archived', 'negative', '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '3', null, '2017-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-HD9Y2', 4, 8, 10, 1, 'archived', 'negative', '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2017-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-67PMY', 3, 10, 10, 4, 'archived', 'positive', '2016-05-16 00:41:21',
        '2016-05-16 00:41:21', '2016-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2016-ENQ-9BYZF', 7, 8, 10, 1, 'archived', 'negative', '2016-05-14 00:39:09',
        '2016-05-16 00:39:09', '2016-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2017-10-15');


--2017
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-I15GC', 8, 4, 10, 1, 'archived', 'negative', '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2018-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-511MC', 8, 3, 10, 4, 'dismissed', null, '2017-05-16 00:41:21',
        '2017-05-16 00:41:21', '2017-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-202O9', 8, 5, 10, 1, 'archived', 'negative', '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-QN0GE', 9, 8, 10, 1, 'archived', 'negative', '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2018-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-2GJJE', 9, 3, 10, 4, 'archived', 'negative', '2017-05-16 00:41:21',
        '2017-05-16 00:41:21', '2017-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-EIKMU', 8, 8, 10, 1, 'archived', 'negative', '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-K0RQS', 8, 8, 10, 1, 'archived', 'negative', '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2018-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-NIGPB', 9, 10, 10, 4, 'dismissed', null, '2017-05-16 00:41:21',
        '2017-05-16 00:41:21', '2017-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-XHG5K', 9, 1, 10, 1, 'archived', 'negative', '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-14A8T', 9, 8, 10, 1, 'dismissed', null, '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2018-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-RQVGH', 9, 10, 10, 4, 'archived', 'positive', '2017-05-16 00:41:21',
        '2017-05-16 00:41:21', '2017-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-9X39B', 9, 8, 10, 1, 'dismissed', null, '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-H9QE3', 9, 8, 10, 1, 'archived', 'negative', '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2018-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-H5JIX', 9, 1, 10, 4, 'archived', 'positive', '2017-05-16 00:41:21',
        '2017-05-16 00:41:21', '2017-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-QASSC', 8, 8, 10, 1, 'archived', 'negative', '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-HD9Y2', 8, 3, 10, 1, 'dismissed', null, '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2018-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-67PMY', 8, 10, 10, 4, 'archived', 'positive', '2017-05-16 00:41:21',
        '2017-05-16 00:41:21', '2017-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '5', null, '2017-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2017-ENQ-9BYZF', 8, 5, 10, 1, 'archived', 'negative', '2017-05-14 00:39:09',
        '2017-05-16 00:39:09', '2017-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '5', null, '2019-10-15');


--2018
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-I15GC', 10, 8, 10, 1, 'archived', 'negative', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '5', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-511MC', 10, 1, 10, 4, 'archived', 'positive', '2018-05-16 00:41:21',
        '2018-05-16 00:41:21', '2018-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '3', null, '2018-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-202O9', 10, 8, 10, 1, 'archived', 'negative', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '3', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-QN0GE', 10, 8, 10, 1, 'archived', 'negative', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-2GJJE', 10, 3, 10, 4, 'archived', 'negative', '2018-05-16 00:41:21',
        '2018-05-16 00:41:21', '2018-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '3', null, '2018-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-EIKMU', 10, 8, 10, 1, 'archived', 'negative', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '3', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-K0RQS', 10, 3, 10, 1, 'archived', 'negative', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-NIGPB', 10, 10, 10, 4, 'archived', 'positive', '2018-05-16 00:41:21',
        '2018-05-16 00:41:21', '2018-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '3', null, '2018-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-XHG5K', 10, 8, 10, 1, 'archived', 'negative', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '3', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-14A8T', 10, 2, 10, 1, 'dismissed', null, '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-RQVGH', 10, 4, 10, 4, 'archived', 'positive', '2018-05-16 00:41:21',
        '2018-05-16 00:41:21', '2018-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '3', null, '2018-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-9X39B', 10, 6, 10, 1, 'archived', 'positive', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '3', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-H9QE3', 10, 8, 10, 1, 'archived', 'negative', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-H5JIX', 10, 7, 10, 4, 'archived', 'positive', '2018-05-16 00:41:21',
        '2018-05-16 00:41:21', '2018-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '3', null, '2018-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-QASSC', 10, 8, 10, 1, 'archived', 'negative', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '3', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-HD9Y2', 10, 2, 10, 1, 'archived', 'positive', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2019-10-15');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-67PMY', 10, 3, 10, 4, 'archived', 'positive', '2018-05-16 00:41:21',
        '2018-05-16 00:41:21', '2018-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2018-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2018-ENQ-9BYZF', 10, 1, 10, 1, 'archived', 'negative', '2018-05-14 00:39:09',
        '2018-05-16 00:39:09', '2018-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-10-15');


--2019
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-I15GC', 12, 8, 10, 1, 'archived', 'positive', '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '3', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-511MC', 11, 1, 10, 4, 'archived', 'positive', '2019-05-16 00:41:21',
        '2019-05-16 00:41:21', '2019-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2019-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-202O9', 11, 8, 10, 1, 'archived', 'positive', '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-QN0GE', 12, 8, 10, 1, 'dismissed', null, '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-2GJJE', 11, 3, 10, 4, 'archived', 'positive', '2019-05-16 00:41:21',
        '2019-05-16 00:41:21', '2019-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2019-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-EIKMU', 11, 8, 10, 1, 'archived', 'positive', '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-K0RQS', 12, 3, 10, 1, 'archived', 'positive', '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-NIGPB', 11, 10, 10, 4, 'archived', 'positive', '2019-05-16 00:41:21',
        '2019-05-16 00:41:21', '2019-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2019-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-XHG5K', 11, 8, 10, 1, 'archived', 'positive', '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-14A8T', 12, 2, 10, 1, 'dismissed', null, '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-RQVGH', 11, 4, 10, 4, 'archived', 'positive', '2019-05-16 00:41:21',
        '2019-05-16 00:41:21', '2019-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2019-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-9X39B', 11, 6, 10, 1, 'archived', 'positive', '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-H9QE3', 12, 8, 10, 1, 'archived', 'negative', '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-H5JIX', 11, 7, 10, 4, 'archived', 'positive', '2019-05-16 00:41:21',
        '2019-05-16 00:41:21', '2019-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '1', null, '2019-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-QASSC', 11, 8, 10, 1, 'dismissed', null, '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '1', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-HD9Y2', 12, 2, 10, 1, 'archived', 'positive', '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Rue de Bolsée 89', '1000', 'Bruxelles', '1', null, '2019-12-31');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-67PMY', 11, 3, 10, 4, 'archived', 'positive', '2019-05-16 00:41:21',
        '2019-05-16 00:41:21', '2019-05-16 02:41:21', 'Rue Grandgagnage 80', '5000', 'Namur', '5', null, '2019-05-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2019-ENQ-9BYZF', 12, 1, 10, 1, 'archived', 'negative', '2019-05-14 00:39:09',
        '2019-05-16 00:39:09', '2019-05-15 00:39:09', 'Place Henri Trou 90', '8000', 'Bruges', '5', null, '2019-12-31');


-- 2020
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-BNJF8', 23, 12, 9, null, 'dismissed', null, '2020-05-18 10:39:54',
        '2020-05-18 10:40:14', null, 'Boulevard Roi Albert II 33', '1030', 'Schaerbeek', '5', null, '2020-07-22');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-KLCOX', 21, 5, 9, 12, 'assigned', null, '2020-05-18 10:51:45', null, null,
        'Rue du tapis 09', '5738', 'Oubeek', '5', null, '2020-05-07');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-QIYZX', 19, 2, 11, 5, 'completed', null, '2020-05-18 10:27:30', null,
        '2020-09-10', 'Rue JJ Merlot 87', '4430', 'Ans', '3', null, null);
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-P5HU4', 15, 5, 9, 1, 'closed', 'negative', '2020-05-18 10:09:00',
        '2020-05-18 10:12:03', '2020-05-20', 'Rue des Guillemins 123', '4000', 'Liege', '5', null, '2020-07-21');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-0XSSI', 24, 3, 9, 1, 'archived', 'positive', '2020-05-18 11:05:11',
        '2020-05-18 11:07:07', '2020-05-28', 'Rue du four 98', '4000', 'Liege', '5', null, null);
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-5NX6Y', 16, 6, 9, 10, 'assigned', null, '2020-05-18 10:44:44', null, null,
        'Rue du XX aout 98', '4000', 'Liege', '5', null, null);
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-ETOJB', 20, 1, 12, 8, 'assigned', null, '2020-05-18 10:30:23', null, null,
        'Tour d''Ivoire 78', '3500', 'Mons', '5', null, '2020-07-29');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-PE5MZ', 17, 5, 9, 1, 'closed', 'negative', '2020-05-18 10:15:05',
        '2020-05-18 10:18:44', '2020-05-18', 'Boulevard d''Avroy 389', '4000', 'Liege', '1', null, '2020-06-09');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-FQ6XP', 20, 12, 12, 8, 'assigned', null, '2020-05-18 10:31:20', null, null,
        'Tour d''Ivoire 78', '3500', 'Mons', '5', null, '2020-07-29');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-FN8XY', 21, 4, 9, 1, 'assigned', null, '2020-05-18 10:46:57', null, null,
        'Rue du nid 98', '1000', 'Bruxelles', '1', null, '2020-03-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-RYDET', 20, 3, null, null, 'incomplete', null, '2020-05-18 10:32:07', null,
        null, 'Tour d''Ivoire 78', '3500', 'Mons', '5', null, '2020-07-29');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-FGVIC', 18, 6, 9, 3, 'pending_closure', 'positive', '2020-05-18 10:19:50',
        null, '2020-05-19', 'Rue du Coq 21', '3000', 'Hasselt', '5', null, '2020-05-29');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-P017S', 20, 4, null, null, 'incomplete', null, '2020-05-18 10:33:00', null,
        null, 'Tour d''Ivoire 78', '3500', 'Mons', '1', null, '2020-06-17');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-ELCLW', 18, 11, 9, 3, 'pending_closure', 'positive', '2020-05-18 10:22:07',
        null, '2020-05-28', 'Rue de Jemeppe 87', '4078', 'Seraing', '5', null, '2020-05-29');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-S5CKF', 23, 3, 9, null, 'approved', null, '2020-05-18 10:36:10', null, null,
        'Boulevard Roi Albert II 33', '1030', 'Schaerbeek', '5', null, '2020-07-22');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-YU9ER', 21, 2, 9, 7, 'assigned', null, '2020-05-18 10:48:43', null, null,
        'Rue du fou 98', '2000', 'Raeren', '1', null, '2020-03-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-A1P0V', 21, 3, 11, 6, 'completed', null, '2020-05-18 10:24:25', null,
        '2020-06-11', 'Place Hector Denis 2', '4430', 'Ans', '1', null, '2020-03-26');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-ZB916', 22, 8, 10, 9, 'assigned', null, '2020-05-18 10:54:11', null, null,
        'Rue du drame 98', '4000', 'Liege', '1', null, '2020-06-10');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-71ZXK', 23, 2, 9, null, 'approved', null, '2020-05-18 10:38:56', null, null,
        'Boulevard Roi Albert II 33', '1030', 'Schaerbeek', '1', null, '2020-06-10');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-ZIBE0', 23, 1, 9, null, 'dismissed', null, '2020-05-18 10:38:07',
        '2020-05-18 10:39:40', null, 'Boulevard Roi Albert II 33', '1030', 'Schaerbeek', '5', null, null);
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-8Y3YB', 21, 4, 9, 4, 'assigned', null, '2020-05-18 10:49:58', null, null,
        'Rue du tiroir 87', '8000', 'Bruges', '3', null, null);
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-1VG6E', 21, 1, 9, 2, 'assigned', null, '2020-05-18 10:50:39', null, null,
        'Rue du plot 73', '6000', 'Anvers', '5', null, '2020-05-07');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-BKBB3', 25, 3, null, null, 'pending_validation', null,
        '2020-05-18 11:37:13', null, null, 'Rue du paradis 89', '4000', 'LIege', '5', null, '2020-07-29');
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-A0H9H', 25, 4, null, null, 'pending_validation', null,
        '2020-05-18 11:37:58', null, null, 'Rue du paradis 89', '4000', 'Liege', '5', null, null);
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-G01GV', 25, 12, null, null, 'pending_validation', null,
        '2020-05-18 11:38:28', null, null, 'Rue du paradis 89', '4000', 'Liege', '3', null, null);
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-43CBD', 25, 4, null, null, 'open', null, '2020-05-18 11:38:59', null, null,
        'Rue du paradis 89', '4000', 'Liege', null, null, null);
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-UT18C', 25, 3, null, null, 'open', null, '2020-05-18 11:39:10', null, null,
        'Rue du paradis 89', '4000', 'Liege', null, null, null);
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-JGKU5', 25, 12, null, null, 'open', null, '2020-05-18 11:39:24', null, null,
        'Rue du paradis 89', '4000', 'Liege', null, null, null);
INSERT INTO inquiries (inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                       open_date, close_date, report_date, street, postalcode, locality, priority, legal_delay,
                       deadline)
VALUES (nextval('inquiries_seq'), '2020-ENQ-9O9N4', 25, 2, null, null, 'open', null, '2020-05-18 11:39:36', null, null,
        'Rue du paradis', '4000', 'Liege', null, null, null);


-- 2014
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2014-CON-EG0EI', 1, 'created', null, 3, '2020-07-29', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2014-CON-7UQZN', 2, 'created', null, 3, null, null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2014-CON-VYUJI', 3, 'created', null, 5, null, null);


--2016
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W51', 4, 'closed', 'not_applicable', 2, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W52', 5, 'closed', 'positive', 2, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W53', 6, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W54', 7, 'closed', 'not_applicable', 2, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W55', 8, 'closed', 'positive', 2, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W56', 9, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W57', 10, 'closed', 'not_applicable', 2, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W58', 11, 'closed', 'positive', 2, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W59', 12, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W60', 13, 'closed', 'not_applicable', 2, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W62', 14, 'closed', 'positive', 2, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W63', 15, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W64', 16, 'closed', 'not_applicable', 2, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W65', 17, 'closed', 'positive', 2, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W66', 18, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W67', 19, 'closed', 'not_applicable', 2, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W68', 20, 'closed', 'positive', 1, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W69', 21, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W70', 4, 'closed', 'not_applicable', 12, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W72', 5, 'closed', 'positive', 12, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W73', 6, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W74', 7, 'closed', 'not_applicable', 12, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W75', 8, 'closed', 'positive', 12, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W76', 9, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W77', 10, 'closed', 'not_applicable', 12, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W78', 11, 'closed', 'positive', 12, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W79', 12, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W80', 13, 'closed', 'not_applicable', 12, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W81', 14, 'closed', 'positive', 12, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W82', 15, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W83', 16, 'closed', 'not_applicable', 12, '2017-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-P0W84', 17, 'closed', 'positive', 12, '2017-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-R0W85', 18, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2016-CON-50W86', 19, 'closed', 'not_applicable', 12, '2017-10-15', 3);


--2017
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W51', 22, 'closed', 'not_applicable', 1, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W52', 23, 'closed', 'positive', 1, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W53', 24, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W54', 25, 'closed', 'not_applicable', 1, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W55', 26, 'closed', 'positive', 1, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W56', 27, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W57', 28, 'closed', 'not_applicable', 1, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W58', 29, 'closed', 'positive', 1, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W59', 30, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W60', 31, 'closed', 'not_applicable', 1, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W62', 32, 'closed', 'positive', 1, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W63', 33, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W64', 34, 'closed', 'not_applicable', 1, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W65', 35, 'closed', 'positive', 1, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W66', 36, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W67', 37, 'closed', 'not_applicable', 1, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W68', 38, 'closed', 'positive', 2, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W69', 39, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W70', 22, 'closed', 'not_applicable', 12, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W72', 23, 'closed', 'positive', 12, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W73', 24, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W74', 25, 'closed', 'not_applicable', 12, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W75', 26, 'closed', 'positive', 12, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W76', 27, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W77', 28, 'closed', 'not_applicable', 12, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W78', 29, 'closed', 'positive', 12, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W79', 30, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W80', 31, 'closed', 'not_applicable', 12, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W81', 32, 'closed', 'positive', 12, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W82', 33, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W83', 34, 'closed', 'not_applicable', 12, '2018-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-P0W84', 35, 'closed', 'positive', 12, '2018-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-R0W85', 36, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2017-CON-50W86', 37, 'closed', 'not_applicable', 12, '2018-10-15', 3);


--2018
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W51', 40, 'closed', 'not_applicable', 2, '2019-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W52', 41, 'closed', 'positive', 2, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W53', 42, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W54', 43, 'closed', 'not_applicable', 2, '2019-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W55', 44, 'closed', 'positive', 2, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W56', 45, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W57', 46, 'closed', 'not_applicable', 2, '2019-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W58', 47, 'closed', 'positive', 2, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W59', 48, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W60', 49, 'closed', 'not_applicable', 2, '2019-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W62', 50, 'closed', 'positive', 2, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W63', 51, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W64', 52, 'closed', 'not_applicable', 2, '2019-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W65', 53, 'closed', 'positive', 2, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W66', 54, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W67', 55, 'closed', 'not_applicable', 2, '2019-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W68', 56, 'closed', 'positive', 1, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W69', 57, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W72', 41, 'closed', 'positive', 12, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W73', 42, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W74', 43, 'closed', 'not_applicable', 12, '2019-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W75', 44, 'closed', 'positive', 12, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W76', 45, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W77', 46, 'closed', 'not_applicable', 12, '2019-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W78', 47, 'closed', 'positive', 12, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W79', 48, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W80', 49, 'closed', 'not_applicable', 12, '2019-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W81', 50, 'closed', 'positive', 12, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W82', 51, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W83', 52, 'closed', 'not_applicable', 12, '2019-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-P0W84', 53, 'closed', 'positive', 12, '2019-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-R0W85', 54, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2018-CON-50W86', 55, 'closed', 'not_applicable', 12, '2019-10-15', 3);


--2019
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W52', 59, 'closed', 'positive', 1, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W53', 60, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W54', 61, 'closed', 'not_applicable', 1, '2020-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W55', 62, 'closed', 'positive', 1, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W56', 63, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W57', 64, 'closed', 'not_applicable', 1, '2020-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W58', 65, 'closed', 'positive', 1, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W59', 66, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W60', 67, 'closed', 'not_applicable', 1, '2020-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W62', 68, 'closed', 'positive', 1, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W63', 69, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W64', 70, 'closed', 'not_applicable', 1, '2020-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W65', 71, 'closed', 'positive', 1, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W66', 72, 'closed', 'not_controlled', 1, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W67', 73, 'closed', 'not_applicable', 1, '2020-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W68', 74, 'closed', 'positive', 2, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W69', 75, 'closed', 'not_controlled', 2, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W70', 58, 'closed', 'not_applicable', 12, '2020-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W72', 59, 'closed', 'positive', 12, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W73', 60, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W74', 61, 'closed', 'not_applicable', 12, '2020-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W75', 62, 'closed', 'positive', 12, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W76', 63, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W77', 64, 'closed', 'not_applicable', 12, '2020-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W78', 65, 'closed', 'positive', 12, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W79', 66, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W80', 67, 'closed', 'not_applicable', 12, '2020-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W81', 68, 'closed', 'positive', 12, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W82', 69, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W83', 70, 'closed', 'not_applicable', 12, '2020-10-15', 3);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-P0W84', 71, 'closed', 'positive', 12, '2020-06-26', 11);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-R0W85', 72, 'closed', 'not_controlled', 12, null, 43);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2019-CON-50W86', 73, 'closed', 'not_applicable', 12, '2020-10-15', 3);


-- 2020
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-EG0EI', 98, 'created', null, 2, '2020-07-29', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-7UQZN', 99, 'created', null, 7, null, null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-VYUJI', 100, 'created', null, 12, null, null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-Z0P7B', 79, 'closed', 'negative', 2, '2020-07-21', 1);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-I0ILP', 83, 'closed', 'positive', 9, '2020-06-09', 14);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-7P5J9', 87, 'evaluated', 'positive', 10, '2020-05-29', 4);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-67XW0', 89, 'evaluated', 'negative', 10, '2020-05-29', 1);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-T4C75', 92, 'evaluated', 'positive', 1, '2020-03-26', 4);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-IO9K0', 78, 'evaluated', 'positive', 12, null, 54);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-NPQNZ', 82, 'created', null, 2, '2020-07-29', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-EZ6CE', 84, 'created', null, 2, '2020-07-29', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-HF5RF', 86, 'created', null, 2, '2020-07-29', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-6F3KW', 88, 'created', null, 1, '2020-06-17', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-Y2ELN', 90, 'created', null, 10, '2020-07-22', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-9CAPG', 94, 'created', null, 9, '2020-06-10', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-ELIK5', 95, 'closed', null, 7, null, null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-1D3X8', 76, 'closed', null, 2, '2020-07-22', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-JWNI1', 81, 'created', null, 7, null, null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-BOCI7', 85, 'created', null, 1, '2020-03-26', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-HVYSM', 91, 'created', null, 9, '2020-03-26', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-DI4T7', 96, 'created', null, 12, null, null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-5BBZD', 97, 'created', null, 2, '2020-05-07', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-8DB3N', 77, 'created', null, 10, '2020-05-07', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-J4VCY', 93, 'created', null, 9, '2020-06-10', null);
INSERT INTO reports (report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
VALUES (nextval('reports_seq'), '2020-CON-MH3DX', 80, 'closed', 'positive', 7, null, 54);


--2016
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 4, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 5, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 6, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 7, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 8, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 9, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 10, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 11, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 12, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 13, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 14, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 15, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 16, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 17, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 18, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 19, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 20, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 21, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 22, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 23, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 24, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 25, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 26, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 27, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 28, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 29, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 30, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 31, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 32, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 33, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 34, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 35, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 36, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 37, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');


--2017
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 38, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 39, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 40, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 41, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 42, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 43, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 44, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 45, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 46, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 47, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 48, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 49, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 50, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 51, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 52, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 53, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 54, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 55, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 56, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 57, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 58, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 59, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 60, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 61, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 62, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 63, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 64, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 65, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 66, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 67, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 68, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 69, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 70, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 71, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');


--2018
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 72, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 73, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 74, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 75, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 76, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 77, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 78, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 79, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 80, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 81, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 82, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 83, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 84, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 85, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 86, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 87, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 88, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 89, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 90, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 91, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 92, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 93, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 94, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 95, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 96, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 97, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 98, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 99, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 100, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 101, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 102, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 103, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 104, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');


--2019
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 105, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 106, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 107, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 108, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 109, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 110, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 111, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 112, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 113, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 114, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 115, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 116, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 117, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 118, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 119, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 120, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 121, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 122, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 123, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 124, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 125, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 126, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 127, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 128, 1, 'Nombre de volontaires', 2, 'personnes',
        'Phillipe Henrard et Catherine Smith');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 129, 1, 'Nombre de volontaires cachés', 1, 'personnes', 'Phillipe Henrard');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 130, 1, 'Nombre de tortionnaires', 9, 'personnes', 'La famille Adams');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 131, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 132, 1, 'Nombre de faux billets trouvés sur place', 3240, 'billets',
        'Somme trouvée dans les caisses de bananes');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 133, 1, 'Somme totale des faux billets', 23980, '€', 'Somme préjudiciable');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 134, 1, 'Nombre de personnes ayant échappé au premier contrôle', 4,
        'personnes', 'Ils etaient dans un autre hangar');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 135, 1, 'Nombre de personnes total des deux interventions', 54, 'personnes',
        'Nombre total d''employé dans l''entreprise');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 136, 1, 'Somme dérobée', 21000, '€', 'Pactole de la fraude');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 137, 1, 'Différence d''employés entre les deux enquêtes', 21, 'employés',
        'Personnes engagées entretemps');


--2020
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 141, 1, 'Nombre de bureaux crées', '65', 'bureaux',
        '43 petits et 22 grands');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 141, 2, 'Surface totale', '3294', 'm²',
        'Surface réelle sans compter les murs');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 141, 3, 'Nombre d''étages', '9', 'étages', 'dont 3 en sous-sol');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 142, 1, 'Nombre de clients présents sur place', '4', 'clients',
        'dont 3 connaissant pertinemment l''existence du club secret');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 142, 2, 'Nombres d''employés', '10', 'employés',
        'dont 5 connaissant pertinemment l''existence du club secret');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 142, 3, 'Nombre de personnes sexuellement exploitées', '3', 'personnes',
        'personnes sans papiers légaux');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 143, 1, 'Nombre de mendiants', '4', 'personnes', 'dont 2 immigrés');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 143, 2, 'Nombre d''animaux domestiqués', '5', 'animaux',
        '3 chiens et 2 rats');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 144, 1, 'Nombre de mendiants', '1', 'personnes', 'Paul Magnette');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 144, 2, 'Nombre d''animaux domestiqués', '4', 'animaux', '4 chats');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 145, 1, 'Nombre de billets retrouvés', '402', 'billets',
        'de nombreuses coupure de 20€');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 145, 2, 'Somme totale du délit', '4980', '€',
        'Somme supérieure à 2500, signalisation à un autre service');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 146, 1, 'Nombre de personnes déclarées', '37', 'personnes',
        '3 gérants et 34 employés');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 146, 2, 'Nombre de personnes réellement sur place', '54', 'personnes',
        '17 "volontaires" non déclarés');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 147, 1, 'Somme préjudiciable', null, '€', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 148, 1, 'Somme préjudiciable', null, '€', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 149, 1, 'Somme préjudiciable', null, '€', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 150, 1, 'Nombre de billets ', null, 'billets', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 150, 2, 'Somme totale des faux billets', null, '€', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 151, 1, 'Nombre de mendiants', null, 'personnes', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 153, 1, 'Fraude avérée ', null, 'oui/non', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 153, 2, 'Somme dédommageable', null, '€', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 152, 1, 'Nombre de personnes sur place', null, 'personnes', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 152, 2, 'Nombre de gérants', null, 'personnes', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 154, 1, 'Somme préjudiciable', null, '€', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 155, 1, 'Somme préjudiciable', null, '€', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 156, 1, 'Présence de faux billets', null, 'oui/non', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 157, 1, 'Nombre d''employés', null, 'personnes', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 158, 1, 'Nombre de volontaires sur place', null, 'personnes', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 159, 1, 'Somme dédommageable', null, '€', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 160, 1, 'Nombre de mendiants', null, 'personnes', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 161, 1, 'Nombre de personnes exploitées sexuellement', null, 'personnes',
        null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 161, 2, 'Nombre d''employés', null, 'personnes', null);
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 162, 1, 'Nombre de personnes dans le batiment', '54', 'personnes',
        'comme nombre calculé');
INSERT INTO report_observations (observation_id, report_id, observation_number, title, value, unit, description)
VALUES (nextval('report_observations_seq'), 162, 2, 'Nombre de personnes illégales', '32', 'personnes',
        'donc 32 personnes de plus que dans les données reçues');
