package be.unamur.ingelog.backend.authentication.feature;

import java.util.Arrays;
import java.util.List;

import static be.unamur.ingelog.backend.authentication.feature.Role.*;
import static java.util.Collections.*;
import static java.util.stream.Collectors.toList;

public enum Feature {
    // Dossiers
    FIND_CASES(AGENT_ADM, INSPECTOR, CHIEF_INSPECTOR),
    CREATE_CASES(AGENT_ADM, INSPECTOR, CHIEF_INSPECTOR),

    // Enquêtes
    ASK_VALIDATION(INSPECTOR, CHIEF_INSPECTOR),
    VALIDATE(CHIEF_INSPECTOR),
    INVALIDATE(CHIEF_INSPECTOR),
    CLOSE_INQUIRY(CHIEF_INSPECTOR),
    ASSIGN(CHIEF_INSPECTOR),
    UPDATE_REPORT_DATE(INSPECTOR, CHIEF_INSPECTOR),
    UPDATE_INQUIRY_RESULT(INSPECTOR, CHIEF_INSPECTOR),
    SEND_BACK_INQUIRY(CHIEF_INSPECTOR),

    // Commentaires
    CREATE_COMMENTS(AGENT_ADM, INSPECTOR, CHIEF_INSPECTOR),
    FIND_COMMENTS(AGENT_ADM, INSPECTOR, CHIEF_INSPECTOR),

    // Constatations
    FIND_OFFENCES(AGENT_ADM, INSPECTOR, CHIEF_INSPECTOR),
    FIND_EMPLOYERS(AGENT_ADM, INSPECTOR, CHIEF_INSPECTOR),
    ENCODE_CONTROLS_COUNT(AGENT_ADM),
    FIND_REPORT_STATUS(AGENT_ADM, INSPECTOR, CHIEF_INSPECTOR),

    // Observations
    CREATE_OBSERVATIONS(INSPECTOR, CHIEF_INSPECTOR),
    ENCODE_ROADMAP(AGENT_ADM),
    FIND_OBSERVATIONS(AGENT_ADM, INSPECTOR, CHIEF_INSPECTOR),

    // Dashboard
    CONSULT_DASHBOARD(DIRECTOR_GENERAL);

    private final Role[] roles;

    Feature(Role... roles) {
        this.roles = roles;
    }

    public List<Role> getRoles() {
        return Arrays.asList(roles);
    }

    /**
     * Renvoie la liste des fonctionnalités auxquelles à accès un utilisateur sur base
     * du rôle qui lui est attribué.
     *
     * @param userRole le rôle de l'utilisateur
     * @return la liste des fonctionnalités auxquelles a accès l'utilisateur
     */
    public static List<Feature> getFeaturesForRole(String userRole) {
        if (isUnknownRole(userRole)) {
            return emptyList();
        }

        Role role = Role.valueOf(userRole);

        return Arrays.stream(Feature.values())
                .filter(feature -> !disjoint(feature.getRoles(), singletonList(role)))
                .collect(toList());
    }

    private static boolean isUnknownRole(String userRole) {
        return Arrays.stream(Role.values())
                .noneMatch(role -> role.toString().equals(userRole));
    }
}
