package be.unamur.ingelog.backend.authentication;

import be.unamur.ingelog.backend.dto.agents.AgentAuthDTO;
import com.google.gson.Gson;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import static be.unamur.ingelog.backend.authentication.SecurityConstants.*;
import static io.jsonwebtoken.SignatureAlgorithm.HS512;
import static io.jsonwebtoken.security.Keys.hmacShaKeyFor;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;

        setFilterProcessesUrl(AUTH_LOGIN_URL);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        var username = request.getParameter(SPRING_SECURITY_FORM_USERNAME_KEY);
        var password = request.getParameter(SPRING_SECURITY_FORM_PASSWORD_KEY);

        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain filterChain, Authentication authentication) throws IOException {
        var token = Jwts.builder()
                .signWith(hmacShaKeyFor(JWT_SECRET.getBytes()), HS512)
                .setHeaderParam("typ", TOKEN_TYPE)
                .setIssuer(TOKEN_ISSUER)
                .setAudience(TOKEN_AUDIENCE)
                .setSubject(authentication.getName())
                .setExpiration(new Date(new Date().getTime() + 900_000)) // 15 minutes
                .claim("authorities", authentication.getAuthorities())
                .compact();

        AgentAuthDTO dto = AgentAuthDTO.builder()
                .username(authentication.getName())
                .firstName(((CustomAuthenticationToken) authentication).getFirstName())
                .lastName(((CustomAuthenticationToken) authentication).getLastName())
                .role(((CustomAuthenticationToken) authentication).getRole())
                .authorities(authentication.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(toList()))
                .token(token)
                .build();

        Gson gson = new Gson();
        PrintWriter out = response.getWriter();

        response.setContentType(APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");

        out.print(gson.toJson(dto));
        out.flush();
    }
}
