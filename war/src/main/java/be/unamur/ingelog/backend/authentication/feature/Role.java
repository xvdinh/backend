package be.unamur.ingelog.backend.authentication.feature;

public enum Role {
    AGENT_ADM,
    INSPECTOR,
    CHIEF_INSPECTOR,
    DIRECTOR_GENERAL
}
