package be.unamur.ingelog.backend.action.records;

import be.unamur.ingelog.backend.domain.action.records.ActionRecord;
import be.unamur.ingelog.backend.service.action.records.ActionRecordService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static java.lang.Math.max;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@Aspect
@Component
public class WebMethodAuditor {
    private final ActionRecordService actionRecordService;

    private ActionRecord currentAction;

    @Autowired
    public WebMethodAuditor(ActionRecordService actionRecordService) {
        this.actionRecordService = actionRecordService;
    }

    @Pointcut("within(be.unamur.ingelog.backend.controller..*)")
    public void inControllerPackage() {
    }

    @Pointcut("!within(*..GlobalExceptionHandler)")
    public void excludeExceptionHandler() {
    }


    @Pointcut("execution (* * (..))")
    public void allMethods() {
    }

    @Before(value = "inControllerPackage() && allMethods() && excludeExceptionHandler()")
    public void beforeMethodExecution(JoinPoint joinPoint) {
        var methodArguments = joinPoint.getArgs();
        var methodName = getMethodName(joinPoint);
        var username = getAuthenticatedUsername();
        var actionTimestamp = LocalDateTime.now();

        currentAction = ActionRecord.builder()
                .methodArguments(methodArguments)
                .methodName(methodName)
                .username(username)
                .actionTimestamp(actionTimestamp)
                .build();
    }

    @AfterReturning(value = "inControllerPackage() && allMethods()", returning = "returnValue")
    public void afterMethodExecution(Object returnValue) {
        if (returnValue == null) return;
        var returnValueString = returnValue.toString();

        if (returnValueString.startsWith("<")) {
            var temp = returnValueString.split(" ");
            var response = temp[0].substring(1) + " " + temp[1];

            currentAction.setResponse(response);
        } else {
            currentAction.setResponse("200 OK");
        }

        actionRecordService.create(currentAction);
    }

    private String getMethodName(JoinPoint joinPoint) {
        var declaringTypeName = joinPoint.getSignature().getDeclaringTypeName();
        var temp = declaringTypeName.split("\\.");

        var className = temp[max(temp.length - 1, 0)];
        var methodName = joinPoint.getSignature().getName();

        return className + "." + methodName;
    }

    private String getAuthenticatedUsername() {
        return getContext().getAuthentication() == null ? null :
                getContext().getAuthentication().getName();
    }
}
