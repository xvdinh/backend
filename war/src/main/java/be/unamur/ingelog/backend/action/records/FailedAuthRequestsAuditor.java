package be.unamur.ingelog.backend.action.records;

import be.unamur.ingelog.backend.domain.action.records.ActionRecord;
import be.unamur.ingelog.backend.service.action.records.ActionRecordService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.security.access.event.AuthorizationFailureEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@Component
public class FailedAuthRequestsAuditor {
    private final ActionRecordService actionRecordService;
    private final ApplicationContext context;

    public FailedAuthRequestsAuditor(ActionRecordService actionRecordService,
                                     ApplicationContext context) {
        this.actionRecordService = actionRecordService;
        this.context = context;
    }

    @EventListener
    public void onAuthorizationFailureEvent(AuthorizationFailureEvent event) {
        var requestAttributes = RequestContextHolder.getRequestAttributes();
        var request = requestAttributes != null ? ((ServletRequestAttributes) requestAttributes).getRequest() : null;
        var destinationMethod = this.getDestination(request);

        if (destinationMethod != null) {
            var methodArguments = new Object[]{};
            var methodName = getMethodName(destinationMethod);
            var username = getAuthenticatedUsername();
            var actionTimestamp = LocalDateTime.now();
            var response = "403 FORBIDDEN";

            var actionRecord = ActionRecord.builder()
                    .methodArguments(methodArguments)
                    .methodName(methodName)
                    .username(username)
                    .actionTimestamp(actionTimestamp)
                    .response(response)
                    .build();

            actionRecordService.create(actionRecord);
        }
    }

    private String getMethodName(Method destinationMethod) {
        var destinationName = destinationMethod.toString();
        var destinationWithoutParameters = destinationName.substring(0, destinationName.indexOf("("));

        var temp = destinationWithoutParameters.split("\\.");

        return temp[temp.length - 2] + "." + temp[temp.length - 1];
    }

    private String getAuthenticatedUsername() {
        return getContext().getAuthentication() == null ? null :
                getContext().getAuthentication().getName();
    }

    private Method getDestination(HttpServletRequest request) {
        for (HandlerMapping handlerMapping : context.getBeansOfType(HandlerMapping.class).values()) {
            HandlerExecutionChain handlerExecutionChain = null;
            try {
                handlerExecutionChain = handlerMapping.getHandler(request);
            } catch (Exception e) {
                // do nothing
            }

            if (handlerExecutionChain != null && handlerExecutionChain.getHandler() instanceof HandlerMethod) {
                return ((HandlerMethod) handlerExecutionChain.getHandler()).getMethod();
            }
        }

        return null;
    }
}
