package be.unamur.ingelog.backend.authentication;

import be.unamur.ingelog.backend.authentication.feature.Feature;
import be.unamur.ingelog.backend.service.agents.AgentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

import static be.unamur.ingelog.backend.authentication.feature.Feature.getFeaturesForRole;
import static java.util.stream.Collectors.toList;

@Slf4j
@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {
    private final AgentService agentService;
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public AuthenticationProviderImpl(AgentService agentService) {
        this.agentService = agentService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        var username = authentication.getPrincipal().toString();
        var password = authentication.getCredentials().toString();
        var user = agentService.authenticate(username);

        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Wrong username or password");
        }

        log.info("User successfully logged in for username {}.", username);
        return new CustomAuthenticationToken(
                user.getUsername(),
                user.getPassword(),
                rolesToGrantedAuthorities(getFeaturesForRole(user.getRole())),
                user.getFirstName(),
                user.getLastName(),
                user.getRole());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    private Collection<? extends GrantedAuthority> rolesToGrantedAuthorities(List<Feature> features) {
        return features.stream()
                .map(Enum::name)
                .map(SimpleGrantedAuthority::new)
                .collect(toList());
    }
}
