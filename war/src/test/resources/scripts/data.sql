-- remplissage des valeurs prédéfinies de la table 'roles'
insert into agent_roles(role_id)
values ('INSPECTOR');
insert into agent_roles(role_id)
values ('CHIEF_INSPECTOR');
insert into agent_roles(role_id)
values ('AGENT_ADM');


-- remplissage des valeurs prédéfinies de la table 'case_states'
insert into case_states(state_id)
values ('initiated');
insert into case_states(state_id)
values ('configured');
insert into case_states(state_id)
values ('approved');
insert into case_states(state_id)
values ('closed');
insert into case_states(state_id)
values ('archived');


-- remplissage des valeurs prédéfinies de la table 'case_motives'
insert into case_motives(motive_id)
values ('complaint');
insert into case_motives(motive_id)
values ('denunciation');
insert into case_motives(motive_id)
values ('investigation_request');
insert into case_motives(motive_id)
values ('regularization_control');


-- remplissage des valeurs prédéfinies de la table 'case_origins'
insert into case_origins(origin_id, motive_id)
values ('anonymous', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('individual', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('employee', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('employer', 'denunciation');
insert into case_origins(origin_id, motive_id)
values ('police', 'investigation_request');
insert into case_origins(origin_id, motive_id)
values ('spf', 'investigation_request');
insert into case_origins(origin_id, motive_id)
values ('inspection_services', 'investigation_request');
insert into case_origins(origin_id, motive_id)
values ('onss', 'investigation_request');


-- remplissage des valeurs prédéfinies de la table 'offence_priorities'
insert into offence_priorities(offence_priority_id)
values ('1');
insert into offence_priorities(offence_priority_id)
values ('2');
insert into offence_priorities(offence_priority_id)
values ('3');
insert into offence_priorities(offence_priority_id)
values ('4');
insert into offence_priorities(offence_priority_id)
values ('5');


-- remplissage des valeurs prédéfinies de la table 'report_states'
insert into report_states(state_id)
values ('created');
insert into report_states(state_id)
values ('encoded');
insert into report_states(state_id)
values ('evaluated');
insert into report_states(state_id)
values ('closed');


-- remplissage des valeurs prédéfinies de la table 'report_status'
insert into report_status(status_id)
values ('pending');
insert into report_status(status_id)
values ('not_applicable');
insert into report_status(status_id)
values ('not_controlled');
insert into report_status(status_id)
values ('positive');
insert into report_status(status_id)
values ('negative');


-- remplissage des valeurs prédéfinies de la table 'inquiry_states'
insert into inquiry_states(state_id)
values ('open');
insert into inquiry_states(state_id)
values ('pending_validation');
insert into inquiry_states(state_id)
values ('incomplete');
insert into inquiry_states(state_id)
values ('approved');
insert into inquiry_states(state_id)
values ('assigned');
insert into inquiry_states(state_id)
values ('completed');
insert into inquiry_states(state_id)
values ('pending_closure');
insert into inquiry_states(state_id)
values ('closed');


-- remplissage des valeurs prédéfinies de la table 'inquiry_results'
insert into inquiry_results(result_id)
values ('positive');
insert into inquiry_results(result_id)
values ('negative');


insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSA', 'insa', 'Alexandre', 'Fortin', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSB', 'insb', 'Alizé', 'Paquette', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSC', 'insc', 'Jacques', 'Tousignant', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSD', 'insd', 'Océane', 'Girouard', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSE', 'inse', 'Yves', 'Quesnel', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSF', 'insf', 'Sylvie', 'Taylor', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSG', 'insg', 'Gaspard', 'Diaz', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'INSH', 'insh', 'Constance', 'Gilbert', 'INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CINA', 'cina', 'Xavier', 'Cloutier', 'CHIEF_INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CINB', 'cinb', 'Laurence', 'Langlois', 'CHIEF_INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CINC', 'cinc', 'Georges', 'Larouche', 'CHIEF_INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'CIND', 'cind', 'Arlette', 'Moreau', 'CHIEF_INSPECTOR');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'ADMA', 'adma', 'Nathalie', 'Desrochers', 'AGENT_ADM');
insert into agents(agent_id, username, password, first_name, last_name, role_id)
values (nextval('agents_seq'), 'ADMB', 'admb', 'Victor', 'Collin', 'AGENT_ADM');


insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-R2D2L', 'JMS Construct SPRL', '55 rue de la Loi', '1000', 'Bruxelles',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-ROB78', 'Shady Business SA', '37 boulevard de l''Arnaque', '5000', 'Namur',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-A2D2L', 'Great Construct', '200 rue des Amandiers', '1030', 'Schaerbeek',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-AOB78', 'Alliance et fils', '1 rue de l''Arbre-Tout-Seul', '1060',
        'Saint-Gilles',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-R9D2L', 'ETC Filsol', '89 rue Ernest Boucquéau', '1082', 'Berchem-Sainte-Agathe',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-R6B78', 'BESIX', '37 boulevard de l''Arnaque', '1340',
        'Ottignies-Louvain-la-Neuve',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-R2T2L', 'Vieille-Montagne', '46 rue du Chemin de fer', '1370', 'Dongelberg',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-ROP78', 'Englebert', '190 rue du Chemin de fer', '5000', 'Namur',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-R2DLL', 'VERHAEREN en CO', '829 boulevard des Droits de l''Homme', '3510',
        'Kermt',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-ROB38', 'EULER HERMES', '35 rue de la Fontaine de Spa', '4000', 'Liège',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-R2D2Q', 'Cockerill-Sambre', '6 rue des Hortensias', '4400', 'Flémalle',
        'Belgique');
insert into employers(employer_id, employer_reference, name, street, postalcode, locality, country)
values (nextval('employers_seq'), 'EMP-ROB71', 'I.NA.CO - N.IM.BO', '32 rue de la Liberté', '6042', 'Charleroi',
        'Belgique');


insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-OX9MU', 1, '2020-03-10', '2020-03-10', null, 'denunciation', 'anonymous',
        'initiated', 'lorem ipsum');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-BZ6L3', 1, '2020-03-15', '2020-03-15', '2020-03-25', 'denunciation', 'employer',
        'closed', 'lorem ipsum');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-CMSGT', 13, '2020-02-01', '2020-04-09', '2020-02-20', 'complaint',
        '2020-PLA-XO29Q', 'closed',
        'Plainte à l''encontre de la société Vieille-Montagne à Dongelberg portant sur la suspicion de faux et usage de faux.');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-XFFKX', 13, '2012-01-06', '2020-04-09', '2012-02-06', 'denunciation',
        'anonymous', 'archived',
        'Dénonciation d''un marchand de sommeil à l''adresse 56 rue de l''Artichaut 4040 Herstal ');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-566XO', 13, '2020-03-07', '2020-04-09', null, 'denunciation', 'individual',
        'approved',
        'Dénonciation déposée pour suspicion de mendicité organisée par un tier. 3 personnes concernée; une avenue de l''Astronomie; deux rue de la Bienfaisance à 7190 Écaussinnes');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-4A5W5', 13, '2008-10-09', '2020-04-09', '2008-10-15', 'denunciation',
        'employee', 'closed',
        'Un employé de la société ETC Filsol à 4000 Liège accuse son employeur de surveillance illégale.');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-IAZKZ', 13, '2020-03-14', '2020-04-09', null, 'denunciation', 'employer',
        'initiated',
        'Délation d''une suspicion d''avantage patrimonial cotisations alliance et fils à 1060 Saint-Gilles.');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-VVHFF', 13, '2020-04-06', '2020-04-09', null, 'investigation_request', 'police',
        'configured',
        'constate la presence d''un volontaire en date du 04/04/2020 adresse 75 rue Wauwermans à 6760 Bleid travaillant pour la société Englebert basé à 5000 Namur');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-XE1PR', 13, '2020-04-08', '2020-04-09', null, 'investigation_request', 'onss',
        'initiated',
        'Suspicion de non payement des cotisations sociales de la société Great Construct pour les ouvriers travaillant sur le chantier situé rue Verboeckhaven 8978 Watou');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-O2HHF', 13, '2016-08-05', '2020-04-09', '2016-09-05', 'investigation_request',
        'spf', 'archived',
        'Le SPF suspecte un traffic d organe suite à l''achat massif de materiel chirugical pour la société I.NA.Co - N.IM.BO promoteur immobilier');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-ARJ4Q', 13, '2019-12-06', '2020-04-09', '2019-12-20', 'investigation_request',
        'inspection_services', 'closed',
        'Suspicion de la présence d''exploitation sexuelle au sein de la société BESIX 1340 Ottignies-Louvain-La-Neuve');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-K5XXX', 13, '2020-03-26', '2020-04-09', null, 'regularization_control',
        '2020-ENQ-CAYWX', 'approved',
        'Vérification de la mise en conformité suite au controle pour exploitation sexuelle au sein de la société BESIX');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-EER31', 13, '2007-12-30', '2020-04-09', '2008-01-02', 'denunciation',
        'employer', 'closed', 'Suspicion d''un concurant sur un avantage patrimonial salaires à Cockerill-Sambre');
insert into cases(case_id, case_reference, creator, trigger_date, open_date, close_date, motive, origin, state,
                  description)
values (nextval('cases_seq'), '2020-DOS-K920B', 13, '2015-08-31', '2020-04-09', '2015-10-06', 'investigation_request',
        'police', 'archived', 'Exploitation economique Verhaeren en co');


insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-OD9NV', 1, 1, null, null, 'open', null, '2020-01-12', null, null,
        'Rue Neuve', '1337', 'Ostende');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-BEYV9', 2, 1, null, null, 'open', null, '2020-02-21', null, null,
        'Rue du Bout de la Haut', '5651', 'Berzée');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-0GFWB', 1, 2, null, null, 'open', null, '2020-03-03', null, null,
        'Rue du Bois Joli', '5000', 'Namur');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-LQLYZ', 3, 7, 10, 7, 'closed', 'negative', '2020-04-09', '2020-02-18',
        '2020-02-14', '46 rue du chemin de fer', '1370', 'Dongelberg');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-RT4AQ', 4, 2, 9, 5, 'closed', 'positive', '2020-04-09', '2012-02-05',
        '2012-02-01', '56 rue de l''Artichaut', '4040', 'Herstal');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-K8QY0', 5, 10, 11, 4, 'assigned', null, '2020-04-09', null, null,
        'avenue de l''Astronomie', '7190', 'Écaussinnes');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-PTG9Z', 5, 10, 11, 4, 'assigned', null, '2020-04-09', null, null,
        'rue de la Bienfaisance', '7190', 'Écaussinnes');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-15IYS', 6, 5, 12, null, 'closed', null, '2020-04-09', '2008-10-15', null,
        '89 rue Ernest Boucquéau', '1082', 'Berchem-Sainte-Agathe');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-ECPGV', 7, 4, 12, null, 'incomplete', null, '2020-04-09', null, null,
        '1 rue de l''Arbre-Tout-Seul', '1060', 'Saint-Gilles');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-ZPF2I', 8, 8, null, null, 'pending_validation', null, '2020-04-09', null,
        null, '75 rue Wauwermans', '6760', 'Bleid');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-IYFIU', 9, 3, null, null, 'open', null, '2020-04-09', null, null,
        'rue Verboeckhaven', '8978', 'Watou');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-T8SLV', 10, 12, 10, 2, 'closed', 'positive', '2020-04-09', '2016-09-05',
        '2016-09-02', '32 rue de la Liberté', '6042', 'Charleroi');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-CAYWX', 11, 6, 10, 1, 'closed', 'positive', '2020-04-09', '2019-12-20',
        '2019-12-10', '37 boulevard de l''Arnaque', '1340', 'Ottignies-Louvain-La-Neuve');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-EGJYQ', 12, 6, 9, 6, 'assigned', null, '2020-04-09', null, null,
        '37 boulevard de l''Arnaque', '1340', 'Ottignies-Louvain-La-Neuve');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-JW6K9', 13, 11, 12, null, 'closed', null, '2020-04-09', '2008-01-02', null,
        '6 rue des Hortensias', '4400', 'Flémalle');
insert into inquiries(inquiry_id, inquiry_reference, case_id, employer_id, supervisor, assignee, state, result,
                      open_date, close_date, report_date, street, postalcode, locality)
values (nextval('inquiries_seq'), '2020-ENQ-N83GZ', 14, 9, 11, 3, 'closed', 'positive', '2020-04-09', '2015-10-06',
        '2015-10-01', '829 boulevard des Droits de l''Homme', '3510', 'Kermt');


insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG001', 'label', '2005-03-24');
insert into law_articles(law_article_id, law_article_reference, label, starting_date, ending_date)
values (nextval('law_articles_seq'), 'RG002001', 'label', '2005-03-24', '2014-12-31');
insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG002002', 'label', '2005-03-24');
insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG002003', 'label', '2015-01-01');
insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG002004', 'label', '2015-01-01');
insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG002005', 'label', '2015-01-01');
insert into law_articles(law_article_id, law_article_reference, label, starting_date)
values (nextval('law_articles_seq'), 'RG012', 'label', '2015-01-01');


insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG001001', 1, 'label', '2005-03-24', 30, '1');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG001002', 1, 'label', '2005-03-24', 60, null);
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG001003', 1, 'label', '2005-03-24', '2014-12-31', 60, '4');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG001004', 1, 'label', '2005-03-24', '2014-12-31', 60, '4');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG002001001', 2, 'label', '2005-03-24', '2014-12-31', 45, '2');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG002002001', 3, 'label', '2005-03-24', '2010-06-30', null, null);
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG002002002', 3, 'label', '2015-01-01', null, null);
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG002003001', 4, 'label', '2015-01-01', '2015-12-31', 45, '2');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG002004001', 5, 'label', '2015-01-01', 30, '1');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG002005001', 6, 'label', '2015-01-01', 60, '5');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, ending_date, legal_delay,
                     priority)
values (nextval('offences_seq'), 'RG002005002', 6, 'label', '2015-01-01', '2017-06-30', 30, '1');
insert into offences(offence_id, offence_reference, law_article_id, label, starting_date, legal_delay, priority)
values (nextval('offences_seq'), 'RG012001', 7, 'label', '2015-01-01', null, '3');


insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-VR1LQ', 1, 'created', null, null, '2020-03-14', null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-F8Z9K', 4, 'created', null, 1, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-JZ0EO', 5, 'created', null, 5, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-GR7PA', 6, 'created', null, 10, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-W91MZ', 7, 'created', null, 10, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-EKFPR', 8, 'created', null, 4, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-B7ZH1', 9, 'created', null, 7, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-31TRJ', 10, 'created', null, 12, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-LHZXJ', 11, 'created', null, 2, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-UYMN6', 12, 'created', null, 11, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-I60EW', 13, 'created', null, 9, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-DJCIZ', 14, 'created', null, 9, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-6P5ST', 15, 'created', null, 6, null, null);
insert into reports(report_id, report_reference, inquiry_id, state, status, offence_id, deadline, controls_count)
values (nextval('reports_seq'), '2020-CON-Q5S0I', 16, 'created', null, 8, null, null);
