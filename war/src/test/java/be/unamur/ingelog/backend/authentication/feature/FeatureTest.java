package be.unamur.ingelog.backend.authentication.feature;


import org.junit.jupiter.api.Test;

import java.util.List;

import static be.unamur.ingelog.backend.authentication.feature.Feature.getFeaturesForRole;
import static org.assertj.core.api.Assertions.assertThat;

public class FeatureTest {
    @Test
    void getFeaturesForRole_whenProvidingNoRole_thenReturnNoFeatures() {
        List<Feature> actual = getFeaturesForRole("not-a-role");

        assertThat(actual).isEmpty();
    }
}
