package be.unamur.ingelog.backend;

import be.unamur.ingelog.backend.util.RestAssuredTest;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;

public class AcceptanceIntegrationTest extends RestAssuredTest {
    /**
     * En tant qu'utilisateur connecté, je veux consulter la liste des dossiers.
     */
    @Test
    @Sql(scripts = "classpath:scripts/schema.sql")
    @Sql(scripts = "classpath:scripts/data.sql")
    void findCases_hasRightSize() {
        givenRequestForUser("CINA", "cina");
    }

    /**
     * En tant qu'utilisateur connecté, je veux consulter la liste des dossiers.
     */
    @Test
    @Sql(scripts = "classpath:scripts/schema.sql")
    @Sql(scripts = "classpath:scripts/data.sql")
    void findCases_hasRightContent() {
        givenRequestForUser("CINA", "cina");
    }

    /**
     * En tant qu'utilisateur connecté, je veux consulter les détails d'un dossier.
     */
    @Test
    @Sql(scripts = "classpath:scripts/schema.sql")
    @Sql(scripts = "classpath:scripts/data.sql")
    void findCase_happyPath() {
        givenRequestForUser("CINA", "cina");
    }

    /**
     * En tant qu'utilisateur connecté, je veux initier un dossier.
     */
    @Test
    @Sql(scripts = "classpath:scripts/schema.sql")
    @Sql(scripts = "classpath:scripts/data.sql")
    void createCase_happyPath() {
        givenRequestForUser("CINA", "cina");
    }

    /**
     * En tant qu'utilisateur connecté, je veux consulter les détails d'une enquête.
     */
    @Test
    @Sql(scripts = "classpath:scripts/schema.sql")
    @Sql(scripts = "classpath:scripts/data.sql")
    void findInquiry_happyPath() {
        givenRequestForUser("CINA", "cina");
    }

    /**
     * En tant qu'utilisateur connecté, je veux ouvrir une enquête.
     */
    @Test
    @Sql(scripts = "classpath:scripts/schema.sql")
    @Sql(scripts = "classpath:scripts/data.sql")
    void createInquiry_happyPath() {
        givenRequestForUser("CINA", "cina");
    }

    /**
     * En tant qu'utilisateur connecté, je veux consulter les détails d'une constatation.
     */
    @Test
    @Sql(scripts = "classpath:scripts/schema.sql")
    @Sql(scripts = "classpath:scripts/data.sql")
    void findReport_happyPath() {
        givenRequestForUser("CINA", "cina");
    }

    /**
     * En tant qu'utilisateur connecté, je veux créer une constatation.
     */
    @Test
    @Sql(scripts = "classpath:scripts/schema.sql")
    @Sql(scripts = "classpath:scripts/data.sql")
    void createReport_happyPath() {
        givenRequestForUser("CINA", "cina");
    }
}
