package be.unamur.ingelog.backend.util;

import be.unamur.ingelog.backend.TestApplication;
import io.restassured.specification.RequestSpecification;
import org.junit.ClassRule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = TestApplication.class)
public abstract class RestAssuredTest {
    @LocalServerPort
    private int port;

    @ClassRule
    public static final PostgreSQLContainer CONTAINER = CustomPostgresqlContainer.getInstance();

    static {
        CONTAINER.start();
    }

    @Test
    public RequestSpecification givenRequestForUser(String username, String password) {
        String token =
                given()
                        .baseUri("http://localhost")
                        .port(port)
                        .basePath("/authenticate")
                        .contentType(APPLICATION_JSON_VALUE)
                        .params("username", username, "password", password)
                        .when()
                        .get()
                        .then()
                        .statusCode(200)
                        .extract()
                        .path("token");

        return given()
                .header("Authorization", "Bearer " + token)
                .baseUri("http://localhost")
                .port(port);
    }
}
