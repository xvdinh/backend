FROM maven:3.6.3 AS MAVEN_TOOL_CHAIN

COPY pom.xml /tmp/
COPY controller /tmp/controller/
COPY domain /tmp/domain/
COPY dto /tmp/dto/
COPY infrastructure /tmp/infrastructure/
COPY repository /tmp/repository/
COPY service /tmp/service/
COPY war /tmp/war/

WORKDIR /tmp/
RUN mvn -DskipTests=true package

FROM tomcat:9.0.30
MAINTAINER avocat

RUN ["rm", "-fr", "/usr/local/tomcat/webapps/ROOT"]
COPY --from=MAVEN_TOOL_CHAIN /tmp/war/target/war-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war
COPY ./war/src/main/resources/bin/setenv.sh /usr/local/tomcat/bin/setenv.sh

CMD ["catalina.sh", "run"]
