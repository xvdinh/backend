package be.unamur.ingelog.backend.controller.cases.motive;

import be.unamur.ingelog.backend.controller.cases.CaseControllerImpl;
import be.unamur.ingelog.backend.dto.cases.CaseMotiveDTO;
import be.unamur.ingelog.backend.service.cases.CaseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CaseControllerImpl.class})
@WebMvcTest(controllers = CaseControllerImpl.class)
class CaseMotiveControllerTest {
    @MockBean
    private CaseService caseService;

    @Autowired
    private MockMvc mock;

    private CaseMotiveDTO complaint;
    private CaseMotiveDTO denunciation;

    @BeforeEach
    void setUp() {
        complaint = CaseMotiveDTO.builder()
                .reference("complaint")
                .build();
        denunciation = CaseMotiveDTO.builder()
                .reference("denunciation")
                .build();
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findAll_hasRightSize() throws Exception {
        when(caseService.findAllMotives())
                .thenReturn(List.of(complaint, denunciation));

        mock.perform(get("/motives"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findAll_hasRightContent() throws Exception {
        when(caseService.findAllMotives())
                .thenReturn(List.of(complaint));

        mock.perform(get("/motives"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].reference", is("complaint")));
    }
}
