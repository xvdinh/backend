package be.unamur.ingelog.backend.controller.cases;

import be.unamur.ingelog.backend.controller.GlobalExceptionHandler;
import be.unamur.ingelog.backend.dto.cases.CaseCreateDTO;
import be.unamur.ingelog.backend.dto.cases.CaseDetailDTO;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.CaseNotFoundException;
import be.unamur.ingelog.backend.service.cases.CaseService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static be.unamur.ingelog.backend.controller.CSRFConstants.CSRF_TOKEN;
import static be.unamur.ingelog.backend.controller.CSRFConstants.TOKEN_ATTR_NAME;
import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CaseControllerImpl.class, GlobalExceptionHandler.class})
@WebMvcTest(controllers = CaseControllerImpl.class)
class CaseControllerTest {
    @MockBean
    private CaseService caseService;

    @Autowired
    private MockMvc mock;

    private final ObjectMapper mapper = new ObjectMapper();

    private static final ReferenceDTO CASE_ONE_REFERENCE;
    private static final CaseCreateDTO CASE_ONE_CREATE;
    private static final CaseDetailDTO CASE_ONE_DETAIL;
    private static final CaseDetailDTO CASE_TWO_DETAIL;

    static {
        CASE_ONE_REFERENCE = ReferenceDTO.builder().reference("2020-DOS-LREF4").build();
        CASE_ONE_CREATE = CaseCreateDTO.builder().build();
        CASE_ONE_DETAIL = CaseDetailDTO.builder().reference("2020-DOS-LREF4").build();
        CASE_TWO_DETAIL = CaseDetailDTO.builder().reference("2020-DOS-OK3WT").build();
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void create_happyPath() throws Exception {
        when(caseService.create(any(CaseCreateDTO.class)))
                .thenReturn(CASE_ONE_REFERENCE);

        mock.perform(post("/cases")
                .sessionAttr(TOKEN_ATTR_NAME, CSRF_TOKEN)
                .param(CSRF_TOKEN.getParameterName(), CSRF_TOKEN.getToken())
                .content(mapper.writeValueAsString(CASE_ONE_CREATE))
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.reference", is("2020-DOS-LREF4")));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void create_withInvalidValues_thenErrorHasRightContent() throws Exception {
        var exception = new FieldsValidationException();
        exception.add("motive", ERR_ILLEGAL_MOTIVE);
        exception.add("triggerDate", ERR_DATE_FORMAT);

        when(caseService.create(any(CaseCreateDTO.class)))
                .thenThrow(exception);

        mock.perform(post("/cases")
                .sessionAttr(TOKEN_ATTR_NAME, CSRF_TOKEN)
                .param(CSRF_TOKEN.getParameterName(), CSRF_TOKEN.getToken())
                .content(mapper.writeValueAsString(CASE_ONE_CREATE))
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasEntry("motive", ERR_ILLEGAL_MOTIVE)))
                .andExpect(jsonPath("$.errors", hasEntry("triggerDate", ERR_DATE_FORMAT)));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findByReference_whenProvidingExistingReference() throws Exception {
        when(caseService.findByCaseReference("2020-DOS-LREF4"))
                .thenReturn(CASE_ONE_DETAIL);

        mock.perform(get("/cases/2020-DOS-LREF4"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reference", is("2020-DOS-LREF4")));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findByReference_whenProvidingNonExistingReference() throws Exception {
        when(caseService.findByCaseReference("2020-DOS-N0P3Z"))
                .thenThrow(new CaseNotFoundException());

        mock.perform(get("/cases/2020-DOS-N0P3Z"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.errors", hasItem(ERR_CASE_NOT_FOUND)));
    }
}
