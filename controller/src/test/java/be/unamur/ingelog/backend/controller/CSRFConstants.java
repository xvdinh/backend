package be.unamur.ingelog.backend.controller;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

public class CSRFConstants {
    private CSRFConstants() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public final static String TOKEN_ATTR_NAME =
            "org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN";

    public final static HttpSessionCsrfTokenRepository TOKEN_REPOSITORY =
            new HttpSessionCsrfTokenRepository();

    public final static CsrfToken CSRF_TOKEN =
            TOKEN_REPOSITORY.generateToken(new MockHttpServletRequest());
}
