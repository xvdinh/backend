package be.unamur.ingelog.backend.controller.inquiries;

import be.unamur.ingelog.backend.controller.GlobalExceptionHandler;
import be.unamur.ingelog.backend.controller.cases.inquiries.InquiryControllerImpl;
import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryDetailDTO;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.InquiryNotFoundException;
import be.unamur.ingelog.backend.service.cases.inquiries.InquiryServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static be.unamur.ingelog.backend.controller.CSRFConstants.CSRF_TOKEN;
import static be.unamur.ingelog.backend.controller.CSRFConstants.TOKEN_ATTR_NAME;
import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_INQUIRY_NOT_FOUND;
import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_NOT_BLANK;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {InquiryControllerImpl.class, GlobalExceptionHandler.class})
@WebMvcTest(controllers = InquiryControllerImpl.class)
class InquiryQueriesControllerTest {
    @MockBean
    private InquiryServiceImpl inquiryService;

    @Autowired
    private MockMvc mock;

    private final ObjectMapper mapper = new ObjectMapper();

    private static final ReferenceDTO INQUIRY_ONE_REFERENCE;
    private static final InquiryCreateDTO INQUIRY_ONE_CREATE;
    private static final InquiryDetailDTO INQUIRY_ONE_DETAIL;
    private static final InquiryDetailDTO INQUIRY_TWO_DETAIL;

    static {
        INQUIRY_ONE_REFERENCE = ReferenceDTO.builder().reference("2020-ENQ-L33TZ").build();
        INQUIRY_ONE_CREATE = InquiryCreateDTO.builder().build();
        INQUIRY_ONE_DETAIL = InquiryDetailDTO.builder().reference("2020-ENQ-L33TZ").build();
        INQUIRY_TWO_DETAIL = InquiryDetailDTO.builder().reference("2020-ENQ-L0LZZ").build();
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void create_happyPath() throws Exception {
        when(inquiryService.create(any(InquiryCreateDTO.class)))
                .thenReturn(INQUIRY_ONE_REFERENCE);

        mock.perform(post("/inquiries")
                .sessionAttr(TOKEN_ATTR_NAME, CSRF_TOKEN)
                .param(CSRF_TOKEN.getParameterName(), CSRF_TOKEN.getToken())
                .content(mapper.writeValueAsString(INQUIRY_ONE_CREATE))
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.reference", is("2020-ENQ-L33TZ")));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void create_withInvalidValues_thenErrorHasRightContent() throws Exception {
        var exception = new FieldsValidationException();
        exception.add("caseId", ERR_NOT_BLANK);
        exception.add("employerId", ERR_NOT_BLANK);

        when(inquiryService.create(any(InquiryCreateDTO.class)))
                .thenThrow(exception);

        mock.perform(post("/inquiries")
                .sessionAttr(TOKEN_ATTR_NAME, CSRF_TOKEN)
                .param(CSRF_TOKEN.getParameterName(), CSRF_TOKEN.getToken())
                .content(mapper.writeValueAsString(INQUIRY_ONE_CREATE))
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasEntry("caseId", ERR_NOT_BLANK)))
                .andExpect(jsonPath("$.errors", hasEntry("employerId", ERR_NOT_BLANK)));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findAll_hasRightSize() {
        /*when(inquiryService.findAll())
                .thenReturn(List.of(INQUIRY_ONE_DETAIL, INQUIRY_TWO_DETAIL));

        mock.perform(get("/inquiries"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));*/
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findAll_hasRightContent() {
        /*when(inquiryService.findAll())
                .thenReturn(List.of(INQUIRY_ONE_DETAIL));

        mock.perform(get("/inquiries"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].reference", is("2020-ENQ-L33TZ")));*/
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findByReference_whenProvidingExistingReference() throws Exception {
        when(inquiryService.findByInquiryReference("2020-ENQ-L33TZ"))
                .thenReturn(INQUIRY_ONE_DETAIL);

        mock.perform(get("/inquiries/2020-ENQ-L33TZ"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reference", is("2020-ENQ-L33TZ")));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findByReference_whenProvidingNonExistingReference() throws Exception {
        when(inquiryService.findByInquiryReference("2020-ENQ-N0P3Z"))
                .thenThrow(new InquiryNotFoundException());

        mock.perform(get("/inquiries/2020-ENQ-N0P3Z"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.errors", hasItem(ERR_INQUIRY_NOT_FOUND)));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findByCaseReference_hasRightSize() throws Exception {
        when(inquiryService.findByCaseReference("2020-DOS-HG43F"))
                .thenReturn(List.of(INQUIRY_ONE_DETAIL, INQUIRY_TWO_DETAIL));

        mock.perform(get("/cases/2020-DOS-HG43F/inquiries"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findByCaseReference_hasRightContent() throws Exception {
        when(inquiryService.findByCaseReference("2020-DOS-HG43F"))
                .thenReturn(List.of(INQUIRY_ONE_DETAIL));

        mock.perform(get("/cases/2020-DOS-HG43F/inquiries"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].reference", is("2020-ENQ-L33TZ")));
    }
}
