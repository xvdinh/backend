package be.unamur.ingelog.backend.controller.reports;

import be.unamur.ingelog.backend.controller.GlobalExceptionHandler;
import be.unamur.ingelog.backend.controller.cases.inquiries.reports.ReportControllerImpl;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportDetailDTO;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.service.cases.inquiries.reports.ReportService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static be.unamur.ingelog.backend.controller.CSRFConstants.CSRF_TOKEN;
import static be.unamur.ingelog.backend.controller.CSRFConstants.TOKEN_ATTR_NAME;
import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_NOT_BLANK;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ReportControllerImpl.class, GlobalExceptionHandler.class})
@WebMvcTest(controllers = ReportControllerImpl.class)
class ReportControllerTest {
    @MockBean
    private ReportService reportService;

    @Autowired
    private MockMvc mock;

    private final ObjectMapper mapper = new ObjectMapper();

    private static final ReferenceDTO REPORT_ONE_REFERENCE;
    private static final ReportCreateDTO REPORT_ONE_CREATE;
    private static final ReportDetailDTO REPORT_ONE_DETAIL;
    private static final ReportDetailDTO REPORT_TWO_DETAIL;

    static {
        REPORT_ONE_REFERENCE = ReferenceDTO.builder().reference("2020-CON-L33TZ").build();
        REPORT_ONE_CREATE = ReportCreateDTO.builder().build();
        REPORT_ONE_DETAIL = ReportDetailDTO.builder().reference("2020-CON-L33TZ").build();
        REPORT_TWO_DETAIL = ReportDetailDTO.builder().reference("2020-CON-L0LZZ").build();
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void create_happyPath() throws Exception {
        when(reportService.create(any(ReportCreateDTO.class)))
                .thenReturn(REPORT_ONE_REFERENCE);

        mock.perform(post("/reports")
                .sessionAttr(TOKEN_ATTR_NAME, CSRF_TOKEN)
                .param(CSRF_TOKEN.getParameterName(), CSRF_TOKEN.getToken())
                .content(mapper.writeValueAsString(REPORT_ONE_CREATE))
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.reference", is("2020-CON-L33TZ")));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void create_withInvalidValues_thenErrorHasRightContent() throws Exception {
        var exception = new FieldsValidationException();
        exception.add("inquiryId", ERR_NOT_BLANK);

        when(reportService.create(any(ReportCreateDTO.class)))
                .thenThrow(exception);

        mock.perform(post("/reports")
                .sessionAttr(TOKEN_ATTR_NAME, CSRF_TOKEN)
                .param(CSRF_TOKEN.getParameterName(), CSRF_TOKEN.getToken())
                .content(mapper.writeValueAsString(REPORT_ONE_CREATE))
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasEntry("inquiryId", ERR_NOT_BLANK)));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findAll_hasRightSize() throws Exception {
        when(reportService.findAllReports())
                .thenReturn(List.of(REPORT_ONE_DETAIL, REPORT_TWO_DETAIL));

        mock.perform(get("/reports"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findAll_hasRightContent() throws Exception {
        when(reportService.findAllReports())
                .thenReturn(List.of(REPORT_ONE_DETAIL));

        mock.perform(get("/reports"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].reference", is("2020-CON-L33TZ")));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findByInquiryReference_whenProvidingExistingReference() throws Exception {
        when(reportService.findByInquiryReference("2020-ENQ-HG43F"))
                .thenReturn(List.of(REPORT_ONE_DETAIL, REPORT_TWO_DETAIL));

        mock.perform(get("/inquiries/2020-ENQ-HG43F/reports"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findByInquiryReference_whenProvidingNonExistingReference() throws Exception {
        when(reportService.findByInquiryReference("2020-ENQ-HG43F"))
                .thenReturn(List.of(REPORT_ONE_DETAIL));

        mock.perform(get("/inquiries/2020-ENQ-HG43F/reports"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].reference", is("2020-CON-L33TZ")));
    }
}
