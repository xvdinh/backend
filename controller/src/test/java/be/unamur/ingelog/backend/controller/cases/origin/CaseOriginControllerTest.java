package be.unamur.ingelog.backend.controller.cases.origin;

import be.unamur.ingelog.backend.controller.cases.CaseControllerImpl;
import be.unamur.ingelog.backend.dto.cases.CaseOriginDTO;
import be.unamur.ingelog.backend.service.cases.CaseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CaseControllerImpl.class})
@WebMvcTest(controllers = CaseControllerImpl.class)
class CaseOriginControllerTest {
    @MockBean
    private CaseService caseService;

    @Autowired
    private MockMvc mock;

    private CaseOriginDTO anonymous;
    private CaseOriginDTO police;

    @BeforeEach
    void setUp() {
        anonymous = CaseOriginDTO.builder()
                .reference("anonymous")
                .motiveReference("denunciation")
                .build();
        police = CaseOriginDTO.builder()
                .reference("police")
                .motiveReference("investigation_request")
                .build();
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findAll_hasRightSize() throws Exception {
        when(caseService.findAllOrigins())
                .thenReturn(List.of(anonymous, police));

        mock.perform(get("/origins"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @WithMockUser(roles = {"INSPECTOR"})
    void findAll_hasRightContent() throws Exception {
        when(caseService.findAllOrigins())
                .thenReturn(List.of(anonymous));

        mock.perform(get("/origins"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].reference", is("anonymous")))
                .andExpect(jsonPath("$[0].motiveReference", is("denunciation")));
    }
}
