package be.unamur.ingelog.backend.controller.employers;

import be.unamur.ingelog.backend.dto.employers.EmployerDetailDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface EmployerController {
    /**
     * Trouve l'ensemble des employeurs.
     *
     * @return l'ensemble des employeurs
     */
    @GetMapping(
            value = "/employers",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<EmployerDetailDTO> findAllEmployers();

    /**
     * Trouve un employeur sur base de la référence fournie.
     *
     * @param reference la référence de l'employeur à trouver
     * @return l'employeur correspondant à la référence, s'il existe
     */
    @GetMapping(
            value = "/employers/{reference}",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    EmployerDetailDTO findByEmployerReference(@PathVariable("reference") String reference);
}
