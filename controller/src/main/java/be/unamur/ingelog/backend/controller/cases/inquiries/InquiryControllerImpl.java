package be.unamur.ingelog.backend.controller.cases.inquiries;

import be.unamur.ingelog.backend.dto.agents.AgentDetailDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.service.cases.inquiries.InquiryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class InquiryControllerImpl implements InquiryController {
    private final InquiryService inquiryService;

    @Autowired
    public InquiryControllerImpl(InquiryService inquiryService) {
        this.inquiryService = inquiryService;
    }

    @Override
    @PreAuthorize("hasAuthority('CREATE_CASES')")
    public ReferenceDTO create(InquiryCreateDTO dto) {
        return inquiryService.create(dto);
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_CASES')")
    public List<InquiryDetailDTO> findByCaseReference(String caseReference) {
        return inquiryService.findByCaseReference(caseReference.toUpperCase());
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_CASES')")
    public InquiryDetailDTO findByInquiryReference(String inquiryReference) {
        return inquiryService.findByInquiryReference(inquiryReference.toUpperCase());
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_CASES')")
    public List<InquiryOverviewDTO> findAllInquiries(List<String> states,
                                                     List<String> excludeStates,
                                                     String result,
                                                     String assignee,
                                                     String supervisor) {
        var filter = InquiryFilterDTO.builder()
                .states(states)
                .excludeStates(excludeStates)
                .result(result)
                .assignee(assignee)
                .supervisor(supervisor)
                .build();
        return inquiryService.findAllInquiries(filter);
    }

    @Override
    @PreAuthorize("hasAuthority('ASK_VALIDATION')")
    public void askValidation(String inquiryReference) {
        inquiryService.askValidation(inquiryReference);
    }

    @Override
    @PreAuthorize("hasAuthority('VALIDATE')")
    public void validate(String inquiryReference) {
        inquiryService.validate(inquiryReference);
    }

    @Override
    @PreAuthorize("hasAuthority('INVALIDATE')")
    public void invalidate(String inquiryReference) {
        inquiryService.invalidate(inquiryReference);
    }

    @Override
    @PreAuthorize("hasAuthority('CLOSE_INQUIRY')")
    public InquiryDismissClosureDTO dismissAndClose(String inquiryReference) {
        return inquiryService.dismissAndClose(inquiryReference);
    }

    @Override
    @PreAuthorize("hasAuthority('CLOSE_INQUIRY')")
    public InquiryClosureDTO close(String inquiryReference) {
        return inquiryService.close(inquiryReference);
    }

    @Override
    @PreAuthorize("hasAuthority('ASSIGN')")
    public void assign(String inquiryReference, AgentDetailDTO agent) {
        var dto = new InquiryAssignDTO(inquiryReference, agent.getUsername(),
                SecurityContextHolder.getContext().getAuthentication().getName());

        inquiryService.assign(dto);
    }

    @Override
    @PreAuthorize("@securityService.isAssigneeByInquiryReference(#inquiryReference)")
    public InquiryStateDTO updateReportDate(String inquiryReference, InquiryReportDateUpdateDTO dto) {
        return inquiryService.updateReportDate(inquiryReference, dto);
    }

    @Override
    @PreAuthorize("@securityService.isAssigneeByInquiryReference(#inquiryReference)")
    public InquiryStateDTO updateResult(String inquiryReference, InquiryResultUpdateDTO dto) {
        return inquiryService.updateResult(inquiryReference, dto);
    }

    @Override
    @PreAuthorize("hasAuthority('SEND_BACK_INQUIRY')")
    public InquiryStateDTO sendBack(String inquiryReference) {
        return inquiryService.sendBack(inquiryReference);
    }

    @Override
    public InquiryStateDTO archive(String inquiryReference) {
        return inquiryService.archive(inquiryReference);
    }

    @Override
    @PreAuthorize("hasAuthority('UPDATE_INQUIRY_RESULT')")
    public List<InquiryResultDTO> findAllResults() {
        return inquiryService.findAllResults();
    }

    @Override
    public List<InquiryTargetDTO> findByTargetedEmployer(String employerReference) {
        return inquiryService.findByTargetedEmployer(employerReference);
    }

    @Override
    public InquirySummaryDTO getSummary(String inquiryReference) {
        return inquiryService.getSummary(inquiryReference);
    }
}
