package be.unamur.ingelog.backend.controller.law.articles;

import be.unamur.ingelog.backend.dto.law.articles.OffenceOverviewDTO;
import be.unamur.ingelog.backend.service.law.articles.OffenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class OffenceControllerImpl implements OffenceController {
    private final OffenceService service;

    @Autowired
    public OffenceControllerImpl(OffenceService service) {
        this.service = service;
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_OFFENCES')")
    public List<OffenceOverviewDTO> findAll(Map<String, String> filters) {
        return service.findAll(filters);
    }
}
