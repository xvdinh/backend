package be.unamur.ingelog.backend.controller.cases.comments;

import be.unamur.ingelog.backend.dto.cases.comments.CommentCreateDTO;
import be.unamur.ingelog.backend.dto.cases.comments.CommentDetailDTO;
import be.unamur.ingelog.backend.service.cases.comments.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CommentControllerImpl implements CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentControllerImpl(CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    @PreAuthorize("hasAuthority('CREATE_COMMENTS')")
    public CommentDetailDTO create(CommentCreateDTO comment, Authentication authentication) {
        return commentService.create(comment, authentication);
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_COMMENTS')")
    public List<CommentDetailDTO> findByCaseReference(String caseReference) {
        return commentService.findByCaseReference(caseReference);
    }
}
