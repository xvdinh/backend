package be.unamur.ingelog.backend.controller.cases.comments;

import be.unamur.ingelog.backend.dto.cases.comments.CommentCreateDTO;
import be.unamur.ingelog.backend.dto.cases.comments.CommentDetailDTO;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface CommentController {
    /**
     * Ajoute un commentaire à un dossier.
     *
     * @param comment le commentaire à ajouter
     * @return le commentaire nouvellement créé
     */
    @PostMapping(
            value = "/comments",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    CommentDetailDTO create(@RequestBody CommentCreateDTO comment, Authentication authentication);

    /**
     * Trouve les commentaires liés à la référence dossier fournie.
     *
     * @param caseReference la référence du dossier pour lequel trouver les commentaires liés
     * @return la liste des commentaires liés au dossier
     */
    @GetMapping(
            value = "/cases/{caseReference}/comments",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<CommentDetailDTO> findByCaseReference(@PathVariable("caseReference") String caseReference);
}
