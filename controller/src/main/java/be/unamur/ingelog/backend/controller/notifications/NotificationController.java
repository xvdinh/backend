package be.unamur.ingelog.backend.controller.notifications;

import be.unamur.ingelog.backend.dto.notifications.NotificationDetailDTO;
import be.unamur.ingelog.backend.dto.notifications.NotificationUpdateDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface NotificationController {
    /**
     * Retourne la liste des notifications de l'utilisateur spécifié.
     *
     * @return la liste des notifications
     */
    @GetMapping(
            value = "/notifications",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<NotificationDetailDTO> findByUsername(@RequestParam(value = "consulted", required = false) Boolean isRead);

    /**
     * Indique qu'une notification a été consultée.
     *
     * @param dto la notification à mettre à jour
     */
    @PutMapping(
            value = "/notifications",
            consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    void markAsConsulted(@RequestBody NotificationUpdateDTO dto);
}
