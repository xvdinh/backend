package be.unamur.ingelog.backend.controller.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportStateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationDetailDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationNumberDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationRoadmapDTO;
import be.unamur.ingelog.backend.service.cases.inquiries.reports.observations.ObservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ObservationControllerImpl implements ObservationController {
    private final ObservationService service;

    @Autowired
    public ObservationControllerImpl(ObservationService service) {
        this.service = service;
    }

    @Override
    @PreAuthorize("hasAuthority('CREATE_OBSERVATIONS')")
    public ObservationNumberDTO create(ObservationCreateDTO dto) {
        return service.create(dto);
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_OBSERVATIONS')")
    public List<ObservationDetailDTO> findByReportReference(String reportReference) {
        return service.findByReportReference(reportReference);
    }

    @Override
    @PreAuthorize("hasAuthority('ENCODE_ROADMAP') or " +
            "@securityService.isAssigneeByReportReference(#dto.reportReference)")
    public ReportStateDTO encodeRoadmap(ObservationRoadmapDTO dto) {
        return service.encodeRoadmap(dto);
    }
}
