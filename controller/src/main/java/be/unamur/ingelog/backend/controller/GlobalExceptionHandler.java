package be.unamur.ingelog.backend.controller;

import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.infrastructure.exceptions.LabeledException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;

import java.nio.file.NoSuchFileException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.*;
import static java.util.Collections.singletonList;
import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(LabeledException.class)
    public ResponseEntity<Object> handleLabeledException(LabeledException exception) {
        HttpStatus status = exception.getStatus();
        String error = exception.getLabel();

        return error(status, singletonList(error));
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<Object> handleBadCredentialsException(BadCredentialsException exception) {
        String error = exception.getMessage();
        return error(UNAUTHORIZED, singletonList(error));
    }

    @ExceptionHandler(MultipartException.class)
    public ResponseEntity<Object> handleMultipartException() {
        return error(BAD_REQUEST, singletonList(ERR_ATTACHMENT_TYPE));
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<Object> handleMaxIploadSizeExceededException() {
        return error(BAD_REQUEST, singletonList(ERR_ATTACHMENT_SIZE));
    }

    @ExceptionHandler(NoSuchFileException.class)
    public ResponseEntity<Object> handleNoSuchFileException() {
        return error(INTERNAL_SERVER_ERROR, singletonList(ERR_INTERNAL_SERVER_ERROR));
    }

    @ExceptionHandler(FieldsValidationException.class)
    public ResponseEntity<Object> handleFieldsValidationException(FieldsValidationException exception) {
        Map<String, String> errors = exception.getErrorsByFields();

        return error(errors);
    }

    private ResponseEntity<Object> error(Map<String, String> errors) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", BAD_REQUEST);
        body.put("errors", errors);

        return new ResponseEntity<>(body, BAD_REQUEST);
    }

    private ResponseEntity<Object> error(HttpStatus status, List<String> errors) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());
        body.put("errors", errors);

        return new ResponseEntity<>(body, status);
    }
}
