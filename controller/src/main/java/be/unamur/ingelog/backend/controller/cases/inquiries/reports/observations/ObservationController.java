package be.unamur.ingelog.backend.controller.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportStateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationDetailDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationNumberDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationRoadmapDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface ObservationController {
    /**
     * Crée une nouvelle observation.
     *
     * @param dto l'observation à créer
     * @return l'observation nouvellement créée
     */
    @PostMapping(
            value = "/observations",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(CREATED)
    ObservationNumberDTO create(@RequestBody ObservationCreateDTO dto);

    /**
     * Trouve les observations liées à la référence de constatation fournie.
     *
     * @param reportReference la référence de la constatation pour laquelle trouver les observations liées
     * @return la liste des observations liées à la constatation
     */
    @GetMapping(
            value = "/reports/{reportReference}/observations",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<ObservationDetailDTO> findByReportReference(@PathVariable("reportReference") String reportReference);

    /**
     * Encode la feuille de route.
     *
     * @param dto les informations de la feuille de route à encoder
     * @return l'état de la constation liée à la feuille de route après encodage
     */
    @PutMapping(
            value = "/observations",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    ReportStateDTO encodeRoadmap(@RequestBody ObservationRoadmapDTO dto);
}
