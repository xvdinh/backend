package be.unamur.ingelog.backend.controller.statistics;

import be.unamur.ingelog.backend.dto.statistics.CollectionOfInquiriesDTO;
import be.unamur.ingelog.backend.service.statistics.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class StatisticControllerImpl implements StatisticController {
    private final StatisticService service;

    @Autowired
    public StatisticControllerImpl(StatisticService service) {
        this.service = service;
    }

    @Override
    public CollectionOfInquiriesDTO findInquiriesData(Map<String, String> filter) {
        return service.findInquiriesData(filter);
    }
}
