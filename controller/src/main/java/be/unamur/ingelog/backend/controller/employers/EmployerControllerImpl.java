package be.unamur.ingelog.backend.controller.employers;

import be.unamur.ingelog.backend.dto.employers.EmployerDetailDTO;
import be.unamur.ingelog.backend.service.employers.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployerControllerImpl implements EmployerController {
    private final EmployerService service;

    @Autowired
    public EmployerControllerImpl(EmployerService service) {
        this.service = service;
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_EMPLOYERS')")
    public List<EmployerDetailDTO> findAllEmployers() {
        return service.findAllEmployers();
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_EMPLOYERS')")
    public EmployerDetailDTO findByEmployerReference(String reference) {
        return service.findByEmployerReference(reference.toUpperCase());
    }
}
