package be.unamur.ingelog.backend.controller.law.articles;

import be.unamur.ingelog.backend.dto.law.articles.OffenceOverviewDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface OffenceController {
    /**
     * Trouve l'ensemble des infractions.
     *
     * @return l'ensemble des infractions
     */
    @GetMapping(
            value = "/offences",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<OffenceOverviewDTO> findAll(@RequestParam Map<String, String> filters);
}
