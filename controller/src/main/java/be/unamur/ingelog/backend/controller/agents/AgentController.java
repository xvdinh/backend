package be.unamur.ingelog.backend.controller.agents;

import be.unamur.ingelog.backend.dto.agents.AgentDetailDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface AgentController {
    /**
     * Retourne la liste des agents, filtrée selon les critères fournis.
     *
     * @param roles les rôles des agents
     * @return la liste filtrée selon les critères fournis
     */
    @GetMapping(
            value = "/agents",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<AgentDetailDTO> findAll(@RequestParam(value = "role", required = false) List<String> roles);

    /**
     * Trouve un agent sur base de son nom d'utilisateur.
     *
     * @param username le nom d'utilisateur
     * @return l'agent correspondant au nom d'utilisateur
     */
    @GetMapping(
            value = "/agents/{username}",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    AgentDetailDTO findByUsername(@PathVariable("username") String username);
}
