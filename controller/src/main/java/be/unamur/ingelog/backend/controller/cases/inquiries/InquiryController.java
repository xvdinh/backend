package be.unamur.ingelog.backend.controller.cases.inquiries;

import be.unamur.ingelog.backend.dto.agents.AgentDetailDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface InquiryController {
    /**
     * Crée une nouvelle enquête.
     *
     * @param dto l'enquête à créer
     * @return l'enquête nouvellement créée
     */
    @PostMapping(
            value = "/inquiries",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(CREATED)
    ReferenceDTO create(@RequestBody InquiryCreateDTO dto);

    /**
     * Retourne la liste des enquêtes associées au dossier dont la référence est fournie.
     *
     * @param caseReference la référence du dossier dont on veut obtenir les enquêtes associées
     * @return la liste des enquêtes associées au dossier
     */
    @GetMapping(
            value = "/cases/{caseReference}/inquiries",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<InquiryDetailDTO> findByCaseReference(@PathVariable("caseReference") String caseReference);

    /**
     * Trouve une enquête sur base de la référence fournie.
     *
     * @param inquiryReference la référence de l'enquête à trouver
     * @return l'enquête correspondant à la référence, si elle existe
     */
    @GetMapping(
            value = "/inquiries/{inquiryReference}",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    InquiryDetailDTO findByInquiryReference(@PathVariable("inquiryReference") String inquiryReference);

    /**
     * Retourne la liste de toutes les enquêtes.
     *
     * @return la liste de toutes les enquêtes
     */
    @GetMapping(
            value = "/inquiries",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<InquiryOverviewDTO> findAllInquiries(
            @RequestParam(value = "state", required = false) List<String> states,
            @RequestParam(value = "exclude_state", required = false) List<String> excludeStates,
            @RequestParam(value = "result", required = false) String result,
            @RequestParam(value = "assignee", required = false) String assignee,
            @RequestParam(value = "supervisor", required = false) String supervisor
    );

    /**
     * Passe l'enquête en demande de validation.
     *
     * @param inquiryReference la référence de l'enquête
     */
    @PostMapping(
            value = "/inquiries/{inquiryReference}/ask_validation")
    @ResponseStatus(OK)
    void askValidation(@PathVariable("inquiryReference") String inquiryReference);

    /**
     * Valide une enquête en demande de validation.
     *
     * @param inquiryReference la référence de l'enquête
     */
    @PostMapping(
            value = "/inquiries/{inquiryReference}/validate")
    @ResponseStatus(OK)
    void validate(@PathVariable("inquiryReference") String inquiryReference);

    /**
     * Invalide une enquête en demande de validation.
     *
     * @param inquiryReference la référence de l'enquête
     */
    @PostMapping(
            value = "/inquiries/{inquiryReference}/invalidate")
    @ResponseStatus(OK)
    void invalidate(@PathVariable("inquiryReference") String inquiryReference);

    /**
     * Clôture sans suite une enquête en demande de validation.
     *
     * @param inquiryReference la référence de l'enquête
     * @return les informations relatives à la clôture sans suite
     */
    @PostMapping(
            value = "/inquiries/{inquiryReference}/dismiss_and_close")
    @ResponseStatus(OK)
    InquiryDismissClosureDTO dismissAndClose(@PathVariable("inquiryReference") String inquiryReference);

    /**
     * Clôture une enquête en demande de clôture.
     *
     * @param inquiryReference la référence de l'enquête
     * @return les informations relatives à la clôture
     */
    @PostMapping(
            value = "/inquiries/{inquiryReference}/close")
    @ResponseStatus(OK)
    InquiryClosureDTO close(@PathVariable("inquiryReference") String inquiryReference);

    /**
     * Assigne un agent à une enquête.
     *
     * @param inquiryReference la référence de l'enquête
     * @param agent            l'agent à assigner
     */
    @PostMapping(
            value = "/inquiries/{inquiryReference}/assign",
            consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    void assign(@PathVariable("inquiryReference") String inquiryReference,
                @RequestBody AgentDetailDTO agent);

    /**
     * Modifie la date de constatation.
     *
     * @param inquiryReference la référence de l'enquête
     * @param dto              les données à mettre à jour
     */
    @PutMapping(
            value = "/inquiries/{inquiryReference}",
            consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    InquiryStateDTO updateReportDate(@PathVariable("inquiryReference") String inquiryReference,
                                     @RequestBody InquiryReportDateUpdateDTO dto);

    /**
     * Modifie le résultat de l'enquête.
     *
     * @param inquiryReference la référence de l'enquête
     * @param dto              les données à mettre à jour
     * @return l'état de l'enquête
     */
    @PutMapping(
            value = "/inquiries/{inquiryReference}/result",
            consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    InquiryStateDTO updateResult(@PathVariable("inquiryReference") String inquiryReference,
                                 @RequestBody InquiryResultUpdateDTO dto);

    /**
     * Renvoie l'enquête en état complété.
     *
     * @param inquiryReference la référence de l'enquête
     * @return les états de l'enquête et du dossier lié
     */
    @PostMapping(
            value = "/inquiries/{inquiryReference}/send_back",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    InquiryStateDTO sendBack(@PathVariable("inquiryReference") String inquiryReference);

    /**
     * Archive l'enquête.
     *
     * @param inquiryReference la référence de l'enquête
     * @return l'état de l'enquête
     */
    @PostMapping(
            value = "/inquiries/{inquiryReference}/archive",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    InquiryStateDTO archive(@PathVariable("inquiryReference") String inquiryReference);

    /**
     * Retourne la liste des résultats possibles pour une enquête.
     *
     * @return la liste des résultats d'enquête
     */
    @GetMapping(
            value = "/inquiry_results",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<InquiryResultDTO> findAllResults();

    /**
     * Retourne la liste des enquêtes liées à un employeur spécifié.
     *
     * @param employerReference la référence de l'employeur
     * @return la liste des enquêtes liées
     */
    @GetMapping(
            value = "/employers/{employerReference}/inquiries",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<InquiryTargetDTO> findByTargetedEmployer(@PathVariable("employerReference") String employerReference);

    /**
     * Retourne le compte rendu de l'enquête spécifiée.
     *
     * @param inquiryReference la référence de l'enquête
     * @return le compte rendu de l'enquête
     */
    @GetMapping(
            value = "/inquiries/{inquiryReference}/summary",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    InquirySummaryDTO getSummary(@PathVariable("inquiryReference") String inquiryReference);
}
