package be.unamur.ingelog.backend.controller.cases;

import be.unamur.ingelog.backend.dto.cases.CaseCreateDTO;
import be.unamur.ingelog.backend.dto.cases.CaseDetailDTO;
import be.unamur.ingelog.backend.dto.cases.CaseMotiveDTO;
import be.unamur.ingelog.backend.dto.cases.CaseOriginDTO;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface CaseController {
    /**
     * Crée un nouveau dossier.
     *
     * @param dto le dossier à créer
     * @return le dossier nouvellement créé
     */
    @PostMapping(
            value = "/cases",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(CREATED)
    ReferenceDTO create(@RequestBody CaseCreateDTO dto);

    /**
     * Trouve la liste complète des dossiers.
     *
     * @return la liste complète des dossiers
     */
    @GetMapping(
            value = "/cases",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<CaseDetailDTO> findAllCases(@RequestParam(value = "state", required = false) List<String> states);

    /**
     * Trouve un dossier sur base de la référence fournie.
     *
     * @param reference la référence du dossier à trouver
     * @return le dossier correspondant à la référence, si elle existe
     */
    @GetMapping(
            value = "/cases/{reference}",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    CaseDetailDTO findByCaseReference(@PathVariable("reference") String reference);

    /**
     * Renvoie la liste des motifs possibles pour un dossier.
     *
     * @return la liste des motifs possibles
     */
    @GetMapping(
            value = "/motives",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<CaseMotiveDTO> findAllMotives();

    /**
     * Renvoie la liste de toutes les origines disponibles.
     *
     * @return la liste de toutes les origines
     */
    @GetMapping(
            value = "/origins",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<CaseOriginDTO> findAllOrigins();
}
