package be.unamur.ingelog.backend.controller.dashboard;

import be.unamur.ingelog.backend.dto.dashboard.*;
import be.unamur.ingelog.backend.service.dashboard.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DashboardControllerImpl implements DashboardController {
    private final DashboardService dashboardService;

    @Autowired
    public DashboardControllerImpl(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    @Override
    @PreAuthorize("hasAuthority('CONSULT_DASHBOARD')")
    public List<TotalInquiriesByStateDTO> getTotalInquiriesByState() {
        return dashboardService.getTotalInquiriesByState();
    }

    @Override
    @PreAuthorize("hasAuthority('CONSULT_DASHBOARD')")
    public List<TotalInquiriesByPriorityDTO> getTotalInquiriesByPriority() {
        return dashboardService.getTotalInquiriesByPriority();
    }

    @Override
    @PreAuthorize("hasAuthority('CONSULT_DASHBOARD')")
    public List<InquiriesDistributionByResultDTO> getInquiriesDistributionByResult() {
        return dashboardService.getInquiriesDistributionByResult();
    }

    @Override
    @PreAuthorize("hasAuthority('CONSULT_DASHBOARD')")
    public List<TotalClosedInquiriesByYearDTO> getTotalClosedInquiriesByYear() {
        return dashboardService.getTotalClosedInquiriesByYear();
    }

    @Override
    @PreAuthorize("hasAuthority('CONSULT_DASHBOARD')")
    public List<TotalInquiriesByAgentDTO> getTotalInquiriesByAgent() {
        return dashboardService.getTotalInquiriesByAgent();
    }
}
