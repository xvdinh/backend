package be.unamur.ingelog.backend.controller.statistics;

import be.unamur.ingelog.backend.dto.statistics.CollectionOfInquiriesDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Map;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface StatisticController {
    /**
     * Retourne les données des enquêtes filtrées selon le filtre fourni.
     *
     * @param filter les critères de filtrage
     * @return les données des enquêtes filtrées
     */
    @GetMapping(
            value = "/statistics",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    CollectionOfInquiriesDTO findInquiriesData(@RequestParam(required = false) Map<String, String> filter);
}
