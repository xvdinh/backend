package be.unamur.ingelog.backend.controller.cases.inquiries.reports;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface ReportController {
    /**
     * Crée une nouvelle constatation.
     *
     * @param dto la constatation à créer
     * @return la constatation nouvellement créée
     */
    @PostMapping(
            value = "/reports",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(CREATED)
    ReferenceDTO create(@RequestBody ReportCreateDTO dto);

    /**
     * Trouve l'ensemble des constatations.
     *
     * @return la liste complète des constatations
     */
    @GetMapping(
            value = "/reports",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<ReportDetailDTO> findAllReports();

    /**
     * Retourne la liste des constatations associées à l'enquête dont la référence est fournie.
     *
     * @param inquiryReference la référence de l'enquête dont on veut obtenir les constatations associées
     * @return la liste des constatations associées à l'enquête
     */
    @GetMapping(
            value = "/inquiries/{inquiryReference}/reports",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<ReportDetailDTO> findByInquiryReference(@PathVariable("inquiryReference") String inquiryReference);

    /**
     * Trouve une constatation sur base de référence.
     *
     * @param reportReference la référence de la constatation
     * @return la constatation correspondant à la référence
     */
    @GetMapping(
            value = "/reports/{reportReference}",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    ReportDetailDTO findByReportReference(@PathVariable("reportReference") String reportReference);

    /**
     * Modifie le nombre de personnes contrôlées avec la valeur fournie.
     *
     * @param dto             le nombre de personnes contrôlées
     * @param reportReference la référence de la constatation à modifier
     * @return l'état de la constatation après modification
     */
    @PutMapping(
            value = "/reports/{reportReference}/controls_count",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    ReportStateDTO updateControlsCount(@RequestBody ReportControlsUpdateDTO dto,
                                       @PathVariable("reportReference") String reportReference);

    /**
     * Modifie le statut de la constatation avec la valeur fournie.
     *
     * @param dto             la valeur du statut
     * @param reportReference la référence de la constatation à modifier
     * @return l'état de la constatation après modification
     */
    @PutMapping(
            value = "/reports/{reportReference}/status",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    ReportStateDTO updateReportStatus(@RequestBody ReportStatusUpdateDTO dto,
                                      @PathVariable("reportReference") String reportReference);

    /**
     * Retourne la liste de tous les statuts de constatation disponibles.
     *
     * @return la liste de toutes les statuts de constatation
     */
    @GetMapping(
            value = "/report_status",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<ReportStatusUpdateDTO> findAllStatus();
}
