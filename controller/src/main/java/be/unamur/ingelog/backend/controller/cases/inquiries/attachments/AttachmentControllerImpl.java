package be.unamur.ingelog.backend.controller.cases.inquiries.attachments;

import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentDetailDTO;
import be.unamur.ingelog.backend.service.cases.inquiries.attachments.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
public class AttachmentControllerImpl implements AttachmentController {
    private final AttachmentService attachmentService;

    @Autowired
    public AttachmentControllerImpl(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @Override
    public AttachmentDetailDTO upload(MultipartFile attachment,
                                      String inquiryReference,
                                      String source,
                                      String documentType) throws IOException {
        var metadata = AttachmentCreateDTO.builder()
                .inquiryReference(inquiryReference)
                .documentType(documentType)
                .source(source)
                .build();

        return attachmentService.upload(attachment, metadata);
    }

    @Override
    public List<AttachmentDetailDTO> findByInquiryReference(String inquiryReference) {
        return attachmentService.findByInquiryReference(inquiryReference);
    }

    @Override
    public ResponseEntity<Resource> download(String attachmentReference) throws IOException {
        return attachmentService.download(attachmentReference);
    }
}
