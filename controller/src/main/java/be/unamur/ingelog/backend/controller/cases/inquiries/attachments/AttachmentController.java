package be.unamur.ingelog.backend.controller.cases.inquiries.attachments;

import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentDetailDTO;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface AttachmentController {
    /**
     * Uploade une nouvelle pièce jointe.
     *
     * @param attachment       la pièce jointe
     * @param inquiryReference la référence de l'enquête
     * @param source           la source de la pièce jointe
     * @param documentType     le type de document
     * @return les meta données de la pièce jointe
     * @throws IOException si un problème d'écriture a été rencontré
     */
    @PostMapping(
            value = "/attachments",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    AttachmentDetailDTO upload(
            @RequestParam(name = "attachment") MultipartFile attachment,
            @RequestParam(name = "inquiryReference", required = false) String inquiryReference,
            @RequestParam(name = "source", required = false) String source,
            @RequestParam(name = "documentType", required = false) String documentType
    ) throws IOException;

    /**
     * Trouve les pièces jointes liées à l'enquête spécifiée.
     *
     * @param inquiryReference la référence de l'enquête
     * @return les pièces jointes liées
     */
    @GetMapping(
            value = "/inquiries/{inquiryReference}/attachments",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<AttachmentDetailDTO> findByInquiryReference(@PathVariable("inquiryReference") String inquiryReference);

    /**
     * Télécharge une pièce jointe.
     *
     * @param attachmentReference la référence de la pièce jointe
     * @return la pièce jointe demandée
     */
    @GetMapping(
            value = "/attachments/{attachmentReference}")
    @ResponseStatus(OK)
    ResponseEntity<Resource> download(@PathVariable("attachmentReference") String attachmentReference) throws IOException;
}
