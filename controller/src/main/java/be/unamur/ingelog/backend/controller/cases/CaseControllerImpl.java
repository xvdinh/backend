package be.unamur.ingelog.backend.controller.cases;

import be.unamur.ingelog.backend.dto.cases.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.service.cases.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CaseControllerImpl implements CaseController {
    private final CaseService caseService;

    @Autowired
    public CaseControllerImpl(CaseService caseService) {
        this.caseService = caseService;
    }

    @Override
    @PreAuthorize("hasAuthority('CREATE_CASES')")
    public ReferenceDTO create(CaseCreateDTO dto) {
        return caseService.create(dto);
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_CASES')")
    public List<CaseDetailDTO> findAllCases(List<String> states) {
        var filter = CaseFilterDTO.builder()
                .states(states)
                .build();

        return caseService.findAllCases(filter);
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_CASES')")
    public CaseDetailDTO findByCaseReference(String caseReference) {
        return caseService.findByCaseReference(caseReference.toUpperCase());
    }

    @Override
    @PreAuthorize("hasAuthority('CREATE_CASES')")
    public List<CaseMotiveDTO> findAllMotives() {
        return caseService.findAllMotives();
    }

    @Override
    @PreAuthorize("hasAuthority('CREATE_CASES')")
    public List<CaseOriginDTO> findAllOrigins() {
        return caseService.findAllOrigins();
    }
}
