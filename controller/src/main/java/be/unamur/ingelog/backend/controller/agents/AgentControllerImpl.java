package be.unamur.ingelog.backend.controller.agents;

import be.unamur.ingelog.backend.dto.agents.AgentDetailDTO;
import be.unamur.ingelog.backend.service.agents.AgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AgentControllerImpl implements AgentController {
    private final AgentService service;

    @Autowired
    public AgentControllerImpl(AgentService service) {
        this.service = service;
    }

    @Override
    public List<AgentDetailDTO> findAll(List<String> roles) {
        return service.findAll(roles);
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_CASES')")
    public AgentDetailDTO findByUsername(String username) {
        return service.findByUsername(username.toUpperCase());
    }
}
