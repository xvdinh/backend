package be.unamur.ingelog.backend.controller.dashboard;

import be.unamur.ingelog.backend.dto.dashboard.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface DashboardController {
    /**
     * Retourne le nombre d'enquêtes par état pour l'année courante.
     *
     * @return le nombre d'enquêtes par état
     */
    @GetMapping(
            value = "/dashboard/total-inquiries-by-state",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<TotalInquiriesByStateDTO> getTotalInquiriesByState();

    /**
     * Retourne le nombre d'enquêtes, en analyse ou ouvertes, par niveau de priorité, pour l'année courante.
     *
     * @return le nombre d'enquêtes par priorité
     */
    @GetMapping(
            value = "/dashboard/total-inquiries-by-priority",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<TotalInquiriesByPriorityDTO> getTotalInquiriesByPriority();

    /**
     * Retourne la répartition entre positif et négatif, parmi les enquêtes clôturées pour l'année courante.
     *
     * @return la répartition des enquêtes par résultat
     */
    @GetMapping(
            value = "/dashboard/inquiries-distribution-by-result",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<InquiriesDistributionByResultDTO> getInquiriesDistributionByResult();

    /**
     * Retourne le nombre d'enquêtes sans suite, cloturées négativement et
     * cloturées positivement par année, pour les 5 dernières années..
     *
     * @return le nombre d'enquêtes clôtureées par année
     */
    @GetMapping(
            value = "/dashboard/total-closed-inquiries-by-year",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<TotalClosedInquiriesByYearDTO> getTotalClosedInquiriesByYear();

    /**
     * Retourne le nombre d'enquêtes, en analyse ou ouvertes, par intervenant, pour l'année courante.
     *
     * @return le nombre d'enquêtes par intervenant
     */
    @GetMapping(
            value = "/dashboard/total-inquiries-by-agent",
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(OK)
    List<TotalInquiriesByAgentDTO> getTotalInquiriesByAgent();
}
