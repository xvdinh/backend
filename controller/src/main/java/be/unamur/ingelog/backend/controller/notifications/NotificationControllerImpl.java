package be.unamur.ingelog.backend.controller.notifications;

import be.unamur.ingelog.backend.dto.notifications.NotificationDetailDTO;
import be.unamur.ingelog.backend.dto.notifications.NotificationUpdateDTO;
import be.unamur.ingelog.backend.service.notifications.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class NotificationControllerImpl implements NotificationController {
    private final NotificationService notificationService;

    @Autowired
    public NotificationControllerImpl(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    public List<NotificationDetailDTO> findByUsername(Boolean isRead) {
        return notificationService.findByUsername(isRead);
    }

    @Override
    @PreAuthorize("@securityService.isNotificationRecipient(#dto)")
    public void markAsConsulted(NotificationUpdateDTO dto) {
        notificationService.markAsConsulted(dto);
    }
}
