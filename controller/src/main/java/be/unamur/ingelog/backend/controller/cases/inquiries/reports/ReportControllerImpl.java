package be.unamur.ingelog.backend.controller.cases.inquiries.reports;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.service.cases.inquiries.reports.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ReportControllerImpl implements ReportController {
    private final ReportService reportService;

    @Autowired
    public ReportControllerImpl(ReportService reportService) {
        this.reportService = reportService;
    }

    @Override
    @PreAuthorize("hasAuthority('CREATE_CASES')")
    public ReferenceDTO create(ReportCreateDTO dto) {
        return reportService.create(dto);
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_CASES')")
    public List<ReportDetailDTO> findAllReports() {
        return reportService.findAllReports();
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_CASES')")
    public List<ReportDetailDTO> findByInquiryReference(String inquiryReference) {
        return reportService.findByInquiryReference(inquiryReference.toUpperCase());
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_CASES')")
    public ReportDetailDTO findByReportReference(String reportReference) {
        return reportService.findByReportReference(reportReference.toUpperCase());
    }

    @Override
    @PreAuthorize("hasAuthority('ENCODE_CONTROLS_COUNT') " +
            "or @securityService.isAssigneeByReportReference(#reportReference)")
    public ReportStateDTO updateControlsCount(ReportControlsUpdateDTO dto, String reportReference) {
        return reportService.updateControlsCount(dto, reportReference);
    }

    @Override
    @PreAuthorize("@securityService.isAssigneeByReportReference(#reportReference)")
    public ReportStateDTO updateReportStatus(ReportStatusUpdateDTO dto, String reportReference) {
        return reportService.updateStatus(dto, reportReference);
    }

    @Override
    @PreAuthorize("hasAuthority('FIND_REPORT_STATUS')")
    public List<ReportStatusUpdateDTO> findAllStatus() {
        return reportService.findAllStatus();
    }
}
