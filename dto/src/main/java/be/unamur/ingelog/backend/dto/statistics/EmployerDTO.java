package be.unamur.ingelog.backend.dto.statistics;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class EmployerDTO {
    private String legalName;
    private String country;
    private String bce;
    private String vat;
}
