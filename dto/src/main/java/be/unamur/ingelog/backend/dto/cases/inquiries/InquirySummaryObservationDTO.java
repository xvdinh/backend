package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class InquirySummaryObservationDTO {
    private String title;
    private String value;
    private String unit;
    private String description;
}
