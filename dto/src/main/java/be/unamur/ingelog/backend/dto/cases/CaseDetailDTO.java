package be.unamur.ingelog.backend.dto.cases;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Value
@Builder
@AllArgsConstructor
public class CaseDetailDTO {
    private final String reference;
    private final LocalDate triggerDate;
    private final LocalDateTime openDate;
    private final LocalDateTime closeDate;
    private final String motive;
    private final String origin;
    private final String state;
    private final String description;
}
