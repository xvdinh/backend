package be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ObservationDetailDTO {
    private String reportReference;
    private Integer observationNumber;
    private String title;
    private String value;
    private String unit;
    private String description;
}
