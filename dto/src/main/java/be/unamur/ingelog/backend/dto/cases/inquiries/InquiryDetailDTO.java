package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Value
@Builder
@AllArgsConstructor
public class InquiryDetailDTO {
    private final String reference;
    private final String caseReference;
    private final String employerReference;
    private final String supervisorUsername;
    private final String assigneeUsername;
    private final String state;
    private final String result;
    private final LocalDateTime openDate;
    private final LocalDateTime closeDate;
    private final LocalDate reportDate;
    private final String street;
    private final String postalCode;
    private final String locality;
    private final LocalDate deadline;
    private final String priority;
}
