package be.unamur.ingelog.backend.dto.statistics;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ResultDTO {
    private String lawTypeCode;
    private String violationTypeCode;
    private String status;
    private Integer totalOfControlledPerson;
}
