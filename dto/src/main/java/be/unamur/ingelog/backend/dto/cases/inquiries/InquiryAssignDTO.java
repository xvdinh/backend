package be.unamur.ingelog.backend.dto.cases.inquiries;

import be.unamur.ingelog.backend.dto.util.ToUpperCaseDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class InquiryAssignDTO {
    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String inquiryReference;

    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String assigneeUsername;

    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String supervisorUsername;
}
