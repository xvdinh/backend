package be.unamur.ingelog.backend.dto.cases;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class CaseMotiveDTO {
    private final String reference;
}
