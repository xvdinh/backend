package be.unamur.ingelog.backend.dto.employers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class EmployerDetailDTO {
    private final String reference;
    private final String name;
    private final String street;
    private final String postalCode;
    private final String locality;
    private final String country;
}
