package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class InquiryFilterDTO {
    private List<String> states;
    private List<String> excludeStates;
    private String result;
    private String assignee;
    private String supervisor;
}
