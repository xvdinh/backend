package be.unamur.ingelog.backend.dto.dashboard;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class InquiriesDistributionByResultDTO {
    private String inquiryResult;
    private Float inquiriesDistribution;
}
