package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class InquirySummaryLawArticleDTO {
    private String lawArticleLabel;
    private List<InquirySummaryReportDTO> reports;

    public void addReport(InquirySummaryReportDTO report) {
        if (this.reports == null) {
            this.reports = new ArrayList<>();
        }
        this.reports.add(report);
    }
}
