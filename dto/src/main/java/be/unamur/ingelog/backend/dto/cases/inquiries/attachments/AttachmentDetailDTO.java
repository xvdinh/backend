package be.unamur.ingelog.backend.dto.cases.inquiries.attachments;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class AttachmentDetailDTO {
    private String inquiryReference;
    private String attachmentReference;
    private String originalName;
    private String documentType;
    private LocalDateTime receptionDate;
    private String source;
}
