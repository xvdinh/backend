package be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.dto.util.ToUpperCaseDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ObservationRoadmapDTO {
    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String reportReference;

    private int observationNumber;
    private String value;
    private String description;
}
