package be.unamur.ingelog.backend.dto.statistics;

import lombok.Data;

import java.util.List;

@Data
public class CollectionOfResultsDTO {
    private List<ResultDTO> items;
    private Integer total;

    public CollectionOfResultsDTO items(List<ResultDTO> items) {
        this.items = items;
        total = items == null ? null : items.size();

        return this;
    }
}
