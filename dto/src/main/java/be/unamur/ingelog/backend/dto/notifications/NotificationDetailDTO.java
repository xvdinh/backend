package be.unamur.ingelog.backend.dto.notifications;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class NotificationDetailDTO {
    private String notificationReference;
    private boolean consulted;
    private String authorUsername;
    private String type;
    private String resourceType;
    private String resourceReference;
    private LocalDateTime notificationTimestamp;
}
