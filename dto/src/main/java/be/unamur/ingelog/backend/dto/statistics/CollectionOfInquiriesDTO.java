package be.unamur.ingelog.backend.dto.statistics;

import lombok.Data;

import java.util.List;

@Data
public class CollectionOfInquiriesDTO {
    private List<InspectionInquiryDTO> items;
    private Integer total;

    public CollectionOfInquiriesDTO items(List<InspectionInquiryDTO> items) {
        this.items = items;
        total = items == null ? null : items.size();

        return this;
    }
}
