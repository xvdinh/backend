package be.unamur.ingelog.backend.dto.cases.comments;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentDetailDTO {
    private String content;
    private String authorFirstName;
    private String authorLastName;
    private LocalDateTime creationDate;
}
