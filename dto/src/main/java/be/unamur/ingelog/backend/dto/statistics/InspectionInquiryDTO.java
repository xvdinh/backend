package be.unamur.ingelog.backend.dto.statistics;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class InspectionInquiryDTO {
    private String reference;
    private LocalDateTime startingDate;
    private LocalDateTime closingDate;
    private EmployerDTO targetEmployer;
    private CollectionOfResultsDTO inquiryResults;
    private Integer legalDelay;
    private String priority;
    private String globalResult;
    private String caseReference;
    private boolean joinedInquiry;
    private String postCode;
}
