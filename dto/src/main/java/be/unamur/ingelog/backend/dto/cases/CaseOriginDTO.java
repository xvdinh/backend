package be.unamur.ingelog.backend.dto.cases;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class CaseOriginDTO {
    private final String reference;
    private final String motiveReference;
}
