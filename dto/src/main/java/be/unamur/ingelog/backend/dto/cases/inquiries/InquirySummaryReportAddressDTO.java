package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class InquirySummaryReportAddressDTO {
    private String street;
    private String postalCode;
    private String locality;
}
