package be.unamur.ingelog.backend.dto.cases.inquiries.reports;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReportControlsUpdateDTO {
    private int totalOfControlledPerson;
}
