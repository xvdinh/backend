package be.unamur.ingelog.backend.dto.cases.inquiries;

import be.unamur.ingelog.backend.dto.util.ToUpperCaseDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InquiryResultUpdateDTO {
    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String result;
}
