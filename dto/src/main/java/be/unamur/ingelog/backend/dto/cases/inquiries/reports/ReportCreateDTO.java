package be.unamur.ingelog.backend.dto.cases.inquiries.reports;

import be.unamur.ingelog.backend.dto.util.ToUpperCaseDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReportCreateDTO {
    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String inquiryReference;

    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String offenceReference;
}
