package be.unamur.ingelog.backend.dto.dashboard;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class TotalInquiriesByPriorityDTO {
    private String priority;
    private Integer totalInquiries;
}
