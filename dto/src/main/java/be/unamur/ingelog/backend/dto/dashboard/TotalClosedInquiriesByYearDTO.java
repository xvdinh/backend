package be.unamur.ingelog.backend.dto.dashboard;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class TotalClosedInquiriesByYearDTO {
    private String year;
    private Integer totalDismissedInquiries;
    private Integer totalClosedPositiveInquiries;
    private Integer totalClosedNegativeInquiries;
}
