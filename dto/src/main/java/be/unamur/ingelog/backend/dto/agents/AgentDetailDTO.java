package be.unamur.ingelog.backend.dto.agents;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class AgentDetailDTO {
    private final String username;
    private final String firstName;
    private final String lastName;
    private final String role;
}
