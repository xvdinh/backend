package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Value
@Builder
@AllArgsConstructor
public class InquiryOverviewDTO {
    private final String reference;
    private final String priority;
    private final LocalDate deadline;
    private final String state;
    private final String employerReference;
    private final String employerName;
    private final LocalDate triggerDate;
    private final LocalDate reportDate;
    private final LocalDateTime closeDate;
    private final String assignee;
    private final String supervisor;
    private final String result;
}
