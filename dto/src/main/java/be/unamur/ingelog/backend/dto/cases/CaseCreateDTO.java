package be.unamur.ingelog.backend.dto.cases;

import be.unamur.ingelog.backend.dto.util.ToUpperCaseDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CaseCreateDTO {
    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String creatorUsername;

    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String motive;

    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String origin;

    private String triggerDate;
    private String description;
}
