package be.unamur.ingelog.backend.dto.agents;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
@AllArgsConstructor
public class AgentAuthDTO {
    private final String username;
    private final String password;
    private final String firstName;
    private final String lastName;
    private final String role;
    private final List<String> authorities;
    private final String token;
}
