package be.unamur.ingelog.backend.dto.references;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class ReferenceDTO {
    private final String reference;
}
