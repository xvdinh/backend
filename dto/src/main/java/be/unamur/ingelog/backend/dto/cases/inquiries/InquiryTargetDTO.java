package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class InquiryTargetDTO {
    private String inquiryReference;
    private LocalDate reportDate;
    private String inquiryState;
    private String inquiryResult;
    private List<InquiryTargetReportDTO> reports;

    public void addReport(InquiryTargetReportDTO report) {
        if (report == null) return;

        if (this.reports == null) {
            this.reports = new ArrayList<>();
        }
        this.reports.add(report);
    }
}
