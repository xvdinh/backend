package be.unamur.ingelog.backend.dto.law.articles;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class OffenceOverviewDTO {
    private final String offenceReference;
    private final String offenceLabel;
}
