package be.unamur.ingelog.backend.dto.cases;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class CaseFilterDTO {
    private List<String> states;
}
