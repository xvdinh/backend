package be.unamur.ingelog.backend.dto.cases.inquiries.attachments;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class AttachmentCreateDTO {
    private String inquiryReference;
    private String source;
    private String documentType;
}
