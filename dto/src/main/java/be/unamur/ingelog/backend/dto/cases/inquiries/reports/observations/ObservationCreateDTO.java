package be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.dto.util.ToUpperCaseDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ObservationCreateDTO {
    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String reportReference;

    private String title;
    private String unit;
}
