package be.unamur.ingelog.backend.dto.cases.inquiries.reports;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
@AllArgsConstructor
public class ReportDetailDTO {
    private final String reference;
    private final String inquiryReference;
    private final String caseReference;
    private final String reportState;
    private final String inquiryState;
    private final String status;
    private final String offenceReference;
    private final String offenceLabel;
    private final String offencePriority;
    private final LocalDate deadline;
    private final LocalDate offenceStartingDate;
    private final LocalDate offenceEndingDate;
    private final String lawArticleReference;
    private final String lawArticleLabel;
    private final LocalDate lawArticleStartingDate;
    private final LocalDate lawArticleEndingDate;
    private final Integer totalOfControlledPersons;
    private final String assigneeUsername;
}
