package be.unamur.ingelog.backend.dto.cases.inquiries;

import be.unamur.ingelog.backend.dto.util.ToUpperCaseDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InquiryCreateDTO {
    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String caseReference;

    @JsonDeserialize(using = ToUpperCaseDeserializer.class)
    private String employerReference;

    private String street;
    private String postalCode;
    private String locality;
}
