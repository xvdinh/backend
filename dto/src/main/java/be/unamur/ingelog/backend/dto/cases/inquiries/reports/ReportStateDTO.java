package be.unamur.ingelog.backend.dto.cases.inquiries.reports;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class ReportStateDTO {
    private String reportState;
}
