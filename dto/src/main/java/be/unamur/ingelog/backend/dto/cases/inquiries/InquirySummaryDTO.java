package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class InquirySummaryDTO {
    private String inquiryReference;
    private String inquiryResult;
    private String inquiryPriority;
    private LocalDateTime inquiryOpenDate;
    private LocalDateTime inquiryCloseDate;
    private LocalDate inquiryDeadline;
    private LocalDate triggerDate;
    private String caseMotive;
    private String caseOrigin;
    private LocalDate reportDate;
    private InquirySummaryReportAddressDTO reportAddress;
    private InquirySummarySupervisorDTO inquirySupervisor;
    private InquirySummaryEmployerDTO targetedEmployer;
    private List<InquirySummaryLawArticleDTO> lawArticles;
    private List<InquirySummaryAttachmentDTO> attachments;
    private List<InquirySummaryCommentDTO> comments;
}
