package be.unamur.ingelog.backend.dto.dashboard;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class TotalInquiriesByStateDTO {
    private String inquiryState;
    private Integer totalInquiries;
}
