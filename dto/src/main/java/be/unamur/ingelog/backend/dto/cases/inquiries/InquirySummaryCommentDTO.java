package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class InquirySummaryCommentDTO {
    private String content;
    private String authorUsername;
    private LocalDateTime creationDate;
}
