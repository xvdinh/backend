package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class InquirySummaryReportDTO {
    private Integer totalNumberOfControlledPersons;
    private String offenceLabel;
    private String reportStatus;
    private List<InquirySummaryObservationDTO> observations;

    public void addObservation(InquirySummaryObservationDTO observation) {
        if (this.observations == null) {
            this.observations = new ArrayList<>();
        }
        this.observations.add(observation);
    }
}
