package be.unamur.ingelog.backend.dto.cases.inquiries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class InquiryTargetReportDTO {
    private String offenceLabel;
    private String reportStatus;
    private String reportState;
}
