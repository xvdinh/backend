package be.unamur.ingelog.backend.dto.builders;

import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryCreateDTO;

public class InquiryTestBuilder {
    public static InquiryCreateDTO.InquiryCreateDTOBuilder aDefaultInquiryCreateDTO() {
        return InquiryCreateDTO.builder()
                .caseReference("2020-DOS-ABCDE")
                .employerReference("EMP-ABCDE")
                .locality("locality")
                .postalCode("1111")
                .street("street");
    }
}
