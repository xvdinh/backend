package be.unamur.ingelog.backend.dto.builders;

import be.unamur.ingelog.backend.dto.agents.AgentDetailDTO;

public class AgentTestBuilder {
    public static AgentDetailDTO aDefaultAdministrativeAgentDetail() {
        return AgentDetailDTO.builder()
                .username("ADMA")
                .firstName("Nathalie")
                .lastName("Desrochers")
                .role("AGENT_ADM")
                .build();
    }

    public static AgentDetailDTO aDefaultInspectorDetail() {
        return AgentDetailDTO.builder()
                .username("INSA")
                .firstName("Alexandre")
                .lastName("Fortin")
                .role("INSPECTOR")
                .build();
    }

    public static AgentDetailDTO aDefaultChiefInspectorDetail() {
        return AgentDetailDTO.builder()
                .username("CINA")
                .firstName("Xavier")
                .lastName("Cloutier")
                .role("CHIEF_INSPECTOR")
                .build();
    }
}
