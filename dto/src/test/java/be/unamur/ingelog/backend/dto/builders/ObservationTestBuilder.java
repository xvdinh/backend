package be.unamur.ingelog.backend.dto.builders;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationCreateDTO;

public class ObservationTestBuilder {
    public static ObservationCreateDTO.ObservationCreateDTOBuilder aDefaultObservationCreateDTO() {
        return ObservationCreateDTO.builder()
                .reportReference("2020-CON-ABCDE")
                .title("title")
                .unit("unit");
    }
}
