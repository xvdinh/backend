package be.unamur.ingelog.backend.dto.builders;

import be.unamur.ingelog.backend.dto.cases.CaseCreateDTO;

public class CaseTestBuilder {
    public static CaseCreateDTO.CaseCreateDTOBuilder aDefaultCaseCreateDTO() {
        return CaseCreateDTO.builder()
                .creatorUsername("ADMA")
                .triggerDate("2020-01-01")
                .description("description")
                .motive("DENUNCIATION")
                .origin("ANONYMOUS");
    }
}
