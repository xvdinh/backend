package be.unamur.ingelog.backend.service.cases;

import be.unamur.ingelog.backend.dto.cases.CaseCreateDTO;
import be.unamur.ingelog.backend.dto.cases.CaseOriginDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.repository.queries.agents.AgentQueries;
import be.unamur.ingelog.backend.repository.queries.cases.CaseQueries;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.InquiryQueries;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.time.LocalDate;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.*;
import static be.unamur.ingelog.backend.service.cases.CaseValidator.MAX_DESCRIPTION_LENGTH;
import static be.unamur.ingelog.backend.service.cases.CaseValidator.MAX_ORIGIN_LENGTH;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class CaseValidatorTest {
    @Mock
    private CaseQueries caseQueries;

    @Mock
    private InquiryQueries inquiryQueries;

    @Mock
    private AgentQueries agentQueries;

    @InjectMocks
    private CaseValidator validator;

    @BeforeEach
    void setUp() {
        initMocks(this);

        when(agentQueries.existsByUsername("CINA"))
                .thenReturn(true);

        when(inquiryQueries.existsByInquiryReference(any(String.class)))
                .thenReturn(true);

        builder = CaseCreateDTO.builder()
                .creatorUsername("CINA")
                .triggerDate("2020-03-04")
                .motive("DENUNCIATION")
                .origin("ANONYMOUS")
                .description("lorem ipsum");
    }

    private CaseCreateDTO.CaseCreateDTOBuilder builder;

    @Test
    void checkCreatorUsername_whenNullValue() {
        CaseCreateDTO dto = builder
                .creatorUsername(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("creatorUsername", ERR_NOT_BLANK));
    }

    @Test
    void checkCreatorUsername_whenBlankValue() {
        CaseCreateDTO dto = builder
                .creatorUsername("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("creatorUsername", ERR_NOT_BLANK));
    }

    @Test
    void checkCreatorUsername_whenUnknownUsername() {
        CaseCreateDTO dto = builder
                .creatorUsername("HULK")
                .build();

        when(agentQueries.existsByUsername("HULK"))
                .thenReturn(false);

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("creatorUsername", ERR_AGENT_NOT_FOUND));
    }

    @Test
    void checkTriggerDate_whenNullValue() {
        CaseCreateDTO dto = builder
                .triggerDate(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("triggerDate", ERR_NOT_BLANK));
    }

    @Test
    void checkTriggerDate_whenBlankValue() {
        CaseCreateDTO dto = builder
                .triggerDate("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("triggerDate", ERR_NOT_BLANK));
    }

    @Test
    void checkTriggerDate_whenWrongFormat() {
        CaseCreateDTO dto = builder
                .triggerDate("01/02/2020")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("triggerDate", ERR_DATE_FORMAT));
    }

    @Test
    void checkTriggerDate_whenFutureDate() {
        CaseCreateDTO dto = builder
                .triggerDate(LocalDate.now().plusDays(1).toString())
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("triggerDate", ERR_PAST_OR_PRESENT));
    }

    @Test
    void checkMotive_whenNullValue() {
        CaseCreateDTO dto = builder
                .motive(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("motive", ERR_NOT_BLANK));
    }

    @Test
    void checkMotive_whenBlankValue() {
        CaseCreateDTO dto = builder
                .motive("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("motive", ERR_NOT_BLANK));
    }

    @Test
    void checkMotive_whenIllegalMotive() {
        CaseCreateDTO dto = builder
                .motive("just_because")
                .build();

        when(caseQueries.existingMotive("just_because"))
                .thenReturn(false);

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("motive", ERR_ILLEGAL_MOTIVE));
    }

    @Test
    void checkOrigin_whenNullValue() {
        CaseCreateDTO dto = builder
                .origin(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("origin", ERR_NOT_BLANK));
    }

    @Test
    void checkOrigin_whenBlankValue() {
        CaseCreateDTO dto = builder
                .origin("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("origin", ERR_NOT_BLANK));
    }

    @Test
    void checkOrigin_whenIllegalOrigin() {
        CaseCreateDTO dto = builder
                .origin("SOMEWHERE")
                .build();

        when(caseQueries.findOriginsForMotive("DENUNCIATION"))
                .thenReturn(singletonList(new CaseOriginDTO("ANONYMOUS", "DENUNCIATION")));

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("origin", ERR_ILLEGAL_ORIGIN));
    }

    @Test
    void checkOrigin_whenMotiveIsRegularizationControl_andOriginHasWrongInquiryReferenceFormat() {
        CaseCreateDTO dto = builder
                .motive("REGULARIZATION_CONTROL")
                .origin("WRONG_FORMAT")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("origin", ERR_INQUIRY_REFERENCE_FORMAT));
    }

    @Test
    void checkOrigin_whenMotiveIsRegularizationControl_andOriginIsUnknownInquiryReference() {
        CaseCreateDTO dto = builder
                .motive("REGULARIZATION_CONTROL")
                .origin("2020-ENQ-N0P3Z")
                .build();

        when(inquiryQueries.existsByInquiryReference("2020-ENQ-N0P3Z"))
                .thenReturn(false);

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("origin", ERR_INQUIRY_NOT_FOUND));
    }

    @Test
    void checkOrigin_whenTooLong() {
        CaseCreateDTO dto = builder
                .motive("COMPLAINT")
                .origin("A".repeat(MAX_ORIGIN_LENGTH + 1))
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("origin", ERR_TOO_LONG));
    }

    @Test
    void checkDescription_whenNullValue() {
        CaseCreateDTO dto = builder
                .description(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("description", ERR_NOT_BLANK));
    }

    @Test
    void checkDescription_whenBlankValue() {
        CaseCreateDTO dto = builder
                .description("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("description", ERR_NOT_BLANK));
    }

    @Test
    void checkDescription_whenTooLong() {
        CaseCreateDTO dto = builder
                .description("a".repeat(MAX_DESCRIPTION_LENGTH + 1))
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("description", ERR_TOO_LONG));
    }
}
