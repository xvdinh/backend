package be.unamur.ingelog.backend.service.inquiries;

import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryCreateDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.repository.queries.cases.CaseQueries;
import be.unamur.ingelog.backend.repository.queries.employers.EmployerQueries;
import be.unamur.ingelog.backend.service.cases.inquiries.InquiryValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.*;
import static be.unamur.ingelog.backend.service.cases.inquiries.InquiryValidator.MAX_LOCALITY_LENGTH;
import static be.unamur.ingelog.backend.service.cases.inquiries.InquiryValidator.MAX_STREET_LENGTH;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class InquiryValidatorTest {
    @Mock
    private CaseQueries caseQueries;

    @Mock
    private EmployerQueries employerQueries;

    @InjectMocks
    private InquiryValidator validator;

    @BeforeEach
    void setUp() {
        initMocks(this);

        when(caseQueries.existsByCaseReference("2020-DOS-L0LZZ"))
                .thenReturn(true);

        when(employerQueries.existsByEmployerReference("EMP-B0B0Z"))
                .thenReturn(true);

        builder = InquiryCreateDTO.builder()
                .caseReference("2020-DOS-L0LZZ")
                .employerReference("EMP-B0B0Z")
                .street("77 rue des Fraudeurs")
                .postalCode("6000")
                .locality("Charleroi");
    }

    private InquiryCreateDTO.InquiryCreateDTOBuilder builder;

    @Test
    void checkCaseReference_whenNullValue() {
        InquiryCreateDTO dto = builder
                .caseReference(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("caseReference", ERR_NOT_BLANK));
    }

    @Test
    void checkCaseReference_whenBlankValue() {
        InquiryCreateDTO dto = builder
                .caseReference("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("caseReference", ERR_NOT_BLANK));
    }

    @Test
    void checkCaseReference_whenUnknownReference() {
        InquiryCreateDTO dto = builder
                .caseReference("2020-DOS-N0P3Z")
                .build();

        when(caseQueries.existsByCaseReference("2020-DOS-N0P3Z"))
                .thenReturn(false);

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("caseReference", ERR_CASE_NOT_FOUND));
    }

    @Test
    void checkEmployerReference_whenNullValue() {
        InquiryCreateDTO dto = builder
                .employerReference(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("employerReference", ERR_NOT_BLANK));
    }

    @Test
    void checkEmployerReference_whenBlankValue() {
        InquiryCreateDTO dto = builder
                .employerReference("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("employerReference", ERR_NOT_BLANK));
    }

    @Test
    void checkEmployerReference_whenUnknownReference() {
        InquiryCreateDTO dto = builder
                .employerReference("EMP-N0P3Z")
                .build();

        when(employerQueries.existsByEmployerReference("EMP-N0P3Z"))
                .thenReturn(false);

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields",
                        singletonMap("employerReference", ERR_EMPLOYER_NOT_FOUND));
    }

    @Test
    void checkStreet_whenNullValue() {
        InquiryCreateDTO dto = builder
                .street(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("street", ERR_NOT_BLANK));
    }

    @Test
    void checkStreet_whenBlankValue() {
        InquiryCreateDTO dto = builder
                .street("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("street", ERR_NOT_BLANK));
    }

    @Test
    void checkStreet_whenTooLong() {
        InquiryCreateDTO dto = builder
                .street("a".repeat(MAX_STREET_LENGTH + 1))
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("street", ERR_TOO_LONG));
    }

    @Test
    void checkPostalCode_whenNullValue() {
        InquiryCreateDTO dto = builder
                .postalCode(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("postalCode", ERR_NOT_BLANK));
    }

    @Test
    void checkPostalCode_whenBlankValue() {
        InquiryCreateDTO dto = builder
                .postalCode("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("postalCode", ERR_NOT_BLANK));
    }

    @Test
    void checkLocality_whenNullValue() {
        InquiryCreateDTO dto = builder
                .locality(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("locality", ERR_NOT_BLANK));
    }

    @Test
    void checkLocality_whenBlankValue() {
        InquiryCreateDTO dto = builder
                .locality("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("locality", ERR_NOT_BLANK));
    }

    @Test
    void checkLocality_whenTooLong() {
        InquiryCreateDTO dto = builder
                .locality("a".repeat(MAX_LOCALITY_LENGTH + 1))
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("locality", ERR_TOO_LONG));
    }
}
