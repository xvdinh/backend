package be.unamur.ingelog.backend.service.cases;

import be.unamur.ingelog.backend.repository.repositories.agents.AgentRepository;
import be.unamur.ingelog.backend.service.util.ReferenceGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static be.unamur.ingelog.backend.domain.builders.AgentTestBuilder.aDefaultAdministrativeAgent;
import static be.unamur.ingelog.backend.domain.builders.CaseTestBuilder.aNewlyCreatedCase;
import static be.unamur.ingelog.backend.dto.builders.CaseTestBuilder.aDefaultCaseCreateDTO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class CaseMapperTest {
    @Mock
    private AgentRepository agentRepository;

    @Mock
    private ReferenceGenerator referenceGenerator;

    @InjectMocks
    private CaseMapper caseMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void toDomain() {
        when(referenceGenerator.generateCaseReference())
                .thenReturn("2020-DOS-ABCDE");

        when(agentRepository.findByUsername("ADMA"))
                .thenReturn(aDefaultAdministrativeAgent().build());

        var dto = aDefaultCaseCreateDTO().build();
        var createdCase = caseMapper.toDomain(dto);

        assertThat(createdCase)
                .usingRecursiveComparison()
                .ignoringFields("openDate")
                .isEqualTo(aNewlyCreatedCase());
    }
}
