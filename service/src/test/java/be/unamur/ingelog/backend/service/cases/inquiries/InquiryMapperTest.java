package be.unamur.ingelog.backend.service.cases.inquiries;

import be.unamur.ingelog.backend.repository.repositories.cases.CaseRepositoryImpl;
import be.unamur.ingelog.backend.repository.repositories.employers.EmployerRepository;
import be.unamur.ingelog.backend.service.util.ReferenceGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static be.unamur.ingelog.backend.domain.builders.CaseTestBuilder.aDefaultCase;
import static be.unamur.ingelog.backend.domain.builders.EmployerTestBuilder.aDefaultEmployer;
import static be.unamur.ingelog.backend.domain.builders.InquiryTestBuilder.aNewlyCreatedInquiry;
import static be.unamur.ingelog.backend.dto.builders.InquiryTestBuilder.aDefaultInquiryCreateDTO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class InquiryMapperTest {
    @Mock
    private CaseRepositoryImpl caseRepository;

    @Mock
    private EmployerRepository employerRepository;

    @Mock
    private ReferenceGenerator referenceGenerator;

    @InjectMocks
    private InquiryMapper inquiryMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void toDomain() {
        when(referenceGenerator.generateInquiryReference())
                .thenReturn("2020-ENQ-ABCDE");

        when(caseRepository.findByCaseReference("2020-DOS-ABCDE"))
                .thenReturn(aDefaultCase().build());

        when(employerRepository.findByEmployerReference("EMP-ABCDE"))
                .thenReturn(aDefaultEmployer().build());

        var dto = aDefaultInquiryCreateDTO().build();
        var inquiry = inquiryMapper.toDomain(dto);

        assertThat(inquiry)
                .usingRecursiveComparison()
                .ignoringFields("openDate")
                .isEqualTo(aNewlyCreatedInquiry());
    }
}
