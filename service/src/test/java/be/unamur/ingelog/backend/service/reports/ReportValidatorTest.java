package be.unamur.ingelog.backend.service.reports;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportCreateDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.InquiryQueries;
import be.unamur.ingelog.backend.repository.queries.law.articles.OffenceQueries;
import be.unamur.ingelog.backend.service.cases.inquiries.reports.ReportValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.*;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class ReportValidatorTest {
    @Mock
    private InquiryQueries inquiryQueries;

    @Mock
    private OffenceQueries offenceQueries;

    @InjectMocks
    private ReportValidator validator;

    @BeforeEach
    void setUp() {
        initMocks(this);

        when(inquiryQueries.existsByInquiryReference("2020-ENQ-Z1Z1K"))
                .thenReturn(true);

        when(offenceQueries.existsByReference("RG001001"))
                .thenReturn(true);

        builder = ReportCreateDTO.builder()
                .inquiryReference("2020-ENQ-Z1Z1K")
                .offenceReference("RG001001");
    }

    private ReportCreateDTO.ReportCreateDTOBuilder builder;

    @Test
    void checkInquiryReference_whenNullValue() {
        ReportCreateDTO dto = builder
                .inquiryReference(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("inquiryReference", ERR_NOT_BLANK));
    }

    @Test
    void checkInquiryReference_whenBlankValue() {
        ReportCreateDTO dto = builder
                .inquiryReference("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("inquiryReference", ERR_NOT_BLANK));
    }

    @Test
    void checkInquiryReference_whenInvalidFormat() {
        ReportCreateDTO dto = builder
                .inquiryReference("invalid_format")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields",
                        singletonMap("inquiryReference", ERR_INQUIRY_REFERENCE_FORMAT));
    }

    @Test
    void checkInquiryReference_whenUnknownReference() {
        ReportCreateDTO dto = builder
                .inquiryReference("2020-ENQ-N0P3Z")
                .build();

        when(inquiryQueries.existsByInquiryReference("2020-ENQ-N0P3Z"))
                .thenReturn(false);

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields",
                        singletonMap("inquiryReference", ERR_INQUIRY_NOT_FOUND));
    }

    @Test
    void checkOffenceReference_whenNullValue() {
        ReportCreateDTO dto = builder
                .offenceReference(null)
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("offenceReference", ERR_NOT_BLANK));
    }

    @Test
    void checkOffenceReference_whenBlankValue() {
        ReportCreateDTO dto = builder
                .offenceReference("")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields", singletonMap("offenceReference", ERR_NOT_BLANK));
    }

    @Test
    void checkOffenceReference_whenInvalidFormat() {
        ReportCreateDTO dto = builder
                .offenceReference("wrong_format")
                .build();

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields",
                        singletonMap("offenceReference", ERR_OFFENCE_REFERENCE_FORMAT));
    }

    @Test
    void checkOffenceReference_whenUnknownReference() {
        ReportCreateDTO dto = builder
                .offenceReference("RG1337")
                .build();

        when(offenceQueries.existsByReference("RG1337"))
                .thenReturn(false);

        assertThatThrownBy(() -> validator.check(dto))
                .isInstanceOf(FieldsValidationException.class)
                .hasFieldOrPropertyWithValue("errorsByFields",
                        singletonMap("offenceReference", ERR_OFFENCE_NOT_FOUND));
    }
}
