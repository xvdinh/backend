package be.unamur.ingelog.backend.service.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.ReportRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static be.unamur.ingelog.backend.domain.builders.ReportObservationTestBuilder.aDefaultReportObservation;
import static be.unamur.ingelog.backend.domain.builders.ReportObservationTestBuilder.aNewlyCreatedObservation;
import static be.unamur.ingelog.backend.domain.builders.ReportTestBuilder.aDefaultReport;
import static be.unamur.ingelog.backend.dto.builders.ObservationTestBuilder.aDefaultObservationCreateDTO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class ObservationMapperTest {
    @Mock
    private ReportRepository reportRepository;

    @InjectMocks
    private ObservationMapper observationMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void toDomain() {
        when(reportRepository.findByReportReference("2020-CON-ABCDE"))
                .thenReturn(aDefaultReport().build());

        var dto = aDefaultObservationCreateDTO().build();
        var observation = observationMapper.toDomain(dto);

        assertThat(observation)
                .usingRecursiveComparison()
                .isEqualTo(aNewlyCreatedObservation());
    }

    @Test
    void toObservationNumberDTO() {
        var observation = aDefaultReportObservation()
                .observationNumber(1)
                .build();

        var dto = observationMapper
                .toObservationNumberDTO(observation);

        assertThat(dto.getObservationNumber()).isEqualTo(1);
    }
}
