package be.unamur.ingelog.backend.service.law.articles;

import be.unamur.ingelog.backend.dto.law.articles.OffenceOverviewDTO;
import be.unamur.ingelog.backend.repository.queries.law.articles.OffenceQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class OffenceServiceImpl implements OffenceService {
    private final OffenceQueries queries;
    private final OffenceValidator validator;

    @Autowired
    public OffenceServiceImpl(OffenceQueries queries,
                              OffenceValidator validator) {
        this.queries = queries;
        this.validator = validator;
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<OffenceOverviewDTO> findAll(Map<String, String> filter) {
        validator.check(filter);

        return queries.findAll(filter);
    }
}
