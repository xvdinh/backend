package be.unamur.ingelog.backend.service.law.articles;

import be.unamur.ingelog.backend.dto.law.articles.OffenceOverviewDTO;

import java.util.List;
import java.util.Map;

public interface OffenceService {
    /**
     * Trouve l'ensemble des infractions.
     *
     * @return l'ensemble des infractions
     */
    List<OffenceOverviewDTO> findAll(Map<String, String> filters);
}
