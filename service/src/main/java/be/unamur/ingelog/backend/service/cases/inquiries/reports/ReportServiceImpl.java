package be.unamur.ingelog.backend.service.cases.inquiries.reports;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.Report;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportStatus;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.ReportNotFoundException;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.reports.ReportQueries;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.InquiryRepository;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.ReportRepository;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.observations.ObservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class ReportServiceImpl implements ReportService {
    private final ReportMapper mapper;
    private final ReportQueries queries;
    private final ReportValidator validator;
    private final ReportRepository reportRepository;
    private final ReportQueries reportQueries;
    private final ObservationRepository observationRepository;
    private final InquiryRepository inquiryRepository;

    @Autowired
    public ReportServiceImpl(ReportMapper mapper,
                             ReportValidator validator,
                             ReportQueries queries,
                             ReportRepository reportRepository,
                             ReportQueries reportQueries,
                             ObservationRepository observationRepository,
                             InquiryRepository inquiryRepository) {
        this.mapper = mapper;
        this.validator = validator;
        this.queries = queries;
        this.reportRepository = reportRepository;
        this.reportQueries = reportQueries;
        this.observationRepository = observationRepository;
        this.inquiryRepository = inquiryRepository;
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<ReportDetailDTO> findByInquiryReference(String inquiryReference) {
        return queries.findByInquiryReference(inquiryReference);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public ReportDetailDTO findByReportReference(String reportReference) throws ReportNotFoundException {
        return queries.findByReportReference(reportReference);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<ReportDetailDTO> findAllReports() {
        return queries.findAllReports();
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<ReportStatusUpdateDTO> findAllStatus() {
        return reportQueries.findAllStatus();
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public ReferenceDTO create(ReportCreateDTO dto) {
        validator.check(dto);

        var report = mapper.toDomain(dto);
        var relatedInquiry = report.getRelatedInquiry();

        enrichWithReports(relatedInquiry);
        relatedInquiry.addReport(report);

        reportRepository.create(report);
        inquiryRepository.update(relatedInquiry);

        return new ReferenceDTO(report.getReportReference());
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public ReportStateDTO updateControlsCount(ReportControlsUpdateDTO dto, String reportReference) {
        validator.check(dto);

        var report = reportRepository.findByReportReference(reportReference);
        enrichWithObservations(report);
        report.setTotalOfControlledPersons(dto.getTotalOfControlledPerson());

        reportRepository.update(report);

        return mapper.toReportStateDTO(report);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public ReportStateDTO updateStatus(ReportStatusUpdateDTO dto, String reportReference) {
        validator.check(dto);

        var report = reportRepository.findByReportReference(reportReference);
        enrichWithInquiry(report);

        report.setReportStatus(ReportStatus.valueOf(dto.getStatus()));
        reportRepository.update(report);

        signalReportUpdate(reportReference);

        return mapper.toReportStateDTO(report);
    }

    private void signalReportUpdate(String reportReference) {
        var inquiry = fetchInquiry(reportReference);
        enrichWithReports(inquiry);

        inquiry.signalEncoding();
        inquiryRepository.update(inquiry);
    }

    private void enrichWithObservations(Report report) {
        report.setReportObservations(observationRepository
                .findByReportReference(report.getReportReference()));
    }

    private void enrichWithInquiry(Report report) {
        report.setRelatedInquiry(inquiryRepository
                .findByReportReference(report.getReportReference()));
    }

    private Inquiry fetchInquiry(String reportReference) {
        return inquiryRepository.findByReportReference(reportReference);
    }

    private void enrichWithReports(Inquiry inquiry) {
        var reports = reportRepository
                .findByInquiryReference(inquiry.getInquiryReference());

        inquiry.setReports(reports);
    }
}
