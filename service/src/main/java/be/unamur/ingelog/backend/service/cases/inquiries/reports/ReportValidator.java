package be.unamur.ingelog.backend.service.cases.inquiries.reports;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportControlsUpdateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportStatusUpdateDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.service.util.validation.FieldsValidator;
import org.springframework.stereotype.Component;

@Component
public class ReportValidator extends FieldsValidator {
    public static final String INQUIRY_REFERENCE_FIELD_NAME = "inquiryReference";
    public static final String OFFENCE_REFERENCE_FIELD_NAME = "offenceReference";
    public static final String TOTAL_OF_CONTROLLED_PERSON_FIELD_NAME = "totalOfControlledPerson";
    public static final String REPORT_STATUS_FIELD_NAME = "status";

    /**
     * Vérifie la validité d'un contrôle lors de sa création.
     *
     * @param dto le contrôle à vérifier
     * @throws FieldsValidationException si le contrôle contient au moins un champ invalide
     */
    public void check(ReportCreateDTO dto) throws FieldsValidationException {
        exception = new FieldsValidationException();

        checkInquiryReference(dto.getInquiryReference());
        checkOffenceReference(dto.getOffenceReference());

        if (exception.hasErrors()) throw exception;
    }

    /**
     * Vérifie la validité de la modification du nombre de personnes contrôlées.
     *
     * @param dto les données du nombre de personnes contrôlées
     * @throws FieldsValidationException si les données contiennent au moins un champ invalide
     */
    public void check(ReportControlsUpdateDTO dto) throws FieldsValidationException {
        exception = new FieldsValidationException();

        checkTotalOfControlledPerson(dto.getTotalOfControlledPerson());

        if (exception.hasErrors()) throw exception;
    }

    /**
     * Vérifie la validité de la modification du statut.
     *
     * @param dto les données du nouveau statut
     * @throws FieldsValidationException si les données contiennent au moins un champ invalide
     */
    public void check(ReportStatusUpdateDTO dto) throws FieldsValidationException {
        exception = new FieldsValidationException();

        checkReportStatus(dto.getStatus());

        if (exception.hasErrors()) throw exception;
    }

    /**
     * Vérifie la validité de la référence d'enquête fournie.
     * Un référence d'enquête est valide si elle est non null et non vide, qu'elle présente un format
     * de référence d'enquête correct et qu'elle correspond à une enquête existante.
     *
     * @param inquiryReference la référence d'enquête à vérifier
     */
    private void checkInquiryReference(String inquiryReference) {
        notBlank(INQUIRY_REFERENCE_FIELD_NAME, inquiryReference);
        inquiryReferenceFormat(INQUIRY_REFERENCE_FIELD_NAME, inquiryReference);
        existingInquiryReference(INQUIRY_REFERENCE_FIELD_NAME, inquiryReference);
    }

    /**
     * Vérifie la validité de la référence d'infraction fournie.
     * Une référence d'infraction est valide si elle est non null et non vide, et qu'elle
     * correspond à une infraction existante.
     *
     * @param offenceReference la référence d'infraction à vérifier
     */
    private void checkOffenceReference(String offenceReference) {
        notBlank(OFFENCE_REFERENCE_FIELD_NAME, offenceReference);
        offenceReferenceFormat(offenceReference);
        existingOffenceReference(offenceReference);
    }

    /**
     * Vérifie la validité du nombre de personnes contrôlées fourni.
     * Un nombre de personnes contrôlées est valide si il est positif.
     *
     * @param totalOfControlledPerson le nombre de personnes contrôlées à vérifier
     */
    private void checkTotalOfControlledPerson(int totalOfControlledPerson) {
        positiveNumber(totalOfControlledPerson);
    }

    /**
     * Vérifie la validité du statut de constatation fourni.
     * Un statut de constatation est valide si il est non null et non vide, et qu'il
     * fait partie des statuts possibles pour une constatation.
     *
     * @param status le statut de constatation à vérifier
     */
    private void checkReportStatus(String status) {
        notBlank(REPORT_STATUS_FIELD_NAME, status);
        existingReportStatus(status);
    }
}
