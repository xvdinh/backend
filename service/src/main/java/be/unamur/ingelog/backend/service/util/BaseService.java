package be.unamur.ingelog.backend.service.util;

import be.unamur.ingelog.backend.domain.cases.Case;
import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.repository.repositories.agents.AgentRepository;
import be.unamur.ingelog.backend.repository.repositories.cases.CaseRepository;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.InquiryRepository;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class BaseService {
    @Autowired
    protected AgentRepository agentRepository;

    @Autowired
    protected InquiryRepository inquiryRepository;

    @Autowired
    protected CaseRepository caseRepository;

    @Autowired
    protected ReportRepository reportRepository;

    protected Inquiry fetchInquiry(String inquiryReference) {
        return inquiryRepository
                .findByInquiryReference(inquiryReference);
    }

    protected void enrichWithCase(Inquiry inquiry) {
        var relatedCase = caseRepository
                .findByInquiryReference(inquiry.getInquiryReference());

        inquiry.setRelatedCase(relatedCase);
    }

    protected void enrichWithReports(Inquiry inquiry) {
        var reports = reportRepository
                .findByInquiryReference(inquiry.getInquiryReference());

        inquiry.setReports(reports);
    }

    protected void enrichWithInquiries(Case relatedCase) {
        var inquiries = inquiryRepository
                .findByCaseReference(relatedCase.getCaseReference());

        relatedCase.setInquiries(inquiries);
    }

    protected String getAuthenticatedUsername() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();
    }
}
