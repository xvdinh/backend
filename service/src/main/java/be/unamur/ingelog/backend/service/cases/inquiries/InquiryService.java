package be.unamur.ingelog.backend.service.cases.inquiries;

import be.unamur.ingelog.backend.dto.cases.inquiries.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.InquiryNotFoundException;

import java.util.List;

public interface InquiryService {
    /**
     * Crée une nouvelle enquête.
     *
     * @param dto l'enquête à créer
     * @return l'enquête nouvellement créée
     */
    ReferenceDTO create(InquiryCreateDTO dto);

    /**
     * Trouve les enquêtes liées au dossier dont est fournie la référence.
     *
     * @param caseReference la référence du dossier
     * @return la liste des enquêtes liées
     */
    List<InquiryDetailDTO> findByCaseReference(String caseReference);

    /**
     * Trouve une enquête sur base de sa référence.
     *
     * @param inquiryReference la référence de l'enquête
     * @return l'enquête correspondant à la référence
     * @throws InquiryNotFoundException si aucune enquête n'a pu être trouvée
     */
    InquiryDetailDTO findByInquiryReference(String inquiryReference) throws InquiryNotFoundException;

    /**
     * Trouve l'ensemble des enquêtes, filtrées selon les filtres éventuellement fournis.
     *
     * @param filter les filtres optionnels
     * @return la liste des enquêtes éventuellement filtrée
     */
    List<InquiryOverviewDTO> findAllInquiries(InquiryFilterDTO filter);

    /**
     * Passe l'enquête en demande de validation.
     *
     * @param inquiryReference la référence de l'enquête
     */
    void askValidation(String inquiryReference);

    /**
     * Valide une enquête en demande de validation.
     *
     * @param inquiryReference la référence de l'enquête
     */
    void validate(String inquiryReference);

    /**
     * Invalide une enquête en demande de validation.
     *
     * @param inquiryReference la référence de l'enquête
     */
    void invalidate(String inquiryReference);

    /**
     * Assigne un agent à une enquête.
     *
     * @param dto les informations nécessaires à la mise à jour
     */
    void assign(InquiryAssignDTO dto);

    /**
     * Clôture sans suite une enquête qui se trouve dans l'état PENDING_VALIDATION.
     *
     * @param inquiryReference la référence de l'enquête
     * @return l'état de l'enquête après clôture sans suite
     */
    InquiryDismissClosureDTO dismissAndClose(String inquiryReference);

    /**
     * Renvoie une enquête dans l'état PENDING_CLOSURE à l'état COMPLETED.
     *
     * @param inquiryReference la référence de l'enquête
     * @return l'état de l'enquête après renvoi
     */
    InquiryStateDTO sendBack(String inquiryReference);

    /**
     * Archive l'enquête.
     *
     * @param inquiryReference la référence de l'enquête
     * @return l'état de l'enquête
     */
    InquiryStateDTO archive(String inquiryReference);

    /**
     * Clôture une enquête qui se trouve dans l'état PENDING_CLOSURE.
     *
     * @param inquiryReference la référence de l'enquête
     * @return l'état de l'enquête après clôture
     */
    InquiryClosureDTO close(String inquiryReference);

    /**
     * Modifie la date de constatation.
     *
     * @param inquiryReference la référence de l'enquête
     * @param dto              les données à mettre à jour
     */
    InquiryStateDTO updateReportDate(String inquiryReference, InquiryReportDateUpdateDTO dto);

    /**
     * Modifie le résultat de l'enquête.
     *
     * @param inquiryReference la référence de l'enquête
     * @param dto              les données à mettre à jour
     */
    InquiryStateDTO updateResult(String inquiryReference, InquiryResultUpdateDTO dto);

    /**
     * Retourne la liste de tous les résultats d'enquête disponibles.
     *
     * @return la liste de toutes les résultats d'enquête
     */
    List<InquiryResultDTO> findAllResults();

    /**
     * Retourne la liste des enquêtes liées à un employeur spécifié.
     *
     * @param employerReference la référence de l'employeur
     * @return la liste des enquêtes liées
     */
    List<InquiryTargetDTO> findByTargetedEmployer(String employerReference);

    /**
     * Retourne le compte rendu de l'enquête spécifiée.
     *
     * @param inquiryReference la référence de l'enquête
     * @return le compte rendu de l'enquête
     */
    InquirySummaryDTO getSummary(String inquiryReference);
}
