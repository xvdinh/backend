package be.unamur.ingelog.backend.service.util.validation;

import be.unamur.ingelog.backend.domain.cases.CaseMotive;
import be.unamur.ingelog.backend.domain.cases.inquiries.InquiryResult;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportStatus;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.repository.queries.agents.AgentQueries;
import be.unamur.ingelog.backend.repository.queries.cases.CaseQueries;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.InquiryQueries;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.reports.ReportQueries;
import be.unamur.ingelog.backend.repository.queries.employers.EmployerQueries;
import be.unamur.ingelog.backend.repository.queries.law.articles.OffenceQueries;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.observations.ObservationRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.*;
import static be.unamur.ingelog.backend.service.cases.CaseValidator.*;
import static be.unamur.ingelog.backend.service.cases.inquiries.InquiryValidator.*;
import static be.unamur.ingelog.backend.service.cases.inquiries.attachments.AttachmentValidator.ALLOWED_FILE_FORMATS;
import static be.unamur.ingelog.backend.service.cases.inquiries.attachments.AttachmentValidator.ATTACHMENT_FIELD_NAME;
import static be.unamur.ingelog.backend.service.cases.inquiries.reports.ReportValidator.*;
import static be.unamur.ingelog.backend.service.cases.inquiries.reports.observations.ObservationValidator.OBSERVATION_NUMBER_FIELD_NAME;
import static be.unamur.ingelog.backend.service.cases.inquiries.reports.observations.ObservationValidator.REPORT_REFERENCE_FIELD_NAME;
import static be.unamur.ingelog.backend.service.util.ReferenceGenerator.*;
import static java.lang.Boolean.FALSE;
import static org.apache.commons.lang3.EnumUtils.isValidEnum;

public abstract class FieldsValidator {
    @Autowired
    private EmployerQueries employerQueries;

    @Autowired
    private AgentQueries agentQueries;

    @Autowired
    private CaseQueries caseQueries;

    @Autowired
    private InquiryQueries inquiryQueries;

    @Autowired
    private OffenceQueries offenceQueries;

    @Autowired
    private ReportQueries reportQueries;

    @Autowired
    private ObservationRepository observationRepository;

    protected FieldsValidationException exception;

    /**
     * Vérifie que la valeur du champ est non null et non vide.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldName  le nom du champ à vérifier
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void notBlank(String fieldName, String fieldValue) {
        if (fieldValue == null || fieldValue.isBlank()) {
            exception.add(fieldName, ERR_NOT_BLANK);
        }
    }

    /**
     * Vérifie que la valeur du champ respecte une taille minimale.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldName  le nom du champ à vérifier
     * @param fieldValue la valeur du champ à vérifier
     * @param minLength  la taille minimale à respecter
     */
    protected void minLength(String fieldName, String fieldValue, int minLength) {
        if (fieldValue == null) return;

        if (fieldValue.length() < minLength) {
            exception.add(fieldName, ERR_TOO_SHORT);
        }
    }

    /**
     * Vérifie que la valeur du champ respecte une taille maximale.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldName  le nom du champ à vérifier
     * @param fieldValue la valeur du champ à vérifier
     * @param maxLength  la taille maximale à respecter
     */
    protected void maxLength(String fieldName, String fieldValue, int maxLength) {
        if (fieldValue == null) return;

        if (fieldValue.length() > maxLength) {
            exception.add(fieldName, ERR_TOO_LONG);
        }
    }

    /**
     * Vérifie que la valeur du champ respecte un format de code postal de quatre chiffres.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void postalCodeFormat(String fieldValue) {
        if (fieldValue == null) return;

        if (!fieldValue.matches("[0-9]{4}")) {
            exception.add(POSTALCODE_FIELD_NAME, ERR_POSTALCODE_FORMAT);
        }
    }

    /**
     * Vérifie que la valeur du champ respecte un format de date yyyy-MM-DD.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldName  le nom du champ à vérifier
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void dateFormat(String fieldName, String fieldValue) {
        if (fieldValue == null) return;

        try {
            LocalDate.parse(fieldValue);
        } catch (DateTimeParseException e) {
            exception.add(fieldName, ERR_DATE_FORMAT);
        }
    }

    /**
     * Vérifie que la valeur du champ est un fichier dont le format est permis.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void allowedFileFormat(MultipartFile fieldValue) {
        if (fieldValue == null) return;

        var extension = FilenameUtils.getExtension(fieldValue.getOriginalFilename());

        if (!ALLOWED_FILE_FORMATS.contains(extension)) {
            exception.add(ATTACHMENT_FIELD_NAME, ERR_ATTACHMENT_TYPE);
        }
    }

    /**
     * Vérifie que la valeur du champ est une date située dans le passé ou le présent.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void pastOrPresent(String fieldValue) {
        if (fieldValue == null) return;

        try {
            LocalDate actual = LocalDate.parse(fieldValue);
            LocalDate now = LocalDate.now();

            if (actual.isAfter(now)) {
                exception.add(TRIGGER_DATE_FIELD_NAME, ERR_PAST_OR_PRESENT);
            }
        } catch (DateTimeParseException e) {
            // cas géré par la méthode dateFormat
        }
    }

    /**
     * Vérifie que la valeur du champ est un nombre positif.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void positiveNumber(int fieldValue) {
        if (fieldValue <= 0) {
            exception.add(TOTAL_OF_CONTROLLED_PERSON_FIELD_NAME, ERR_NEGATIVE_NUMBER);
        }
    }

    /**
     * Vérifie que la valeur du champ présente un format de référence dossier correct.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldName  le nom du champ à vérifier
     * @param fieldValue la valeur du champ à vérifier
     */
    public void caseReferenceFormat(String fieldName, String fieldValue) {
        if (fieldValue == null) return;

        String regex = "[12][0-9]{3}-" + CASE_CODE + "-[A-Z0-9]{5}";

        if (!fieldValue.matches(regex)) {
            exception.add(fieldName, ERR_CASE_REFERENCE_FORMAT);
        }
    }

    /**
     * Vérifie que la valeur du champ présente un format de référence enquête correct.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldName  le nom du champ à vérifier
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void inquiryReferenceFormat(String fieldName, String fieldValue) {
        if (fieldValue == null) return;

        String regex = "[12][0-9]{3}-" + INQUIRY_CODE + "-[A-Z0-9]{5}";

        if (!fieldValue.matches(regex)) {
            exception.add(fieldName, ERR_INQUIRY_REFERENCE_FORMAT);
        }
    }

    /**
     * Vérifie que la valeur du champ présente un format de référence infraction correct.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void offenceReferenceFormat(String fieldValue) {
        if (fieldValue == null) return;

        String regex = "RG[0-9]+";

        if (!fieldValue.matches(regex)) {
            exception.add(OFFENCE_REFERENCE_FIELD_NAME, ERR_OFFENCE_REFERENCE_FORMAT);
        }
    }

    /**
     * Vérifie que la valeur du champ présente un format de référence constatation correct.
     * Si ce n'est pas le cas, une erreur est ajoutée à l'exception pour ce champ.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void reportReferenceFormat(String fieldValue) {
        if (fieldValue == null) return;

        String regex = "[12][0-9]{3}-" + REPORT_CODE + "-[A-Z0-9]{5}";

        if (!fieldValue.matches(regex)) {
            exception.add(REPORT_REFERENCE_FIELD_NAME, ERR_REPORT_REFERENCE_FORMAT);
        }
    }

    /**
     * Vérifie que la valeur du champ représente une référence d'enquête connue.
     *
     * @param fieldName  le nom du champ à vérifier
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void existingInquiryReference(String fieldName, String fieldValue) {
        if (fieldValue == null) return;

        if (FALSE.equals(inquiryQueries.existsByInquiryReference(fieldValue))) {
            exception.add(fieldName, ERR_INQUIRY_NOT_FOUND);
        }
    }

    /**
     * Vérifie que la valeur du champ représente une référence d'enquête connue.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void existingReportReference(String fieldValue) {
        if (fieldValue == null) return;

        if (FALSE.equals(reportQueries.existsByReportReference(fieldValue))) {
            exception.add(REPORT_REFERENCE_FIELD_NAME, ERR_REPORT_NOT_FOUND);
        }
    }

    /**
     * Vérifie que la valeur du champ représente une référence d'infraction connue.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void existingOffenceReference(String fieldValue) {
        if (fieldValue == null) return;

        if (FALSE.equals(offenceQueries.existsByReference(fieldValue))) {
            exception.add(OFFENCE_REFERENCE_FIELD_NAME, ERR_OFFENCE_NOT_FOUND);
        }
    }

    /**
     * Vérifie que la valeur du champ représente une référence d'employeur connue.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void existingEmployerReference(String fieldValue) {
        if (fieldValue == null) return;

        if (FALSE.equals(employerQueries.existsByEmployerReference(fieldValue))) {
            exception.add(EMPLOYER_REFERENCE_FIELD_NAME, ERR_EMPLOYER_NOT_FOUND);
        }
    }

    /**
     * Vérifie que la valeur du champ représente une référence de dossier connue.
     *
     * @param fieldName  le nom du champ à vérifier
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void existingCaseReference(String fieldName, String fieldValue) {
        if (fieldValue == null) return;

        if (FALSE.equals(caseQueries.existsByCaseReference(fieldValue))) {
            exception.add(fieldName, ERR_CASE_NOT_FOUND);
        }
    }

    /**
     * Vérifie que les valeurs représentent une observation connue.
     *
     * @param observationNumber le numéro d'observation
     * @param reportReference   la référence de constatation
     */
    protected void existingObservation(int observationNumber, String reportReference) {
        if (reportReference == null) return;

        if (FALSE.equals(observationRepository
                .existsByNumberAndReportReference(observationNumber, reportReference))) {
            exception.add(OBSERVATION_NUMBER_FIELD_NAME, ERR_OBSERVATION_NOT_FOUND);
        }
    }

    /**
     * Vérifie que la valeur du champ représente un nom d'utilisateur d'agent connu.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void existingAgentUsername(String fieldValue) {
        if (fieldValue == null) return;

        if (FALSE.equals(agentQueries.existsByUsername(fieldValue))) {
            exception.add(CREATOR_USERNAME_FIELD_NAME, ERR_AGENT_NOT_FOUND);
        }
    }

    /**
     * Vérifie que la valeur du champ représente un motif dossier connu.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void existingCaseMotive(String fieldValue) {
        if (fieldValue == null) return;

        if (!isValidEnum(CaseMotive.class, fieldValue)) {
            exception.add(MOTIVE_FIELD_NAME, ERR_ILLEGAL_MOTIVE);
        }
    }

    /**
     * Vérifie que la valeur du champ représente un statut de constatation valide.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void existingReportStatus(String fieldValue) {
        if (fieldValue == null) return;

        if (!isValidEnum(ReportStatus.class, fieldValue)) {
            exception.add(REPORT_STATUS_FIELD_NAME, ERR_ILLEGAL_REPORT_STATUS);
        }
    }

    /**
     * Vérifie que la valeur du champ représente un résultat d'enquête valide.
     *
     * @param fieldValue la valeur du champ à vérifier
     */
    protected void existingInquiryResult(String fieldValue) {
        if (fieldValue == null) return;

        if (!isValidEnum(InquiryResult.class, fieldValue)) {
            exception.add(RESULT_FIELD_NAME, ERR_ILLEGAL_INQUIRY_RESULT);
        }
    }

    /**
     * Vérifie que la valeur du champ représente une origine dossier valide.
     *
     * @param fieldValue la valeur du champ à vérifier
     * @param motive     le motif dossier encodé
     */
    protected void validCaseOrigin(String fieldValue, String motive) {
        if (fieldValue == null || motive == null) return;

        if (motive.equals("REGULARIZATION_CONTROL")) {
            inquiryReferenceFormat(ORIGIN_FIELD_NAME, fieldValue);
            existingInquiryReference(ORIGIN_FIELD_NAME, fieldValue);
            return;
        }

        var originsForMotive = caseQueries.findOriginsForMotive(motive);
        boolean isInvalidOrigin = !originsForMotive.isEmpty() && originsForMotive.stream()
                .noneMatch(origin -> origin.getReference().equals(fieldValue));

        if (isInvalidOrigin) {
            exception.add(ORIGIN_FIELD_NAME, ERR_ILLEGAL_ORIGIN);
        }
    }
}
