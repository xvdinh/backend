package be.unamur.ingelog.backend.service.cases.comments;

import be.unamur.ingelog.backend.dto.cases.comments.CommentCreateDTO;
import be.unamur.ingelog.backend.dto.cases.comments.CommentDetailDTO;
import be.unamur.ingelog.backend.repository.queries.cases.comments.CommentQueries;
import be.unamur.ingelog.backend.repository.repositories.cases.comments.CommentRepositoryImpl;
import be.unamur.ingelog.backend.service.notifications.events.CommentCreationEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentValidator validator;
    private final CommentMapper mapper;
    private final CommentQueries queries;
    private final CommentRepositoryImpl commentRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public CommentServiceImpl(CommentQueries queries,
                              CommentValidator validator,
                              CommentMapper mapper,
                              CommentRepositoryImpl commentRepository,
                              ApplicationEventPublisher applicationEventPublisher) {
        this.queries = queries;
        this.validator = validator;
        this.mapper = mapper;
        this.commentRepository = commentRepository;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public CommentDetailDTO create(CommentCreateDTO comment, Authentication authentication) {
        validator.check(comment);

        var commentToCreate = mapper.toDomain(comment);
        commentRepository.create(commentToCreate);

        var event = new CommentCreationEvent(this, commentToCreate);
        applicationEventPublisher.publishEvent(event);

        return mapper.toDetailDTO(commentToCreate);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<CommentDetailDTO> findByCaseReference(String caseReference) {
        return queries.findByCaseReference(caseReference);
    }
}
