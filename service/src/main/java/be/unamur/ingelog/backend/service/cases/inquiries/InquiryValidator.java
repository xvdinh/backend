package be.unamur.ingelog.backend.service.cases.inquiries;

import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryReportDateUpdateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryResultUpdateDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.service.util.validation.FieldsValidator;
import org.springframework.stereotype.Component;

@Component
public class InquiryValidator extends FieldsValidator {
    public static final String CASE_REFERENCE_FIELD_NAME = "caseReference";
    public static final String EMPLOYER_REFERENCE_FIELD_NAME = "employerReference";
    public static final String STREET_FIELD_NAME = "street";
    public static final String POSTALCODE_FIELD_NAME = "postalCode";
    public static final String LOCALITY_FIELD_NAME = "locality";
    public static final String REPORT_DATE_FIELD_NAME = "reportDate";
    public static final String RESULT_FIELD_NAME = "result";

    public static final int MIN_STREET_LENGTH = 1;
    public static final int MAX_STREET_LENGTH = 255;
    public static final int MIN_LOCALITY_LENGTH = 1;
    public static final int MAX_LOCALITY_LENGTH = 255;

    /**
     * Vérifie la validité de l'enquête à créer.
     *
     * @param dto l'enquête à vérifier
     * @throws FieldsValidationException si l'enquête contient au moins un champ invalide
     */
    public void check(InquiryCreateDTO dto) throws FieldsValidationException {
        exception = new FieldsValidationException();

        checkCaseReference(dto.getCaseReference());
        checkEmployerReference(dto.getEmployerReference());
        checkStreet(dto.getStreet());
        checkPostalCode(dto.getPostalCode());
        checkLocality(dto.getLocality());

        if (exception.hasErrors()) throw exception;
    }

    public void check(InquiryReportDateUpdateDTO dto) throws FieldsValidationException {
        exception = new FieldsValidationException();

        checkReportDate(dto.getReportDate());

        if (exception.hasErrors()) throw exception;
    }

    public void check(InquiryResultUpdateDTO dto) {
        exception = new FieldsValidationException();

        checkInquiryResult(dto.getResult());

        if (exception.hasErrors()) throw exception;
    }

    /**
     * Vérifie la validité de la référence dossier fournie.
     * Une référence dossier est valide si elle est non null et non vide, qu'elle présente un
     * format de référence de dossier correct et qu'elle correspond à un dossier existant.
     *
     * @param caseReference la référence dossier à vérifier
     */
    private void checkCaseReference(String caseReference) {
        notBlank(CASE_REFERENCE_FIELD_NAME, caseReference);
        caseReferenceFormat(CASE_REFERENCE_FIELD_NAME, caseReference);
        existingCaseReference(CASE_REFERENCE_FIELD_NAME, caseReference);
    }

    /**
     * Vérifie la validité de la référence employeur fournie.
     * Une référence employeur est valide si elle est non null, non vide et qu'elle correspond
     * à un employeur existant.
     *
     * @param employerReference la référence employeur à vérifier
     */
    private void checkEmployerReference(String employerReference) {
        notBlank(EMPLOYER_REFERENCE_FIELD_NAME, employerReference);
        existingEmployerReference(employerReference);
    }

    /**
     * Vérifie la validité de la rue fournie.
     * Une rue est valide si elle est non null, non vide, et qu'elle se conforme à des
     * critères de taille spécifiques.
     *
     * @param street la rue à vérifier
     */
    private void checkStreet(String street) {
        notBlank(STREET_FIELD_NAME, street);
        minLength(STREET_FIELD_NAME, street, MIN_STREET_LENGTH);
        maxLength(STREET_FIELD_NAME, street, MAX_STREET_LENGTH);
    }

    /**
     * Vérifie la validité du code postal fourni.
     * Un code postal est valide s'il est non null, non vide, et qu'il se conforme à des
     * critères de taille spécifiques.
     *
     * @param postalCode le code postal à vérifier
     */
    private void checkPostalCode(String postalCode) {
        notBlank(POSTALCODE_FIELD_NAME, postalCode);
        postalCodeFormat(postalCode);
    }

    /**
     * Vérifie la validité de la localité fournie.
     * Une localité est valide si elle est non null, non vide, et qu'elle se conforme à des
     * critères de taille spécifiques.
     *
     * @param locality la localité à vérifier
     */
    private void checkLocality(String locality) {
        notBlank(LOCALITY_FIELD_NAME, locality);
        minLength(LOCALITY_FIELD_NAME, locality, MIN_LOCALITY_LENGTH);
        maxLength(LOCALITY_FIELD_NAME, locality, MAX_LOCALITY_LENGTH);
    }

    /**
     * Vérifie la validité de la date de constatation fournie.
     *
     * @param reportDate la date de constatation à vérifier
     */
    private void checkReportDate(String reportDate) {
        notBlank(REPORT_DATE_FIELD_NAME, reportDate);
        dateFormat(REPORT_DATE_FIELD_NAME, reportDate);
    }

    /**
     * Vérifie la validité du résultat d'enquête fourni.
     *
     * @param result le résultat d'enquête à vérifier
     */
    private void checkInquiryResult(String result) {
        notBlank(RESULT_FIELD_NAME, result);
        existingInquiryResult(result);
    }
}
