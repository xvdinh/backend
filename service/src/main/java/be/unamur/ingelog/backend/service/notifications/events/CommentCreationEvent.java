package be.unamur.ingelog.backend.service.notifications.events;

import be.unamur.ingelog.backend.domain.cases.CaseComment;
import org.springframework.context.ApplicationEvent;

public class CommentCreationEvent extends ApplicationEvent {
    private final CaseComment caseComment;

    public CommentCreationEvent(Object source, CaseComment caseComment) {
        super(source);
        this.caseComment = caseComment;
    }

    public CaseComment getCaseComment() {
        return this.caseComment;
    }
}
