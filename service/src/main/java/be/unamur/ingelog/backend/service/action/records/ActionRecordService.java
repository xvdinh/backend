package be.unamur.ingelog.backend.service.action.records;

import be.unamur.ingelog.backend.domain.action.records.ActionRecord;

public interface ActionRecordService {
    /**
     * Enregistre une action réalisée par l'utilisateur.
     *
     * @param actionRecord l'action réalisée
     */
    void create(ActionRecord actionRecord);
}
