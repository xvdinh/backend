package be.unamur.ingelog.backend.service.util;

import be.unamur.ingelog.backend.repository.queries.cases.CaseQueries;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.InquiryQueries;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.attachments.AttachmentQueries;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.reports.ReportQueries;
import be.unamur.ingelog.backend.repository.queries.notifications.NotificationQueries;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Boolean.TRUE;
import static java.lang.String.format;

@Service
public class ReferenceGenerator {
    public static final String CASE_CODE = "DOS";
    public static final String INQUIRY_CODE = "ENQ";
    public static final String REPORT_CODE = "CON";
    public static final String NOTIFICATION_CODE = "NOTIF";
    public static final String ATTACHMENT_CODE = "ATTACH";

    public static final String ALLOWED_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private final CaseQueries caseQueries;
    private final InquiryQueries inquiryQueries;
    private final ReportQueries reportQueries;
    private final NotificationQueries notificationQueries;
    private final AttachmentQueries attachmentQueries;

    public ReferenceGenerator(CaseQueries caseQueries,
                              InquiryQueries inquiryQueries,
                              ReportQueries reportQueries,
                              NotificationQueries notificationQueries,
                              AttachmentQueries attachmentQueries) {
        this.caseQueries = caseQueries;
        this.inquiryQueries = inquiryQueries;
        this.reportQueries = reportQueries;
        this.notificationQueries = notificationQueries;
        this.attachmentQueries = attachmentQueries;
    }

    /**
     * Génère une référence de dossier unique.
     *
     * @return la référence de dossier
     */
    public String generateCaseReference() {
        String candidateReference;

        do {
            candidateReference = format(generateCommonPart(), CASE_CODE);
        } while (TRUE.equals(caseQueries.existsByCaseReference(candidateReference)));

        return candidateReference;
    }

    /**
     * Génère une référence d'enquête unique.
     *
     * @return la référence d'enquête
     */
    public String generateInquiryReference() {
        String candidateReference;

        do {
            candidateReference = format(generateCommonPart(), INQUIRY_CODE);
        } while (TRUE.equals(inquiryQueries.existsByInquiryReference(candidateReference)));

        return candidateReference;
    }

    /**
     * Génère une référence de constatation unique.
     *
     * @return la référence de constatation
     */
    public String generateReportReference() {
        String candidateReference;

        do {
            candidateReference = format(generateCommonPart(), REPORT_CODE);
        } while (TRUE.equals(reportQueries.existsByReportReference(candidateReference)));

        return candidateReference;
    }

    /**
     * Génère une référence de notification unique.
     *
     * @return la référence de notification
     */
    public String generateNotificationReference() {
        String candidateReference;

        do {
            candidateReference = format(generateCommonPart(), NOTIFICATION_CODE);
        } while (TRUE.equals(notificationQueries.existsByNotificationReference(candidateReference)));

        return candidateReference;
    }

    /**
     * Génère une référence de pièce jointe unique.
     *
     * @return la référence de pièce jointe
     */
    public String generateAttachmentReference() {
        String candidateReference;

        do {
            candidateReference = format(generateCommonPart(), ATTACHMENT_CODE);
        } while (TRUE.equals(attachmentQueries.existsByAttachmentReference(candidateReference)));

        return candidateReference;
    }

    /**
     * Génère la partie commune à toutes les références.
     *
     * @return la partie commune de la référence
     */
    private String generateCommonPart() {
        var commonPart = new StringBuilder();
        int currentYear = LocalDate.now().getYear();

        commonPart.append(currentYear);
        commonPart.append("-%s-");

        for (int i = 0; i < 5; i++) {
            int random = ThreadLocalRandom.current().nextInt(0, ALLOWED_CHARACTERS.length());
            commonPart.append(ALLOWED_CHARACTERS.charAt(random));
        }

        return commonPart.toString();
    }
}
