package be.unamur.ingelog.backend.service.cases.inquiries.attachments;

import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentDetailDTO;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.attachments.AttachmentQueries;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.attachments.AttachmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class AttachmentServiceImpl implements AttachmentService {
    public static final String UPLOADED_FOLDER = "/tmp/";

    private final AttachmentRepository attachmentRepository;
    private final AttachmentQueries attachmentQueries;
    private final AttachmentValidator attachmentValidator;
    private final AttachmentMapper attachmentMapper;

    @Autowired
    public AttachmentServiceImpl(AttachmentRepository attachmentRepository,
                                 AttachmentQueries attachmentQueries,
                                 AttachmentValidator attachmentValidator,
                                 AttachmentMapper attachmentMapper) {
        this.attachmentRepository = attachmentRepository;
        this.attachmentQueries = attachmentQueries;
        this.attachmentValidator = attachmentValidator;
        this.attachmentMapper = attachmentMapper;
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public AttachmentDetailDTO upload(MultipartFile file, AttachmentCreateDTO metadata) throws IOException {
        attachmentValidator.check(file, metadata);

        var attachment = attachmentMapper.toDomain(file, metadata);
        var path = UPLOADED_FOLDER + attachment.getAttachmentReference() + ".pdf";
        attachment.setPath(path);

        Files.write(Paths.get(path), file.getBytes());
        attachmentRepository.create(attachment);

        return attachmentMapper.toDetailDTO(attachment);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<AttachmentDetailDTO> findByInquiryReference(String inquiryReference) {
        return attachmentQueries.findByInquiryReference(inquiryReference);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public ResponseEntity<Resource> download(String attachmentReference) throws IOException {
        var attachment = attachmentRepository.findByAttachmentReference(attachmentReference);

        var path = Paths.get(attachment.getPath());
        var resource = new ByteArrayResource(Files.readAllBytes(path));
        var originalName = attachment.getOriginalName().replace(" ", "");

        return ResponseEntity.ok()
                .header(CONTENT_DISPOSITION, "attachment;filename=" + originalName)
                .contentType(MediaType.APPLICATION_PDF)
                .body(resource);
    }
}
