package be.unamur.ingelog.backend.service.dashboard;

import be.unamur.ingelog.backend.dto.dashboard.*;

import java.util.List;

public interface DashboardService {
    /**
     * Retourne le nombre d'enquêtes par état pour l'année courante.
     *
     * @return le nombre d'enquêtes par état
     */
    List<TotalInquiriesByStateDTO> getTotalInquiriesByState();

    /**
     * Retourne le nombre d'enquêtes, en analyse ou ouvertes, par niveau de priorité, pour l'année courante.
     *
     * @return le nombre d'enquêtes par priorité
     */
    List<TotalInquiriesByPriorityDTO> getTotalInquiriesByPriority();

    /**
     * Retourne la répartition entre positif et négatif, parmi les enquêtes clôturées pour l'année courante.
     *
     * @return la répartition des enquêtes par résultat
     */
    List<InquiriesDistributionByResultDTO> getInquiriesDistributionByResult();

    /**
     * Retourne le nombre d'enquêtes sans suite, cloturées négativement et
     * cloturées positivement par année, pour les 5 dernières années..
     *
     * @return le nombre d'enquêtes clôtureées par année
     */
    List<TotalClosedInquiriesByYearDTO> getTotalClosedInquiriesByYear();

    /**
     * Retourne le nombre d'enquêtes, en analyse ou ouvertes, par intervenant, pour l'année courante.
     *
     * @return le nombre d'enquêtes par intervenant
     */
    List<TotalInquiriesByAgentDTO> getTotalInquiriesByAgent();
}
