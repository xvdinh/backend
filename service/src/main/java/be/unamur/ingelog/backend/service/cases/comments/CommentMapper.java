package be.unamur.ingelog.backend.service.cases.comments;

import be.unamur.ingelog.backend.domain.cases.CaseComment;
import be.unamur.ingelog.backend.dto.cases.comments.CommentCreateDTO;
import be.unamur.ingelog.backend.dto.cases.comments.CommentDetailDTO;
import be.unamur.ingelog.backend.repository.repositories.agents.AgentRepositoryImpl;
import be.unamur.ingelog.backend.repository.repositories.cases.CaseRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CommentMapper {
    private final AgentRepositoryImpl agentRepository;
    private final CaseRepositoryImpl caseRepository;

    @Autowired
    public CommentMapper(AgentRepositoryImpl agentRepository, CaseRepositoryImpl caseRepository) {
        this.agentRepository = agentRepository;
        this.caseRepository = caseRepository;
    }

    public CaseComment toDomain(CommentCreateDTO dto) {
        return CaseComment.builder()
                .creationDate(LocalDateTime.now())
                .author(agentRepository.findByUsername(getAuthenticatedUsername()))
                .content(dto.getContent())
                .relatedCase(caseRepository.findByCaseReference(dto.getCaseReference()))
                .build();
    }

    public CommentDetailDTO toDetailDTO(CaseComment comment) {
        return CommentDetailDTO.builder()
                .authorFirstName(comment.getAuthor().getFirstName())
                .authorLastName(comment.getAuthor().getLastName())
                .creationDate(comment.getCreationDate())
                .content(comment.getContent())
                .build();
    }

    private String getAuthenticatedUsername() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();
    }
}
