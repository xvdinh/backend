package be.unamur.ingelog.backend.service.cases;

import be.unamur.ingelog.backend.domain.cases.Case;
import be.unamur.ingelog.backend.domain.cases.CaseMotive;
import be.unamur.ingelog.backend.dto.cases.CaseCreateDTO;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.repository.repositories.agents.AgentRepository;
import be.unamur.ingelog.backend.service.util.ReferenceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static be.unamur.ingelog.backend.domain.cases.CaseState.INITIATED;

@Component
public class CaseMapper {
    private final AgentRepository agentRepository;
    private final ReferenceGenerator referenceGenerator;

    @Autowired
    public CaseMapper(AgentRepository agentRepository, ReferenceGenerator referenceGenerator) {
        this.agentRepository = agentRepository;
        this.referenceGenerator = referenceGenerator;
    }

    /**
     * Mappe un CaseCreateDTO en Case.
     *
     * @param dto le CaseCreateDTO source
     * @return le Case obtenu à partir du CaseCreateDTO
     */
    public Case toDomain(CaseCreateDTO dto) {
        return Case.builder()
                .caseReference(referenceGenerator.generateCaseReference())
                .creator(agentRepository.findByUsername(dto.getCreatorUsername()))
                .triggerDate(LocalDate.parse(dto.getTriggerDate()))
                .openDate(LocalDateTime.now())
                .caseMotive(CaseMotive.valueOf(dto.getMotive()))
                .caseOrigin(dto.getOrigin())
                .caseState(INITIATED)
                .description(dto.getDescription())
                .build();
    }

    /**
     * Mappe un Case en ReferenceDTO.
     *
     * @param createdCase le Case source
     * @return le ReferenceDTO à partir du Case
     */
    public ReferenceDTO toReferenceDTO(Case createdCase) {
        return ReferenceDTO.builder()
                .reference(createdCase.getCaseReference())
                .build();
    }
}
