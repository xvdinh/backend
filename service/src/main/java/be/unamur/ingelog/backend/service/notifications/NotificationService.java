package be.unamur.ingelog.backend.service.notifications;

import be.unamur.ingelog.backend.dto.notifications.NotificationDetailDTO;
import be.unamur.ingelog.backend.dto.notifications.NotificationUpdateDTO;
import be.unamur.ingelog.backend.service.notifications.events.*;
import org.springframework.context.event.EventListener;

import java.util.List;

public interface NotificationService {
    /**
     * Indique qu'une notification a été consultée.
     *
     * @param dto la notification à mettre à jour
     */
    void markAsConsulted(NotificationUpdateDTO dto);

    /**
     * Crée une notification lorsqu'un événement de création de commentaire est reçu.
     *
     * @param event l'événement de création de commentaire
     */
    @EventListener
    void onCommentCreationEvent(CommentCreationEvent event);

    /**
     * Crée une notification lorsqu'un événement d'assignation d'enquête est reçu.
     *
     * @param event l'événement d'assignation d'enquête
     */
    @EventListener
    void onInquiryAssignmentEvent(InquiryAssignmentEvent event);

    /**
     * Crée une notification lorsqu'un événément de clôture d'enquête est reçu.
     *
     * @param event l'événement de clôture d'enquête
     */
    @EventListener
    void onInquiryClosureEvent(InquiryClosureEvent event);

    /**
     * Crée une notification lorsqu'un événement de demande de clôture d'enquête est reçu.
     *
     * @param event l'événement de demande de clôture d'enquête
     */
    @EventListener
    void onInquiryClosureRequestEvent(InquiryClosureRequestEvent event);

    /**
     * Crée une notification lorsqu'un événement de renvoi d'enquête est reçu.
     *
     * @param event l'événement de renvoi d'enquête
     */
    @EventListener
    void onInquirySendBackEvent(InquirySendBackEvent event);

    /**
     * Retourne la liste des notifications de l'utilisateur spécifié.
     *
     * @return la liste des notifications
     */
    List<NotificationDetailDTO> findByUsername(Boolean isRead);
}
