package be.unamur.ingelog.backend.service.agents;

import be.unamur.ingelog.backend.dto.agents.AgentAuthDTO;
import be.unamur.ingelog.backend.dto.agents.AgentDetailDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.AgentNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;

import java.util.List;

public interface AgentService {
    /**
     * Renvoie la liste des agents, filtrée selon les crtières éventuels fournis.
     *
     * @param roles les rôles sur base desquels filtrer la liste
     * @return la liste des agents filtrée
     */
    List<AgentDetailDTO> findAll(List<String> roles);

    /**
     * Trouve un agent sur base de son nom d'utilisateur et de son mot de passe.
     *
     * @param username le nom d'utilisateur de l'agent
     * @return l'agent si une correspondance est trouvée pour le nom d'utilisateur et le mot de passe fournis
     * @throws BadCredentialsException si aucune correspondance n'a été trouvée
     */
    AgentAuthDTO authenticate(String username) throws BadCredentialsException;

    /**
     * Trouve un agent sur base de son nom d'utilisateur.
     *
     * @param username le nom d'utilisateur
     * @return l'agent correspondant au nom d'utilisateur fourni
     * @throws AgentNotFoundException si aucun agent n'a été trouvé
     */
    AgentDetailDTO findByUsername(String username) throws AgentNotFoundException;
}
