package be.unamur.ingelog.backend.service.cases.comments;


import be.unamur.ingelog.backend.dto.cases.comments.CommentCreateDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.service.util.validation.FieldsValidator;
import org.springframework.stereotype.Component;

@Component
public class CommentValidator extends FieldsValidator {
    public static final String CASE_REFERENCE_FIELD_NAME = "caseReference";
    public static final String CONTENT_FIELD_NAME = "content";

    public static final int MIN_CONTENT_LENGTH = 1;
    public static final int MAX_CONTENT_LENGTH = 1000;

    /**
     * Vérifie la validité du commentaire à créer.
     *
     * @param dto le commentaire à vérifier
     * @throws FieldsValidationException si le commentaire contient au moins un champ invalide
     */
    public void check(CommentCreateDTO dto) throws FieldsValidationException {
        exception = new FieldsValidationException();

        checkCaseReference(dto.getCaseReference());
        checkContent(dto.getContent());

        if (exception.hasErrors()) throw exception;
    }

    /**
     * Vérifie la validité de la référence dossier fournie.
     * Une référence dossier est valide si elle est non null et non vide, qu'elle présente un
     * format de référence de dossier correct et qu'elle correspond à un dossier existant.
     *
     * @param caseReference la référence dossier à vérifier
     */
    private void checkCaseReference(String caseReference) {
        notBlank(CASE_REFERENCE_FIELD_NAME, caseReference);
        caseReferenceFormat(CASE_REFERENCE_FIELD_NAME, caseReference);
        existingCaseReference(CASE_REFERENCE_FIELD_NAME, caseReference);
    }

    /**
     * Vérifie la validité de la localité fournie.
     * Une localité est valide si elle est non null, non vide, et qu'elle se conforme à des
     * critères de taille spécifiques.
     *
     * @param content la localité à vérifier
     */
    private void checkContent(String content) {
        notBlank(CONTENT_FIELD_NAME, content);
        minLength(CONTENT_FIELD_NAME, content, MIN_CONTENT_LENGTH);
        maxLength(CONTENT_FIELD_NAME, content, MAX_CONTENT_LENGTH);
    }
}
