package be.unamur.ingelog.backend.service.employers;

import be.unamur.ingelog.backend.dto.employers.EmployerDetailDTO;

import java.util.List;

public interface EmployerService {
    /**
     * Trouve l'ensemble des employeurs.
     *
     * @return l'ensemble des employeurs
     */
    List<EmployerDetailDTO> findAllEmployers();

    /**
     * Trouve un employeur sur base de sa référence.
     *
     * @param reference la référence de l'employeur à trouver
     * @return l'employeur correspondant à la référence, s'il existe
     */
    EmployerDetailDTO findByEmployerReference(String reference);
}
