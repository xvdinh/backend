package be.unamur.ingelog.backend.service.cases.inquiries.attachments;

import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentCreateDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.service.util.validation.FieldsValidator;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Component
public class AttachmentValidator extends FieldsValidator {
    public static final String ATTACHMENT_FIELD_NAME = "attachment";
    public static final List<String> ALLOWED_FILE_FORMATS = List.of("pdf");
    public static final String INQUIRY_REFERENCE_FIELD_NAME = "inquiryReference";
    public static final String SOURCE_FIELD_NAME = "source";
    public static final String DOCUMENT_TYPE_FIELD_NAME = "documentType";

    public static final int MIN_SOURCE_LENGTH = 1;
    public static final int MAX_SOURCE_LENGTH = 255;
    public static final int MIN_DOCUMENT_TYPE_LENGTH = 1;
    public static final int MAX_DOCUMENT_TYPE_LENGTH = 255;

    /**
     * Vérifie la validité des meta données fournies.
     *
     * @param attachment la pièce jointe
     * @param metadata   les meta données
     * @throws FieldsValidationException si les meta données ne sont pas valides
     */
    public void check(MultipartFile attachment, AttachmentCreateDTO metadata) throws FieldsValidationException {
        exception = new FieldsValidationException();

        checkAttachment(attachment);
        checkInquiryReference(metadata.getInquiryReference());
        checkSource(metadata.getSource());
        checkDocumentType(metadata.getDocumentType());

        if (exception.hasErrors()) throw exception;
    }

    /**
     * Vérifie la validité de la pièce jointe fournie.
     * Une pièce jointe est valide si elle est au format PDF.
     *
     * @param attachment la pièce jointe à vérifier
     */
    private void checkAttachment(MultipartFile attachment) {
        allowedFileFormat(attachment);
    }

    /**
     * Vérifie la validité du type de document fourni.
     * Un type de document est valide si il est non null et non vide, et qu'il
     * rencontre des critères de taille spécifiques.
     *
     * @param documentType le type de document à vérifier
     */
    private void checkDocumentType(String documentType) {
        notBlank(DOCUMENT_TYPE_FIELD_NAME, documentType);
        minLength(DOCUMENT_TYPE_FIELD_NAME, documentType, MIN_DOCUMENT_TYPE_LENGTH);
        maxLength(DOCUMENT_TYPE_FIELD_NAME, documentType, MAX_DOCUMENT_TYPE_LENGTH);
    }

    /**
     * Vérifie la validité de la source fournie.
     * Une source est valide si elle est non null et non vied, et qu'elle
     * rencontre des critères de taille spécifiques.
     *
     * @param source la source à vérifier
     */
    private void checkSource(String source) {
        notBlank(SOURCE_FIELD_NAME, source);
        minLength(SOURCE_FIELD_NAME, source, MIN_SOURCE_LENGTH);
        maxLength(SOURCE_FIELD_NAME, source, MAX_SOURCE_LENGTH);
    }

    /**
     * Vérifie la validité de la référence d'enquête fournie.
     * Une référence d'enquête est valide si elle est non null et non vide, et qu'elle
     * correspond à une enquête existant.
     *
     * @param inquiryReference la référence d'enquête à vérifier
     */
    private void checkInquiryReference(String inquiryReference) {
        notBlank(INQUIRY_REFERENCE_FIELD_NAME, inquiryReference);
        inquiryReferenceFormat(INQUIRY_REFERENCE_FIELD_NAME, inquiryReference);
        existingInquiryReference(INQUIRY_REFERENCE_FIELD_NAME, inquiryReference);
    }
}
