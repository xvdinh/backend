package be.unamur.ingelog.backend.service.cases.inquiries.reports;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.Report;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportStateDTO;
import be.unamur.ingelog.backend.repository.repositories.cases.CaseRepositoryImpl;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.InquiryRepositoryImpl;
import be.unamur.ingelog.backend.repository.repositories.law.articles.OffenceRepositoryImpl;
import be.unamur.ingelog.backend.service.util.ReferenceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportState.CREATED;

@Component
public class ReportMapper {
    private final InquiryRepositoryImpl inquiryRepository;
    private final OffenceRepositoryImpl offenceRepository;
    private final CaseRepositoryImpl caseRepository;
    private final ReferenceGenerator referenceGenerator;

    @Autowired
    public ReportMapper(ReferenceGenerator referenceGenerator,
                        InquiryRepositoryImpl inquiryRepository,
                        OffenceRepositoryImpl offenceRepository,
                        CaseRepositoryImpl caseRepository) {
        this.referenceGenerator = referenceGenerator;
        this.inquiryRepository = inquiryRepository;
        this.offenceRepository = offenceRepository;
        this.caseRepository = caseRepository;
    }

    /**
     * Mappe un ReportCreateDTO en Report.
     *
     * @param dto le ReportCreateDTO source
     * @return le Report obtenu à partir du ReportCreateDTO
     */
    public Report toDomain(ReportCreateDTO dto) {
        var relatedInquiry = inquiryRepository.findByInquiryReference(dto.getInquiryReference());

        var report = Report.builder()
                .reportReference(referenceGenerator.generateReportReference())
                .reportState(CREATED)
                .relatedInquiry(relatedInquiry)
                .build();

        relatedInquiry.setRelatedCase(caseRepository.findByInquiryReference(dto.getInquiryReference()));
        report.setOffence(offenceRepository.findByOffenceReference(dto.getOffenceReference()));

        return report;
    }

    /**
     * Mappe un Report en ReportStateDTO.
     *
     * @param report le Report source
     * @return le ReportStateDTO obtenu à partir du Report
     */
    public ReportStateDTO toReportStateDTO(Report report) {
        return ReportStateDTO.builder()
                .reportState(report.getReportState().toString())
                .build();
    }
}
