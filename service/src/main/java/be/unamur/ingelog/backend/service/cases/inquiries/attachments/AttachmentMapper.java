package be.unamur.ingelog.backend.service.cases.inquiries.attachments;

import be.unamur.ingelog.backend.domain.cases.inquiries.attachments.Attachment;
import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentDetailDTO;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.InquiryRepository;
import be.unamur.ingelog.backend.service.util.ReferenceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.Objects;

import static org.springframework.util.StringUtils.cleanPath;

@Service
public class AttachmentMapper {
    private final InquiryRepository inquiryRepository;
    private final ReferenceGenerator referenceGenerator;

    @Autowired
    public AttachmentMapper(InquiryRepository inquiryRepository,
                            ReferenceGenerator referenceGenerator) {
        this.inquiryRepository = inquiryRepository;
        this.referenceGenerator = referenceGenerator;
    }

    /**
     * Mappe un AttachmentCreateDTO en Attachment.
     *
     * @return l'Attachment obtenu à partir de l'AttachmentCreateDTO
     */
    public Attachment toDomain(MultipartFile file, AttachmentCreateDTO dto) {
        var relatedInquiry = inquiryRepository
                .findByInquiryReference(dto.getInquiryReference());

        return Attachment.builder()
                .attachmentReference(referenceGenerator.generateAttachmentReference())
                .originalName(cleanPath(Objects.requireNonNull(file.getOriginalFilename())))
                .relatedInquiry(relatedInquiry)
                .documentType(dto.getDocumentType())
                .receptionDate(LocalDateTime.now())
                .source(dto.getSource())
                .build();
    }

    /**
     * Mappe un Attachment en AttachmentDetailDTO.
     *
     * @param attachment l'Attachment source
     * @return l'AttachmentDetailDTO obtenu à partir de l'Attachment
     */
    public AttachmentDetailDTO toDetailDTO(Attachment attachment) {
        return AttachmentDetailDTO.builder()
                .inquiryReference(attachment.getRelatedInquiry().getInquiryReference())
                .attachmentReference(attachment.getAttachmentReference())
                .originalName(attachment.getOriginalName())
                .documentType(attachment.getDocumentType())
                .source(attachment.getSource())
                .receptionDate(attachment.getReceptionDate())
                .build();
    }
}
