package be.unamur.ingelog.backend.service.notifications.events;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import org.springframework.context.ApplicationEvent;

public class InquiryClosureEvent extends ApplicationEvent {
    private final Inquiry inquiry;

    public InquiryClosureEvent(Object source, Inquiry inquiry) {
        super(source);
        this.inquiry = inquiry;
    }

    public Inquiry getInquiry() {
        return inquiry;
    }
}
