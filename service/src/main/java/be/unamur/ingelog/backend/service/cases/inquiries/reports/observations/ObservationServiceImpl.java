package be.unamur.ingelog.backend.service.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.Report;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportObservation;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportStateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationDetailDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationNumberDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationRoadmapDTO;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.reports.observations.ObservationQueries;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.InquiryRepository;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.ReportRepository;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.ReportRepositoryImpl;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.observations.ObservationRepository;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.observations.ObservationRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class ObservationServiceImpl implements ObservationService {
    private final ObservationValidator validator;
    private final ObservationMapper mapper;
    private final ObservationQueries queries;
    private final ReportRepository reportRepository;
    private final ObservationRepository observationRepository;
    private final InquiryRepository inquiryRepository;

    @Autowired
    public ObservationServiceImpl(ObservationValidator validator,
                                  ObservationMapper mapper,
                                  ObservationQueries queries,
                                  ReportRepositoryImpl reportRepository,
                                  ObservationRepositoryImpl observationRepository,
                                  InquiryRepository inquiryRepository) {
        this.validator = validator;
        this.mapper = mapper;
        this.queries = queries;
        this.reportRepository = reportRepository;
        this.observationRepository = observationRepository;
        this.inquiryRepository = inquiryRepository;
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public ObservationNumberDTO create(ObservationCreateDTO dto) {
        validator.check(dto);

        var observation = mapper.toDomain(dto);
        var relatedReport = observation.getRelatedReport();

        enrichWithObservations(relatedReport);
        relatedReport.addObservation(observation);

        observationRepository.create(observation);

        return mapper.toObservationNumberDTO(observation);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<ObservationDetailDTO> findByReportReference(String reportReference) {
        return queries.findByReportReference(reportReference);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public ReportStateDTO encodeRoadmap(ObservationRoadmapDTO dto) {
        validator.check(dto);

        var observation = fetchObservation(dto);
        enrichWithReport(observation, dto.getReportReference());
        enrichWithInquiry(observation);

        observation.encodeRoadmap(dto.getValue(), dto.getDescription());
        observationRepository.update(observation);

        return signalObservationEncoding(dto.getReportReference());
    }

    /**
     * Signale l'encodage d'une observation à la constatation liée.
     *
     * @param reportReference la référence de la constatation
     * @return l'état de la constatation
     */
    private ReportStateDTO signalObservationEncoding(String reportReference) {
        var relatedReport = reportRepository.findByReportReference(reportReference);
        enrichWithObservations(relatedReport);

        relatedReport.signalEncoding();
        reportRepository.update(relatedReport);

        return new ReportStateDTO(relatedReport.getReportState().toString());
    }

    private void enrichWithObservations(Report report) {
        var reportReference = report.getReportReference();
        var observations = observationRepository
                .findByReportReference(reportReference);

        report.setReportObservations(observations);
    }

    private ReportObservation fetchObservation(ObservationRoadmapDTO dto) {
        Integer observationNumber = dto.getObservationNumber();
        String reportReference = dto.getReportReference();

        return observationRepository
                .findByReportReferenceAndObservationNumber(reportReference, observationNumber);
    }

    private void enrichWithReport(ReportObservation observation, String reportReference) {
        var relatedReport = reportRepository.findByReportReference(reportReference);

        observation.setRelatedReport(relatedReport);
    }

    private void enrichWithInquiry(ReportObservation observation) {
        var report = observation.getRelatedReport();
        var inquiry = inquiryRepository.findByReportReference(report.getReportReference());

        report.setRelatedInquiry(inquiry);
    }
}
