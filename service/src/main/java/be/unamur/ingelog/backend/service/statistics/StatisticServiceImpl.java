package be.unamur.ingelog.backend.service.statistics;

import be.unamur.ingelog.backend.dto.statistics.CollectionOfInquiriesDTO;
import be.unamur.ingelog.backend.repository.queries.statistics.StatisticQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class StatisticServiceImpl implements StatisticService {
    private final StatisticQueries queries;

    @Autowired
    public StatisticServiceImpl(StatisticQueries queries) {
        this.queries = queries;
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public CollectionOfInquiriesDTO findInquiriesData(Map<String, String> filter) {
        return queries.findInquiriesData(filter);
    }
}
