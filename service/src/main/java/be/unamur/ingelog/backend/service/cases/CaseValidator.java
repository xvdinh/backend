package be.unamur.ingelog.backend.service.cases;

import be.unamur.ingelog.backend.dto.cases.CaseCreateDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.service.util.validation.FieldsValidator;
import org.springframework.stereotype.Component;

@Component
public class CaseValidator extends FieldsValidator {
    public static final String TRIGGER_DATE_FIELD_NAME = "triggerDate";
    public static final String CREATOR_USERNAME_FIELD_NAME = "creatorUsername";
    public static final String DESCRIPTION_FIELD_NAME = "description";
    public static final String MOTIVE_FIELD_NAME = "motive";
    public static final String ORIGIN_FIELD_NAME = "origin";

    public static final int MIN_DESCRIPTION_LENGTH = 1;
    public static final int MAX_DESCRIPTION_LENGTH = 255;
    public static final int MIN_ORIGIN_LENGTH = 1;
    public static final int MAX_ORIGIN_LENGTH = 255;

    /**
     * Vérifie la validité du dossier à créer.
     *
     * @param dto le dossier à vérifier
     * @throws FieldsValidationException si le dossier contient au moins un champ invalide
     */
    public void check(CaseCreateDTO dto) throws FieldsValidationException {
        exception = new FieldsValidationException();

        checkCreatorUsername(dto.getCreatorUsername());
        checkTriggerDate(dto.getTriggerDate());
        checkMotive(dto.getMotive());
        checkOrigin(dto.getMotive(), dto.getOrigin());
        checkDescription(dto.getDescription());

        if (exception.hasErrors()) throw exception;
    }

    /**
     * Vérifie la validité du nom d'utilisateur du créateur.
     * Un nom d'utilisateur est valide s'il est non null et non vide, et qu'il correspond à
     * un agent existant.
     *
     * @param creatorUsername le nom d'utilisateur à vérifier
     */
    private void checkCreatorUsername(String creatorUsername) {
        notBlank(CREATOR_USERNAME_FIELD_NAME, creatorUsername);
        existingAgentUsername(creatorUsername);
    }

    /**
     * Vérifie la validité de la date de déclencheur fournie.
     * Une date de déclencheur est valide si elle est non null et non vide, qu'elle présente un format
     * yyyy-MM-dd et qu'elle n'est pas située dans le futur.
     *
     * @param triggerDate la date de déclencheur à vérifier
     */
    private void checkTriggerDate(String triggerDate) {
        notBlank(TRIGGER_DATE_FIELD_NAME, triggerDate);
        dateFormat(TRIGGER_DATE_FIELD_NAME, triggerDate);
        pastOrPresent(triggerDate);
    }

    /**
     * Vérifie la validité du motif fourni.
     * Un motif est valide s'il est non null, non vide, et qu'il est contenu dans la liste
     * des motifs prédéfinis.
     *
     * @param motive le motif à vérifier
     */
    private void checkMotive(String motive) {
        notBlank(MOTIVE_FIELD_NAME, motive);
        existingCaseMotive(motive);
    }

    /**
     * Vérifie la validité de l'origine fournie.
     * Une origine est valide si elle est non null et non vide. Si le motif fourni n'accepte
     * que des origines prédéfinies, l'origine doit se trouver dans cette liste. Si le motif
     * n'accepte qu'une référence d'enquête, l'origine doit avoir un format de référence d'enquête
     * correct et correspondre à une enquête existante. Sinon, elle est libre mais doit se conformer
     * à des critères de taille spécifiques.
     *
     * @param motive le motif correspondant à l'origine
     * @param origin l'origine à vérifier
     */
    private void checkOrigin(String motive, String origin) {
        notBlank(ORIGIN_FIELD_NAME, origin);
        validCaseOrigin(origin, motive);
        minLength(ORIGIN_FIELD_NAME, origin, MIN_ORIGIN_LENGTH);
        maxLength(ORIGIN_FIELD_NAME, origin, MAX_ORIGIN_LENGTH);
    }

    /**
     * Vérifie la validité de la description fournie.
     * Une description est valide si elle est non null et non vide et qu'elle se conforme à des
     * critères de taille spécifiques.
     *
     * @param description la description à vérifier
     */
    private void checkDescription(String description) {
        notBlank(DESCRIPTION_FIELD_NAME, description);
        minLength(DESCRIPTION_FIELD_NAME, description, MIN_DESCRIPTION_LENGTH);
        maxLength(DESCRIPTION_FIELD_NAME, description, MAX_DESCRIPTION_LENGTH);
    }
}
