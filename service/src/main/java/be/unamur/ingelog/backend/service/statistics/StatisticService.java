package be.unamur.ingelog.backend.service.statistics;

import be.unamur.ingelog.backend.dto.statistics.CollectionOfInquiriesDTO;

import java.util.Map;

public interface StatisticService {
    /**
     * Retourne les données des enquêtes filtrées selon le filtre fourni.
     *
     * @param filter les critères de filtrage
     * @return les données des enquêtes filtrées
     */
    CollectionOfInquiriesDTO findInquiriesData(Map<String, String> filter);
}
