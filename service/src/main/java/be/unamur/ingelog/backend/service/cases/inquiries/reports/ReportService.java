package be.unamur.ingelog.backend.service.cases.inquiries.reports;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.ReportNotFoundException;

import java.util.List;

public interface ReportService {
    /**
     * Crée une nouvelle constatation dans la base de données.
     *
     * @param dto la constatation à créer
     * @return la référence de la constatation nouvellement créée
     */
    ReferenceDTO create(ReportCreateDTO dto);

    /**
     * Trouve les constatations liées à la référence de l'enquête fournie.
     *
     * @param inquiryReference la référence de l'enquête pour laquelle trouver les constatations liées
     * @return la liste des constatations liées à l'enquête identifiée par la référence
     */
    List<ReportDetailDTO> findByInquiryReference(String inquiryReference);

    /**
     * Trouve une constatation sur base de sa référence.
     *
     * @param reportReference la référence de la constatation
     * @return la constatation correspondant à la référence, si elle existe
     * @throws ReportNotFoundException si aucune constatation n'a été trouvée
     */
    ReportDetailDTO findByReportReference(String reportReference) throws ReportNotFoundException;

    /**
     * Trouve l'ensemble des constatations.
     *
     * @return la liste complète des constatations
     */
    List<ReportDetailDTO> findAllReports();

    /**
     * Modifie le nombre de personnes contrôlées avec la valeur fournie.
     *
     * @param dto       le nombre de personnes contrôlées
     * @param reference la référence de la constatation à modifier
     * @return l'état de la constatation liée au nombre de personnes contrôlées après encodage
     */
    ReportStateDTO updateControlsCount(ReportControlsUpdateDTO dto, String reference);

    /**
     * Modifie le statut de la constatation avec la valeur fournie.
     *
     * @param dto       la valeur du nouveau statut
     * @param reference la référence de la constatation à modifier
     */
    ReportStateDTO updateStatus(ReportStatusUpdateDTO dto, String reference);

    /**
     * Retourne la liste de tous les statuts de constatation disponibles.
     *
     * @return la liste de toutes les statuts de constatation
     */
    List<ReportStatusUpdateDTO> findAllStatus();
}
