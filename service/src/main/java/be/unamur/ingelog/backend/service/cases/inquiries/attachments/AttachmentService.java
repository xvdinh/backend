package be.unamur.ingelog.backend.service.cases.inquiries.attachments;

import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.attachments.AttachmentDetailDTO;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface AttachmentService {
    /**
     * Upload un fichier sur le serveur.
     *
     * @param attachment le fichier à uploader
     * @param metadata   les meta données du fichier
     * @return les meta données complétées
     * @throws IOException si un problème d'écriture survient
     */
    AttachmentDetailDTO upload(MultipartFile attachment, AttachmentCreateDTO metadata) throws IOException;

    /**
     * Retourne la liste des pièces jointes liées à l'enquête.
     *
     * @param inquiryReference la référence de l'enquête
     * @return la liste des pièces jointes
     */
    List<AttachmentDetailDTO> findByInquiryReference(String inquiryReference);

    /**
     * Télécharge une pièce jointe.
     *
     * @param attachmentReference la référence de la pièce jointe
     * @return la pièce jointe demandée
     */
    ResponseEntity<Resource> download(String attachmentReference) throws IOException;
}
