package be.unamur.ingelog.backend.service.dashboard;

import be.unamur.ingelog.backend.dto.dashboard.*;
import be.unamur.ingelog.backend.repository.queries.dashboard.DashboardQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class DashboardServiceImpl implements DashboardService {
    private final DashboardQueries dashboardQueries;

    @Autowired
    public DashboardServiceImpl(DashboardQueries dashboardQueries) {
        this.dashboardQueries = dashboardQueries;
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<TotalInquiriesByStateDTO> getTotalInquiriesByState() {
        return dashboardQueries.getTotalInquiriesByState();
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<TotalInquiriesByPriorityDTO> getTotalInquiriesByPriority() {
        return dashboardQueries.getTotalInquiriesByPriority();
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<InquiriesDistributionByResultDTO> getInquiriesDistributionByResult() {
        return dashboardQueries.getInquiriesDistributionByResult();
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<TotalClosedInquiriesByYearDTO> getTotalClosedInquiriesByYear() {
        return dashboardQueries.getTotalClosedInquiriesByYear();
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<TotalInquiriesByAgentDTO> getTotalInquiriesByAgent() {
        return dashboardQueries.getTotalInquiriesByAgent();
    }
}
