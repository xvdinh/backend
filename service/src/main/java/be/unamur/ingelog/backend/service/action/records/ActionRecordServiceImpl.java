package be.unamur.ingelog.backend.service.action.records;

import be.unamur.ingelog.backend.domain.action.records.ActionRecord;
import be.unamur.ingelog.backend.repository.repositories.action.records.ActionRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.transaction.annotation.Propagation.REQUIRED;

@Service
public class ActionRecordServiceImpl implements ActionRecordService {
    private final ActionRecordRepository actionRecordRepository;

    @Autowired
    public ActionRecordServiceImpl(ActionRecordRepository actionRecordRepository) {
        this.actionRecordRepository = actionRecordRepository;
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void create(ActionRecord actionRecord) {
        actionRecordRepository.create(actionRecord);
    }
}
