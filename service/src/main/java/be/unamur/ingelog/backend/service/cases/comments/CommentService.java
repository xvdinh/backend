package be.unamur.ingelog.backend.service.cases.comments;

import be.unamur.ingelog.backend.dto.cases.comments.CommentCreateDTO;
import be.unamur.ingelog.backend.dto.cases.comments.CommentDetailDTO;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface CommentService {
    /**
     * Ajoute un commentaire à un dossier.
     *
     * @param comment le commentaire à ajouter
     * @return le commentaire nouvellement créé
     */
    CommentDetailDTO create(CommentCreateDTO comment, Authentication authentication);

    /**
     * Trouve les commentaires liés à la référence dossier fournie.
     *
     * @param caseReference la référence du dossier pour lequel trouver les commentaires liés
     * @return la liste des commentaires liés au dossier
     */
    List<CommentDetailDTO> findByCaseReference(String caseReference);
}
