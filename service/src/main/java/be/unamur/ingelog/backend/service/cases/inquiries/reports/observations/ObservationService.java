package be.unamur.ingelog.backend.service.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportStateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationDetailDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationNumberDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationRoadmapDTO;

import java.util.List;

public interface ObservationService {
    /**
     * Crée une nouvelle observation.
     *
     * @param dto l'observation à créer
     * @return le numéro de l'observation nouvellement créée
     */
    ObservationNumberDTO create(ObservationCreateDTO dto);

    /**
     * Trouve les observations liées à la référence de constatation fournie.
     *
     * @param reportReference la référence de la constatation pour laquelle trouver les observations liées
     * @return la liste des observations liées à la constatation
     */
    List<ObservationDetailDTO> findByReportReference(String reportReference);

    /**
     * Encode la feuille de route.
     *
     * @param dto les informations de la feuille de route à encoder
     * @return l'état de la constatation liée à la feuille de route après encodage
     */
    ReportStateDTO encodeRoadmap(ObservationRoadmapDTO dto);
}
