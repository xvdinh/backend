package be.unamur.ingelog.backend.service.employers;

import be.unamur.ingelog.backend.dto.employers.EmployerDetailDTO;
import be.unamur.ingelog.backend.repository.queries.employers.EmployerQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class EmployerServiceImpl implements EmployerService {
    private final EmployerQueries queries;

    @Autowired
    public EmployerServiceImpl(EmployerQueries queries) {
        this.queries = queries;
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<EmployerDetailDTO> findAllEmployers() {
        return queries.findAllEmployers();
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public EmployerDetailDTO findByEmployerReference(String reference) {
        return queries.findByEmployerReference(reference);
    }
}
