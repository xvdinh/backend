package be.unamur.ingelog.backend.service.agents;

import be.unamur.ingelog.backend.dto.agents.AgentAuthDTO;
import be.unamur.ingelog.backend.dto.agents.AgentDetailDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.AgentNotFoundException;
import be.unamur.ingelog.backend.repository.queries.agents.AgentQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class AgentServiceImpl implements AgentService {
    private final AgentQueries agentQueries;

    @Autowired
    public AgentServiceImpl(AgentQueries agentQueries) {
        this.agentQueries = agentQueries;
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<AgentDetailDTO> findAll(List<String> roles) {
        return agentQueries.findAll().stream()
                .filter(agent -> roles == null || roles.contains(agent.getRole()))
                .collect(toList());
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public AgentAuthDTO authenticate(String username) throws BadCredentialsException {
        return agentQueries.authenticate(username);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public AgentDetailDTO findByUsername(String username) throws AgentNotFoundException {
        return agentQueries.findByUsername(username);
    }
}
