package be.unamur.ingelog.backend.service.util;

import be.unamur.ingelog.backend.dto.notifications.NotificationUpdateDTO;
import be.unamur.ingelog.backend.repository.repositories.agents.AgentRepository;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.InquiryRepository;
import be.unamur.ingelog.backend.repository.repositories.notifications.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityService {
    private final InquiryRepository inquiryRepository;
    private final AgentRepository agentRepository;
    private final NotificationRepository notificationRepository;

    @Autowired
    public SecurityService(InquiryRepository inquiryRepository,
                           AgentRepository agentRepository,
                           NotificationRepository notificationRepository) {
        this.inquiryRepository = inquiryRepository;
        this.agentRepository = agentRepository;
        this.notificationRepository = notificationRepository;
    }

    /**
     * Indique si l'utilisateur connecté est assigné à l'enquête relative
     * à la constatation modifiée.
     *
     * @param reportReference la référence de la constatation
     * @return true si l'utilisateur connecté est assigné; false sinon
     */
    public boolean isAssigneeByReportReference(String reportReference) {
        var relatedInquiry = inquiryRepository.findByReportReference(reportReference);
        var authenticatedUser = agentRepository.findByUsername(getAuthenticatedUsername());

        return relatedInquiry.getAssignee().getAgentId()
                .equals(authenticatedUser.getAgentId());
    }

    /**
     * Indique si l'utilisateur connecté est assigné à l'enquête correspondant à la référence.
     *
     * @param inquiryReference la référence de l'enquête
     * @return true si l'utilisateur connecté est assigné; false sinon
     */
    public boolean isAssigneeByInquiryReference(String inquiryReference) {
        var relatedInquiry = inquiryRepository.findByInquiryReference(inquiryReference);
        var authenticatedUser = agentRepository.findByUsername(getAuthenticatedUsername());

        return relatedInquiry.getAssignee().getAgentId()
                .equals(authenticatedUser.getAgentId());
    }

    /**
     * Indique si l'utilisateur connecté est le destinaitaire de la notification.
     *
     * @param dto le NotificationUpdateDTO qui contient les informations de la notification
     * @return true si l'utilisateur connecté est le destinataire; false sinon
     */
    public boolean isNotificationRecipient(NotificationUpdateDTO dto) {
        var notificationReference = dto.getNotificationReference();
        var notification = notificationRepository.findByNotificationReference(notificationReference);
        var authenticatedUser = agentRepository.findByUsername(getAuthenticatedUsername());

        return notification.getRecipient().getAgentId()
                .equals(authenticatedUser.getAgentId());
    }

    /**
     * Retourne le nom d'utilisateur de l'utilisateur connecté.
     *
     * @return le nom d'utilisateur
     */
    private String getAuthenticatedUsername() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();
    }
}
