package be.unamur.ingelog.backend.service.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationRoadmapDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import be.unamur.ingelog.backend.service.util.validation.FieldsValidator;
import org.springframework.stereotype.Service;

@Service
public class ObservationValidator extends FieldsValidator {
    public static final String REPORT_REFERENCE_FIELD_NAME = "reportReference";
    public static final String TITLE_FIELD_NAME = "title";
    public static final String UNIT_FIELD_NAME = "unit";
    public static final String OBSERVATION_NUMBER_FIELD_NAME = "observationNumber";
    public static final String VALUE_FIELD_NAME = "value";
    public static final String DESCRIPTION_FIELD_NAME = "description";

    public static final int MIN_TITLE_LENGTH = 1;
    public static final int MAX_TITLE_LENGTH = 255;
    public static final int MIN_UNIT_LENGTH = 1;
    public static final int MAX_UNIT_LENGTH = 255;
    public static final int MIN_VALUE_LENGTH = 1;
    public static final int MAX_VALUE_LENGTH = 255;
    public static final int MIN_DESCRIPTION_LENGTH = 1;
    public static final int MAX_DESCRIPTION_LENGTH = 1000;

    public static final int MAX_OBSERVATION_NUMBER = 5;

    /**
     * Vérifie la validité de l'enquête à créer.
     *
     * @param dto l'enquête à vérifier
     * @throws FieldsValidationException si l'enquête contient au moins un champ invalide
     */
    public void check(ObservationCreateDTO dto) throws FieldsValidationException {
        exception = new FieldsValidationException();

        checkReportReference(dto.getReportReference());
        checkTitle(dto.getTitle());
        checkUnit(dto.getUnit());

        if (exception.hasErrors()) throw exception;
    }

    /**
     * Vérifie la validité de la feuille de route encodée.
     *
     * @param dto la feuille de route à vérifier
     * @throws FieldsValidationException si la feuille de route contient au moins un champ invalide
     */
    public void check(ObservationRoadmapDTO dto) throws FieldsValidationException {
        exception = new FieldsValidationException();

        checkReportReference(dto.getReportReference());
        checkObservationNumber(dto.getObservationNumber(), dto.getReportReference());
        checkValue(dto.getValue());
        checkDescription(dto.getDescription());

        if (exception.hasErrors()) throw exception;
    }

    /**
     * Vérifie la validité de la référence de constatation fournie.
     * Une référence de constatation est valide si elle est non null et non vide, qu'elle présente un
     * format de référence de constatation correct et qu'elle correspond à une constatation existante.
     *
     * @param reportReference la référence de constatation à vérifier
     */
    private void checkReportReference(String reportReference) {
        notBlank(REPORT_REFERENCE_FIELD_NAME, reportReference);
        reportReferenceFormat(reportReference);
        existingReportReference(reportReference);
    }

    /**
     * Vérifie la validité de l'intitulé fourni.
     * Un intitulé est valide s'il est non null et non vide, et qu'il se conforme à des
     * critères de taille spécifiques.
     *
     * @param title l'intitulé à vérifier
     */
    private void checkTitle(String title) {
        notBlank(TITLE_FIELD_NAME, title);
        minLength(TITLE_FIELD_NAME, title, MIN_TITLE_LENGTH);
        maxLength(TITLE_FIELD_NAME, title, MAX_TITLE_LENGTH);
    }

    /**
     * Vérifie la validité de l'unité fournie.
     * Une unité est valide si elle est non null et non vide, et qu'elle se conforme à des
     * critères de taille spécifiques.
     *
     * @param unit l'unité à vérifier
     */
    private void checkUnit(String unit) {
        notBlank(UNIT_FIELD_NAME, unit);
        minLength(UNIT_FIELD_NAME, unit, MIN_UNIT_LENGTH);
        maxLength(UNIT_FIELD_NAME, unit, MAX_UNIT_LENGTH);
    }

    /**
     * Vérifie la validité du numéro d'observation fourni.
     * Un numéro d'observation est valide si, combiné à la référence de constatation, il
     * correspond à une observation existante.
     *
     * @param observationNumber le numéro d'observation à vérifier
     * @param reportReference   la référence de constatation
     */
    private void checkObservationNumber(int observationNumber, String reportReference) {
        notBlank(OBSERVATION_NUMBER_FIELD_NAME, observationNumber == 0 ? "" : Integer.toString(observationNumber));
        existingObservation(observationNumber, reportReference);
    }

    /**
     * Vérifie la validité de la valeur fournie.
     * Une valeur est valide si elle est non nulle et non vide, et qu'elle se conforme à des
     * critères de taille spécifiques.
     *
     * @param value la valeur à vérifier
     */
    private void checkValue(String value) {
        notBlank(VALUE_FIELD_NAME, value);
        minLength(VALUE_FIELD_NAME, value, MIN_VALUE_LENGTH);
        maxLength(VALUE_FIELD_NAME, value, MAX_VALUE_LENGTH);
    }

    /**
     * Vérifie la validité de la description fournie.
     * Une description est valide si elle est non nulle et non vide, et qu'elle se conforme à des
     * critères de taille spécifiques.
     *
     * @param description la description à vérifier
     */
    private void checkDescription(String description) {
        notBlank(DESCRIPTION_FIELD_NAME, description);
        minLength(DESCRIPTION_FIELD_NAME, description, MIN_DESCRIPTION_LENGTH);
        maxLength(DESCRIPTION_FIELD_NAME, description, MAX_DESCRIPTION_LENGTH);
    }
}
