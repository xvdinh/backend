package be.unamur.ingelog.backend.service.cases;

import be.unamur.ingelog.backend.dto.cases.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.CaseNotFoundException;

import java.util.List;

public interface CaseService {
    /**
     * Crée un nouveau dossier.
     *
     * @param dto le dossier à créer
     * @return la référence du dossier nouvellement créé
     */
    ReferenceDTO create(CaseCreateDTO dto);

    /**
     * Trouve l'ensemble des dossiers.
     *
     * @return la liste complète des dossiers
     */
    List<CaseDetailDTO> findAllCases(CaseFilterDTO filter);

    /**
     * Trouve un dossier sur base de sa référence.
     *
     * @param caseReference la référence du dossier à trouver
     * @return le dossier correspondant à la référence, s'il existe
     * @throws CaseNotFoundException si aucun dossier n'a pu être trouvé pour cette référence
     */
    CaseDetailDTO findByCaseReference(String caseReference) throws CaseNotFoundException;

    /**
     * Renvoie la liste de toutes les origines disponibles.
     *
     * @return la liste de toutes les origines
     */
    List<CaseOriginDTO> findAllOrigins();

    /**
     * Trouve la liste des motifs possibles pour la création d'un dossier.
     *
     * @return la liste des motifs possibles pour la création d'un dossier
     */
    List<CaseMotiveDTO> findAllMotives();
}
