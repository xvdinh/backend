package be.unamur.ingelog.backend.service.law.articles;

import be.unamur.ingelog.backend.infrastructure.exceptions.FieldsValidationException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Map;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_DATE_FORMAT;

@Service
public class OffenceValidator {
    public static final String ACTIVE_AT_FIELD_NAME = "activeAt";

    private FieldsValidationException exception;

    /**
     * Vérifie la validité des valeurs contenues dans le filtre.
     *
     * @param filter le filtre dont on vérifie les valeurs
     */
    public void check(Map<String, String> filter) {
        exception = new FieldsValidationException();

        checkActiveAt(filter.get("activeAt"));

        if (exception.hasErrors()) throw exception;
    }

    /**
     * Vérifie la validité de la date
     *
     * @param date la date à vérifier
     */
    private void checkActiveAt(String date) {
        if (date == null) return;

        try {
            LocalDate.parse(date);
        } catch (DateTimeParseException e) {
            exception.add(ACTIVE_AT_FIELD_NAME, ERR_DATE_FORMAT);
        }
    }
}
