package be.unamur.ingelog.backend.service.cases.inquiries;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryCreateDTO;
import be.unamur.ingelog.backend.repository.repositories.cases.CaseRepositoryImpl;
import be.unamur.ingelog.backend.repository.repositories.employers.EmployerRepository;
import be.unamur.ingelog.backend.service.util.ReferenceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static be.unamur.ingelog.backend.domain.cases.inquiries.InquiryState.OPEN;

@Component
public class InquiryMapper {
    private final CaseRepositoryImpl caseRepository;
    private final EmployerRepository employerRepository;
    private final ReferenceGenerator referenceGenerator;

    @Autowired
    public InquiryMapper(CaseRepositoryImpl caseRepository,
                         EmployerRepository employerRepository,
                         ReferenceGenerator referenceGenerator) {
        this.caseRepository = caseRepository;
        this.employerRepository = employerRepository;
        this.referenceGenerator = referenceGenerator;
    }

    /**
     * Mappe un InquiryCreateDTO en Inquiry.
     *
     * @param dto l'InquiryCreateDTO source
     * @return l'Inquiry obtenu à partir de l'InquiryCreateDTO
     */
    public Inquiry toDomain(InquiryCreateDTO dto) {
        return Inquiry.builder()
                .inquiryReference(referenceGenerator.generateInquiryReference())
                .relatedCase(caseRepository.findByCaseReference(dto.getCaseReference()))
                .targetedEmployer(employerRepository.findByEmployerReference(dto.getEmployerReference()))
                .inquiryState(OPEN)
                .openDate(LocalDateTime.now())
                .street(dto.getStreet())
                .postalCode(dto.getPostalCode())
                .locality(dto.getLocality())
                .build();
    }
}
