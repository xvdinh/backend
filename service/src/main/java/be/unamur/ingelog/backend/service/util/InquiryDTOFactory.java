package be.unamur.ingelog.backend.service.util;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryClosureDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryDismissClosureDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.InquiryStateDTO;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;

public class InquiryDTOFactory {
    private InquiryDTOFactory() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static InquiryDismissClosureDTO anInquiryDismissClosureDTO(Inquiry inquiry) {
        return InquiryDismissClosureDTO.builder()
                .inquiryState(inquiry.getInquiryState().toString())
                .closeDate(inquiry.getCloseDate())
                .supervisorUsername(inquiry.getSupervisor().getUsername())
                .build();
    }

    public static InquiryClosureDTO anInquiryClosureDTO(Inquiry inquiry) {
        return InquiryClosureDTO.builder()
                .inquiryState(inquiry.getInquiryState().toString())
                .closeDate(inquiry.getCloseDate())
                .build();
    }

    public static InquiryStateDTO anInquiryStateDTO(Inquiry inquiry) {
        return InquiryStateDTO.builder()
                .inquiryState(inquiry.getInquiryState().toString())
                .build();
    }

    public static ReferenceDTO aReferenceDTO(Inquiry inquiry) {
        return new ReferenceDTO(inquiry.getInquiryReference());
    }
}
