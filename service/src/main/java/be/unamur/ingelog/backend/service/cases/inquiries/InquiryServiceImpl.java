package be.unamur.ingelog.backend.service.cases.inquiries;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.domain.cases.inquiries.InquiryResult;
import be.unamur.ingelog.backend.dto.cases.inquiries.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.InquiryNotFoundException;
import be.unamur.ingelog.backend.repository.queries.cases.inquiries.InquiryQueries;
import be.unamur.ingelog.backend.service.notifications.events.InquiryAssignmentEvent;
import be.unamur.ingelog.backend.service.notifications.events.InquiryClosureEvent;
import be.unamur.ingelog.backend.service.notifications.events.InquiryClosureRequestEvent;
import be.unamur.ingelog.backend.service.notifications.events.InquirySendBackEvent;
import be.unamur.ingelog.backend.service.util.BaseService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static be.unamur.ingelog.backend.service.util.InquiryDTOFactory.*;
import static java.util.stream.Collectors.toList;
import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class InquiryServiceImpl extends BaseService implements InquiryService {
    private final InquiryQueries inquiryQueries;
    private final InquiryMapper inquiryMapper;
    private final InquiryValidator inquiryValidator;
    private final ApplicationEventPublisher applicationEventPublisher;

    public InquiryServiceImpl(InquiryQueries inquiryQueries,
                              InquiryMapper inquiryMapper,
                              InquiryValidator inquiryValidator,
                              ApplicationEventPublisher applicationEventPublisher) {
        this.inquiryQueries = inquiryQueries;
        this.inquiryMapper = inquiryMapper;
        this.inquiryValidator = inquiryValidator;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<InquiryDetailDTO> findByCaseReference(String caseReference) {
        return inquiryQueries.findByCaseReference(caseReference);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public InquiryDetailDTO findByInquiryReference(String inquiryReference) throws InquiryNotFoundException {
        return inquiryQueries.findByInquiryReference(inquiryReference);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<InquiryOverviewDTO> findAllInquiries(InquiryFilterDTO filter) {
        return inquiryQueries.findAllInquiries().stream()
                .filter(inquiry -> filter.getStates() == null || filter.getStates().contains(inquiry.getState()))
                .filter(inquiry -> filter.getExcludeStates() == null || !filter.getExcludeStates().contains(inquiry.getState()))
                .filter(inquiry -> filter.getAssignee() == null || filter.getAssignee().equals(inquiry.getAssignee()))
                .filter(inquiry -> filter.getSupervisor() == null || filter.getSupervisor().equals(inquiry.getSupervisor()))
                .filter(inquiry -> filter.getResult() == null || filter.getResult().equals(inquiry.getResult()))
                .collect(toList());
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<InquiryTargetDTO> findByTargetedEmployer(String employerReference) {
        return inquiryQueries.findByTargetedEmployer(employerReference);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public InquirySummaryDTO getSummary(String inquiryReference) {
        return inquiryQueries.getSummary(inquiryReference);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<InquiryResultDTO> findAllResults() {
        return inquiryQueries.findAllResults();
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public ReferenceDTO create(InquiryCreateDTO dto) {
        inquiryValidator.check(dto);

        Inquiry inquiry = inquiryMapper.toDomain(dto);
        inquiryRepository.create(inquiry);

        signalInquiryCreation(inquiry.getInquiryReference());

        return aReferenceDTO(inquiry);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void askValidation(String inquiryReference) {
        var inquiry = fetchInquiry(inquiryReference);
        inquiry.askValidation();

        inquiryRepository.update(inquiry);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void validate(String inquiryReference) {
        var inquiry = fetchInquiry(inquiryReference);

        inquiry.setSupervisor(agentRepository.findByUsername(getAuthenticatedUsername()));
        inquiry.validate();

        inquiryRepository.update(inquiry);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void invalidate(String inquiryReference) {
        var inquiry = fetchInquiry(inquiryReference);
        inquiry.invalidate();

        inquiryRepository.update(inquiry);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void assign(InquiryAssignDTO dto) {
        var inquiry = fetchInquiry(dto.getInquiryReference());
        inquiry.assign(agentRepository.findByUsername(dto.getAssigneeUsername()));

        inquiryRepository.update(inquiry);

        var event = new InquiryAssignmentEvent(this, inquiry);
        applicationEventPublisher.publishEvent(event);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public InquiryDismissClosureDTO dismissAndClose(String inquiryReference) {
        var inquiry = fetchInquiry(inquiryReference);
        enrichWithReports(inquiry);

        inquiry.setSupervisor(agentRepository.findByUsername(getAuthenticatedUsername()));
        inquiry.dismissAndClose();

        inquiryRepository.update(inquiry);
        inquiry.getReports().forEach(reportRepository::update);

        signalInquiryClosure(inquiryReference);

        return anInquiryDismissClosureDTO(inquiry);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public InquiryStateDTO sendBack(String inquiryReference) {
        var inquiry = fetchInquiry(inquiryReference);
        inquiry.sendBack();

        inquiryRepository.update(inquiry);

        var event = new InquirySendBackEvent(this, inquiry);
        applicationEventPublisher.publishEvent(event);

        return anInquiryStateDTO(inquiry);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public InquiryStateDTO archive(String inquiryReference) {
        var inquiry = fetchInquiry(inquiryReference);
        inquiry.archive();

        inquiryRepository.update(inquiry);

        return anInquiryStateDTO(inquiry);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public InquiryClosureDTO close(String inquiryReference) {
        var inquiry = fetchInquiry(inquiryReference);
        enrichWithReports(inquiry);

        inquiry.close();

        inquiryRepository.update(inquiry);
        inquiry.getReports().forEach(reportRepository::update);
        signalInquiryClosure(inquiryReference);

        var event = new InquiryClosureEvent(this, inquiry);
        applicationEventPublisher.publishEvent(event);

        return anInquiryClosureDTO(inquiry);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public InquiryStateDTO updateReportDate(String inquiryReference, InquiryReportDateUpdateDTO dto) {
        inquiryValidator.check(dto);

        var inquiry = fetchInquiry(inquiryReference);
        enrichWithCase(inquiry);
        enrichWithReports(inquiry);

        var reportDate = LocalDate.parse(dto.getReportDate());
        inquiry.setReportDate(reportDate);

        inquiryRepository.update(inquiry);

        return new InquiryStateDTO(inquiry.getInquiryState().toString());
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public InquiryStateDTO updateResult(String inquiryReference, InquiryResultUpdateDTO dto) {
        inquiryValidator.check(dto);

        var inquiry = fetchInquiry(inquiryReference);
        inquiry.setResult(InquiryResult.valueOf(dto.getResult()));
        inquiryRepository.update(inquiry);

        var event = new InquiryClosureRequestEvent(this, inquiry);
        applicationEventPublisher.publishEvent(event);

        return anInquiryStateDTO(inquiry);
    }

    /**
     * Signale la création de l'enquête au dossier lié.
     *
     * @param inquiryReference la référence de l'enquête qui vient d'être créée
     */
    private void signalInquiryCreation(String inquiryReference) {
        var relatedCase = caseRepository.findByInquiryReference(inquiryReference);
        relatedCase.signalInquiryCreation();

        caseRepository.update(relatedCase);
    }

    /**
     * Signale la fermeture de l'enquête au dossier lié.
     *
     * @param inquiryReference la référence de l'enquête qui vient d'être clôturée
     */
    private void signalInquiryClosure(String inquiryReference) {
        var relatedCase = caseRepository.findByInquiryReference(inquiryReference);
        enrichWithInquiries(relatedCase);

        relatedCase.signalInquiryClosure();

        caseRepository.update(relatedCase);
    }
}
