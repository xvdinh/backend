package be.unamur.ingelog.backend.service.cases;

import be.unamur.ingelog.backend.dto.cases.*;
import be.unamur.ingelog.backend.dto.references.ReferenceDTO;
import be.unamur.ingelog.backend.infrastructure.exceptions.found.CaseNotFoundException;
import be.unamur.ingelog.backend.repository.queries.cases.CaseQueries;
import be.unamur.ingelog.backend.repository.repositories.cases.CaseRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Service
public class CaseServiceImpl implements CaseService {
    private final CaseValidator caseValidator;
    private final CaseMapper caseMapper;
    private final CaseQueries caseQueries;
    private final CaseRepositoryImpl caseRepository;

    @Autowired
    public CaseServiceImpl(CaseQueries caseQueries,
                           CaseMapper caseMapper,
                           CaseValidator caseValidator,
                           CaseRepositoryImpl caseRepository) {
        this.caseQueries = caseQueries;
        this.caseMapper = caseMapper;
        this.caseValidator = caseValidator;
        this.caseRepository = caseRepository;
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public ReferenceDTO create(CaseCreateDTO dto) {
        caseValidator.check(dto);

        var caseToCreate = caseMapper.toDomain(dto);
        caseRepository.create(caseToCreate);

        return caseMapper.toReferenceDTO(caseToCreate);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<CaseDetailDTO> findAllCases(CaseFilterDTO filter) {
        return caseQueries.findAllCases().stream()
                .filter(currentCase -> filter.getStates() == null
                        || filter.getStates().contains(currentCase.getState()))
                .collect(toList());
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public CaseDetailDTO findByCaseReference(String caseReference) throws CaseNotFoundException {
        return caseQueries.findByCaseReference(caseReference);
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<CaseOriginDTO> findAllOrigins() {
        return caseQueries.findAllOrigins();
    }

    @Override
    @Transactional(propagation = SUPPORTS)
    public List<CaseMotiveDTO> findAllMotives() {
        return caseQueries.findAllMotives();
    }
}
