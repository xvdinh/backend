package be.unamur.ingelog.backend.service.cases.inquiries.reports.observations;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.Report;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportObservation;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.ReportStateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationCreateDTO;
import be.unamur.ingelog.backend.dto.cases.inquiries.reports.observations.ObservationNumberDTO;
import be.unamur.ingelog.backend.repository.repositories.cases.inquiries.reports.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObservationMapper {
    private final ReportRepository reportRepository;

    @Autowired
    public ObservationMapper(ReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }

    /**
     * Mappe un ObservationCreateDTO en un ReportObservation.
     *
     * @param dto l'ObservationCreateDTO source
     * @return le ReportObservation obtenu à partir de l'ObservationCreateDTO
     */
    public ReportObservation toDomain(ObservationCreateDTO dto) {
        var relatedReport = reportRepository.findByReportReference(dto.getReportReference());

        return ReportObservation.builder()
                .relatedReport(relatedReport)
                .title(dto.getTitle())
                .unit(dto.getUnit())
                .build();
    }

    /**
     * Mappe un Report en un ReportStateDTO.
     *
     * @param report le Report source
     * @return le ReportStateDTO obtenu à partir du Report
     */
    public ReportStateDTO toReportStateDTO(Report report) {
        return ReportStateDTO.builder()
                .reportState(report.getReportState().toString())
                .build();
    }

    /**
     * Mappe un ReportObservation en un ObservationNumberDTO.
     *
     * @param observation le ReportObservation source
     * @return l'ObservationNumberDTO obtenu à partir du ReportObservation
     */
    public ObservationNumberDTO toObservationNumberDTO(ReportObservation observation) {
        return new ObservationNumberDTO(observation.getObservationNumber());
    }
}
