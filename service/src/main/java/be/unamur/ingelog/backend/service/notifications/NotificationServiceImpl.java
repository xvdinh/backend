package be.unamur.ingelog.backend.service.notifications;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.notifications.Notification;
import be.unamur.ingelog.backend.dto.notifications.NotificationDetailDTO;
import be.unamur.ingelog.backend.dto.notifications.NotificationUpdateDTO;
import be.unamur.ingelog.backend.repository.queries.notifications.NotificationQueries;
import be.unamur.ingelog.backend.repository.repositories.notifications.NotificationRepository;
import be.unamur.ingelog.backend.service.notifications.events.*;
import be.unamur.ingelog.backend.service.util.ReferenceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static be.unamur.ingelog.backend.domain.notifications.NotificationType.*;
import static be.unamur.ingelog.backend.domain.notifications.ResourceType.NOTIF_CASE;
import static be.unamur.ingelog.backend.domain.notifications.ResourceType.NOTIF_INQUIRY;
import static java.util.stream.Collectors.toList;
import static org.springframework.transaction.annotation.Propagation.REQUIRED;

@Service
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notificationRepository;
    private final NotificationQueries notificationQueries;
    private final ReferenceGenerator referenceGenerator;

    @Autowired
    public NotificationServiceImpl(NotificationRepository notificationRepository,
                                   NotificationQueries notificationQueries,
                                   ReferenceGenerator referenceGenerator) {
        this.notificationRepository = notificationRepository;
        this.notificationQueries = notificationQueries;
        this.referenceGenerator = referenceGenerator;
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void markAsConsulted(NotificationUpdateDTO dto) {
        notificationRepository.markAsConsulted(dto.getNotificationReference());
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void onCommentCreationEvent(CommentCreationEvent event) {
        var comment = event.getCaseComment();
        var author = comment.getAuthor();
        var caseReference = comment.getRelatedCase().getCaseReference();
        var recipients = notificationRepository.findRecipientsByCaseReference(caseReference);

        var notification = Notification.builder()
                .notificationReference(referenceGenerator.generateNotificationReference())
                .isRead(false)
                .author(comment.getAuthor())
                .type(NOTIF_COMMENT)
                .resourceType(NOTIF_CASE)
                .resourceReference(caseReference)
                .notificationTime(LocalDateTime.now());

        recipients.stream()
                .filter(recipient -> areNotEquals(author, recipient))
                .forEach(recipient -> notificationRepository.create(notification
                        .recipient(recipient)
                        .build()));
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void onInquiryAssignmentEvent(InquiryAssignmentEvent event) {
        var inquiry = event.getInquiry();

        var notification = Notification.builder()
                .notificationReference(referenceGenerator.generateNotificationReference())
                .isRead(false)
                .author(inquiry.getSupervisor())
                .recipient(inquiry.getAssignee())
                .type(NOTIF_ASSIGN)
                .resourceType(NOTIF_INQUIRY)
                .resourceReference(inquiry.getInquiryReference())
                .notificationTime(LocalDateTime.now())
                .build();

        notificationRepository.create(notification);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void onInquiryClosureEvent(InquiryClosureEvent event) {
        var inquiry = event.getInquiry();

        var notification = Notification.builder()
                .notificationReference(referenceGenerator.generateNotificationReference())
                .isRead(false)
                .author(inquiry.getSupervisor())
                .recipient(inquiry.getAssignee())
                .type(NOTIF_CLOSURE)
                .resourceType(NOTIF_INQUIRY)
                .resourceReference(inquiry.getInquiryReference())
                .notificationTime(LocalDateTime.now())
                .build();

        notificationRepository.create(notification);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void onInquiryClosureRequestEvent(InquiryClosureRequestEvent event) {
        var inquiry = event.getInquiry();

        var notification = Notification.builder()
                .notificationReference(referenceGenerator.generateNotificationReference())
                .isRead(false)
                .author(inquiry.getAssignee())
                .recipient(inquiry.getSupervisor())
                .type(NOTIF_PENDING_CLOSURE)
                .resourceType(NOTIF_INQUIRY)
                .resourceReference(inquiry.getInquiryReference())
                .notificationTime(LocalDateTime.now())
                .build();

        notificationRepository.create(notification);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public void onInquirySendBackEvent(InquirySendBackEvent event) {
        var inquiry = event.getInquiry();

        var notification = Notification.builder()
                .notificationReference(referenceGenerator.generateNotificationReference())
                .isRead(false)
                .author(inquiry.getSupervisor())
                .recipient(inquiry.getAssignee())
                .type(NOTIF_SEND_BACK)
                .resourceType(NOTIF_INQUIRY)
                .resourceReference(inquiry.getInquiryReference())
                .notificationTime(LocalDateTime.now())
                .build();

        notificationRepository.create(notification);
    }

    @Override
    @Transactional(propagation = REQUIRED)
    public List<NotificationDetailDTO> findByUsername(Boolean isRead) {
        return notificationQueries.findByUsername(getAuthenticatedUsername()).stream()
                .filter(notification -> isRead == null || notification.isConsulted() == isRead)
                .collect(toList());
    }

    /**
     * Retourne le nom d'utilisateur de l'utilisateur connecté.
     *
     * @return le nom d'utlisateur
     */
    private String getAuthenticatedUsername() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();
    }

    private boolean areNotEquals(Agent author, Agent recipient) {
        return !recipient.getAgentId().equals(author.getAgentId());
    }
}
