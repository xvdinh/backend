package be.unamur.ingelog.backend.infrastructure.exceptions.argument;

import be.unamur.ingelog.backend.infrastructure.exceptions.LabeledException;
import org.springframework.http.HttpStatus;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_UNAUTHORIZED;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

public class UnauthorizedException extends LabeledException {
    private static final HttpStatus STATUS = UNAUTHORIZED;
    private static final String LABEL = ERR_UNAUTHORIZED;

    public UnauthorizedException() {
        super(STATUS, LABEL);
    }
}
