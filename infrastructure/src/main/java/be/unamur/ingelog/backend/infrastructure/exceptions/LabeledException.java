package be.unamur.ingelog.backend.infrastructure.exceptions;

import org.springframework.http.HttpStatus;

public abstract class LabeledException extends RuntimeException {
    private final HttpStatus status;
    private final String label;

    protected LabeledException(HttpStatus status, String label) {
        super();
        this.status = status;
        this.label = label;
    }

    public HttpStatus getStatus() {
        return this.status;
    }

    public String getLabel() {
        return label;
    }
}
