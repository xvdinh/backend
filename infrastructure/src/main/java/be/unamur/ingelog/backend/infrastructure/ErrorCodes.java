package be.unamur.ingelog.backend.infrastructure;

public class ErrorCodes {
    private ErrorCodes() {
        throw new IllegalStateException("This class cannot be instantiated (utility class)");
    }

    public static final String ERR_NOT_BLANK = "ERR_NOT_BLANK";
    public static final String ERR_DATE_FORMAT = "ERR_DATE_FORMAT";
    public static final String ERR_PAST_OR_PRESENT = "ERR_PAST_OR_PRESENT";
    public static final String ERR_TOO_SHORT = "ERR_TOO_SHORT";
    public static final String ERR_TOO_LONG = "ERR_TOO_LONG";
    public static final String ERR_NEGATIVE_NUMBER = "ERR_NEGATIVE_NUMBER";
    public static final String ERR_BEFORE_TRIGGER_DATE = "ERR_BEFORE_TRIGGER_DATE";
    public static final String ERR_OBSERVATIONS_LIMIT = "ERR_OBSERVATIONS_LIMIT";

    public static final String ERR_POSTALCODE_FORMAT = "ERR_POSTALCODE_FORMAT";
    public static final String ERR_CASE_REFERENCE_FORMAT = "ERR_CASE_REFERENCE_FORMAT";
    public static final String ERR_INQUIRY_REFERENCE_FORMAT = "ERR_INQUIRY_REFERENCE_FORMAT";
    public static final String ERR_OFFENCE_REFERENCE_FORMAT = "ERR_OFFENCE_REFERENCE_FORMAT";
    public static final String ERR_REPORT_REFERENCE_FORMAT = "ERR_REPORT_REFERENCE_FORMAT";

    public static final String ERR_ILLEGAL_MOTIVE = "ERR_ILLEGAL_MOTIVE";
    public static final String ERR_ILLEGAL_ORIGIN = "ERR_ILLEGAL_ORIGIN";
    public static final String ERR_ILLEGAL_STATUS = "ERR_ILLEGAL_STATUS";
    public static final String ERR_ILLEGAL_ROLE = "ERR_ILLEGAL_ROLE";
    public static final String ERR_UNAUTHORIZED = "ERR_UNAUTHORIZED";
    public static final String ERR_ILLEGAL_ASSIGNEE_ROLE = "ERR_ILLEGAL_ASSIGNEE_ROLE";
    public static final String ERR_ILLEGAL_INQUIRY_STATE = "ERR_ILLEGAL_INQUIRY_STATE";
    public static final String ERR_ILLEGAL_REPORT_STATE = "ERR_ILLEGAL_REPORT_STATE";
    public static final String ERR_ILLEGAL_REPORT_STATUS = "ERR_ILLEGAL_REPORT_STATUS";
    public static final String ERR_ILLEGAL_INQUIRY_RESULT = "ERR_ILLEGAL_INQUIRY_RESULT";
    public static final String ERR_DUPLICATE_OFFENCE = "ERR_DUPLICATE_OFFENCE";

    public static final String ERR_AGENT_NOT_FOUND = "ERR_AGENT_NOT_FOUND";
    public static final String ERR_CASE_NOT_FOUND = "ERR_CASE_NOT_FOUND";
    public static final String ERR_INQUIRY_NOT_FOUND = "ERR_INQUIRY_NOT_FOUND";
    public static final String ERR_EMPLOYER_NOT_FOUND = "ERR_EMPLOYER_NOT_FOUND";
    public static final String ERR_OFFENCE_NOT_FOUND = "ERR_OFFENCE_NOT_FOUND";
    public static final String ERR_REPORT_NOT_FOUND = "ERR_REPORT_NOT_FOUND";
    public static final String ERR_OBSERVATION_NOT_FOUND = "ERR_OBSERVATION_NOT_FOUND";
    public static final String ERR_ATTACHMENT_NOT_FOUND = "ERR_ATTACHMENT_NOT_FOUND";
    public static final String ERR_NOTIFICATION_NOT_FOUND = "ERR_NOTIFICATION_NOT_FOUND";

    public static final String ERR_INTERNAL_SERVER_ERROR = "ERR_INTERNAL_ERROR";
    public static final String ERR_ATTACHMENT_SIZE = "ERR_ATTACHMENT_SIZE";
    public static final String ERR_ATTACHMENT_TYPE = "ERR_ATTACHMENT_TYPE";
}
