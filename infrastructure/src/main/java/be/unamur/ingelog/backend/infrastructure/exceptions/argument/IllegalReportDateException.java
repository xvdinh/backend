package be.unamur.ingelog.backend.infrastructure.exceptions.argument;

import be.unamur.ingelog.backend.infrastructure.exceptions.LabeledException;
import org.springframework.http.HttpStatus;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_BEFORE_TRIGGER_DATE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

public class IllegalReportDateException extends LabeledException {
    private static final HttpStatus STATUS = BAD_REQUEST;
    private static final String LABEL = ERR_BEFORE_TRIGGER_DATE;

    public IllegalReportDateException() {
        super(STATUS, LABEL);
    }
}
