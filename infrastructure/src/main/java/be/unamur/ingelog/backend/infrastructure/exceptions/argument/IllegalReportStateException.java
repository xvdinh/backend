package be.unamur.ingelog.backend.infrastructure.exceptions.argument;

import be.unamur.ingelog.backend.infrastructure.exceptions.LabeledException;
import org.springframework.http.HttpStatus;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_ILLEGAL_REPORT_STATE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

public class IllegalReportStateException extends LabeledException {
    private static final HttpStatus STATUS = BAD_REQUEST;
    private static final String LABEL = ERR_ILLEGAL_REPORT_STATE;

    public IllegalReportStateException() {
        super(STATUS, LABEL);
    }
}
