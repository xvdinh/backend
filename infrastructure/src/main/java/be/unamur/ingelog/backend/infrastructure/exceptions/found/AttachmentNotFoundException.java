package be.unamur.ingelog.backend.infrastructure.exceptions.found;

import be.unamur.ingelog.backend.infrastructure.exceptions.LabeledException;
import org.springframework.http.HttpStatus;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_ATTACHMENT_NOT_FOUND;
import static org.springframework.http.HttpStatus.NOT_FOUND;

public class AttachmentNotFoundException extends LabeledException {
    private static final HttpStatus STATUS = NOT_FOUND;
    private static final String LABEL = ERR_ATTACHMENT_NOT_FOUND;

    public AttachmentNotFoundException() {
        super(STATUS, LABEL);
    }
}
