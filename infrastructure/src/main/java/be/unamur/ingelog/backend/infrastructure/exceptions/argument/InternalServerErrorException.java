package be.unamur.ingelog.backend.infrastructure.exceptions.argument;

import be.unamur.ingelog.backend.infrastructure.exceptions.LabeledException;
import org.springframework.http.HttpStatus;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

public class InternalServerErrorException extends LabeledException {
    private static final HttpStatus STATUS = INTERNAL_SERVER_ERROR;
    private static final String LABEL = ERR_INTERNAL_SERVER_ERROR;

    public InternalServerErrorException() {
        super(STATUS, LABEL);
    }
}
