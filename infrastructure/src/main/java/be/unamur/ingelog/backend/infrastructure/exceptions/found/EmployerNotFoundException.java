package be.unamur.ingelog.backend.infrastructure.exceptions.found;

import be.unamur.ingelog.backend.infrastructure.exceptions.LabeledException;
import org.springframework.http.HttpStatus;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_EMPLOYER_NOT_FOUND;
import static org.springframework.http.HttpStatus.NOT_FOUND;

public class EmployerNotFoundException extends LabeledException {
    private static final HttpStatus STATUS = NOT_FOUND;
    private static final String LABEL = ERR_EMPLOYER_NOT_FOUND;

    public EmployerNotFoundException() {
        super(STATUS, LABEL);
    }
}
