package be.unamur.ingelog.backend.infrastructure.exceptions;

import java.util.HashMap;
import java.util.Map;

public class FieldsValidationException extends RuntimeException {
    private final Map<String, String> errorsByFields = new HashMap<>();

    /**
     * Ajoute l'erreur au champ fourni, si aucune erreur n'existait au préalable pour ce champ.
     *
     * @param field        le champ auquel ajouter l'erreur
     * @param errorMessage le message d'erreur à associer au champ
     */
    public void add(String field, String errorMessage) {
        if (errorsByFields.containsKey(field)) return;

        errorsByFields.put(field, errorMessage);
    }

    public Map<String, String> getErrorsByFields() {
        return errorsByFields;
    }

    public boolean hasErrors() {
        return !errorsByFields.isEmpty();
    }
}
