package be.unamur.ingelog.backend.infrastructure.exceptions.argument;

import be.unamur.ingelog.backend.infrastructure.exceptions.LabeledException;
import org.springframework.http.HttpStatus;

import static be.unamur.ingelog.backend.infrastructure.ErrorCodes.ERR_OBSERVATIONS_LIMIT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

public class ObservationsLimitException extends LabeledException {
    private static final HttpStatus STATUS = BAD_REQUEST;
    private static final String LABEL = ERR_OBSERVATIONS_LIMIT;

    public ObservationsLimitException() {
        super(STATUS, LABEL);
    }
}
