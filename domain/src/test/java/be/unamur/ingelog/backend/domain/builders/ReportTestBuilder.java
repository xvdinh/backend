package be.unamur.ingelog.backend.domain.builders;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.Report;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportState;

import static be.unamur.ingelog.backend.domain.builders.InquiryTestBuilder.aDefaultInquiry;

public class ReportTestBuilder {
    public static Report.ReportBuilder aDefaultReport() {
        return Report.builder()
                .reportId(1)
                .reportReference("2020-CON-ABCDE")
                .relatedInquiry(aDefaultInquiry()
                        .build())
                .reportState(ReportState.CREATED)
                .reportStatus(null)
                .offence(null)
                .deadline(null)
                .totalOfControlledPersons(null);
    }
}
