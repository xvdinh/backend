package be.unamur.ingelog.backend.domain.builders;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.domain.cases.inquiries.InquiryResult;
import be.unamur.ingelog.backend.domain.cases.inquiries.InquiryState;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static be.unamur.ingelog.backend.domain.builders.AgentTestBuilder.aDefaultChiefInspector;
import static be.unamur.ingelog.backend.domain.builders.AgentTestBuilder.aDefaultInspector;
import static be.unamur.ingelog.backend.domain.builders.CaseTestBuilder.aDefaultCase;
import static be.unamur.ingelog.backend.domain.builders.EmployerTestBuilder.aDefaultEmployer;
import static be.unamur.ingelog.backend.domain.cases.inquiries.InquiryState.OPEN;
import static java.util.Collections.emptyList;

public class InquiryTestBuilder {
    public static Inquiry.InquiryBuilder aDefaultInquiry() {
        return Inquiry.builder()
                .inquiryId(1)
                .inquiryReference("2020-ENQ-F3F3Z")
                .targetedEmployer(null)
                .supervisor(aDefaultChiefInspector()
                        .build())
                .assignee(aDefaultInspector()
                        .build())
                .inquiryState(InquiryState.CLOSED)
                .inquiryResult(InquiryResult.POSITIVE)
                .openDate(LocalDateTime.parse("2020-03-04T00:00:00"))
                .closeDate(LocalDateTime.parse("2020-04-04T00:00:00"))
                .reportDate(LocalDate.parse("2020-03-18"))
                .street("12 rue de l'Industrie")
                .postalCode("1000")
                .locality("Bruxelles")
                .priority("2")
                .legalDelay(60)
                .deadline(LocalDate.parse("2020-04-12"))
                .reports(emptyList())
                .attachments(null)
                .relatedCase(aDefaultCase()
                        .build());
    }

    public static Inquiry aNewlyCreatedInquiry() {
        return Inquiry.builder()
                .inquiryReference("2020-ENQ-ABCDE")
                .targetedEmployer(aDefaultEmployer()
                        .build())
                .inquiryState(OPEN)
                .street("street")
                .postalCode("1111")
                .locality("locality")
                .relatedCase(aDefaultCase()
                        .build())
                .build();
    }
}
