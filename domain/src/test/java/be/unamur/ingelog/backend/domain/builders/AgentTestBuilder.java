package be.unamur.ingelog.backend.domain.builders;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.agents.AgentRole;

public class AgentTestBuilder {
    public static Agent.AgentBuilder aDefaultAdministrativeAgent() {
        return Agent.builder()
                .agentId(1)
                .username("ADMA")
                .password("adma")
                .firstName("Nathalie")
                .lastName("Desrochers")
                .agentRole(AgentRole.AGENT_ADM);
    }

    public static Agent.AgentBuilder aDefaultInspector() {
        return Agent.builder()
                .agentId(2)
                .username("INSA")
                .password("insa")
                .firstName("Alexandre")
                .lastName("Fortin")
                .agentRole(AgentRole.INSPECTOR);
    }

    public static Agent.AgentBuilder aDefaultChiefInspector() {
        return Agent.builder()
                .agentId(3)
                .username("CINA")
                .password("cina")
                .firstName("Xavier")
                .lastName("Cloutier")
                .agentRole(AgentRole.CHIEF_INSPECTOR);
    }
}
