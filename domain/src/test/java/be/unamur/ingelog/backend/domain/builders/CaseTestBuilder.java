package be.unamur.ingelog.backend.domain.builders;

import be.unamur.ingelog.backend.domain.cases.Case;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static be.unamur.ingelog.backend.domain.builders.AgentTestBuilder.aDefaultAdministrativeAgent;
import static be.unamur.ingelog.backend.domain.cases.CaseMotive.DENUNCIATION;
import static be.unamur.ingelog.backend.domain.cases.CaseState.INITIATED;

public class CaseTestBuilder {
    public static Case.CaseBuilder aDefaultCase() {
        return Case.builder()
                .caseId(1)
                .caseReference("2020-DOS-ABCDE")
                .creator(aDefaultAdministrativeAgent()
                        .build())
                .triggerDate(LocalDate.parse("2020-01-01"))
                .openDate(LocalDateTime.parse("2020-01-01T00:00:00"))
                .closeDate(null)
                .caseMotive(DENUNCIATION)
                .caseOrigin("ANONYMOUS")
                .caseState(INITIATED)
                .description("description");
    }

    public static Case aNewlyCreatedCase() {
        return Case.builder()
                .caseReference("2020-DOS-ABCDE")
                .creator(aDefaultAdministrativeAgent()
                        .build())
                .triggerDate(LocalDate.parse("2020-01-01"))
                .caseMotive(DENUNCIATION)
                .caseOrigin("ANONYMOUS")
                .caseState(INITIATED)
                .description("description")
                .build();
    }
}
