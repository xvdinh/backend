package be.unamur.ingelog.backend.domain.builders;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportObservation;

import static be.unamur.ingelog.backend.domain.builders.ReportTestBuilder.aDefaultReport;

public class ReportObservationTestBuilder {
    public static ReportObservation.ReportObservationBuilder aDefaultReportObservation() {
        return ReportObservation.builder()
                .observationId(1)
                .relatedReport(aDefaultReport()
                        .build())
                .observationNumber(1)
                .title("title")
                .value("value")
                .unit("unit")
                .description("description");
    }

    public static ReportObservation aNewlyCreatedObservation() {
        return ReportObservation.builder()
                .relatedReport(aDefaultReport()
                        .build())
                .title("title")
                .unit("unit")
                .build();
    }
}
