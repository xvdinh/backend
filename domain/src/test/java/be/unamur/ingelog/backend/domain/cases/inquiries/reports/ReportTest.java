package be.unamur.ingelog.backend.domain.cases.inquiries.reports;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static be.unamur.ingelog.backend.domain.builders.CaseTestBuilder.aDefaultCase;
import static be.unamur.ingelog.backend.domain.builders.InquiryTestBuilder.aDefaultInquiry;
import static be.unamur.ingelog.backend.domain.builders.OffenceTestBuilder.aDefaultOffence;
import static be.unamur.ingelog.backend.domain.builders.ReportTestBuilder.aDefaultReport;
import static org.assertj.core.api.Assertions.assertThat;

class ReportTest {
    @Nested
    @DisplayName("Lorsqu'une infraction est ajoutée à une constatation")
    class SetOffence {
        @Test
        @DisplayName("La constatation est liée à l'infraction")
        void setOffence_theOffenceIsLinkedToTheReport() {
            var report = aDefaultReport().build();
            var offence = aDefaultOffence().build();

            report.setOffence(offence);

            assertThat(report.getOffence())
                    .usingRecursiveComparison()
                    .isEqualTo(offence);
        }

        @Test
        @DisplayName("La date d'échéance de la constatation est calculée et mise à jour")
        void setOffence_theReportDeadlineIsCalculated() {
            var relatedCase = aDefaultCase()
                    .triggerDate(LocalDate.parse("2020-01-01"))
                    .build();

            var relatedInquiry = aDefaultInquiry()
                    .relatedCase(relatedCase)
                    .build();

            var report = aDefaultReport()
                    .relatedInquiry(relatedInquiry)
                    .build();

            var offence = aDefaultOffence()
                    .legalDelay(10)
                    .build();

            report.setOffence(offence);

            assertThat(report.getDeadline()).isEqualTo(LocalDate.parse("2020-01-15"));
        }

        @Test
        @DisplayName("L'enquête liée est notifiée et éventuellement mise à jour")
        void setOffence_theRelatedInquiryIsNotified() {
            var relatedCase = aDefaultCase()
                    .triggerDate(LocalDate.parse("2020-01-01"))
                    .build();

            var relatedInquiry = aDefaultInquiry()
                    .relatedCase(relatedCase)
                    .priority("5")
                    .deadline(LocalDate.parse("2020-03-03"))
                    .build();

            var report = aDefaultReport()
                    .relatedInquiry(relatedInquiry)
                    .build();

            var offence = aDefaultOffence()
                    .priority("2")
                    .legalDelay(10)
                    .build();

            report.setOffence(offence);

            assertThat(relatedInquiry.getPriority()).isEqualTo("2");
            assertThat(relatedInquiry.getDeadline()).isEqualTo(LocalDate.parse("2020-01-15"));
        }
    }
}
