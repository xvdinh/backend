package be.unamur.ingelog.backend.domain.builders;

import be.unamur.ingelog.backend.domain.law.articles.Offence;

import java.time.LocalDate;

public class OffenceTestBuilder {
    public static Offence.OffenceBuilder aDefaultOffence() {
        return Offence.builder()
                .offenceId(1)
                .offenceReference("RG001001")
                .lawArticle(null)
                .label("label")
                .startingDate(LocalDate.parse("2020-01-01"))
                .endingDate(null)
                .legalDelay(10)
                .priority("5");
    }
}
