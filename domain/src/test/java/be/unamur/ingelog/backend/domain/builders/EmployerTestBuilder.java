package be.unamur.ingelog.backend.domain.builders;

import be.unamur.ingelog.backend.domain.employers.Employer;

public class EmployerTestBuilder {
    public static Employer.EmployerBuilder aDefaultEmployer() {
        return Employer.builder()
                .employerId(1)
                .employerReference("EMP-ABCDE")
                .name("name")
                .street("street")
                .postalCode("1111")
                .locality("locality")
                .country("country")
                .countryCode("CC");
    }
}
