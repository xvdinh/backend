package be.unamur.ingelog.backend.domain.cases.inquiries;

import be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportState;
import be.unamur.ingelog.backend.infrastructure.exceptions.argument.IllegalInquiryStateException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static be.unamur.ingelog.backend.domain.builders.InquiryTestBuilder.aDefaultInquiry;
import static be.unamur.ingelog.backend.domain.builders.OffenceTestBuilder.aDefaultOffence;
import static be.unamur.ingelog.backend.domain.builders.ReportTestBuilder.aDefaultReport;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class InquiryTest {
    @Nested
    @DisplayName("Lorsque le résultat d'une enquête est modifié")
    class SetResult {
        @Nested
        @DisplayName("Si l'état n'est pas à COMPLETED")
        class NoCompletedState {
            @Test
            @DisplayName("Une exception est lancée")
            void setResult_whenInquiryStateIsNotCOMPLETED_thenThrowsIllegalInquiryStateException() {
                var inquiry = aDefaultInquiry()
                        .inquiryState(InquiryState.ASSIGNED)
                        .build();

                assertThatThrownBy(() -> inquiry.setResult(InquiryResult.POSITIVE))
                        .isInstanceOf(IllegalInquiryStateException.class);
            }
        }

        @Nested
        @DisplayName("Si l'état est à COMPLETED")
        class CompletedState {
            @Test
            @DisplayName("Le résultat prend la valeur fournie")
            void setResult_whenInquiryStateIsCOMPLETED_thenInquiryResultIsUpdated() {
                var inquiry = aDefaultInquiry()
                        .inquiryState(InquiryState.COMPLETED)
                        .build();

                inquiry.setResult(InquiryResult.POSITIVE);

                assertThat(inquiry.getInquiryResult()).isEqualTo(InquiryResult.POSITIVE);
            }

            @Test
            @DisplayName("L'état de l'enquête passe à PENDING_CLOSURE")
            void setResult_whenInquiryStateIsCOMPLETED_thenInquiryStateIsUpatedToPENDING_CLOSURE() {
                var inquiry = aDefaultInquiry()
                        .inquiryState(InquiryState.COMPLETED)
                        .build();

                inquiry.setResult(InquiryResult.POSITIVE);

                assertThat(inquiry.getInquiryState()).isEqualTo(InquiryState.PENDING_CLOSURE);
            }
        }
    }

    @Nested
    @DisplayName("Lorsqu'une enquête est clôturée")
    class Close {
        @Nested
        @DisplayName("Si l'état n'est pas à PENDING_CLOSURE")
        class NoPendingClosureState {
            @Test
            @DisplayName("Une exception est lancée")
            void close_whenInquiryStateIsNotPENDING_CLOSURE_thenThrowsIllegalInquiryStateException() {
                var inquiry = aDefaultInquiry()
                        .inquiryState(InquiryState.ASSIGNED)
                        .build();

                assertThatThrownBy(inquiry::close)
                        .isInstanceOf(IllegalInquiryStateException.class);
            }
        }

        @Nested
        @DisplayName("Si l'état est à PENDING_CLOSURE")
        class PendingClosureState {
            @Test
            @DisplayName("L'état de l'enquête passe à CLOSED")
            void close_whenInquiryStateIsPENDING_CLOSURE_thenInquiryStateIsUpdatedToCLOSED() {
                var inquiry = aDefaultInquiry()
                        .inquiryState(InquiryState.PENDING_CLOSURE)
                        .build();

                inquiry.close();

                assertThat(inquiry.getInquiryState()).isEqualTo(InquiryState.CLOSED);
            }

            @Test
            @DisplayName("Toutes les constatations liées sont clôturées")
            void close_whenInquiryStateIsPENDING_CLOSURE_thenAllRelatedReportsAreCLOSED() {
                var inquiry = aDefaultInquiry()
                        .inquiryState(InquiryState.PENDING_CLOSURE)
                        .reports(List.of(aDefaultReport()
                                        .reportState(ReportState.CREATED)
                                        .build(),
                                aDefaultReport()
                                        .reportState(ReportState.ENCODED)
                                        .build()))
                        .build();

                inquiry.close();

                inquiry.getReports()
                        .forEach(report -> assertThat(report.getReportState() == ReportState.CLOSED));
            }
        }
    }

    @Nested
    @DisplayName("Lorsqu'une enquête est renvoyée en correction")
    class SendBack {
        @Test
        @DisplayName("Si l'état n'est pas à PENDING_CLOSURE, une exception est renvoyée")
        void sendBack_whenInquiryStateIsNotPENDING_CLOSURE_thenThrowsIllegalInquiryStateException() {
            var inquiry = aDefaultInquiry()
                    .inquiryState(InquiryState.COMPLETED)
                    .build();

            assertThatThrownBy(inquiry::sendBack)
                    .isInstanceOf(IllegalInquiryStateException.class);
        }

        @Test
        @DisplayName("Si l'état est à PENDING_CLOSURE, il passe à COMPLETED")
        void sendBack_whenInquiryStateIsPENDING_CLOSURE_thenInquiryStateBecomesCOMPLETED() {
            var inquiry = aDefaultInquiry()
                    .inquiryState(InquiryState.PENDING_CLOSURE)
                    .build();

            inquiry.sendBack();

            assertThat(inquiry.getInquiryState()).isEqualTo(InquiryState.COMPLETED);
        }
    }

    @Nested
    @DisplayName("Lorsque qu'une nouvelle constatation est liée à une enquête")
    class SignalReportCreation {
        @Test
        @DisplayName("Si l'enquête a une priorité plus élevée et une date d'échéance plus proche, rien ne change")
        void signalReportCreation_whenReportDeadlineAndPriorityAreWeakerThanInquiry() {
            var inquiry = aDefaultInquiry()
                    .priority("3")
                    .deadline(LocalDate.parse("2020-03-01"))
                    .build();

            var report = aDefaultReport()
                    .deadline(LocalDate.parse("2020-03-02"))
                    .offence(aDefaultOffence()
                            .priority("4")
                            .build())
                    .build();

            inquiry.signalReportCreation(report);

            assertThat(inquiry.getDeadline()).isEqualTo(LocalDate.parse("2020-03-01"));
            assertThat(inquiry.getPriority()).isEqualTo("3");
        }

        @Test
        @DisplayName("Si l'enquête a une date d'échéance moins proche, sa date d'échéance devient celle de la constatation")
        void signalReportCreation_whenReportDeadlineIsMoreUrgentThanInquiry() {
            var inquiry = aDefaultInquiry()
                    .priority("3")
                    .deadline(LocalDate.parse("2020-03-01"))
                    .build();

            var report = aDefaultReport()
                    .deadline(LocalDate.parse("2020-02-26"))
                    .offence(aDefaultOffence()
                            .priority("4")
                            .build())
                    .build();

            inquiry.signalReportCreation(report);

            assertThat(inquiry.getDeadline()).isEqualTo(LocalDate.parse("2020-02-26"));
            assertThat(inquiry.getPriority()).isEqualTo("3");
        }

        @Test
        @DisplayName("Si l'enquête a une priorité moins élevée, sa priorité devient celle de la constatation")
        void signalReportCreation_whenReportPriorityIsGreaterThanInquiry() {
            var inquiry = aDefaultInquiry()
                    .priority("3")
                    .deadline(LocalDate.parse("2020-03-01"))
                    .build();

            var report = aDefaultReport()
                    .deadline(LocalDate.parse("2020-03-02"))
                    .offence(aDefaultOffence()
                            .priority("2")
                            .build())
                    .build();

            inquiry.signalReportCreation(report);

            assertThat(inquiry.getDeadline()).isEqualTo(LocalDate.parse("2020-03-01"));
            assertThat(inquiry.getPriority()).isEqualTo("2");
        }

        @Test
        @DisplayName("Si l'enquête n'a pas de priorité, elle prend celle de la constatation")
        void signalReportCreation_whenInquiryHasNoPriorPriority() {
            var inquiry = aDefaultInquiry()
                    .priority(null)
                    .deadline(LocalDate.parse("2020-03-01"))
                    .build();

            var report = aDefaultReport()
                    .deadline(LocalDate.parse("2020-03-02"))
                    .offence(aDefaultOffence()
                            .priority("4")
                            .build())
                    .build();

            inquiry.signalReportCreation(report);

            assertThat(inquiry.getDeadline()).isEqualTo(LocalDate.parse("2020-03-01"));
            assertThat(inquiry.getPriority()).isEqualTo("4");
        }

        @Test
        @DisplayName("Si l'enquête n'a pas de date d'échéance, elle prend celle de la constatation")
        void signalReportCreation_whenInquiryHasNoPriorDeadline() {
            var inquiry = aDefaultInquiry()
                    .priority("3")
                    .deadline(null)
                    .build();

            var report = aDefaultReport()
                    .deadline(LocalDate.parse("2020-03-02"))
                    .offence(aDefaultOffence()
                            .priority("4")
                            .build())
                    .build();

            inquiry.signalReportCreation(report);

            assertThat(inquiry.getDeadline()).isEqualTo(LocalDate.parse("2020-03-02"));
            assertThat(inquiry.getPriority()).isEqualTo("3");
        }

        @Test
        @DisplayName("Si la constatation n'a pas de date d'échéance, la date d'échéance de l'enquête ne change pas")
        void signalReportCreation_whenReportHasNoDeadline() {
            var inquiry = aDefaultInquiry()
                    .priority("3")
                    .deadline(LocalDate.parse("2020-03-02"))
                    .build();

            var report = aDefaultReport()
                    .deadline(null)
                    .offence(aDefaultOffence()
                            .priority("4")
                            .build())
                    .build();

            inquiry.signalReportCreation(report);

            assertThat(inquiry.getDeadline()).isEqualTo(LocalDate.parse("2020-03-02"));
            assertThat(inquiry.getPriority()).isEqualTo("3");
        }
    }
}
