package be.unamur.ingelog.backend.domain.cases;

public enum CaseState {
    INITIATED,
    IN_PROGRESS,
    CLOSED;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
