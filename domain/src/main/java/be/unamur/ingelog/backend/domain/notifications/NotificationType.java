package be.unamur.ingelog.backend.domain.notifications;

public enum NotificationType {
    NOTIF_COMMENT,
    NOTIF_ASSIGN,
    NOTIF_PENDING_CLOSURE,
    NOTIF_CLOSURE,
    NOTIF_SEND_BACK
}
