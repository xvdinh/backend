package be.unamur.ingelog.backend.domain.law.articles;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
public class Offence {
    private Integer offenceId;
    private String offenceReference;
    private LawArticle lawArticle;
    private String label;
    private LocalDate startingDate;
    private LocalDate endingDate;
    private Integer legalDelay;
    private String priority;
}
