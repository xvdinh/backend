package be.unamur.ingelog.backend.domain.cases.inquiries.reports;

public enum ReportStatus {
    PENDING,
    NOT_APPLICABLE,
    NOT_CONTROLLED,
    POSITIVE,
    NEGATIVE;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
