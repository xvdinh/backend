package be.unamur.ingelog.backend.domain.cases.inquiries.reports;

public enum ReportState {
    CREATED,
    ENCODED,
    EVALUATED,
    CLOSED;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
