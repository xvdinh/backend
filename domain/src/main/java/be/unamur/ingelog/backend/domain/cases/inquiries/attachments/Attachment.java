package be.unamur.ingelog.backend.domain.cases.inquiries.attachments;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class Attachment {
    private Integer attachmentId;
    private String attachmentReference;
    private String originalName;
    private String documentType;
    private LocalDateTime receptionDate;
    private String source;
    private String path;
    private Inquiry relatedInquiry;
}
