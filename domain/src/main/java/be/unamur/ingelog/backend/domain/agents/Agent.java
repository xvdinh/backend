package be.unamur.ingelog.backend.domain.agents;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Agent {
    private Integer agentId;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private AgentRole agentRole;
}
