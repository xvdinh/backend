package be.unamur.ingelog.backend.domain.cases.inquiries;

public enum InquiryResult {
    POSITIVE,
    NEGATIVE;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
