package be.unamur.ingelog.backend.domain.cases.inquiries.reports;

import be.unamur.ingelog.backend.infrastructure.exceptions.argument.IllegalInquiryStateException;
import be.unamur.ingelog.backend.infrastructure.exceptions.argument.IllegalReportStateException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import static be.unamur.ingelog.backend.domain.cases.inquiries.InquiryState.ASSIGNED;
import static be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportState.CREATED;

@Data
@Builder
@AllArgsConstructor
public class ReportObservation {
    private Integer observationId;
    private Integer observationNumber;
    private String title;
    private String value;
    private String unit;
    private String description;
    private Report relatedReport;

    /**
     * Encode la feuille de route.
     *
     * @param value       la valeur à encoder
     * @param description la description à encoder
     * @throws IllegalReportStateException  si la constatation ne se trouve pas dans l'état CREATED
     * @throws IllegalInquiryStateException si l'enquête liée ne se trouve pas dans l'état ASSIGNED
     */
    public void encodeRoadmap(String value, String description)
            throws IllegalReportStateException, IllegalInquiryStateException {
        if (ASSIGNED != this.relatedReport.getRelatedInquiry().getInquiryState()) {
            throw new IllegalInquiryStateException();
        }

        if (CREATED != this.relatedReport.getReportState()) {
            throw new IllegalReportStateException();
        }

        this.value = value;
        this.description = description;
    }
}
