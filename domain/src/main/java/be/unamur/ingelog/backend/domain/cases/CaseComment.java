package be.unamur.ingelog.backend.domain.cases;

import be.unamur.ingelog.backend.domain.agents.Agent;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class CaseComment {
    private Integer commentId;
    private String content;
    private Agent author;
    private Case relatedCase;
    private LocalDateTime creationDate;
}
