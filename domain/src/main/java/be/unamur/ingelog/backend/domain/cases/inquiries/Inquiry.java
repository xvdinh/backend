package be.unamur.ingelog.backend.domain.cases.inquiries;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.cases.Case;
import be.unamur.ingelog.backend.domain.cases.inquiries.attachments.Attachment;
import be.unamur.ingelog.backend.domain.cases.inquiries.reports.Report;
import be.unamur.ingelog.backend.domain.employers.Employer;
import be.unamur.ingelog.backend.infrastructure.exceptions.argument.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static be.unamur.ingelog.backend.domain.agents.AgentRole.CHIEF_INSPECTOR;
import static be.unamur.ingelog.backend.domain.agents.AgentRole.INSPECTOR;
import static be.unamur.ingelog.backend.domain.cases.inquiries.InquiryState.*;
import static be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportState.EVALUATED;

@Data
@Builder
@AllArgsConstructor
public class Inquiry {
    private Integer inquiryId;
    private String inquiryReference;
    private Employer targetedEmployer;
    private Agent supervisor;
    private Agent assignee;
    private InquiryState inquiryState;
    private InquiryResult inquiryResult;
    private LocalDateTime openDate;
    private LocalDateTime closeDate;
    private LocalDate reportDate;
    private String street;
    private String postalCode;
    private String locality;
    private String priority;
    private Integer legalDelay;
    private LocalDate deadline;
    private List<Report> reports;
    private List<Attachment> attachments;
    private Case relatedCase;

    /**
     * Modifie le responsable de l'enquête.
     *
     * @param supervisor le responsable à affecter
     * @throws IllegalSupervisorRoleException si la personne à assigner n'a pas le rôle CHIEF_INSPECTOR
     */
    public void setSupervisor(Agent supervisor) throws IllegalSupervisorRoleException {
        if (CHIEF_INSPECTOR != supervisor.getAgentRole()) {
            throw new IllegalSupervisorRoleException();
        }

        this.supervisor = supervisor;
    }

    /**
     * Modifie la date de constatation.
     *
     * @param reportDate la valeur de la date de constatation à affecter
     * @throws IllegalInquiryStateException si l'enquête ne se trouve pas dans l'état ASSIGNED
     * @throws IllegalReportDateException   si la date de constatation est antérieure à la date de
     *                                      déclencheur du dossier ou à celle d'ouverture de l'enquête
     */
    public void setReportDate(LocalDate reportDate) throws IllegalInquiryStateException, IllegalReportDateException {
        if (ASSIGNED != inquiryState) {
            throw new IllegalInquiryStateException();
        }

        if (reportDate.isBefore(relatedCase.getTriggerDate()) ||
                reportDate.isBefore(this.openDate.toLocalDate())) {
            throw new IllegalReportDateException();
        }

        this.reportDate = reportDate;
        this.signalEncoding();
    }

    /**
     * Demande la validation de l'enquête.
     *
     * @throws IllegalInquiryStateException si l'enquête ne se trouve pas dans l'état OPEN ou INCOMPLETE
     */
    public void askValidation() throws IllegalInquiryStateException {
        if (OPEN != this.inquiryState && INCOMPLETE != this.inquiryState) {
            throw new IllegalInquiryStateException();
        }

        this.inquiryState = PENDING_VALIDATION;
    }

    /**
     * Valide une enquête en demande de validation.
     *
     * @throws IllegalInquiryStateException si l'enquête ne se trouve pas dans l'état PENDING_VALIDATION
     */
    public void validate() throws IllegalInquiryStateException {
        if (PENDING_VALIDATION != this.inquiryState) {
            throw new IllegalInquiryStateException();
        }

        this.inquiryState = APPROVED;
    }

    /**
     * Invalide une enquête en demande de validation.
     *
     * @throws IllegalInquiryStateException si l'enquête ne se trouve pas dans l'état PENDING_VALIDATION
     */
    public void invalidate() throws IllegalInquiryStateException {
        if (PENDING_VALIDATION != this.inquiryState) {
            throw new IllegalInquiryStateException();
        }

        this.inquiryState = INCOMPLETE;
    }

    /**
     * Assigne un agent à une enquête.
     *
     * @param assignee l'agent à assigner à l'enquête
     * @throws IllegalInquiryStateException si l'enquête ne se trouve pas dans l'état APPROVED ou ASSIGNED
     * @throws IllegalAssigneeRoleException si l'assigné n'a pas le rôle INSPECTOR ou CHIEF_INSPECTOR
     */
    public void assign(Agent assignee) throws IllegalInquiryStateException, IllegalAssigneeRoleException {
        if (APPROVED != this.inquiryState && ASSIGNED != this.inquiryState) {
            throw new IllegalInquiryStateException();
        }

        if (INSPECTOR != assignee.getAgentRole() && CHIEF_INSPECTOR != assignee.getAgentRole()) {
            throw new IllegalAssigneeRoleException();
        }

        this.assignee = assignee;
        this.inquiryState = ASSIGNED;
    }

    /**
     * Met à jour le résultat de l'enquête.
     *
     * @param inquiryResult la valeur du résultat de l'enquête à affecter
     * @throws IllegalInquiryStateException si l'enquête n'est pas dans l'état COMPLETED
     */
    public void setResult(InquiryResult inquiryResult) throws IllegalInquiryStateException {
        if (COMPLETED != inquiryState) {
            throw new IllegalInquiryStateException();
        }

        this.inquiryResult = inquiryResult;
        this.inquiryState = PENDING_CLOSURE;
    }

    /**
     * Clôture l'enquête sans suite.
     *
     * @throws IllegalInquiryStateException si l'enquête n'est pas dans l'état PENDING_VALIDATION
     */
    public void dismissAndClose() throws IllegalInquiryStateException {
        if (PENDING_VALIDATION != this.inquiryState) {
            throw new IllegalInquiryStateException();
        }

        this.inquiryState = DISMISSED;
        this.closeDate = LocalDateTime.now();
        this.reports.forEach(Report::close);
    }

    /**
     * Renvoie une enquête de l'état PENDING_CLOSURE à l'état COMPLETED.
     *
     * @throws IllegalInquiryStateException si l'enquête n'est pas dans l'état PENDING_CLOSURE
     */
    public void sendBack() throws IllegalInquiryStateException {
        if (PENDING_CLOSURE != this.inquiryState) {
            throw new IllegalInquiryStateException();
        }

        this.inquiryState = COMPLETED;
    }

    /**
     * Clôture l'enquête.
     *
     * @throws IllegalInquiryStateException si l'enquête n'est pas dans l'état PENDING_CLOSURE
     */
    public void close() throws IllegalInquiryStateException {
        if (PENDING_CLOSURE != this.inquiryState) {
            throw new IllegalInquiryStateException();
        }

        this.inquiryState = CLOSED;
        this.closeDate = LocalDateTime.now();
        this.reports.forEach(Report::close);
    }

    /**
     * Signale la mise à jour d'une constatation liée à cette enquête.
     */
    public void signalEncoding() {
        boolean allReportsEvaluated = this.reports.stream()
                .allMatch(report -> report.getReportState() == EVALUATED);

        if (allReportsEvaluated && this.reportDate != null) {
            this.inquiryState = COMPLETED;
        }
    }

    /**
     * Signale la création d'une nouvelle constatation liée à cette enquête.
     *
     * @param report la constatation qui vient d'être créée
     */
    public void signalReportCreation(Report report) {
        var reportDeadline = report.getDeadline();
        var reportPriority = report.getOffence().getPriority();

        if (this.deadline == null ||
                reportDeadline != null && reportDeadline.isBefore(this.deadline)) {
            this.deadline = reportDeadline;
        }

        if (this.priority == null || this.priority.compareTo(reportPriority) > 0) {
            this.priority = reportPriority;
        }
    }

    /**
     * Ajoute une constatation à l'enquête.
     *
     * @param reportToAdd la constatation à ajouter
     * @throws IllegalInquiryStateException si l'enquête n'est pas dans l'état ASSIGNED
     */
    public void addReport(Report reportToAdd) throws IllegalInquiryStateException {
        if (OPEN != this.inquiryState && INCOMPLETE != this.inquiryState) {
            throw new IllegalInquiryStateException();
        }

        var offenceId = reportToAdd.getOffence().getOffenceId();

        boolean isAnAlreadyReportedOffence = this.reports.stream()
                .map(Report::getOffence)
                .anyMatch(offence -> offence.getOffenceId().equals(offenceId));

        if (isAnAlreadyReportedOffence) {
            throw new DuplicateOffenceException();
        }
    }

    /**
     * Archive l'enquête.
     *
     * @throws IllegalInquiryStateException si l'enquête n'est pas dans l'état CLOSED
     */
    public void archive() throws IllegalInquiryStateException {
        if (CLOSED != this.inquiryState) {
            throw new IllegalInquiryStateException();
        }

        this.inquiryState = ARCHIVED;
    }
}
