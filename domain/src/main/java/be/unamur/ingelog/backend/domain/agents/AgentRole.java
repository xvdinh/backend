package be.unamur.ingelog.backend.domain.agents;

public enum AgentRole {
    AGENT_ADM,
    INSPECTOR,
    CHIEF_INSPECTOR
}
