package be.unamur.ingelog.backend.domain.notifications;

import be.unamur.ingelog.backend.domain.agents.Agent;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class Notification {
    private Integer notificationId;
    private String notificationReference;
    private boolean isRead;
    private Agent author;
    private Agent recipient;
    private NotificationType type;
    private ResourceType resourceType;
    private String resourceReference;
    private LocalDateTime notificationTime;
}
