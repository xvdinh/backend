package be.unamur.ingelog.backend.domain.cases.inquiries;

public enum InquiryState {
    OPEN,
    PENDING_VALIDATION,
    INCOMPLETE,
    APPROVED,
    ASSIGNED,
    COMPLETED,
    PENDING_CLOSURE,
    CLOSED,
    DISMISSED,
    ARCHIVED;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
