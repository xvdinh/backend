package be.unamur.ingelog.backend.domain.notifications;

public enum ResourceType {
    NOTIF_CASE,
    NOTIF_INQUIRY
}
