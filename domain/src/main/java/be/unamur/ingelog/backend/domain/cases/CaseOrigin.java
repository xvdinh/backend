package be.unamur.ingelog.backend.domain.cases;

import static be.unamur.ingelog.backend.domain.cases.CaseMotive.DENUNCIATION;
import static be.unamur.ingelog.backend.domain.cases.CaseMotive.INVESTIGATION_REQUEST;

public enum CaseOrigin {
    ANONYMOUS(DENUNCIATION),
    INDIVIDUAL(DENUNCIATION),
    EMPLOYEE(DENUNCIATION),
    EMPLOYER(DENUNCIATION),
    POLICE(INVESTIGATION_REQUEST),
    SPF(INVESTIGATION_REQUEST),
    INSPECTION_SERVICES(INVESTIGATION_REQUEST),
    ONSS(INVESTIGATION_REQUEST);

    private final CaseMotive caseMotive;

    CaseOrigin(CaseMotive caseMotive) {
        this.caseMotive = caseMotive;
    }

    public CaseMotive getCaseMotive() {
        return caseMotive;
    }


}
