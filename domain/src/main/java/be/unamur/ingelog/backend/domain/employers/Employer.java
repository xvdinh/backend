package be.unamur.ingelog.backend.domain.employers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Employer {
    private Integer employerId;
    private String employerReference;
    private String name;
    private String street;
    private String postalCode;
    private String locality;
    private String country;
    private String countryCode;
}
