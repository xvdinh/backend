package be.unamur.ingelog.backend.domain.law.articles;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
public class LawArticle {
    private Integer lawArticleId;
    private String lawArticleReference;
    private String label;
    private LocalDate startingDate;
    private LocalDate endingDate;
}
