package be.unamur.ingelog.backend.domain.cases;

public enum CaseMotive {
    COMPLAINT,
    DENUNCIATION,
    INVESTIGATION_REQUEST,
    REGULARIZATION_CONTROL
}
