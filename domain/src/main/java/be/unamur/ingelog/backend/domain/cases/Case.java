package be.unamur.ingelog.backend.domain.cases;

import be.unamur.ingelog.backend.domain.agents.Agent;
import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.domain.cases.inquiries.InquiryState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class Case {
    private Integer caseId;
    private String caseReference;
    private Agent creator;
    private LocalDate triggerDate;
    private LocalDateTime openDate;
    private LocalDateTime closeDate;
    private CaseMotive caseMotive;
    private String caseOrigin;
    private CaseState caseState;
    private String description;
    private List<Inquiry> inquiries;

    /**
     * Signale la création d'une enquête liée au dossier.
     */
    public void signalInquiryCreation() {
        this.caseState = CaseState.IN_PROGRESS;
    }

    /**
     * Signale la fermeture d'une enquête liée au dossier.
     */
    public void signalInquiryClosure() {
        boolean allInquiriesClosed = this.inquiries.stream()
                .allMatch(inquiry -> inquiry.getInquiryState() == InquiryState.CLOSED);

        if (allInquiriesClosed) {
            this.close();
        }
    }

    /**
     * Clôture le dossier.
     */
    private void close() {
        this.caseState = CaseState.CLOSED;
        this.closeDate = LocalDateTime.now();
    }
}
