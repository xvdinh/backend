package be.unamur.ingelog.backend.domain.cases.inquiries.reports;

import be.unamur.ingelog.backend.domain.cases.inquiries.Inquiry;
import be.unamur.ingelog.backend.domain.law.articles.Offence;
import be.unamur.ingelog.backend.infrastructure.exceptions.argument.IllegalInquiryStateException;
import be.unamur.ingelog.backend.infrastructure.exceptions.argument.IllegalReportStateException;
import be.unamur.ingelog.backend.infrastructure.exceptions.argument.ObservationsLimitException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

import static be.unamur.ingelog.backend.domain.cases.inquiries.InquiryState.ASSIGNED;
import static be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportState.*;
import static be.unamur.ingelog.backend.domain.cases.inquiries.reports.ReportStatus.PENDING;
import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;

@Data
@Builder
@AllArgsConstructor
public class Report {
    private Integer reportId;
    private String reportReference;
    private ReportState reportState;
    private ReportStatus reportStatus;
    private Offence offence;
    private LocalDate deadline;
    private Integer totalOfControlledPersons;
    private List<ReportObservation> reportObservations;
    private Inquiry relatedInquiry;

    public static final int MAX_OBSERVATION_NUMBER = 5;

    /**
     * Clôture la constatation.
     */
    public void close() {
        this.reportState = ReportState.CLOSED;
    }

    /**
     * Ajoute une infraction à la constatation.
     *
     * @param offence l'infraction à ajouter
     */
    public void setOffence(Offence offence) {
        this.offence = offence;

        LocalDate triggerDate = this.relatedInquiry.getRelatedCase().getTriggerDate();
        Integer legalDelay = this.offence.getLegalDelay();

        this.deadline = calculateDeadline(triggerDate, legalDelay);
        this.relatedInquiry.signalReportCreation(this);
    }

    /**
     * Calcule la date d'échéance de la constatation.
     *
     * @param triggerDate la date de déclencheur du dossier lié
     * @param legalDelay  le délai légal en jours
     * @return la date d'échéance
     */
    private LocalDate calculateDeadline(LocalDate triggerDate, Integer legalDelay) {
        if (legalDelay == null) return null;

        int addedDays = 0;
        LocalDate calculatedDeadline = triggerDate;

        while (addedDays < legalDelay) {
            calculatedDeadline = calculatedDeadline.plusDays(1);
            if (!(calculatedDeadline.getDayOfWeek() == SATURDAY || calculatedDeadline.getDayOfWeek() == SUNDAY)) {
                ++addedDays;
            }
        }
        return calculatedDeadline;
    }

    /**
     * Met à jour le nombre de personnes contrôlées.
     *
     * @param totalOfControlledPersons le nombre de personnes contrôlées
     * @throws IllegalInquiryStateException si la constatation n'est pas dans l'état CREATED
     */
    public void setTotalOfControlledPersons(int totalOfControlledPersons)
            throws IllegalInquiryStateException {
        if (CREATED != this.reportState) {
            throw new IllegalInquiryStateException();
        }

        this.totalOfControlledPersons = totalOfControlledPersons;

        this.signalEncoding();
    }

    /**
     * Met à jour le statut de la constatation.
     *
     * @param reportStatus le statut de la constatation
     */
    public void setReportStatus(ReportStatus reportStatus)
            throws IllegalInquiryStateException, IllegalReportStateException {
        if (ASSIGNED != this.relatedInquiry.getInquiryState()) {
            throw new IllegalInquiryStateException();
        }

        if (ENCODED != this.reportState) {
            throw new IllegalReportStateException();
        }

        this.reportStatus = reportStatus;
        this.reportState = reportStatus == PENDING ? ENCODED : EVALUATED;
    }

    /**
     * Signale à la constatation que l'une de ses observations a été encodée.
     */
    public void signalEncoding() {
        boolean allObservationsEncoded = this.reportObservations.stream()
                .allMatch(observation -> observation.getDescription() != null && observation.getValue() != null);

        if (allObservationsEncoded && this.totalOfControlledPersons != null) {
            this.reportState = ENCODED;
        }
    }

    /**
     * Ajoute une observation à la constatation.
     *
     * @param observation l'observation à ajouter
     * @throws ObservationsLimitException si la limite d'observations est atteinte
     */
    public void addObservation(ReportObservation observation) throws ObservationsLimitException {
        if (this.reportObservations.size() >= MAX_OBSERVATION_NUMBER) {
            throw new ObservationsLimitException();
        }

        observation.setObservationNumber(this.reportObservations.size() + 1);
        this.reportObservations.add(observation);
    }
}
