package be.unamur.ingelog.backend.domain.action.records;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class ActionRecord {
    private String methodName;
    private Object[] methodArguments;
    private String username;
    private LocalDateTime actionTimestamp;
    private String response;
}
